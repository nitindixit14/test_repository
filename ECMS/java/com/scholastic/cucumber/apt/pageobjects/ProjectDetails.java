package com.scholastic.cucumber.apt.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.ProductDetailsDialogLocators;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

public class ProjectDetails extends BaseTestPage<TestPage> implements ProductDetailsDialogLocators{


	@FindBy(locator = COVER_IMAGE)
	private WebElement coverimage;

	@FindBy(locator = BAR_CODE)
	private WebElement barcode;
	
	@FindBy(locator = PRODUCTION_EDITOR_LABEL)
	private WebElement productioneditorlabel;
	
	@FindBy(locator = PRODUCTION_TRACK_INFO_MSG)
	private WebElement productiontrackinfomsg;
	
	@FindBy(locator = EPIC_COMMENTS_DATA)
	private WebElement epiccommentsdata;
	
	@FindBy(locator =EPIC_MSG_COMMENTS)
	private WebElement epicmsgcomments;
	
	
	public WebElement getProductioneditorlabel() {
		return productioneditorlabel;
	}
	
	public WebElement getCoverimage() {
		return coverimage;
	}

	public WebElement getBarcode() {
		return barcode;
	}
	public WebElement getProductiontrackinfomsg() {
		return productiontrackinfomsg ;
	}
	public WebElement getValueElement(String label) {
		String xpath = String.format(VALUE_ELEMENT, label);
	return	getDriver().findElement(By.xpath(xpath));	
	}
	
	public WebElement getButton(String label)
	{
		String xpath = String.format(PROJECT_DETAILS_BUTTONS, label);
	return	getDriver().findElement(By.xpath(xpath));	
	}
	
	public WebElement getLabelElement(String label) {
		String xpath = String.format(PROJECT_DETAILS_LABELS, label);
	return	getDriver().findElement(By.xpath(xpath));	
	}
	
	public WebElement getEpicMsgComments(String label)
	{
		String xpath = String.format(EPIC_MSG_COMMENTS,label);
	//	System.out.println("@@@@@@@@@@@@@@@@@@@@@@    " +xpath);
	return	getDriver().findElement(By.xpath(xpath));	
	}
	
	
	
	
	public WebElement getEpicCommentsData() {
		return epiccommentsdata;
	}
	public Dialog getDialog() {
		return new Dialog(APP_COMMON_DIALOG);
	}
	
	public void switchToDetailsDialog()
	{
		getDialog();
		WaitUtils.waitForDisplayed(getDialog().getDialogTitleElement());
		getDriver().switchTo().frame(0);
	}
	
	
	@Override
	protected void openPage() {
		// TODO Auto-generated method stub
		
	}
	
}
