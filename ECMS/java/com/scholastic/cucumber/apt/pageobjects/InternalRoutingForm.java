/**
 * 
 */
package com.scholastic.cucumber.apt.pageobjects;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.Keys.InternalRoutingFormLocators;
import com.scholastic.cucumber.apt.stepdefs.DBConnection;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

import junit.framework.Assert;

/**
 * @author chirag.jayswal
 *
 */
public class InternalRoutingForm extends BaseTestPage<TestPage> implements InternalRoutingFormLocators {

	@FindBy(locator = PAGE)
	private WebElement page;
	
	@Override
	protected void openPage() {
		getDriver().get("http://tg-ecms-otbpm-s.digital.scholastic.com/routing_forms/routingcover_9780000005137_2016-05-03_T21-17-36.pdf");
	}

	public WebElement getValueElement(String lable) {
		String xpath = String.format(VALUE_ELEMENT, lable);
		WaitUtils.waitForPresent(getDriver(), By.xpath(xpath));
		return page.findElement(By.xpath(xpath));
	}
	
	public WebElement getLabelElement(String label)
	{
		String xpath = String.format(LABEL_ELEMENT, label);
		WaitUtils.waitForPresent(TestBaseProvider.getTestBase().getDriver(), By.xpath(xpath));
		return page.findElement(By.xpath(xpath));
	}
	
	public WebElement getpdfCheckboxes(String label){
		String xpath = String.format(PDF_CHECKBOXES, label);
		WaitUtils.waitForPresent(TestBaseProvider.getTestBase().getDriver(), By.xpath(xpath));
		return page.findElement(By.xpath(xpath));
	}

	public ArrayList<String> getAllLabelsInPdf()
	{
		 ArrayList<String> labels=new ArrayList<>();
		List<WebElement> pageLabels=page.findElements(By.xpath(ALL_PDF_LABELS));
		for(WebElement label:pageLabels)
		{
			labels.add(label.getText().trim());
		}
		List<WebElement> pageLabels1=page.findElements(By.xpath(ALL_LABELS));
		for(WebElement label:pageLabels1)
		{
			labels.add(label.getText().trim());
		}
		List<WebElement> pageLabels2=page.findElements(By.xpath(ALL_LABELS_2));
		for(WebElement label:pageLabels2)
		{
			labels.add(label.getText().trim());
		}
		labels.add(getLabelElement("Corrections").getText());
		labels.add(getLabelElement("Mechs").getText());
		return labels;
	}
	
	public String getCompleteTextInPdf(String label)
	{
		String text="";
		String xpath = String.format(COMPLETE_TEXT, label);
		WaitUtils.waitForPresent(TestBaseProvider.getTestBase().getDriver(), By.xpath(xpath));
		List<WebElement> elements=page.findElements(By.xpath(xpath));
		for(WebElement elm:elements)
		{
			if(getAllLabelsInPdf().contains(elm.getText().trim()))
			{
				break;
			}
			else
			{
			text=text+elm.getText()+" ";
			}
		}
		return text.trim();
	}
	
//	public ArrayList<String> getTextFromPdf()
//	{
//	ArrayList<String> arr =new ArrayList<>();
//	arr.add("Internal Routing Form");
//	arr.add("Cover/Jacket/Case");
//	arr.add("Date: _______________");
//	arr.add("After routing return to: _________________________________________________ by: __________________________________");
//	arr.add("Title:");
//	arr.add("ISBN:");
//	arr.add("Art Director:");arr.add("Pub Date:");arr.add("ISBN13:");arr.add("Cover Designer:");
//	arr.add("Trim Size:");arr.add("Record #:");arr.add("Interior Designer:");
//	arr.add("Page Count:");
//	arr.add("Trade Price:");
//	arr.add("Production Editor:");
//	arr.add("Can. Price:");arr.add("Editor:");arr.add("BF Price:");arr.add("Mfg. Manager:");arr.add("Mechs");arr.add("Proofs");arr.add("Special Effects:");arr.add("❏");
//	arr.add("Sketches");arr.add("1st separator proofs");arr.add("__________________");arr.add("1st pass mechs");
//	arr.add("Revised separator proofs");arr.add("2nd pass mechs");arr.add("Press Proof");arr.add("3rd pass mechs");arr.add("Epson");
//	arr.add("4th pass mechs");arr.add("Case Samples");arr.add("Cover # of Colors:");arr.add("5th pass mechs");arr.add("Other");
//	arr.add("Jacket # of Colors:");
//	arr.add("Product Type:");arr.add("Binding:");arr.add("Target Due to Mfg:");arr.add("Corrections • Comments • Changes");
//	arr.add("Art/Design");arr.add("Production");arr.add("Editorial");arr.add("Ed. Director/");arr.add("Publisher");
//	arr.add("Studio/");
//	arr.add("Outside OK");
//	arr.add("(Use back for additional comments)");
//	arr.add("______________________");
//	
//	return arr;
//	}
	
	
	
	
	
	
	
	

	
//__________________
//______________
//_______________________
//
//______________________







	public static void main(String[] args) throws Exception {
		InternalRoutingForm pdfForm = new InternalRoutingForm();
		pdfForm.openPage();
		// Thread.sleep(10000);
		System.out.println(pdfForm.getValueElement("Mfg. Manager").getText());
		System.out.println(pdfForm.getCompleteTextInPdf("Mfg. Manager"));
		//WebElement ele = pdfForm.getValueElement("Internal Routing");
		//System.out.println("Internal Routing: " + ele.getText());
//		System.out.println("text is: "+pdfForm.getCompleteTextInPdf("Herewith"));
//	String	proofsText=pdfForm.getCompleteTextInPdf("Herewith");
//		String commaSpaceRegex = "(((\\w+)\\s*(\\w+))*(\\,\\s+)?((\\w+)\\s*(\\w+))*)*";
//		Pattern pattern = Pattern.compile(commaSpaceRegex);
//		Matcher matcher = pattern.matcher(proofsText);
//		Assert.assertTrue(matcher.matches());	
		System.out.println("matched");
		//Assert.assertTrue(proofsText.matches(commaSpaceRegex));
		System.out.println("matched");
		// load the PDF file using PDFBox
		// try {
		// File f = File.createTempFile("tempGeneredPdf", ".pdf");
		// FileUtils.copyURLToFile(new
		// URL("http://tg-ecms-otbpm-d.digital.scholastic.com/routing_forms/CoverRouting_1454518827825.pdf"),
		// f);
		// PDDocument pdf = PDDocument.load(f); // create the DOM parser
		// PDFDomTree parser = new PDFDomTree(); // parse the file and get the
		// DOM Document
		// Writer output = new PrintWriter("generatedPDF.htm", "utf-8");
		// parser.writeText(pdf, output);
		// output.close();
		//// Document dom = parser.createDOM(pdf);
		//
		// // PDFToHTML.main(new
		// String[]{"http://tg-ecms-otbpm-d.digital.scholastic.com/routing_forms/CoverRouting_1454518827825.pdf"});
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}

}
