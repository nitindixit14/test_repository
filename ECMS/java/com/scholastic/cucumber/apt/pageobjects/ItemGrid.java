/**
 * 
 */
package com.scholastic.cucumber.apt.pageobjects;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.Keys.ItemGridLocators;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.Section;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.torque.automation.core.TestDataUtils;

/**
 * Reusable grid component for any grid-data representation in ECMS.
 * 
 * @author chirag.jayswal
 *
 */
public class ItemGrid extends Section implements ItemGridLocators {

	@FindBy(locator = TABLE_HEADERS_LIST)
	private List<WebElement> headers;

	private List<String> headerNames;
	private List<WebElement> colValues;

	@FindBy(locator = TABLE_ROWS_LIST)
	private List<WebElement> rowEles;

	@FindBy(locator = CHECKBOXES_HEADER)
	private WebElement checkboxesHeader;

	@FindBy(locator = CHECKBOX)
	private List<WebElement> checkbox;

	@FindBy(locator = TITLE_HEADER)
	private WebElement titleHeader;

	public ItemGrid(String loc) {
		super(loc);
	}

	public WebElement getTitleHeader() {
		return titleHeader;
	}

	public List<WebElement> getRowEles() {
		return rowEles;
	}

	public WebElement getContextClick() {
		return findElement(LocatorUtils.getBy(CONTEXT_CLICK_ELEMENTS));
	}

	public WebElement getcheckboxesHeader() {
		return checkboxesHeader;
	}

	public List<WebElement> getCheckbox() {
		return checkbox;
	}

	/**
	 * This will return only non empty rows. It is equivalent to getRows(false)
	 * 
	 * @return non-empty rows which are displayed
	 */
	public List<Row> getRows() {
		return getRows(false);
	}

	public List<Row> getRows(boolean includeEmpty) {
		List<Row> rows = new ArrayList<Row>();
		for (int i = 0; i < rowEles.size(); i++) {
			WebElement row = rowEles.get(i);
			Row rowObj = new Row(row);
			if (row.isDisplayed() && (includeEmpty || !rowObj.isEmpty()))
				rows.add(rowObj);
		}
		return rows;
	}

	public WebElement getHeader(String name) {
		String header = String.format(COLUMN_HEADER_BY_NAME, name);
		return findElement(LocatorUtils.getBy(header));
	}

	public List<WebElement> getcolValues(String colID) {

		List<WebElement> columnValues = findElements(
				LocatorUtils.getBy(String.format(TestDataUtils.getString(COLUMN_VALUES_BY_ID_LIST), colID)));
		if (colValues == null) {
			for (int i = 0; i < rowEles.size(); i++) {
				WebElement ele = columnValues.get(i);
				colValues.add(ele);
			}
		}
		return colValues;
	}

	public List<String> getHeaderNames() {
		if (headerNames == null) {
			headerNames = new ArrayList<String>();
			for (int i = 0; i < headers.size(); i++) {
				WebElement ele = headers.get(i);
				headerNames.add(ele.getText());
			}
		}
		return headerNames;
	}

	public int getColumnIndex(String colName) {
		return getHeaderNames().indexOf(colName);
	}

	public class Row extends Section {
		@FindBy(locator = TABLE_COLUMNS_LIST)
		private List<WebElement> columns;

		public Row(WebElement ele) {
			super(ele);
		}

		public WebElement getColumn(int index) {
			return columns.get(index);
		}

		public WebElement getColumn(String name) {
			return columns.get(getColumnIndex(name));
		}

		public String getColumnText(int index) {
			StringBuilder res = new StringBuilder();
			WebElement col = columns.get(index);
			res.append(col.getText());
			List<WebElement> inputs = col.findElements(By.tagName("input"));
			if (!inputs.isEmpty()) {
				for (WebElement input : inputs) {
					res.append(input.getAttribute("value"));
				}
			}
			return res.toString();
		}

		public String getColumnText(String name) {
			return getColumnText(getColumnIndex(name));
		}

		public boolean isEmpty() {
			List<WebElement> inputs = findElements(By.cssSelector("input"));
			return inputs.isEmpty() || !inputs.get(0).isDisplayed();
		}

		public String getText() {
			StringBuilder res = new StringBuilder();
			for (int i = 1; i < columns.size(); i++) {
				WebElement col = columns.get(i);
				res.append(col.getText());
				List<WebElement> inputs = col.findElements(By.tagName("input"));
				if (!inputs.isEmpty()) {
					for (WebElement input : inputs) {
						res.append(input.getAttribute("value"));
					}
				}
			}
			return res.toString();
		}
		
		public WebElement getHistoryTableButtons(String label)
		{
			WebDriver driver=TestBaseProvider.getTestBase().getDriver();
			String xpathExpression=String.format(HISTORY_TABLE_EDIT_BUTTON,label);
		return	driver.findElement(LocatorUtils.getBy(xpathExpression));
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<String> getColumnTextForList(String labels,String projects)
	{
	    List<String> labelsList = new Gson().fromJson(labels, List.class);
	    ArrayList<String> list=new ArrayList<>();
	    int projectsNo=Integer.parseInt(projects);
	    for(int i=0;i<projectsNo;i++)
	    {
	    	for(String label:labelsList)
	    	{
	    	list.add(getRows().get(i).getColumnText(label).trim());
	    	}
	    }
	    return list;
	}

	public WebElement getHeaderElement(String header_name) {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebElement element;
		if (header_name.equalsIgnoreCase("Title")) {
			return getTitleHeader();
		}
		if (header_name.equalsIgnoreCase("Checkboxes")) {
			return getcheckboxesHeader();
		}

		else {
			String columns_xpath = String.format(ALL_HEADERS, getColumnIndex(header_name) + 1);
			element = driver.findElement(By.xpath(columns_xpath));
			return element;
		}
	}

	// checking ascending order
	@SuppressWarnings("unused")
	public boolean isSorted(List<WebElement> list) {
		boolean sorted = true;
		for (int i = 1; i < list.size(); i++) {
			String curr = list.get(i - 1).getText();
			String next = list.get(i).getText();
			if (curr.compareTo(next) > 0)
				sorted = false;
			break;
		}

		return sorted;
	}

	// checking descending order
	@SuppressWarnings("unused")
	public boolean isSortedDesc(List<String> list, String option) throws ParseException {
		if (option.contains("Date") || option.contains("Target")) {
			boolean sorted = true;
			Date[] date1 = new Date[list.size()];
			for (int index = 0; index < list.size(); index++) {
				SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				if ((list.get(index).toString() != null) && (!"".equals(list.get(index).toString()))) {
					date1[index] = df.parse(list.get(index).toString());
				}
			}
			for (int index1 = 1; index1 < list.size(); index1++) {
				Date curr = date1[index1 - 1];
				Date next = date1[index1];
				if (curr == null && next != null) {
					sorted = false;
					break;
				}
				if (curr != null && next != null) {
					if (curr.compareTo(next) < 0) {
						sorted = false;
						break;
					}
				}
			}
			return sorted;
		}
		if (option.contains("# of Pages") || option.contains("Trade Price")) {
			boolean sorted = true;
			for (int i = 1; i < list.size(); i++) {
				double curr = tryParseInt(list.get(i - 1));
				double next = tryParseInt(list.get(i));
				if (curr < next) {
					sorted = false;
					break;
				}
			}
			return sorted;
		}

		else {
			boolean sorted = true;
			for (int i = 1; i < list.size(); i++) {
				String curr = list.get(i - 1);
				String next = list.get(i);
				if (curr != null && next != null) {
					if (curr.toUpperCase().compareTo(next.toUpperCase()) < 0) {
						sorted = false;
						break;
					}
				}
			}
			return sorted;
		}
	}

	public boolean allItemsSame(List<String> list, String option) {
		boolean isNull = true;
		for (int i = 1; i < list.size(); i++) {
			String curr = list.get(i - 1).toString();
			String next = list.get(i).toString();
			if (curr.equals(next))
				continue;
			else
				isNull = false;
			break;
		}
		return isNull;

	}

	@SuppressWarnings("unused")
	public boolean isSortedAscend(List<String> list, String option) throws ParseException {
		if (option.contains("Date") || option.contains("Target")) {
			boolean sorted = true;
			Date[] date1 = new Date[list.size()];
			for (int index = 0; index < list.size(); index++) {
				SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				if ((list.get(index).toString() != null) && (!"".equals(list.get(index).toString()))) {
					date1[index] = df.parse(list.get(index).toString());
				}
			}
			for (int index1 = 1; index1 < list.size(); index1++) {
				Date curr = date1[index1 - 1];
				Date next = date1[index1];
				if (curr != null && next == null) {
					sorted = false;
					break;
				}
				if (curr != null && next != null) {
					if (curr.compareTo(next) > 0) {
						sorted = false;
						break;
					}
				}
			}
			return sorted;
		}
		if (option.contains("# of Pages") || option.contains("Trade Price")) {
			boolean sorted = true;
			for (int i = 1; i < list.size(); i++) {
				double curr = tryParseInt(list.get(i - 1));
				double next = tryParseInt(list.get(i));
				if (curr > next) {
					sorted = false;
					break;
				}
			}
			return sorted;
		} else {
			boolean sorted = true;
			for (int i = 1; i < list.size(); i++) {
				String curr = list.get(i - 1);
				String next = list.get(i);
				if (curr != null && next != null) {
					if (curr.toUpperCase().compareTo(next.toUpperCase()) > 0) {
						sorted = false;
						break;
					}
				}
			}
			return sorted;
		}
	}

	double tryParseInt(String value) {
		try {
			return Double.parseDouble(value);
		} catch (NumberFormatException nfe) {
			// Log exception.
			return 0;
		}
	}

	public ArrayList<String> allColumnsText(String option) {

		 WebDriver driver=TestBaseProvider.getTestBase().getDriver();
			MyProjectsPage myprojects=new MyProjectsPage();
			myprojects.waitForResultsLoadingComplete();
			ArrayList<String> col_text=new ArrayList<>();
			ArrayList<String> designer_text=new ArrayList<>();
			ItemGrid itemgrid=myprojects.getProjectGrid();
			boolean navNextPage = false;
			do {
				if (navNextPage) {                              
					myprojects.clickElementByJS(myprojects.getNextButton());
					myprojects.waitForResultsLoadingComplete();
					WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
				}
					int index=itemgrid.getColumnIndex(option);
				List<WebElement> inputFields=	driver.findElements(By.xpath(String.format("//td[%d]/div[1][@style='visibility: visible;']/following::input[1]", index)));
				for(WebElement input:inputFields)
				{
					col_text.add(input.getAttribute("value").trim());
					
				}
				
				navNextPage = true;
			} while (myprojects.getNextButton().isEnabled());
			myprojects.clickElementByJS(myprojects.getFirstButton());
			
			if (option.contains("Designer")) {
				for (String s : col_text) {
					if (s.contains(" ")) {
						String[] lname = s.split(" ");
						designer_text.add(lname[1].trim());
					} else {
						designer_text.add(s);
					}
				}
				return designer_text;
			}
			else
			{	
				return col_text;
			}
			
		}
	
	public ArrayList<String> getdesignerValues(String option)
	{
		 WebDriver driver=TestBaseProvider.getTestBase().getDriver();
			MyProjectsPage myprojects=new MyProjectsPage();
			myprojects.waitForResultsLoadingComplete();
			ArrayList<String> col_text=new ArrayList<>();
			ItemGrid itemgrid=myprojects.getProjectGrid();
			boolean navNextPage = false;
			do {
				if (navNextPage) {                              
					myprojects.clickElementByJS(myprojects.getNextButton());
					myprojects.waitForResultsLoadingComplete();
					WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
				}
					int index=itemgrid.getColumnIndex(option);
				List<WebElement> inputFields=driver.findElements(By.xpath(String.format("//td[%d]/div[1][@style='visibility: visible;']/following::input[1]", index)));
				for(WebElement input:inputFields)
				{
					col_text.add(input.getAttribute("value").trim());
					
				}
				
				navNextPage = true;
			} while (myprojects.getNextButton().isEnabled());
			myprojects.clickElementByJS(myprojects.getFirstButton());
			return col_text;
	}
	

	public void hideColumn(String column_name) {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		Actions action = new Actions(driver);
		if (column_name.equalsIgnoreCase("Checkboxes")) {
			WebElement checkbox = getHeaderElement("Checkboxes");
			action.contextClick(checkbox).build().perform();
		} else {
			TestBaseProvider.getTestBase().getContext().setProperty("column.index",
					itemgrid.getColumnIndex(column_name));
			action.contextClick(itemgrid.getHeader(column_name)).build().perform();
		}
		WebElement element = driver.findElement(By.xpath(".//*[@id='__internal__bodyNode']/div[8]"));
		action.moveToElement(element).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.RETURN).build().perform();
	}

	public void showAllColumns() throws InterruptedException {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		contextClick();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();

		WebElement element = driver.findElement(By.xpath(".//*[@id='__internal__bodyNode']/div[7]/div[3]/div[7]"));
		// Thread.sleep(1000);
		element.click();
	}

	public void contextClick() {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		List<String> headerNames = itemgrid.getHeaderNames();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		Actions action = new Actions(driver);
		for (int i = 0; i < headerNames.size(); i++) {
			if (itemgrid.getHeader(headerNames.get(i)).isDisplayed()) {
				action.contextClick(itemgrid.getHeader(headerNames.get(i))).build().perform();
				break;
			}
		}
	}

	public void columnSelector() {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		contextClick();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath(".//*[@id='__internal__bodyNode']/div[8]"));
		action.moveToElement(element);
		action.moveToElement(element).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.RETURN).build()
				.perform();

	}

	public void rightClickElements(String header) {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();

		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();

		for (int i = 1; i <= 23; i++) {
			String contextXpath = ".//*[@id='__internal__bodyNode']/div[7]/div[3]/div[%d]";
			WebElement element1 = driver.findElement(By.xpath(String.format(contextXpath, i)));

			if (element1.getText().equalsIgnoreCase(header) || header == "Checkboxes") {
				element1.click();
			}
		}
	}

	public void ChangeElementWidthByJS(WebElement element, String width) throws InterruptedException {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		// Thread.sleep(1000);
		executor.executeScript("arguments[0].style.width=arguments[1];", element, width);
	}
}
