package com.scholastic.cucumber.apt.pageobjects;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.scholastic.cucumber.apt.Keys.TransmittalHistoryDialogLocators;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.Section;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.scholastic.torque.webdriver.ExtendedElement;
import com.torque.automation.core.TestDataUtils;

public class TransmittalHistoryDialog extends Section implements TransmittalHistoryDialogLocators {

	public TransmittalHistoryDialog(String loc) {
		super(loc);
	}

	public ItemGrid getHistoryGrid() {
		return new ItemGrid(TRANSMITTAL_HISTORY_TABLE);
	}
	
	@FindBy(locator = HISTORY_COVER_IMG)
	private WebElement coverImg;
	
	public WebElement getCoverImage() {
		return coverImg;
	}
	
	@FindBy(locator = HISTORY_POPUP_YES_BUTTON)
	private WebElement popAcceptButton;
	
	public WebElement getpopAcceptButton() {
		return popAcceptButton;
	}
	
	
	@FindBy(locator = HISTORY_NEW_TFORM_BUTTON)
	private WebElement formButton;
	
	public WebElement getFormButton() {
		return formButton;
	}
	
	public Dialog getDialog() {
		return new Dialog(APP_COMMON_DIALOG);
	}
	
	public void switchToHistoryDialog()
	{
		getDialog();
		WaitUtils.waitForDisplayed(getDialog().getDialogTitleElement());
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().frame(0);
	}

	public void switchToDialog() {
		try {
			TestDataUtils.getDriver().switchTo().frame(findElement(LocatorUtils.getBy(MY_PROJECTS_DIALOG_IFRAME)));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void openTransmittalHistory(int rowNum)
	{
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		WebElement transmittalHistoryCol1 = projectsPage.getProjectGrid().getRows().get(rowNum)
				.getColumn("Transmittal History");
		transmittalHistoryCol1.findElement(By.tagName("img")).click();
	}
	
	public WebElement getLabel(String label) {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String xpathExpression=String.format(TRANSMITTAL_HISTORY_LABELS,label);
	return	driver.findElement(LocatorUtils.getBy(xpathExpression));
	}
	
	public String getValueElement(String lable) {
		String xpath = String.format(VALUE_ELEMENT, lable);
//		WaitUtils.waitForPresent((ExtendedElement) By.xpath(xpath));
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return TestBaseProvider.getTestBase().getDriver().findElement(By.xpath(xpath)).getAttribute("value");
	}

	public Row getFirstHistoryTypeRow(String type)
	{
		Row firstRow=null;
		for (Row row : getHistoryGrid().getRows()) {
			String transmittalType = row.getColumnText("Type");
			WebElement input = row.findElement(By.tagName("input"));
			if (transmittalType.equalsIgnoreCase(type)) {
				firstRow=row;
				break;
			}
		}
		return firstRow;
	}
	
	public ArrayList<Row> getHistoryTypeRows(String type)
	{
		Row firstRow=null;
		ArrayList<Row> rows=new ArrayList<>();
		for (Row row : getHistoryGrid().getRows()) {
			String transmittalType = row.getColumnText("Type");
			WebElement input = row.findElement(By.tagName("input"));
			if (transmittalType.equalsIgnoreCase(type)) {
				rows.add(row);
			}
		}
		return rows;
	}
	
	public boolean sortLatestDate(ArrayList<String> dateArray)
	{
		boolean sorted = true;
		for (int i = 1; i<dateArray.size(); i++) {
   	 String[] datesplit1=dateArray.get(i-1).split("/");
   	 String[] datesplit2=dateArray.get(i).split("/");
   	 int m1=Integer.parseInt(datesplit1[0]);
   	 int m2=Integer.parseInt(datesplit2[0]);
   	 
   	 int d1=Integer.parseInt(datesplit1[1]);
   	 int d2=Integer.parseInt(datesplit1[1]);
   	 
   	 int y1=Integer.parseInt(datesplit1[2]);
   	 int y2=Integer.parseInt(datesplit1[2]);
   	 if(y1<y2)
   	 {
   		 sorted = false;
   		 break;
   	 }
   	 if(m1<m2)
   	 { sorted = false;
   		 break;
   	 }
   	 if(d1<d2)
   	 { sorted = false;
   		 break;
   	 }
	}
		return sorted;
	}
}
