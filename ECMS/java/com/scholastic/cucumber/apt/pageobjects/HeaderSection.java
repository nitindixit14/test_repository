package com.scholastic.cucumber.apt.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.scholastic.cucumber.apt.Keys.HeaderSectionLocators;
import com.scholastic.torque.common.Section;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.scholastic.torque.webdriver.ExtendedElement;

import static com.scholastic.torque.common.WaitUtils.*;

public class HeaderSection extends Section implements HeaderSectionLocators {

	@FindBy(locator = LOGOUT_BTN)
	private WebElement logout;

	@FindBy(locator = MYPROJECTS_BREADCRUMB)
	private WebElement myProjectsBreadCrumb;

	public HeaderSection(String loc) {
		super(loc);
	}

	public HeaderSection() {
		this(HEADER_SECTION_CONTAINER);
	}

	public WebElement getLogoutButton() {
		return logout;
	}

	public WebElement getMyProjectsBreadCrumb() {
		return myProjectsBreadCrumb;
	}

	public enum APTMenu {

		APT("APT", null), MYPROJECT("My Projects", APT), ManageStudioList("Manage Studio List",
				APT), ManageMyTeam("Manage My Team", APT), Administrator, Home, SCCG, TBD;

		private String title;
		private APTMenu parent;

		private APTMenu() {
			this.title = name();
		}

		private APTMenu(String title, APTMenu parent) {
			this.title = title;
			this.parent = parent;
		}

		/**
		 * 
		 * @return element object or null in case element not present
		 */
		public WebElement getElement() {
			if (parent != null) {
				//parent.waitForPresent();
				WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loadHolder")));
				WaitUtils.waitForNotDisplayed(TestBaseProvider.getTestBase().getDriver().findElement(By.cssSelector(".loadHolder")));
				WebElement element=parent.getElement();
				WaitUtils.waitForEnabled(element);
				element.click();
				WaitUtils.waitForNotDisplayed(TestBaseProvider.getTestBase().getDriver().findElement(By.cssSelector(".loadHolder")));
				WebDriverWait wait1 = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
				wait1.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loadHolder")));
			} else {
				new HeaderSection().waitForPresent();
				waitForDisplayed(new HeaderSection().getWrappedElement());
			}
			List<WebElement> eles = TestBaseProvider.getTestBase().getDriver().findElements(By.linkText(title));
			return eles.size() > 0 ? eles.get(0) : null;
		}

		public List<WebElement> getSubMenus() {
			String xpath = String.format(SUBMENU_BY_TITLE_FORMAT, title);
			return TestBaseProvider.getTestBase().getDriver().findElements(By.xpath(xpath));
		}

		public void open() {
			getElement().click();
		}

		public void waitForPresent() {
			WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(title)));
//			WaitUtils.waitForPresent((ExtendedElement) By.linkText(title));
		}

		public String getTitle() {
			return title;
		}

		public static APTMenu title(String title) {
			for (APTMenu menu : APTMenu.values()) {
				if (menu.getTitle().equalsIgnoreCase(title) || menu.name().equalsIgnoreCase(title))
					return menu;
			}
			return null;
		}
	}
	
	public void switchToDefault()
	{
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
	}

}
