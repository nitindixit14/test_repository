package com.scholastic.cucumber.apt.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.UserHomePageLocators;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;

public class UserHomePage extends BaseTestPage<TestPage> implements UserHomePageLocators {

	@FindBy(locator = USER_NAME_LABEL)
	private WebElement userNameText;

	@FindBy(locator = ACCOUNT_LINK)
	private WebElement accountLink;

	@FindBy(locator = CHANGE_PASSWORD_LINK)
	private WebElement changePasswordLink;

	// will get text ends with current page label for e.g: "Scholastic > My
	// Projects", "Scholastic > Manage Studio List"
	@FindBy(locator = PAGE_INFO_LABELS)
	private WebElement pageInfoLabels;

	/**
	 * This method returns signed in user name text displayed on the page
	 */
	public String getUserNameText() {
		return userNameText.getText();
	}

	public WebElement getUserName() {
		return userNameText;
	}

	public WebElement getAccountLink() {
		return accountLink;
	}

	public WebElement getChangePasswordLink() {
		return changePasswordLink;
	}

	public WebElement getPageInfoLabels() {
		return pageInfoLabels;
	}

	public HeaderSection getHeaderSection() {
		return new HeaderSection();
	}

	public WebElement getLogOut() {
		return getHeaderSection().getLogoutButton();
	}

	public void switchToDefault()
	{
		getDriver().switchTo().defaultContent();
	}
	
	@Override
	protected void openPage() {
	}

}
