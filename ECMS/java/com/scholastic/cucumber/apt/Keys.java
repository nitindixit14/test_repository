/**
 * 
 */
package com.scholastic.cucumber.apt;

/**
 * Place holder for all constants.
 * 
 * @author chirag.jayswal
 *
 */
public interface Keys {

	public interface AdvanceSearchDialogLocators {
		String SEARCH_BTN = "search.advanced.search.btn";
		String ALL_THE_WORDS_INPUT = "search.advanced.allthesewords.input";
		String EXACT_WORD_INPUT = "search.advanced.exactword.input";
		String ANY_OF_WORDS_INPUT = "search.advanced.anyofthesewords.input";
		String NONE_OF_THESE_WORDS_INPUT = "search.advanced.noneofthesewords.input";
		String SEARCH_BY_OPT_FORMAT = "xpath=//label[text()='%s']/input";
		String DIALOG_IFRAME = "dialog.myprojects.iframe";
		String TARGET_DUE_TO_MFG = "search.advanced.targetduetomfg.label";
		String APP_COMMON_DIALOG = "app.common.dialog";
	}
	
	public interface AdvancedSearchPageLocators{
		String APP_COMMON_DIALOG = "app.common.dialog";
		String ADVANCED_SEARCH_LABEL_INPUTS="xpath=//div[contains(text(),'%s')]/following::input[1]";
		String ADVSEARCH_BTN="xpath=//*[text()='Search']";
		String ERROR_MESSAGES_LIST = "myprojects.error.messages.list";
		String SEARCH_BY_FROMDATE_INPUTS="xpath=//div[contains(text(),'%s')]/following::input[1]";
		String SEARCH_BY_TODATE_INPUTS="xpath=//div[contains(text(),'%s')]/following::input[2]";

	}

	public interface HeaderSectionLocators {
		String HEADER_SECTION_CONTAINER = "header.section.container";
		String LOGOUT_BTN = "logout.button";
		String SUBMENU_BY_TITLE_FORMAT = ".//li[contains(.,'%s')]//li";
		String MYPROJECTS_BREADCRUMB = "myprojects.breadcrumb";
	}

	public interface OTBPMSigninPageLocators {
		String USER_NAME_INPUT = "otds.username";
		String PASSWORD_INPUT = "otds.password";
		String SIGN_IN_BTN = "otbpm.signin.button";
		String ERROR_MESSSAGE_AREA = "otbpm.signin.errormessage";
		String PAGE_URL = "otbpm.url";
		String LOADING_MESSAGE = "otbpm.loadingmessage";
	}

	public interface InternalRoutingFormLocators {
		String PAGE = "internalrouting.page";
		String VALUE_ELEMENT = ".//*[contains(.,'%s')]/following-sibling::*[1]";
		String LABEL_ELEMENT = ".//*[contains(text(),'%s')]";
		String ALL_LABELS="//*[@id='pageContainer1']//div[2]/*[contains(.,'❏')]/following::div[1]";
		String ALL_LABELS_2="//*[@id='pageContainer1']//div[2]/*[contains(.,'Corrections')]//following::div";
		String ALL_PDF_LABELS="//*[@id='pageContainer1']//div[2]/*[contains(.,':')]";
		
		String PDF_CHECKBOXES="//div[contains(text(),'%s')]/preceding::div[contains(.,'❏')]";
		
		String COMPLETE_TEXT=".//*[contains(.,'%s')]/following-sibling::*";
	}

	public interface TransmittalPdfLocators{
		String PAGE = "internalrouting.page";
		String LABEL_ELEMENT = ".//*[contains(text(),'%s')]";
		
		String VALUE_ELEMENT = ".//*[starts-with(.,'%s')]/following-sibling::*[1]";
		
		String ALL_PDF_LABELS="//*[@id='pageContainer1']//div[2]/*[contains(.,':')]";
		
		String COMPLETE_TEXT=".//*[contains(.,'%s')]/following-sibling::*";
	}
	
	
	public interface ProductDetailsDialogLocators{
		String VALUE_ELEMENT="//label[text()='%s']//following::input[1]";
		String COVER_IMAGE="project.details.cover.image";
		String BAR_CODE="project.details.barcode.img";
		String PROJECT_DETAILS_BUTTONS="//button[contains(@title,'%s')]";
		String PROJECT_DETAILS_LABELS="//label[text()='%s']";
		String APP_COMMON_DIALOG = "app.common.dialog";
		String PRODUCTION_EDITOR_LABEL="xpath=//*[@id='xfe22']/label";
		String PRODUCTION_TRACK_INFO_MSG="xpath=//*[@id='rte1']/p";
		String EPIC_COMMENTS_DATA="xpath=//*[@id='epicComments']/div/div";
		String EPIC_MSG_COMMENTS ="//*[@id='textarea_%s']";
		
	}
	
	public interface ItemGridLocators {
		String TABLE_HEADERS_LIST = "itemgrid.header.list";
		String TABLE_ROWS_LIST = "itemgrid.rows.list";
		String TABLE_COLUMNS_LIST = "itemgrid.columns.list";
		String COLUMN_HEADER_BY_NAME = "xpath=.//th[contains(.,'%s')]/label";
		String COLUMN_VALUES_BY_ID_LIST = "css=td input#%s";
		String CONTEXT_CLICK_ELEMENTS = "itemgrid.contextclick";
		String CHECKBOX = "itemgrid.checkbox";
		String CHECKBOXES_HEADER = "itemgrid.checkboxes.header";
		String TITLE_HEADER = "itemgrid.table.header";
		String ALL_HEADERS = ".//*[@id='tbmyProjects']/thead/tr/th[%d]";
		String HISTORY_TABLE_EDIT_BUTTON="xpath=//img[contains(@title,'%s')]";
	}

	public interface MyProjectsPageLocators {
		String SEARCH_BOX_INPUT = "myprojects.searchbox.input";
		String LOADING_MESSAGE = "myprojects.loading.message";
		String SEARCH_BUTTON = "myprojects.search.button";
		String ASSIGN_BUTTON = "myprojects.assign.button";
		String SHOW_TEAM_PROJECT_LINK = "myprojects.showteam.project.link";
		String ADVANCE_SEARCH_LINK = "advanced.search.link";
		String NEXT_BUTTON = "myprojects.next.button";
		String ERROR_MESSAGES_LIST = "myprojects.error.messages.list";
		String TRANSMITTAL_HISTORY_IMG = "myprojects.transmittal.history.img";
		String MY_PROJECTS_TABLE = "myprojects.table";
		String APP_COMMON_DIALOG = "app.common.dialog";
		String TABLE_ROWS_LIST = "myprojects.table.rows.list";
		String RESULT_FRAME = "display_frame";
		String MY_PROJECTS_DIALOG_IFRAME = "dialog.myprojects.iframe";
		String MY_PROJECTS_TITLE = "myprojects.title";

		String PREVIOUS_BUTTON = "myprojects.previuos.button";
		String FIRST_BUTTON = "myprojects.first.button";
		String LAST_BUTTON = "myprojects.last.button";
		String CLOSE_BUTTON = "myprojects.dialog.close.img";
		String PAGING_DISPLAY = "myprojects.display.items.perpage";
		String PAGING_FIVE = "myprojects.displayitems.perpage.five.dropdown";
		String PAGING_TEN = "myprojects.displayitems.perpage.ten.dropdown";
		String PAGING_FIFTEEN = "myprojects.displayitems.perpage.fifteen.dropdown";
		String TOTAL_PROJECTS = "myprojects.total.number.projects";
		String SELECTED_PAGING = "myprojects.paging.selected.option";
		String PAGE_NAVIGATOR = "myprojects.page.navigator";
		String TOTAL_PAGES = "myprojects.total.number.pages";
		String PRODUCT_DETAILS_FILEDS="xpath=//label[contains(text(),'%s')]//following::input[1]";
		
		String SEARCH_TYPE_CHECKBOXES = "xpath=.//label[contains(text(),'%s')]/input[1]";
	}
	
	public interface AssignDialogLocators{
		String ASSIGN_DIALOG_INPUTS = "xpath=//input[contains(@title,'%s')][@type='text']";
		String ASSIGN_DIALOG_SAVE_BUTTON = "assign.dialog.save.button";
		String ASSIGN_DIALOG_REMOVE_ARTDIRECTOR="assign.dialog.remove.artdirector";
		String APP_COMMON_DIALOG = "app.common.dialog";
		String ASSIGN_DIALOG_LABELS="xpath=//input[contains(@title,'%s')][@type='text']/preceding::label[1]";
		String ASSIGN_REMOVE_BUTTONS="xpath=//input[contains(@title,'%s')][@type='text']/following::div[3]";
		String ASSIGN_AUTOSUGGEST="assign.autosuggest.container";
		String ASSIGN_DROPDOWN="//div[contains(@title,'%s')]";
	}

	public interface SignOutPageLocators {
		String GO_TO_SIGNIN_PAGE_BUTTON = "signout.signin.page.button";
		String SIGN_OUT_MESSAGE = "signout.message";
	}

	public interface TransmittalHistoryDialogLocators {
		String TRANSMITTAL_HISTORY_TABLE = "transmittalhistory.dialog.table";
		String MY_PROJECTS_DIALOG_IFRAME = "dialog.myprojects.iframe";
		String APP_COMMON_DIALOG = "app.common.dialog";
		String HISTORY_COVER_IMG="transmittalhistory.coverimage";
		String TRANSMITTAL_HISTORY_LABELS= "xpath=.//label[contains(text(),'%s')]";
		String VALUE_ELEMENT="//label[contains(text(),'%s')]//following::input[1]";		
		String HISTORY_NEW_TFORM_BUTTON="transmittalhistory.new.transmittalform.button";
		String HISTORY_POPUP_YES_BUTTON="transmittalhistory.popup.new.button";
	}
	
	public interface TransmittalFormLocators{
		String TRANSMITTAL_FORM_SUBMIT_BUTTON="transmittal.form.submit.button";
		String TRANSMITTAL_FORM_FILES_POSTED_INPUT="transmittalform.filesPostedTo.input";
		String TRANSMITTAL_FORM_TEXTFIELDS_INPUT = "xpath=.//*[text()='%s']/following-sibling::div[1]/input[1]";
		String TRANSMITTAL_FORM_CHECKBOXES = "xpath=.//label[contains(text(),'%s')]/input[1]";
		String TRANSMITTAL_FORM_ALL_LABELS= "xpath=.//label[contains(text(),'%s')]";
		String TRANSMITTAL_FORM_HEREWITH_OTHER_CHECKBOX="transmittal.form.herewith.other.checkbox";
		String HEREWITH_OTHER_INPUT="herewith.other.input";
		String TRANSMITTAL_FORM_PROOFTYPE_OTHER_CHECKBOX="transmittal.form.prooftype.other.checkbox";
		String PROOFTYPE_OTHER_INPUT="prooftype.other.input";
		String TRANSMITTAL_FORM_SPECIAL_INSTRUCTIONS_INPUT="transmittal.form.specialInstructions.Input";
		String TRANSMITTAL_FORM_MECH_OTHER="transmittal.form.mech.other";
		String VALUE_ELEMENT="//label[contains(text(),'%s')]//following::input[1]";		
		String INFORMATIONBOX_CONTAINER="transmittal.form.informationbox";
		String MECH_OTHER_INPUT="form.mech.other.input";
		String APP_COMMON_DIALOG = "app.common.dialog";
		String FORM_UPDATE_BUTTON="form.update.button";
		String FORM_POPUP_BUTTONS="xpath=.//button[text()='%s']";
	}

	public interface UserHomePageLocators {
		String USER_NAME_LABEL = "userhomepage.username";
		String ACCOUNT_LINK = "userhomepage.accountlink";
		String CHANGE_PASSWORD_LINK = "userhomepage.change.password";
		String PAGE_INFO_LABELS = "userhomepage.pageinfo.labels";
	}

	public interface ItemListLocators {
		String GROUP_HEADER_LOC = "itemlist.group.header";
		String ITEM_LOC = "itemlist.item.loc";
		String ITEM_BY_NAME_LOC_FORMAT = ".//div[contains(@class,'item')][contains(.,'%s')]";
	}

	public interface ManageMyTeamPageLocators {
		String ALL_DESIGNERS_CONTAINER = "managemyteam.alldesigners.container";
		String DESIGNERS_REPORT_TO_ME_CONTAINER = "managemyteam.designers.report.container";
		String ASSIGN_DESIGNER_BUTTON = "managemyteam.assign.designer.button";
		String REMOVE_DESIGNER_BUTTON = "managemyteam.remove.designer.button";
		String SELECT_DESIGNER_MSG_POPUP = "managemyteam.select.designer.msg.popup";

	}

	public interface EPICSigninPageLocators {
		String USER_NAME_INPUT = "epic.signin.username";
		String PASSWORD_INPUT = "epic.signin.password";
		String SIGN_IN_BTN = "epic.signin.button";
		String ERROR_MESSSAGE_AREA = "epic.error.message.area";
		String EPIC_URL = "epic.url";
		String LOADING_MESSAGE = "epic.loading.message";
	}

	public interface EPICHomePageLocators {
		String SEARCH_FIELD = "epic.homepage.locators";
		String SEARCH_BUTTON = "epic.searchbutton";
		String SEARCH_ISBN13 = "epic.search.isbn.thirteen";
	}

	public interface EPICKeyInfoLocators {
		String CLONE_BUTTON = "epic.keyinfo.clone";
		String OWNER_DROPDOWN = "epic.owner.dropdown";
		String FIRST_USE_DATE = "xpath=//table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[19]/td[2]/input[1]";
		String BOTH_RADIOBUTTON = "epic.keyinfo.both.radio.button";
		String INCLUDE_RADIOBUTTON = "epic.keyinfo.include.radio.button";

		String UNSCHEDULED = "epic.keyinfo.unscheduled.checkbox";
		String TRADE_GROUP = "epic.keyinfo.trade.group";
		String BUDGET_CATEGORY = "epic.keyinfo.budget.category";
		String PROPERTIES_OPTION = "epic.keyinfo.properties.list";
		String PROPERTIES_BUTTON = "epic.keyinfo.properties.button";
		String PUB_DATE = "epic.keyinfo.pubdate";
		String TRADE_INSTORE_DATE = "epic.keyinfo.trade.instoredate";
		String EDITORAIL_AGE_DROPDOWN1 = "epic.keyinfo.editorialage.min";
		String EDITORAIL_AGE_DROPDOWN2 = "epic.keyinfo.editorialage.max";
		String MAJOR_CATEGORY = "epic.keyinfo.major.category";
		String PRIMARY_CATEGORY = "epic.keyinfo.primarycategory";
		String PRIMARY_CATEGORY_CHECKBOX = "epic.primary.checkbox";
		String PRIMARY_CATEGORY_OK_BUTTON = "epic.primary.ok.button";
		String SAVE_BUTTON = "epic.save.button";
		String CONTRACT_SAVE_BUTTON = "epic.contract.save.button";
		String RECENT_ITEMS_TAB = "epic.recenttab";
		String FIRST_RECENT_ITEM = "epic.recent.firsttab";
		String REQUEST_ISBN = "epic.request.isbn";
		String REQUEST_SCHOLASTIC_ISBN = "epic.request.scholastic.isbn";
		String REQUEST_SCHOLASTIC_ISBN_SUBMIT_BUTTON = "epic.isbn.submit.button";
		String CONTINUE_BUTTON = "epic.continue.button";
		String CONTINUE_ADDING_BUTTON = "epic.continue.adding.button";

	}
}
