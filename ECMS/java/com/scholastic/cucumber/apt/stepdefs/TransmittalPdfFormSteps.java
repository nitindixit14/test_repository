package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.EpicHomePage;
import com.scholastic.cucumber.apt.pageobjects.InternalRoutingForm;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.cucumber.apt.pageobjects.TransmittalPdf;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransmittalPdfFormSteps {

	@Then("^The current system date is displayed in the top right corner$")
	public void the_current_system_date_is_displayed_in_the_top_right_corner() throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		try{
			driver.switchTo().defaultContent();
		if(driver.getCurrentUrl().contains("pdf"))
		{
			driver.switchTo().defaultContent();
		}
		else
		{
			MyProjectsPage myprojects=new MyProjectsPage();
			myprojects.switchToWindow();
			driver.switchTo().defaultContent();
		}
		}
		catch(Exception e)
		{
			driver.switchTo().defaultContent();
		}
		String routingFormDate=routingform.getValueElement("Date").getText();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		   //get current date
		Calendar cal = Calendar.getInstance();
		   Assert.assertEquals(dateFormat.format(cal.getTime()).toString(), routingFormDate.trim());
	}
	
	@SuppressWarnings("unchecked")
	@Then("^information \"([^\"]*)\" is displayed in \"([^\"]*)\" Transmittal Form$")
	public void information_is_displayed_in_Transmittal_Form(String fields, String arg2) throws Throwable {
//		TestBaseProvider.getTestBase().getDriver().get("http://tg-ecms-otbpm-s.digital.scholastic.com/transmittal_forms/transmittalcover_9780439665421_2016-05-13_T10-50-17.pdf");
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		ArrayList<String> pdfValues=new ArrayList<>();
		List<String> items = new Gson().fromJson(fields, List.class);
		for (String item : items) {
	//		WaitUtils.waitForDisplayed(transmittalpdf.getValueElement(item));
			WebDriver driver=TestBaseProvider.getTestBase().getDriver();
			try{
				driver.switchTo().defaultContent();
			if(driver.getCurrentUrl().contains("pdf"))
			{
				driver.switchTo().defaultContent();
			}
			else
			{
				MyProjectsPage myprojects=new MyProjectsPage();
				myprojects.switchToWindow();
				driver.switchTo().defaultContent();
			}
			}
			catch(Exception e)
			{
				driver.switchTo().defaultContent();
			}
			WebElement element=driver.findElement(By.xpath(String.format("//div[contains(text(),':')][starts-with(text(),'%s')]/following::div[1]",item)));
		String transmittalpdfvalue=element.getText();
		if(transmittalpdf.getAllLabelsInPdf().contains(transmittalpdfvalue))
		{
			pdfValues.add("");
		}
		else if(item.contains("Price") && transmittalpdfvalue.contains("$"))
		{
			pdfValues.add(transmittalpdfvalue.replace("$", ""));
		}
		else
		{
			pdfValues.add(transmittalpdfvalue);
			if(item.equals("ISBN"))
			{
				TestBaseProvider.getTestBase().getContext().setProperty("isbn", transmittalpdfvalue);
			}
		}
		}
		TestBaseProvider.getTestBase().getContext().setProperty("pdf.values", pdfValues);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Then("^information \"([^\"]*)\" value matches database value for a project$")
	public void information_value_matches_database_value_for_a_project(String type) throws Throwable {
		ArrayList<String>pdfValues=(ArrayList<String>) TestBaseProvider.getTestBase().getContext().getProperty("pdf.values");
		DBConnection dbcon=new DBConnection();
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalPdf tp=new TransmittalPdf();
		EpicHomePage homepage=new EpicHomePage();
		String databaseFields=TestBaseProvider.getTestBase().getString("database."+type+".fields");
		String isbn=(String) TestBaseProvider.getTestBase().getContext().getProperty("isbn");
		ArrayList<String> databaseResults=new ArrayList<>();
		ResultSet rs=dbcon.dbConnection("SELECT TOP 1 * FROM dbo.APT_PROJECTS_VIEW,dbo.TRANSMITTAL_HISTORY WHERE APT_PROJECTS_VIEW.ISBN=TRANSMITTAL_HISTORY.ISBN AND APT_PROJECTS_VIEW.ISBN='"+ isbn + "' ORDER BY DATE_CREATED DESC");
		List<String> dbitems = new Gson().fromJson(databaseFields, List.class);
		while (rs.next()) {
			for (String item : dbitems) {
				if (item.contains("DATE")) {
					String date = rs.getString(item);
					databaseResults.add(myprojects.changeDateFormat(date));
				} else {
					databaseResults.add(rs.getString(item));
				}
			}
		}
		tp.assertResults(pdfValues,databaseResults);
	}
	
	@When("^user does not select 'Advanced Reader Copy'$")
	public void user_does_not_select_Advanced_Reader_Copy() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		 WebElement transmittalType=transmittals.getTransmittalFormButtons("Advanced Reader Copy");
		 if( transmittalType.isSelected())
		  {
			  transmittalType.click();
		  }
	}

	@Then("^'ARC' is not displayed in the \"([^\"]*)\" Transmittal Form pdf$")
	public void arc_is_not_displayed_in_the_Transmittal_Form_pdf(String arg1) throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		Assert.assertFalse(transmittalpdf.getAllLabelsInPdf().contains("ARC:"));
	}
	
	@When("^user checks 'Advanced Reader Copy'$")
	public void user_checks_Advanced_Reader_Copy() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		 WebElement transmittalType=transmittals.getTransmittalFormButtons("Advanced Reader Copy");
		 if( transmittalType.isSelected())
		  {
		  }
		 else
		 {
			 transmittalType.click();
		 }
	}

	@Then("^'ARC:YES' is displayed in the \"([^\"]*)\" Transmittal Form pdf$")
	public void arc_YES_is_displayed_in_the_Transmittal_Form_pdf(String arg1) throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		 Thread.sleep(1000);
		AssertUtils.assertDisplayed(transmittalpdf.getLabelElement("ARC:"));
		String arcValue=routingform.getValueElement("ARC:").getText();
		Assert.assertEquals("Yes", arcValue);
	}
	
	@SuppressWarnings("unchecked")
	@When("^User enters data into the \"([^\"]*)\" field$")
	public void user_enters_data_into_the_field(String fields) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		WebElement inputFields;
		ArrayList<String> formValues=new ArrayList<>();
		String inputData=TestBaseProvider.getTestBase().getString("transmittalFormInput.data");
		  List<String> items = new Gson().fromJson(fields, List.class);
		  List<String> dataItems=new Gson().fromJson(inputData, List.class);
		  for (int i = 0; i < items.size(); i++) {
				String item = items.get(i);
				String dataItem = dataItems.get(i);
				formValues.add(dataItem.trim());
				if(item.contains("Files"))
				{
				inputFields=transmittals.getFilesPostedToInput();
				}
				else if(item.contains("Special"))
				{
					inputFields=transmittals.getSpecialInstructionsInput();
				}
				else
				{
		inputFields=transmittals.getTransmittalFormTextFields(item);
				}
		inputFields.clear();
		inputFields.sendKeys(dataItem);
			}
		  TestBaseProvider.getTestBase().getContext().setProperty("form.values", formValues);
	}

	@SuppressWarnings("unchecked")
	@Then("^the data entered into the \"([^\"]*)\" feild matches the user's input$")
	public void the_data_entered_into_the_feild_matches_the_user_s_input(String fields) throws Throwable {
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		ArrayList<String> formValues=(ArrayList<String>) TestBaseProvider.getTestBase().getContext().getProperty("form.values");
		ArrayList<String> pdfValues=new ArrayList<>();
		List<String> items = new Gson().fromJson(fields, List.class);
		 for(String item:items)
		 {
			String transmittalpdfvalue= transmittalpdf.getValueElement(item).getText().trim();
			 if(transmittalpdf.getAllLabelsInPdf().contains(transmittalpdfvalue))
				{
					pdfValues.add("");
				}
			 else
			 {
			 pdfValues.add(transmittalpdfvalue);
			 }
		 }
		 Assert.assertEquals(formValues, pdfValues);
		
	}

	@SuppressWarnings("unchecked")
	@Then("^is displayed with the label as \"([^\"]*)\"$")
	public void is_displayed_with_the_label_as(String fields) throws Throwable {
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		List<String> items = new Gson().fromJson(fields, List.class);
		 for(String item:items)
		 {
			 String routingValue=transmittalpdf.getLabelElement(item).getText().trim();
			 Assert.assertEquals(routingValue, item);
		 }
	}

	@SuppressWarnings("unchecked")
	@Then("^The selected option \"([^\"]*)\" is displayed in the \"([^\"]*)\" Transmittal Form$")
	public void the_selected_option_is_displayed_in_the_Transmittal_Form(String options, String arg2) throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		ArrayList<String> routingValues=new ArrayList<>();
		ArrayList<String> formValues=new ArrayList<>();
		routingValues.add(routingform.getValueElement("Mech").getText().trim());
		routingValues.add(routingform.getValueElement("Herewith").getText().trim());
		routingValues.add(routingform.getValueElement("Proofs").getText().trim());
		List<String> items = new Gson().fromJson(options, List.class);
		 for(String item:items)
		 {
			 formValues.add(item.trim()); 
		 }
		 Collections.sort(routingValues);
		 Collections.sort(formValues);
		 Assert.assertEquals(formValues, routingValues); 
	}
	
	@Then("^'Spine' information is not displayed on the printed Form$")
	public void spine_information_is_not_displayed_on_the_printed_Form() throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		Assert.assertFalse(transmittalpdf.getAllLabelsInPdf().contains("Spine"));
	}
	
}
