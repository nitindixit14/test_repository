package com.scholastic.cucumber.apt.stepdefs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.SwitchToFrame;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.thoughtworks.selenium.Wait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class PagingSteps {

	@Then("^'Projects per page' selector is displayed and labeled as 'Show X Per Page'$")
	public void projects_per_page_selector_is_displayed_and_labeled_as_Show_X_Per_Page() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getWrappedElement());
		// Thread.sleep(10000);
		AssertUtils.assertDisplayed(myprojects.getDisplaypageOption());
		myprojects.clickPagingOption();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String show = driver.findElement(By.xpath(".//*[@id='xfe18']/label")).getText();
		String perPage = driver.findElement(By.xpath(".//*[@id='text3']/span")).getText();
		driver.switchTo().defaultContent();
		String paging = myprojects.getSelectedPagingOption().getText();
		Assert.assertEquals("Show " + paging + " Per Page", show + " " + paging + " " + perPage);
	}

	@Then("^X is a selectable field with the options ' (\\d+)-(\\d+)-(\\d+)'$")
	public void x_is_a_selectable_field_with_the_options(int pagingOption1, int pagingOption2, int pagingOption3) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		AssertUtils.assertDisplayed(myprojects.getPagingOp1());
		Assert.assertEquals(myprojects.getPagingOp1().getText(), Integer.toString(pagingOption1));
		AssertUtils.assertDisplayed(myprojects.getPagingOp2());
		Assert.assertEquals(myprojects.getPagingOp2().getText(), Integer.toString(pagingOption2));
		AssertUtils.assertDisplayed(myprojects.getPagingOp3());
		Assert.assertEquals(myprojects.getPagingOp3().getText(), Integer.toString(pagingOption3));
	}

	@Given("^User with projects \"([^\"]*)\" is logged in OTBPM$")
	public void user_with_projects_is_logged_in_OTBPM(String assignedProjectUser) throws Throwable {
		OTBPMSigninPage signinPage = new OTBPMSigninPage();
		signinPage.launchPage();
		String userID = TestBaseProvider.getTestBase().getString("paging." + assignedProjectUser + ".name");
		String password = TestBaseProvider.getTestBase().getString("paging." + assignedProjectUser + ".password");
		signinPage.signIn(userID, password);
	}

	@Then("^first page is displayed	with \"([^\"]*)\"$")
	public void first_page_is_displayed_with(String assigned_projects) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		if (assigned_projects.equals("NONE")) {
			ItemGrid projectgrid = myProjects.getProjectGrid();
			Assert.assertEquals(0, projectgrid.getRows().size());
		} else {
			Assert.assertEquals("1", myProjects.getCurrentPageNum());
		}
	}

	@Then("^'Projects per page' value is '(\\d+)' projects per page$")
	public void projects_per_page_value_is_projects_per_page(int arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		// Thread.sleep(10000);
		myprojects.clickPagingOption();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		AssertUtils.assertDisplayed(myprojects.getPagingOp1());
		Assert.assertEquals(myprojects.getPagingOp1().getText(), "5");
	}

	@Then("^The count of projects displayed on the page is \"([^\"]*)\"$")
	public void the_count_of_projects_displayed_on_the_page_is(String projectsDisplayed) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		//Thread.sleep(10000);
		if (projectsDisplayed.equals("0")) {
			Assert.assertEquals(projectsDisplayed, Integer.toString(itemgrid.getRows().size()));
		} else {
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			driver.switchTo().defaultContent();
			String paging = myProjects.getSelectedPagingOption().getText();
			myProjects.waitForResultsLoadingComplete();
			if (projectsDisplayed.equals("TOTAL_PROJECTS")) {
				Assert.assertTrue(Integer.parseInt(paging) >= itemgrid.getRows().size());
			} else {
				Assert.assertEquals(projectsDisplayed, Integer.toString(itemgrid.getRows().size()));
			}
		}
	}

	@Then("^first page is displayed$")
	public void first_page_is_displayed() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		Assert.assertEquals("1", myProjects.getCurrentPageNum());
	}

	@Given("^'Projects per page' values is set to \"([^\"]*)\"$")
	public void projects_per_page_values_is_set_to(String projectsPerPage) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.getPagingOption(projectsPerPage);
	}

	@When("^User searches for 'At least (\\d+) results'$")
	public void user_searches_for_At_least_results(int arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		String searchKey = TestBaseProvider.getTestBase().getString("signin.small.results.search");
		myprojects.searchFor(searchKey);
	}

	@Then("^'Projects per page' values is  \"([^\"]*)\"$")
	public void projects_per_page_values_is(String projectsPerPage) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		//Thread.sleep(1000);
		WaitUtils.waitForDisplayed(myprojects.getDisplaypageOption());
		myprojects.clickPagingOption();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		Assert.assertEquals(myprojects.getSelectedPagingOption().getText().toString(), projectsPerPage.toString());
	}

	@When("^User navigates to page \"([^\"]*)\"$")
	public void user_navigates_to_page(String pageNum) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.getPageNavigator().clear();
		myprojects.getPageNavigator().sendKeys(pageNum);
		myprojects.getPageNavigator().sendKeys(Keys.ENTER);
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getWrappedElement());
		Thread.sleep(1000);
	}
	
	@Given("^user searches for big result set$")
	public void user_searches_for_big_result_set() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		String searhKey = TestBaseProvider.getTestBase().getString("signin.search");
		myprojects.searchFor(searhKey);
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getWrappedElement());
		// Thread.sleep(1000);
	}

	@Given("^Page displayed is \"([^\"]*)\"$")
	public void page_displayed_is(String pageNum) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.getPageNavigator().clear();
		if (pageNum.contentEquals("LAST")) {
			myprojects.getPageNavigator().sendKeys(myprojects.getLastPageNum());
		} else {
			myprojects.getPageNavigator().sendKeys(pageNum);
		}
		myprojects.getPageNavigator().sendKeys(Keys.ENTER);
		getContext().setProperty("total.page.number", myprojects.getLastPageNum());
		getContext().setProperty("total.project.number", myprojects.getLastProjectNum());
		getContext().setProperty("previous.page.number", myprojects.getCurrentPageNum());
		getContext().setProperty("previous.projects.number", myprojects.getCurrentProjectNum());
		myprojects.clickPagingOption();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		// WaitUtils.waitForEnabled(myprojects.getProjectGrid().getWrappedElement());
		// Thread.sleep(10000);
		getContext().setProperty("previous.paging.option", myprojects.getSelectedPagingOption().getText());
	}

	@When("^User selects the \"([^\"]*)\"$")
	public void user_selects_the(String arrowIcon) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		if (arrowIcon.contentEquals("LAST")) {
			// myprojects.getLastButton().click();
			myprojects.clickElementByJS(myprojects.getLastButton());
		}
		if (arrowIcon.contentEquals("FIRST")) {
			// myprojects.getFirstButton().click();
			myprojects.clickElementByJS(myprojects.getFirstButton());
		}
		if (arrowIcon.contentEquals("PREVIOUS")) {
			// myprojects.getPreviousButton().click();
			myprojects.clickElementByJS(myprojects.getPreviousButton());
		}
		if (arrowIcon.contentEquals("NEXT")) {
			// myprojects.getNextButton().click();
			myprojects.clickElementByJS(myprojects.getNextButton());
		}
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getWrappedElement());
	}

	@Then("^\"([^\"]*)\" page is displayed$")
	public void page_is_displayed(String targetPage) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		if (targetPage.contentEquals("LAST")) {
			targetPage = myprojects.getLastPageNum();
		}
		Assert.assertEquals(myprojects.getCurrentPageNum(), targetPage);
	}

	private Configuration getContext() {
		return TestBaseProvider.getTestBase().getContext();
	}

	@When("^user selects the page \"([^\"]*)\"$")
	public void user_selects_the_page(String pageNum) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		getContext().setProperty("total.page.number", myprojects.getLastPageNum());
		getContext().setProperty("total.project.number", myprojects.getLastProjectNum());
		getContext().setProperty("previous.page.number", myprojects.getCurrentPageNum());
		getContext().setProperty("previous.projects.number", myprojects.getCurrentProjectNum());
		myprojects.clickPagingOption();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		// WaitUtils.waitForEnabled(myprojects.getProjectGrid().getWrappedElement());
		// Thread.sleep(10000);
		getContext().setProperty("previous.paging.option", myprojects.getSelectedPagingOption().getText());
		myprojects.waitForResultsLoadingComplete();
		WaitUtils.waitForEnabled(myprojects.getPageNavigator());
		myprojects.getPageNavigator().clear();
		myprojects.getPageNavigator().sendKeys(pageNum);
		myprojects.getPageNavigator().sendKeys(Keys.ENTER);
	}

	@Then("^number of projects per page and max number of projects displayed remains$")
	public void number_of_projects_per_page_and_max_number_of_projects_displayed_remains() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getWrappedElement());
		// Thread.sleep(10000);
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		String pagingOption = myprojects.getSelectedPagingOption().getText();
		String previousPagingOption = (String) getContext().getProperty("previous.paging.option");
		myprojects.waitForResultsLoadingComplete();
		Assert.assertEquals(pagingOption, previousPagingOption);
		String lastPageNumexpected = (String) getContext().getProperty("total.page.number");
		Assert.assertEquals(myprojects.getLastPageNum(), lastPageNumexpected);
		String totalProjectsexpected = (String) getContext().getProperty("total.project.number");
		Assert.assertEquals(myprojects.getLastProjectNum(), totalProjectsexpected);
	}

	@Then("^numbers for projects is updated for 'X,Y,Z'$")
	public void numbers_for_projects_is_updated_for_X_Y_Z() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		String actualCurrentProjectsNum = myprojects.getCurrentProjectNum();
		String previousProjectsNum = (String) getContext().getProperty("previous.projects.number");
		String actualLastProjectsNum = myprojects.getLastProjectNum();
		String previousLastProjectsNum = (String) getContext().getProperty("total.project.number");
		String previousPageNum = (String) getContext().getProperty("previous.page.number");
		if (previousPageNum.equals(myprojects.getCurrentPageNum())) {
			Assert.assertTrue(actualCurrentProjectsNum.equals(previousProjectsNum));
			Assert.assertTrue(actualLastProjectsNum.equals(previousLastProjectsNum));
		} else {
			Assert.assertFalse(actualCurrentProjectsNum.equals(previousProjectsNum));
			Assert.assertTrue(actualLastProjectsNum.equals(previousLastProjectsNum));
		}
	}

	@Then("^pagination values is updated for 'R,S,T'$")
	public void pagination_values_is_updated_for_R_S_T() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		String actualCurrentPageNum = myProjects.getCurrentPageNum();
		String previousPageNum = (String) getContext().getProperty("previous.page.number");
		String actualLastProjectsNum = myProjects.getLastProjectNum();
		String previousLastProjectsNum = (String) getContext().getProperty("total.project.number");
		if (previousPageNum.equals(myProjects.getCurrentPageNum())) {
			Assert.assertTrue(actualCurrentPageNum.equals(previousPageNum));
			Assert.assertTrue(actualLastProjectsNum.equals(previousLastProjectsNum));
		} else {
			Assert.assertFalse(actualCurrentPageNum.equals(previousPageNum));
			Assert.assertTrue(actualLastProjectsNum.equals(previousLastProjectsNum));
		}
	}

	@Then("^error message is displayed$")
	public void error_message_is_displayed() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		List<WebElement> errors = projectsPage.getErrorMessages();
		// Thread.sleep(1000);
		assertThat(errors.size(), greaterThan(0));
	}

	@Then("^Projects count  is displayed as 'Project X -Y of Z'$")
	public void projects_count_is_displayed_as_Project_X_Y_of_Z() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		AssertUtils.assertDisplayed(myProjects.getDisplayTotalProjects());
		Assert.assertEquals("Projects " + myProjects.getCurrentProjectNum() + " of " + myProjects.getLastProjectNum(),
				myProjects.getDisplayTotalProjects().getText());
	}

	@Then("^X matches position in the list for the first project displayed on the page$")
	public void x_matches_position_in_the_list_for_the_first_project_displayed_on_the_page() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		String[] splitFisrtProject = myProjects.getCurrentProjectNum().split(" ");
		int firstProjectOnPage = Integer.parseInt(splitFisrtProject[0]);
		int currentPageNumber = Integer.parseInt(myProjects.getCurrentPageNum());
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		myProjects.clickPagingOption();
		driver.switchTo().defaultContent();
		int pagingOption = Integer.parseInt(myProjects.getSelectedPagingOption().getText());
		myProjects.waitForResultsLoadingComplete();
		int firstProject = (currentPageNumber * pagingOption - pagingOption) + 1;
		Assert.assertEquals(firstProject, firstProjectOnPage);
	}

	@Then("^Y matches the position on the list for the last project displayed on the page$")
	public void y_matches_the_position_on_the_list_for_the_last_project_displayed_on_the_page() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		String[] splitLastProject = myProjects.getCurrentProjectNum().split("- ");
		int lastProjectOnPage = Integer.parseInt(splitLastProject[1]);
		int currentPageNumber = Integer.parseInt(myProjects.getCurrentPageNum());
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		int pagingOption = Integer.parseInt(myProjects.getSelectedPagingOption().getText());
		myProjects.waitForResultsLoadingComplete();
		int lastProject = currentPageNumber * pagingOption;
		Assert.assertEquals(lastProject, lastProjectOnPage);
	}

	@Then("^Z matched the total number of Search Results$")
	public void z_matched_the_total_number_of_Search_Results() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		myProjects.clickElementByJS(myProjects.getLastButton());
		WaitUtils.waitForEnabled(myProjects.getProjectGrid().getWrappedElement());
		// Thread.sleep(1000);
		String[] splitLastProject = myProjects.getCurrentProjectNum().split("- ");
		int lastProjectOnPage = Integer.parseInt(splitLastProject[1]);
		Assert.assertEquals(Integer.parseInt(myProjects.getLastProjectNum()), lastProjectOnPage);
	}

	@Then("^Page count is displayed and labeled  as 'Page S of T'$")
	public void page_count_is_displayed_and_labeled_as_Page_S_of_T() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		String PageCountDisplay = myProjects.getTotalPages().getText();
		Assert.assertEquals("Page " + myProjects.getCurrentPageNum() + " of " + myProjects.getLastPageNum(),
				PageCountDisplay);
	}

	@Then("^S matches the number of the page being displayed	\"([^\"]*)\"$")
	public void s_matches_the_number_of_the_page_being_displayed(String pageNumber) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		Assert.assertEquals(myProjects.getCurrentPageNum(), pageNumber);
	}

	@Then("^T matches the total of pages required to display all the projects$")
	public void t_matches_the_total_of_pages_required_to_display_all_the_projects() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		myProjects.clickElementByJS(myProjects.getLastButton());
		Assert.assertEquals(myProjects.getCurrentPageNum(), myProjects.getLastPageNum());
	}

	@Given("^'Page number' is displayed$")
	public void page_number_is_displayed() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		AssertUtils.assertDisplayed(myProjects.getPageNavigator());
	}

	@Then("^'Page number' value is \"([^\"]*)\"$")
	public void page_number_value_is(String edit_unedit) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		if (edit_unedit.equals("NOT_EDITABLE")) {
			AssertUtils.assertDisabled(myProjects.getPageNavigator());
		}
		if (edit_unedit.equals("EDITABLE")) {
			AssertUtils.assertEnabled(myProjects.getPageNavigator());
		}
	}

	@SuppressWarnings("unchecked")
	@Then("^Navigation arrows \"([^\"]*)\" are displayed$")
	public void navigation_arrows_are_displayed(String navigationArrows) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		List<String> arrows = new Gson().fromJson(navigationArrows, List.class);
		for (String arrow : arrows) {
			if (arrow.equals("First")) {
				Assert.assertTrue(null, myProjects.getFirstButton().isDisplayed());
			}
			if (arrow.equals("Previous")) {
				Assert.assertTrue(myProjects.getPreviousButton().isDisplayed());
			}
			if (arrow.equals("Next")) {
				Assert.assertTrue(myProjects.getNextButton().isDisplayed());
			}
			if (arrow.equals("Last")) {
				Assert.assertTrue(myProjects.getLastButton().isDisplayed());
			}

		}
	}

	@SuppressWarnings("unchecked")
	@Then("^Navigation arrows \"([^\"]*)\" are enabled$")
	public void navigation_arrows_are_enabled(String navigationArrows) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		List<String> arrows = new Gson().fromJson(navigationArrows, List.class);
		for (String arrow : arrows) {
			if (arrow.equals("None")) {
				AssertUtils.assertDisabled(myProjects.getFirstButton());
				AssertUtils.assertDisabled(myProjects.getPreviousButton());
				AssertUtils.assertDisabled(myProjects.getNextButton());
				AssertUtils.assertDisabled(myProjects.getLastButton());
			}
			if (arrow.equals("First")) {
				AssertUtils.assertEnabled(myProjects.getFirstButton());
			}
			if (arrow.equals("Previous")) {
				AssertUtils.assertEnabled(myProjects.getPreviousButton());
			}
			if (arrow.equals("Next")) {
				AssertUtils.assertEnabled(myProjects.getNextButton());
			}
			if (arrow.equals("Last")) {
				AssertUtils.assertEnabled(myProjects.getLastButton());
			}
		}
	}
}