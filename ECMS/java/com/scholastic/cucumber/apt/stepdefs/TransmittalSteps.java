package com.scholastic.cucumber.apt.stepdefs;

import static com.scholastic.torque.common.AssertUtils.assertTextMatches;
import static org.hamcrest.Matchers.equalToIgnoringCase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.internal.Configuration;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.components.ItemList;
import com.scholastic.cucumber.apt.pageobjects.Dialog;
import com.scholastic.cucumber.apt.pageobjects.HeaderSection;
import com.scholastic.cucumber.apt.pageobjects.InternalRoutingForm;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.cucumber.apt.pageobjects.TransmittalPdf;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.cucumber.apt.pageobjects.UserHomePage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class TransmittalSteps {

	@Given("^user with all assignments is logged in OTBPM$")
	public void user_with_all_assignments_is_logged_in_OTBPM() throws Throwable {
		OTBPMSigninPage signin = new OTBPMSigninPage();
		signin.launchPage();
		signin.signIn(TestBaseProvider.getTestBase().getContext().getString("transmittals.actualName.name"),
				TestBaseProvider.getTestBase().getString("transmittals.actualName.password"));
	}

	@When("^at least one of these projects that has all assignments$")
	public void at_least_one_of_these_projects_that_has_all_assignments() throws Throwable {
	  TransmittalForm transmittals=new TransmittalForm();
	  MyProjectsPage myprojects=new MyProjectsPage();
	  myprojects.waitForResultsLoadingComplete();
	  ItemGrid itemgrid=myprojects.getProjectGrid();
	 Row row= transmittals.findallAssignments();
	String colText= row.getColumnText("Art Director");
	  getContext().setProperty("designer.director.name",colText);
	}
	
	private org.apache.commons.configuration.Configuration getContext()
	{
		return TestBaseProvider.getTestBase().getContext();
		
	}

	@Then("^the names in the Cover Designer, Interior Designer and Art Director is the actual name of the user$")
	public void the_names_in_the_Cover_Designer_Interior_Designer_and_Art_Director_is_the_actual_name_of_the_user() throws Throwable {
		String colNames= (String) getContext().getProperty("designer.director.name");
		UserHomePage userHomePage=new UserHomePage();
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
			Assert.assertTrue(userHomePage.getUserNameText().equals(colNames));
	}
	
	@SuppressWarnings("unchecked")
	@When("^the user navigates to the \"([^\"]*)\" of that project and views names of \"([^\"]*)\"$")
	public void the_user_navigates_to_the_of_that_project_and_views_names_of(String location, String roles) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    UserHomePage userHomePage=new UserHomePage();
	    TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		getContext().setProperty("user.name", userHomePage.getUserNameText());
	    TransmittalForm transmittals=new TransmittalForm();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    ArrayList<String> names=new ArrayList<>();
	    Row row= transmittals.findallAssignments();
	    if(location.equals("Project Details") || location.equals("Online Transmittal Form"))
	    {
	    if(location.equals("Project Details"))
	    {
	   row.getColumn("Title").click();
	    }
	    if(location.equals("Online Transmittal Form"))
	    {
	    	row.getColumn("Transmittal Form").click();
	    }
	   WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	   myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		driver.switchTo().frame(0);
	   List<String> items = new Gson().fromJson(roles, List.class);
		for (String item : items) { 
	   WebElement userName=driver.findElement(By.xpath(String.format("//label[contains(text(),'%s')]/following::div[1]/input[1]",item)));
	   names.add(userName.getAttribute("value").trim());
		}
	    }
	    if(location.equals("Cover Routing Form") || location.equals("Interior Routing Form"))
	    {
	    if(location.equals("Cover Routing Form"))
	    {
	   row.getColumn("Cover Routing").click();
	    }
	    if(location.equals("Interior Routing Form"))
	    {
	    	row.getColumn("Interior Routing").click();
	    }
	   myprojects.switchToWindow();
	   InternalRoutingForm routingForm=new InternalRoutingForm();
	   List<String> items = new Gson().fromJson(roles, List.class);
		for (String item : items) {
			names.add(routingForm.getValueElement(item).getText());
		}
	    }
	    if(location.equals("Transmittal History"))
	    {
	    	row.getColumn("Transmittal History").click();
	    	WebElement transmittalHistoryWindow = myprojects.getDialog().getDialogTitleElement();
			WaitUtils.waitForDisplayed(transmittalHistoryWindow);
				TransmittalHistoryDialog transmittaldialog = myprojects.getTransmittalHistoryDialog();
				transmittaldialog.switchToDialog();
				ItemGrid items = transmittaldialog.getHistoryGrid();
				for(Row row1:items.getRows()){
					names.add(row1.getColumnText("Transmittal Generated by"));	
				}
				}
		getContext().setProperty("roles.names", names);
	}

	@SuppressWarnings("unchecked")
	@Then("^the names of the Cover Designer, Interior Designer and Art Director is the actual name of the user$")
	public void the_names_of_the_Cover_Designer_Interior_Designer_and_Art_Director_is_the_actual_name_of_the_user() throws Throwable {
	  ArrayList<String> names=  (ArrayList<String>) getContext().getProperty("roles.names");
	 String userHomePageName=(String) getContext().getProperty("user.name");
	  for (String name : names) {
		 Assert.assertTrue(userHomePageName.equals(name));
	}
	}
	
	@Given("^the user selects the Transmittal Form of a project$")
	public void the_user_selects_the_Transmittal_Form_of_a_project() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		Thread.sleep(2000);
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		myprojectsgrid.showAllColumns();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex("Transmittal Form");
		try{
		WaitUtils.waitForDisplayed(itemgrid.getRows().get(0).getColumn(index));
		itemgrid.getRows().get(0).getColumn(index).click();
		}
		catch(Exception e)
		{
			WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	    	final WebElement details = driver.findElement(By.xpath(".//*[@id='tbmyProjects']/tbody/tr[2]/td[14]/div/input"));
	    	details.click();
		}
		
	}

	@When("^the user selects \"([^\"]*)\"$")
	public void the_user_selects(String type) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.switchToTransmittalForm();
	   WebElement transmittalType=transmittals.getTransmittalFormButtons(type);
	  if( transmittalType.isSelected())
	  {
	  }
	  else
	  {
		  transmittalType.click();
	  }
	}
	
	@When("^user selects 'Proofs Types' first \"([^\"]*)\" options$")
	public void user_selects_Proofs_Types_first_options(String option) throws Throwable {
		selects_the_required_fields(option);
	}
	
	@When("^user selects 'HereWiths' first \"([^\"]*)\" options$")
	public void user_selects_HereWiths_first_options(String option) throws Throwable {
		selects_the_required_fields(option);
	}
	
	@When("^user selects 'Mech' option \"([^\"]*)\"$")
	public void user_selects_Mech_option(String option) throws Throwable {
		selects_the_required_fields(option);
	}

	@SuppressWarnings("unchecked")
	@When("^selects the required fields \"([^\"]*)\"$")
	public void selects_the_required_fields(String fields) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
	   List<String> items = new Gson().fromJson(fields, List.class);
		for (String item : items) { 	  
	   if(item.equals("OtherHereWith"))
	   {
		   transmittals.getHerewithOtherCheckbox().click();
		  // WaitUtils.waitForDisplayed(transmittals.getHerewithOtherInput());
		   transmittals.getHerewithOtherInput().clear();
		   String herewithInput="otherHereWith";
		   transmittals.getHerewithOtherInput().sendKeys(herewithInput);
		   getContext().setProperty("herewith.input", herewithInput);
		   Thread.sleep(1000);
	   }
	   
	   else  if(item.equals("OtherProofType"))
	   {
		   transmittals.getProofTypeOtherCheckbox().click();
		   WaitUtils.waitForDisplayed(transmittals.getProofTypeOtherInput());
		   transmittals.getProofTypeOtherInput().clear();
		   String proofInput="otherProof";
		   transmittals.getProofTypeOtherInput().sendKeys(proofInput);
		   getContext().setProperty("prooftypes.input", proofInput);
	   }
	   
	   else if(item.contains("Files"))
		{
			String filesText=getContext().getString("transmittal.form.filesPosted.input");
			 transmittals.getFilesPostedToInput().clear();
			 transmittals.getFilesPostedToInput().sendKeys(filesText);
			 getContext().setProperty("files.posted.to", filesText);
			 Thread.sleep(10000);
		}
	   
	   else{
		 transmittals.getTransmittalFormButtons(item).click();
	   }
		}
		Thread.sleep(2000);
	}

	@When("^user submits the form$")
	public void user_submits_the_form() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.getSubmitButton().click();
	}
	
	@When("^generates the transmittal form$")
	public void generates_the_transmittal_form() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.switchToWindow();
		   WebDriver driver=TestBaseProvider.getTestBase().getDriver();
			try{
				driver.switchTo().defaultContent();
			if(driver.getCurrentUrl().contains("pdf"))
			{
				driver.switchTo().defaultContent();
			}
			else
			{
				MyProjectsPage myprojects1=new MyProjectsPage();
				myprojects1.switchToWindow();
				driver.switchTo().defaultContent();
			}
			}
			catch(Exception e)
			{
				driver.switchTo().defaultContent();
			}
		   Thread.sleep(2000);
	}

	@Then("^multiple herewiths should be separated by a comma followed by space are displayed in the PDF in the herewith section$")
	public void multiple_herewiths_should_be_separated_by_a_comma_followed_by_space_are_displayed_in_the_PDF_in_the_herewith_section() throws Throwable {
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
				String herewiths=transmittalpdf.getTransmittalPdfValue("Herewith");
				Assert.assertTrue(herewiths.contains(", "));			
	}
	
	
	@When("^user view the Publication Date of a project$")
	public void user_view_the_Publication_Date_of_a_project() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		String pubDate="";
		 boolean navNextPage = false;
			do {
				if (navNextPage) {
					myprojects.getNextButton().click();
					myprojects.waitForResultsLoadingComplete();
				}
		
		for (int row=0;row<itemgrid.getRows().size();row++) {
			if(itemgrid.getRows().get(row).getColumnText("Pub Date").trim().isEmpty())
			{
				navNextPage = true;
				continue;
			}
			else if(itemgrid.getRows().get(row).getColumnText("First Use Date").trim().isEmpty())
				{
				navNextPage = true;
				continue;
			}
			else
			{
				pubDate=itemgrid.getRows().get(row).getColumnText("Pub Date").trim();
				getContext().setProperty("pubdate.row", row);
				navNextPage = false;
				break;
			}
		}
			}
			 while (navNextPage);
			
		getContext().setProperty("myprojects.pubdate", pubDate);
	}

	@Then("^publication date on the \"([^\"]*)\" should be the same as the actual Publication Date$")
	public void publication_date_on_the_should_be_the_same_as_the_actual_Publication_Date(String location) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		int row=(int) getContext().getProperty("pubdate.row");
		 WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String pubDate=	(String) getContext().getProperty("myprojects.pubdate");
		String pubDateInForm="";
		if(location.equals("Cover Routing") || location.equals("Interior Routing"))
		{
	   itemgrid.getRows().get(row).getColumn(location).click();
	   myprojects.switchToWindow();
	   InternalRoutingForm routingForm=new InternalRoutingForm();
	   pubDateInForm=routingForm.getValueElement("Pub Date:").getText().trim();
		Assert.assertEquals(pubDate, pubDateInForm);
		}
		 if(location.equals("Project Details"))
		 {
			 itemgrid.getRows().get(row).getColumn("Title").click();
			 myprojects.getDialog();
				WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
			driver.switchTo().frame(0);
			String productDetailsXpath=String.format("//label[contains(text(),'%s')]//following::input[1]","Trade Use");	
			WebElement element=driver.findElement(By.xpath(productDetailsXpath));
			pubDateInForm=element.getAttribute("value").trim();
			 Date actualDate = null;
	        SimpleDateFormat yy = new SimpleDateFormat( "MM/dd/yy" );
	        SimpleDateFormat yyyy = new SimpleDateFormat( "MM/dd/yyyy" );
	  
	        try {
	            actualDate = yy.parse( pubDateInForm );
	        }
	        catch ( ParseException pe ) {
	            System.out.println( pe.toString() );
	        }
			Assert.assertEquals(pubDate, yyyy.format( actualDate ).trim());
		 }
}
	
	@SuppressWarnings("unchecked")
	@When("^and the user selects other required values \"([^\"]*)\"$")
	public void and_the_user_selects_other_required_values(String otherFields) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		List<String> items = new Gson().fromJson(otherFields, List.class);
		for (String item : items) {
			if(item.equals("Other"))
			   {
				   transmittals.getHerewithOtherCheckbox().click();
				   transmittals.getHerewithOtherInput().clear();
				   transmittals.getHerewithOtherInput().sendKeys("otherInfo");
			   }
			
			   else{
				   transmittals.getTransmittalFormButtons(item).click();
			   }
		}
		transmittals.getSubmitButton().click();
	}
	
	@When("^the user generates a PDF$")
	public void the_user_generates_a_PDF() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.switchToWindow();
	}

	@Then("^the content in Verify Files posted to section is displayed as expected\\.$")
	public void the_content_in_Verify_Files_posted_to_section_is_displayed_as_expected() throws Throwable {
		 InternalRoutingForm routingForm=new InternalRoutingForm();
		String fileText= (String) getContext().getProperty("files.posted.to");
			String filesPosted=routingForm.getValueElement("Files Posted").getText();
			Assert.assertTrue(filesPosted.equals(fileText));
	}
	
	@Then("^\"([^\"]*)\" herewith is displayed as \"([^\"]*)\" in the PDF$")
	public void herewith_is_displayed_as_in_the_PDF(String arg1, String arg2) throws Throwable {
		 InternalRoutingForm routingForm=new InternalRoutingForm();
				String fileText="4/c Flat Art";
				String filesPosted=routingForm.getValueElement("Herewith:").getText();
				Assert.assertTrue(filesPosted.equals(fileText));
	}
//	
//	@When("^the user navigates to the \"([^\"]*)\" page$")
//	public void the_user_navigates_to_the_page(String location) throws Throwable {
//		MyProjectsPage myprojects=new MyProjectsPage();
//		myprojects.waitForResultsLoadingComplete();
//		ItemGrid itemgrid=myprojects.getProjectGrid();
//		String pubDate="";
//		 boolean navNextPage = false;
//			do {
//				if (navNextPage) {
//					myprojects.getNextButton().click();
//					myprojects.waitForResultsLoadingComplete();
//				}
//		
//		for (int row=0;row<itemgrid.getRows().size();row++) {
//			if(itemgrid.getRows().get(row).getColumnText("Pub Date").trim().isEmpty() && itemgrid.getRows().get(row).getColumnText("First Use Date").trim().isEmpty())
//			{
//				System.out.println("pubdate:"+itemgrid.getRows().get(row).getColumnText("Pub Date").trim()+";");
//				navNextPage = true;
//				continue;
//			}
//			else
//			{
//	}
//		
	@Then("^the dates displayed on the page, should be a consistant format of MM/DD/YYYY$")
	public void the_dates_displayed_on_the_page_should_be_a_consistant_format_of_MM_DD_YYYY() throws Throwable {
	    
	}
	
	@Then("^the PDF should contain the same valid value in the type section$")
	public void the_PDF_should_contain_the_same_valid_value_in_the_type_section() throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		String herewithOther=(String) getContext().getProperty("herewith.input");
		String prooftypeOther=(String) getContext().getProperty("prooftypes.input");
		String pdfHerewith=routingform.getValueElement("Herewith:").getText();
		String pdfProofType=routingform.getValueElement("Proofs:").getText();
		Assert.assertEquals(herewithOther, pdfHerewith.trim());
		Assert.assertEquals(prooftypeOther, pdfProofType.trim());
	}
	
	@Then("^multiple proof types should be separated by a comma followed by space are displayed in the PDF in the proof type section$")
	public void multiple_proof_types_should_be_separated_by_a_comma_followed_by_space_are_displayed_in_the_PDF_in_the_proof_type_section() throws Throwable {
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		String proofsText=transmittalpdf.getTransmittalPdfValue("Proofs").trim();
		Assert.assertTrue(proofsText.contains(", "));
//		String commaSpaceRegex = "(,\\s)";
//				Pattern pattern = Pattern.compile(commaSpaceRegex);
//				Matcher matcher = pattern.matcher(proofsText);
//				Assert.assertTrue(matcher.matches());	
	}
	
	@SuppressWarnings("unchecked")
	@Then("^project \"([^\"]*)\" is displayed$")
	public void project_is_displayed(String label) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().frame(0);
		List<String> types = new Gson().fromJson(label, List.class);
		for (String value : types) { 	  
		AssertUtils.assertDisplayed(transmittals.getValueElement(value));
		}
	}

	@Then("^project \"([^\"]*)\" value is not editable$")
	public void project_value_is_not_editable(String label) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		String input="a";
		List<String> types = new Gson().fromJson(label, List.class);
		for (String value : types) { 	 
		transmittals.getValueElement(value).sendKeys(input);
		String transmittalValue=transmittals.getValueElement(value).getAttribute("value").trim();
		Assert.assertFalse(transmittalValue.equals(input));
		}
	}
	
	@When("^New 'Transmittal Form' page is opened$")
	public void new_Transmittal_Form_page_is_opened() throws Throwable {

	}

	@Then("^'Other' label text has a capitalized \"([^\"]*)\" under the Mechs, Proof Types and Herewith sections$")
	public void other_label_text_has_a_capitalized_under_the_Mechs_Proof_Types_and_Herewith_sections(String arg1) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.switchToFrame();
	    String Expected="Other";
	    String Actual= transmittals.getLabel("Other").getText();
	    Assert.assertEquals("FAIL-The text didn't match",Expected,Actual);
		
		
		
	}
	
}
