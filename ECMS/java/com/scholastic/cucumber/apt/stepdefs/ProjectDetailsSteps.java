package com.scholastic.cucumber.apt.stepdefs;

import java.sql.Driver;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ManageMyTeamPage;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.ProjectDetails;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.thoughtworks.selenium.Selenium;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.AssertionFailedError;

public class ProjectDetailsSteps {

	@When("^User opens Project Details for \"([^\"]*)\"$")
	public void user_opens_Project_Details_for(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		 myprojects.waitForResultsLoadingComplete();
		 
		
			try{ 
				 ItemGrid itemgrid=myprojects.getProjectGrid();
					int index = itemgrid.getColumnIndex("Title");
			itemgrid.getRows().get(0).getColumn(index).click();
			}
			 catch(Exception e)
		    {
		    	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		    	final WebElement details = driver.findElement(By.xpath(".//*[@id='tbmyProjects']/tbody/tr[2]/td[4]/div/input"));
		    	details.click();
		    }
			myprojects.getDialog();
			
			
	}

	@Then("^Title is displayed as \"([^\"]*)\"$")
	public void title_is_displayed_as(String title) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		WebElement productDetailWindow = myprojects.getDialog().getDialogTitleElement();
		WaitUtils.waitForDisplayed(productDetailWindow);
		Assert.assertTrue(productDetailWindow.getText().contains(title));
	}
	
	@Then("^Small 'Cover image' is displayed$")
	public void small_Cover_image_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisplayed(details.getCoverimage());
	}
	
	@Then("^Barcode icon is displayed$")
	public void barcode_icon_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisplayed(details.getBarcode());
	}
	
	@When("^User opens 'Transmittal History'$")
	public void user_opens_Transmittal_History() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		details.getButton("Transmittal History").click();
	}
	
	@Then("^'Transmittal History button' is displayed$")
	public void transmittal_History_button_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisplayed(details.getButton("Transmittal History"));
	}

	@Then("^'Transmittal History' view is displayed in a new window$")
	public void transmittal_History_view_is_displayed_in_a_new_window() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebElement element=myprojects.getDialog().getDialogTitleElement();
		Assert.assertEquals(element.getText(), "Transmittal History");	
	}
	
	@Then("^'Online Transmittal Form' view is displayed in a new window$")
	public void online_Transmittal_Form_view_is_displayed_in_a_new_window() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebElement element=myprojects.getDialog().getDialogTitleElement();
		Assert.assertTrue(element.getText().contains("Production/Manufacturing Transmittal Form for Cover"));
	}
	
	@Then("^'New Transmittal Form button' is displayed$")
	public void new_Transmittal_Form_button_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisplayed(details.getButton("New Transmittal"));
		details.getButton("New Transmittal").click();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebElement element=myprojects.getDialog().getDialogTitleElement();
		Assert.assertTrue(element.getText().contains("Production/Manufacturing Transmittal Form for Cover"));
	}
	
	@When("^user clicks on 'Assign'$")
	public void user_clicks_on_Assign() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisplayed(details.getButton("Assign"));
		details.getButton("Assign").click();
	}

	@Then("^'Assign' view is displayed in a new window$")
	public void assign_view_is_displayed_in_a_new_window() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebElement element=myprojects.getDialog().getDialogTitleElement();
		Assert.assertTrue(element.getText().contains("Assign"));	
	}
	
	@Then("^label for \"([^\"]*)\" is displayed as \"([^\"]*)\"$")
	public void label_for_is_displayed_as(String label, String arg2) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		Assert.assertEquals(details.getLabelElement(label).getText(),label);
	}

	@Then("^\"([^\"]*)\" is not editable$")
	public void is_not_editable(String label) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		String input="noneditabletest";
		details.getValueElement(label).sendKeys(input);
		Assert.assertFalse(details.getValueElement(label).getText().contains(input));		
	}

	@SuppressWarnings("static-access")
	@Then("^\"([^\"]*)\" is displayed and populated from database \"([^\"]*)\" using \"([^\"]*)\"$")
	public void is_displayed_and_populated_from_database_using(String label, String dblabel, String isbn) throws Throwable {
	   	MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		AssertUtils.assertDisplayed(details.getValueElement(label));
		DBConnection dbcon=new DBConnection();
		String dbvalue="";
		String detailsValue=details.getValueElement(label).getAttribute("value").trim();
		ResultSet rs=dbcon.dbConnection("SELECT *"
				+ " FROM APT_PROJECTS_VIEW,PLANNING_INFO"
				+ " WHERE APT_PROJECTS_VIEW.CORP_ID_VALUE=PLANNING_INFO.CORP_ID_VALUE AND "
				+ "ISBN_13='" + isbn+ "'");
		while(rs.next())
		{
			String result=rs.getString(dblabel);
		if(result==null)
		{
			dbvalue="";
		}
		else if(dblabel.contains("DATE") || dblabel.contains("TRADE_USE") || dblabel.contains("FIRST_CLUB_USE")|| dblabel.contains("TARGET_DUE_MFG"))
		{
			dbvalue=myprojects.changeDateFormat(result);
			SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yy");
		    SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy");
		    Date date = format1.parse(detailsValue);
		    detailsValue=format2.format(date).toString();
		}
		else 
		{
			dbvalue=result;
		}
		}
		TestBaseProvider.getTestBase().getContext().setProperty("details.value", detailsValue);
		Assert.assertEquals(detailsValue, dbvalue);
		myprojects.waitForResultsLoadingComplete();
		myprojects.getcloseButton().click();
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		myprojects.waitForResultsLoadingComplete();
	}

	@Then("^\"([^\"]*)\" matches same value displayed in \"([^\"]*)\"$")
	public void matches_same_value_displayed_in(String label, String projectsColumn) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		String detailsValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("details.value");
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		if(!projectsColumn.equals(""))
		{
			String projectsValue=itemgrid.getRows().get(0).getColumnText(projectsColumn);
			Assert.assertEquals(detailsValue.trim(), projectsValue.trim());
		}	
	}
	
	@Then("^'New Transmittal Form button' is not displayed$")
	public void new_Transmittal_Form_button_is_not_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertNotDisplayed(details.getButton("New Transmittal"));
		AssertUtils.assertDisabled(details.getButton("New Transmittal"));
	}
	
	@Then("^'Cover Routing button' is displayed$")
	public void cover_Routing_button_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisplayed(details.getButton("Cover Routing"));
	}
	
	@When("^user is in 'Project Details' page for \"([^\"]*)\"$")
	public void user_is_in_Project_Details_page_for(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.switchToResults();
		
		
		//WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		//driver.switchTo().frame(driver.findElement(By.cssSelector("iframe#display_frame.container")));
		
	    
	}
	
	@Then("^'Production Editor' label is displayed$")
	public void production_Editor_label_is_displayed() throws Throwable {
		
		ProjectDetails details=new ProjectDetails();
		details.switchToDetailsDialog();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();	
		//AssertUtils.assertDisplayed(driver.findElement(By.xpath("//*[@id='xfe22']/label")));
        //System.out.println((details.getLabelElement("Production Editor")));
		//WaitUtils.waitForDisplayed((details.getLabelElement("Production Editor")));
		AssertUtils.assertDisplayed(details.getProductioneditorlabel());
		
	  }

	@Then("^'Managing Editor' label is not displayed$")
	public void managing_Editor_label_is_not_displayed() throws Throwable {
		ProjectDetails details=new ProjectDetails();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		//details.switchToDetailsDialog();
		Thread.sleep(2000);
		
		
		
		//AssertUtils.assertNotPresent(details.getLabelElement("Managing Editor"));
		
		Assert.assertFalse(driver.findElement(By.xpath("//*[contains(text(),'Managing Editor')]")).isDisplayed());
		
		
	//	AssertUtils.assertNotDisplayed(driver.findElement(By.xpath("//*[contains(text(),'Managing Editor')]")));
		//AssertUtils.assertNotDisplayed(details.getLabelElement("Managing Editor"));
		
		
	}
	@When("^user opens 'Cover Routing' Form$")
	public void user_opens_Cover_Routing_Form() throws Throwable {
		ProjectDetails details=new ProjectDetails();
		details.switchToDetailsDialog();
		details.getButton("Cover Routing").click();
		Thread.sleep(1000);
		
	}
	@When("^user opens 'Interior Routing' Form$")
	public void user_opens_Interior_Routing_Form() throws Throwable {
		
		ProjectDetails details=new ProjectDetails();
		details.switchToDetailsDialog();
		details.getButton("Interior Routing").click();
		Thread.sleep(1000);
		
	 }
	@Then("^'Interior Routing button' is displayed$")
	public void interior_Routing_button_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisplayed(details.getButton("Interior Routing"));
		
	}

	@Then("^'Assign button'is displayed$")
	public void assign_button_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisplayed(details.getButton("Assign"));
	}

	@When("^user expands the 'Production Tracking information'$")
	public void user_expands_the_Production_Tracking_information() throws Throwable {
		ProjectDetails details=new ProjectDetails();
		details.switchToDetailsDialog();
		details.getLabelElement("Production Tracking Information").click();
		Thread.sleep(1000);
	}

	@Then("^'Production Tracking Information Coming Soon\\.\\.\\.\\.\\.\\.' text is displayed$")
	public void production_Tracking_Information_Coming_Soon_text_is_displayed() throws Throwable {
		
	    String expected="Production Tracking Information Coming Soon......";
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		WebElement w=driver.findElement(By.xpath("//*[@id='rte1']/p"));
		String Actual=w.getText();
		AssertUtils.assertDisplayed(w);
		WaitUtils.waitForDisplayed(w);
		Assert.assertEquals(Actual,expected ,"FAIL-The text didn't Match");
	    
	}

	@When("^User selects the Barcode icon$")
	public void user_selects_the_Barcode_icon() throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		ProjectDetails details=new ProjectDetails();
		details.switchToDetailsDialog();
		Thread.sleep(3000);
		details.getBarcode().click();
		driver.switchTo().defaultContent();
		 
	}

	@Then("^No Available barcode message is displayed$")
	public void no_Available_barcode_message_is_displayed() throws Throwable {
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		ProjectDetails details=new ProjectDetails();
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String Msg ="Barcode EPS file is not available at the moment to download.";
		AssertUtils.assertDisplayed(myteam.getselectDesignerMsgPopup());
	    Assert.assertEquals(myteam.getselectDesignerMsgPopup().getText(),Msg,"FAIL-The Text didn't match");
	}
	@When("^User opens 'New Transmittal Form'$")
	public void user_opens_New_Transmittal_Form() throws Throwable {
		ProjectDetails details=new ProjectDetails();
		details.switchToDetailsDialog();
		details.getButton("New Transmittal").click();
		
	}
	@Then("^list of Available barcodes are displayed in  a new window$")
	public void list_of_Available_barcodes_are_displayed_in_a_new_window() throws Throwable {
		ProjectDetails details=new ProjectDetails();
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		WebElement w=driver.findElement(By.xpath("//*[@id='header2']/label"));
		AssertUtils.assertDisplayed(w);
		
		
	}
    @Then("^the number of barcodes listed is \"([^\"]*)\"$")
	public void the_number_of_barcodes_listed_is(String arg1) throws Throwable {
    	
		
	}
    @Then("^'Production tracking information' is displayed$")
    public void production_tracking_information_is_displayed() throws Throwable {
    	
    	ProjectDetails details=new ProjectDetails();
    	details.switchToDetailsDialog();
    	AssertUtils.assertDisplayed(details.getLabelElement("Production Tracking Information"));
    	
    	
    }

    @Then("^'Production tracking information' is collapsed$")
    public void production_tracking_information_is_collapsed() throws Throwable {
    	
    	ProjectDetails details=new ProjectDetails();
    	
    	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
    	
    	AssertUtils.assertNotDisplayed(details.getProductiontrackinfomsg());
        
    }

    @Then("^'Production tracking information' is expandable$")
    public void production_tracking_information_is_expandable() throws Throwable {
    	ProjectDetails details=new ProjectDetails();
    	
    	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
    	details.getLabelElement("Production Tracking Information").click();
    	AssertUtils.assertDisplayed(details.getProductiontrackinfomsg());
    	
    	
    }

    @Then("^'Production tracking information' is collapsable$")
    public void production_tracking_information_is_collapsable() throws Throwable {
       
    	ProjectDetails details=new ProjectDetails();
    	details.getLabelElement("Production Tracking Information").click();
    	AssertUtils.assertNotDisplayed(details.getProductiontrackinfomsg());
    	
    }
    @Then("^'EPIC comments section' is displayed$")
    public void epic_comments_section_is_displayed() throws Throwable {
    	ProjectDetails details=new ProjectDetails();
    	details.switchToDetailsDialog();
    	AssertUtils.assertDisplayed(details.getLabelElement("EPIC Comments"));
        
    }

    @Then("^'EPIC comments section' is collapsed$")
    public void epic_comments_section_is_collapsed() throws Throwable {
    	ProjectDetails details=new ProjectDetails();
    	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
    	AssertUtils.assertNotDisplayed(details.getEpicCommentsData());
    	
    }

    @Then("^'EPIC comments section' is expandable$")
    public void epic_comments_section_is_expandable() throws Throwable {
    	ProjectDetails details=new ProjectDetails();
    	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
    	details.getLabelElement("EPIC Comments").click();
    	
    	AssertUtils.assertDisplayed(details.getEpicCommentsData());
    	
    }

    @Then("^'EPIC comments section' is collapsable$")
    public void epic_comments_section_is_collapsable() throws Throwable {
    	ProjectDetails details=new ProjectDetails();
    	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
    	details.getLabelElement("EPIC Comments").click();
    	Thread.sleep(2000);
    	WebElement w=driver.findElement(By.xpath("//*[@id='epicComments']/div/div"));
    	AssertUtils.assertNotDisplayed(details.getEpicCommentsData());    
    	
     }
    @Then("^'New Transmittal button' is not displayed$")
    public void new_Transmittal_button_is_not_displayed() throws Throwable {
    	
    	MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails details=new ProjectDetails();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		AssertUtils.assertDisabled(details.getButton("New Transmittal"));
    	
    }
    @When("^user expands 'EPIC comments'$")
    public void user_expands_EPIC_comments() throws Throwable {
     
    	ProjectDetails details=new ProjectDetails();
    	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
    	details.getLabelElement("EPIC Comments").click();
    
    	
    }

    @Then("^\"([^\"]*)\"are displayed when available$")
    public void are_displayed_when_available(String type) throws Throwable {
    	
    	ProjectDetails details=new ProjectDetails();
    	
    	
    	//need to do
    	details.getEpicMsgComments(type);
    	WaitUtils.waitForDisplayed(details.getEpicMsgComments(type));
    	AssertUtils.assertDisplayed(details.getEpicMsgComments(type));
    }

    @Then("^label for \"([^\"]*)\" is displayed as \"([^\"]*)\" when comments are available$")
    public void label_for_is_displayed_as_when_comments_are_available(String type, String label) throws Throwable {
      
    	
    	
    	
    	
    	}
    
    
    
}
