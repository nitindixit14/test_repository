package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.AssignDialog;
import com.scholastic.cucumber.apt.pageobjects.EpicHomePage;
import com.scholastic.cucumber.apt.pageobjects.EpicKeyInfoPage;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.ProjectDetails;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class AssignProjectsSteps {

	@When("^\"([^\"]*)\" is in OTBPM Art Directors List$")
	public void is_in_OTBPM_Art_Directors_List(String arg1) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
		ItemGrid itemGrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getcheckboxesHeader());
		itemGrid.showAllColumns();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	   Assert.assertTrue(itemgrid.allColumnsText("Art Director").size()>0);
	    
	}

	@When("^New EPIC record exists in OTBPM$")
	public void new_EPIC_record_exists_in_OTBPM() throws Throwable {
		  MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
	}

	@SuppressWarnings("static-access")
	@When("^User Query \"([^\"]*)\" for New record from OTBPM database$")
	public void user_Query_for_New_record_from_OTBPM_database(String arg1) throws Throwable {
	    DBConnection dbcon=new DBConnection();
	    String isbn=(String) TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
	    ResultSet rs=dbcon.dbConnection("SELECT ART_DIRECTOR_NAME FROM APT_PROJECTS_VIEW WHERE ISBN_13='" + isbn+ "'");
	    while(rs.next())
	    {
	    	TestBaseProvider.getTestBase().getContext().setProperty("db.artdirector.name",rs.getString("ART_DIRECTOR_NAME"));
	    }
	}

	@Then("^Art Director value from OTBPM database matches displayed \"([^\"]*)\" in EPIC$")
	public void art_Director_value_from_OTBPM_database_matches_displayed_in_EPIC(String artDirector) throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		String name="";
		if (artDirector.contains(",")) {
			String[] fullname= artDirector.split(",");
			name=fullname[1].replace(",", "").trim()+fullname[0];
		}
		else
		{
			name=artDirector;
		}
		name=name.replaceAll("\\s", "");
		dbValue=dbValue.replaceAll("\\s", "");
		Assert.assertEquals(dbValue.toUpperCase(), name.toUpperCase());
	}
	
	@Then("^\"([^\"]*)\" value displayed on Project List matches OTBPM database value for Art Director$")
	public void value_displayed_on_Project_List_matches_OTBPM_database_value_for_Art_Director(String arg1) throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		 MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    int index=itemgrid.getColumnIndex("Art Director");
		   String myProjectsValue= itemgrid.getRows().get(0).getColumnText(index);
		   Assert.assertEquals(myProjectsValue.toUpperCase(), dbValue.toUpperCase());
	}

	@Then("^\"([^\"]*)\" value displayed on Project Details page matches OTBPM database value for Art Director$")
	public void value_displayed_on_Project_Details_page_matches_OTBPM_database_value_for_Art_Director(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails productDetails=new  ProjectDetails();
		 myprojects.waitForResultsLoadingComplete();
		 ItemGrid itemgrid1=myprojects.getProjectGrid();
			String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
			int index1 = itemgrid1.getColumnIndex("Title");
			itemgrid1.getRows().get(0).getColumn(index1).click();
			productDetails.switchToDetailsDialog();
			String elmValue = productDetails.getValueElement("Art Director").getAttribute("value").trim();
			Assert.assertEquals(elmValue.toUpperCase(), dbValue.toUpperCase());
				myprojects.waitForResultsLoadingComplete();
				myprojects.clickCloseButton();
				TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
				myprojects.waitForResultsLoadingComplete();
	}
	@Then("^\"([^\"]*)\" value displayed on Transmittal Form page matches OTBPM database value for Art Director$")
	public void value_displayed_on_Transmittal_Form_page_matches_OTBPM_database_value_for_Art_Director(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		int index = myprojectsgrid.getColumnIndex("Transmittal Form");
		WaitUtils.waitForDisplayed(myprojectsgrid.getRows().get(0).getColumn(index));
		myprojectsgrid.getRows().get(0).getColumn(index).click();	
		TransmittalForm form=new TransmittalForm();
		form.switchToTransmittalForm();
		String elmValue = form.getValueElement("Art Director:").getAttribute("value").trim();
		Assert.assertEquals(elmValue.toUpperCase(), dbValue.toUpperCase());
		myprojects.waitForResultsLoadingComplete();
		myprojects.clickCloseButton();
		form.clickFormpopUp("No");
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		myprojects.waitForResultsLoadingComplete();
	}
	
	@When("^\"([^\"]*)\" is NOT in OTBPM Art Directors List$")
	public void is_NOT_in_OTBPM_Art_Directors_List(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
			ItemGrid itemGrid = myprojects.getProjectGrid();
			WaitUtils.waitForEnabled(myprojects.getProjectGrid().getcheckboxesHeader());
			itemGrid.showAllColumns();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		   Assert.assertTrue(itemgrid.allColumnsText("Art Director").toString().equals("[]"));
	}

	@Then("^Art Director value from OTBPM database is empty$")
	public void art_Director_value_from_OTBPM_database_is_empty() throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		 Assert.assertTrue(dbValue==null);
	}
	
	@When("^User Assigns Art Director as \"([^\"]*)\" in Assign view$")
	public void user_Assigns_Art_Director_as_in_Assign_view(String newArtDirector) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		AssignDialog assign=new AssignDialog();
		assign.switchToAssignDialog();		
		
		assign.getAssignInputs("Art Director").sendKeys("Rajesh Chandrasekar");
		Actions action=new Actions(TestBaseProvider.getTestBase().getDriver());
		action.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.RETURN).build().perform();
		assign.clickAssignSaveBtn();
		
	}

	@When("^User return to search results view$")
	public void user_return_to_search_results_view() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    Thread.sleep(20000);
	}
	
	@When("^user selects a project to 'UnAssign'$")
	public void user_selects_a_project_to_UnAssign() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		List<WebElement> checkboxes = itemgrid.findElements(By.id("check_tbmyProjects"));
		if (!(checkboxes.get(1)).isSelected()) {
			checkboxes.get(1).click();
		}
		myprojects.clickElementByJS(myprojects.getAssignButton());
	}

	@When("^User Unassigns Art Director for project in Assign view$")
	public void user_Unassigns_Art_Director_for_project_in_Assign_view() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		AssignDialog assign=new AssignDialog();
		assign.switchToAssignDialog();
		
		assign.clickRemoveADBtn();
		assign.clickAssignSaveBtn();
	}

	@Then("^Art Director value is blank on Project List$")
	public void art_Director_value_is_blank_on_Project_List() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getcheckboxesHeader());
		//itemGrid.showAllColumns();
	   Assert.assertTrue(itemgrid.allColumnsText("Art Director").toString().equals("[]"));
	}

	@Then("^Art Director value is blank on Project Details page$")
	public void art_Director_value_is_blank_on_Project_Details_page() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		ProjectDetails productDetails=new  ProjectDetails();
		 myprojects.waitForResultsLoadingComplete();
		 ItemGrid itemgrid1=myprojects.getProjectGrid();
			int index1 = itemgrid1.getColumnIndex("Title");
			itemgrid1.getRows().get(0).getColumn(index1).click();
			productDetails.switchToDetailsDialog();
			String elmValue1 = productDetails.getValueElement("Art Director").getAttribute("value").trim();
			Assert.assertTrue(elmValue1.trim().equals(""));
			myprojects.waitForResultsLoadingComplete();
			myprojects.clickCloseButton();
			TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
			myprojects.waitForResultsLoadingComplete();
	}

	@Then("^Art Director value is blank on Transmittal Form page$")
	public void art_Director_value_is_blank_on_Transmittal_Form_page() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		int index = myprojectsgrid.getColumnIndex("Transmittal Form");
		WaitUtils.waitForDisplayed(myprojectsgrid.getRows().get(0).getColumn(index));
		myprojectsgrid.getRows().get(0).getColumn(index).click();	
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebDriver driver1=TestBaseProvider.getTestBase().getDriver();
		driver1.switchTo().frame(0);
		String elmValue = driver1.findElement(By.xpath("//label[text()='Art Director:']//following::input[1]")).getAttribute("value").trim();
		Assert.assertTrue(elmValue.trim().equals(""));

	}

	@Then("^Art Director value is empty in OTBPM database$")
	public void art_Director_value_is_empty_in_OTBPM_database() throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		 Assert.assertTrue(dbValue==null);
	}
	
	@Then("^Art Director value from OTBPM database remains as \"([^\"]*)\"$")
	public void art_Director_value_from_OTBPM_database_remains_as(String artDirector) throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		String name="";
		if (artDirector.contains(",")) {
			String[] fullname= artDirector.split(",");
			name=fullname[1].replace(",", "").trim()+fullname[0];
		}
		else
		{
			name=artDirector;
		}
		name=name.replaceAll("\\s", "");
		dbValue=dbValue.replaceAll("\\s", "");
		Assert.assertEquals(dbValue.toUpperCase(), name.toUpperCase());
	}
	
	
	@Then("^A table is displayed with the following \"([^\"]*)\"$")
	public void a_table_is_displayed_with_the_following(String labels) throws Throwable {
		
		
		
	WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String[] expected_text={"Title","ISBN","Art Director","Cover Designer","Interior Designer"};
		List<WebElement> Actual=driver.findElements(By.xpath("//*[@id='assignTableDetails']/thead/tr/th/label"));
		for(int i=0;i<Actual.size();i++)
		{ 
			//System.out.println("Expected text is  " +expected_text[i]);
			//System.out.println("Actual text is    " +Actual.get(i).getText());
			
			
			AssertUtils.assertTextMatches(Actual.get(i),
				    Matchers.containsString(expected_text[i]));
		
	   }
		
		
	}
	
	@Then("^the value for \"([^\"]*)\" is populated for the project$")
	public void the_value_for_is_populated_for_the_project(String arg1) throws Throwable {
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		WebElement table=driver.findElement(By.xpath("//*[@id='assignTableDetails']"));
		
		List<WebElement> rows=table.findElements(By.tagName("tr"));
		
		
		for(int rnum=0;rnum<rows.size();rnum++){
			
		List<WebElement> columns=rows.get(rnum).findElements(By.tagName("td"));

		          System.out.println("Number of columns:"+columns.size());

		          for(int cnum=0;cnum<columns.size();cnum++)

		             {

		                       System.out.println(columns.get(cnum).getText());
                      }
		          
		          
             }
		
		//need to do 
		/*
		List<WebElement> values=driver.findElements(By.xpath("//*[@id='assignTableDetails']/tbody/tr/td/div/input"));
		for(int i=0;i<values.size();i++)
		{ 
			
		     System.out.println("Values are   "+values.get(i).getText());
			 AssertUtils.assertDisplayed(values.get(i));
		     
		}*/
	}

	@Then("^the values match information in Project List$")
	public void the_values_match_information_in_Project_List() throws Throwable {
	   
		
	    }
	@Then("^'Assign' Title is displayed$")
	public void assign_Title_is_displayed() throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("display_frame");
		WebElement w=driver.findElement(By.xpath("//*[@id='fb_title']"));
		Assert.assertEquals(w.getText(),"Assign");
	
	}
	
	@Then("^The list of projects to assign include \"([^\"]*)\" in that order$")
	public void the_list_of_projects_to_assign_include_in_that_order(String arg1) throws Throwable {
	    
		
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			String[] expected_text={"Title","ISBN","Art Director","Cover Designer","Interior Designer"};
			List<WebElement> Actual=driver.findElements(By.xpath("//*[@id='assignTableDetails']/thead/tr/th/label"));
			for(int i=0;i<Actual.size();i++)
			{ 
				//System.out.println("Expected text is  " +expected_text[i]);
				//System.out.println("Actual text is    " +Actual.get(i).getText());
				AssertUtils.assertTextMatches(Actual.get(i),
					    Matchers.containsString(expected_text[i]));
			
		   }
		
	}
}
