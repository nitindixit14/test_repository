package com.scholastic.cucumber.apt.stepdefs;

import static com.scholastic.torque.common.StringMatchers.containsIgnoreCase;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;
import java.util.Set;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.scholastic.cucumber.apt.pageobjects.InternalRoutingForm;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransmittalHistoryUISteps {

	@When("^user open transmittal history$")
	public void user_open_transmittal_history() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		Thread.sleep(10000);
		WebElement transmittalHistoryCol1 = projectsPage.getProjectGrid().getRows().get(0)
				.getColumn("Transmittal History");
		transmittalHistoryCol1.findElement(By.tagName("img")).click();
	}

	@Then("^verify the text color of differen row$")
	public void verify_the_text_color_of_differen_row() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();

		TransmittalHistoryDialog transmittalHistoryDialog = projectsPage.getTransmittalHistoryDialog();
		ItemGrid historyResults = transmittalHistoryDialog.getHistoryGrid();
		Thread.sleep(10000);
		transmittalHistoryDialog.waitForPresent();
		transmittalHistoryDialog.switchToDialog();
		int column = historyResults.getColumnIndex("Type");
		List<Row> rows = historyResults.getRows();
		assertThat("Transmittal History Data", rows.size(), Matchers.greaterThan(0));
		for (Row row : rows) {
			String transmittalType = row.getColumnText(column);
			WebElement input = row.findElement(By.tagName("input"));
			// ensure text color of row for given type
			if (transmittalType.equalsIgnoreCase("cover")) {
				assertThat(input.getCssValue("color"), containsIgnoreCase("rgb(101, 81, 145)"));
			} else if (transmittalType.equalsIgnoreCase("interior")) {
				assertThat(input.getCssValue("color"), containsIgnoreCase("rgb(185, 127, 65)"));
			} else if (transmittalType.equalsIgnoreCase("jacket")) {
				assertThat(input.getCssValue("color"), containsIgnoreCase("rgb(49, 160, 84)"));
			}

		}

	}

	@When("^user open Interior Routing$")
	public void user_open_Interior_Routing() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		Thread.sleep(10000);
		WebElement interiorRoutingCol1 = projectsPage.getProjectGrid().getRows().get(0).getColumn("Interior Routing");
		interiorRoutingCol1.findElement(By.tagName("img")).click();
	}

	@When("^verify generated PDF document$")
	public void verifyGeneratedPDF() throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String curWindow = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		for (String window : windows) {
			System.err.println("window " + window);
			if (window.startsWith("interior") || !window.equalsIgnoreCase(curWindow))
				;
			driver.switchTo().window(window);
		}

		InternalRoutingForm irf = new InternalRoutingForm();
		WebElement ele = irf.getValueElement("Internal Routing Form");
		AssertUtils.assertTextMatches(ele, Matchers.equalToIgnoringCase("Interior"));
	}
}
