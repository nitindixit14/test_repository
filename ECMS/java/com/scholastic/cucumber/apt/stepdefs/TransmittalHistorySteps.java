package com.scholastic.cucumber.apt.stepdefs;

import java.sql.Driver;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.ProjectDetails;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.cucumber.apt.pageobjects.UserHomePage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransmittalHistorySteps {

	@When("^User views Transmittal History for  \"([^\"]*)\"$")
	public void user_views_Transmittal_History_for(String project) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.searchFor(project);
	    try{
	    ItemGrid itemgrid=myprojects.getProjectGrid();	
	    itemgrid.getRows().get(0).getColumn("Transmittal History").click();
	    }
	    catch(Exception e)
	    {
	    	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	    	final WebElement historybtn = driver.findElement(By.xpath(".//*[@id='tbmyProjects']/tbody/tr[2]/td[13]/div/img"));
	    	historybtn.click();
	    }
	    history.switchToHistoryDialog();
	}

	@Then("^Window Title is displayed as 'Transmittal History'$")
	public void window_Title_is_displayed_as_Transmittal_History() throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		Assert.assertEquals("Transmittal History", history.getDialog().getDialogTitleElement().getText().trim());
	}
	
	@Then("^Cover Image is displayed$")
	public void cover_Image_is_displayed() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
	     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	     AssertUtils.assertDisplayed(history.getCoverImage());
	}
	
	@Then("^\"([^\"]*)\" is displayed with \"([^\"]*)\"$")
	public void is_displayed_with(String productInfo, String label) throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
	     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	     Assert.assertEquals(label,history.getLabel(productInfo).getText());
	}

	@SuppressWarnings({ "static-access" })
	@Then("^\"([^\"]*)\" values are populated from database \"([^\"]*)\" for \"([^\"]*)\"$")
	public void values_are_populated_from_database_for(String infoLabel, String dblabel, String isbn) throws Throwable {
	 	DBConnection dbcon=new DBConnection();
	 	MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	 	String dbvalue="";
		ResultSet rs=dbcon.dbConnection("select * from APT_PROJECTS_VIEW WHERE ISBN_13='"+isbn+"'");
		while(rs.next())
		{
			dbvalue=rs.getString(dblabel);
		}	
		Assert.assertEquals(dbvalue,history.getValueElement(infoLabel));
	}

	@Then("^\"([^\"]*)\" values match values displayed in My Project page$")
	public void values_match_values_displayed_in_My_Project_page(String infoLabel) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	   String historyInfoValue= history.getValueElement(infoLabel);
	   myprojects.waitForResultsLoadingComplete();
	   myprojects.getcloseButton().click();
	   myprojects.waitForResultsLoadingComplete();
	   ItemGrid itemgrid=myprojects.getProjectGrid();
	   String projectsValue="";
	   Row row=itemgrid.getRows().get(0);
	  if(infoLabel.equals("ISBN 13"))
	  {
		  projectsValue=row.getColumnText("ISBN"); 
	  }
	  else if(infoLabel.equals("ISBN"))
	  {
			myprojects.waitForResultsLoadingComplete();
			ItemGrid ig=myprojects.getProjectGrid();
			ig.getRows().get(0).getColumn("Title").click();
			ProjectDetails pd=new ProjectDetails();
			pd.switchToDetailsDialog();
			projectsValue=pd.getValueElement(infoLabel).getAttribute("value");
	  }
	  else
	  {
		   projectsValue=row.getColumnText(infoLabel);
	  }
	  Assert.assertEquals(historyInfoValue, projectsValue);
	}
	
	@When("^User submits a New \"([^\"]*)\"  with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_submits_a_New_with_and(String type, String mechs, String proofs) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    history.getFormButton().click();
	    TransmittalForm tf=new TransmittalForm();
	    tf.switchToTransmittalForm();
	    tf.getTransmittalFormButtons(type).click();
	    Thread.sleep(1000);
	    tf.getTransmittalFormButtons(mechs).click();
	    Thread.sleep(2000);
	    tf.getTransmittalFormButtons(proofs).click();
	    TestBaseProvider.getTestBase().getContext().setProperty("proofs", proofs);
	    tf.getSubmitButton().click();
	    WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	   String currWindow= driver.getWindowHandle();
	   myprojects.switchToWindow();
	   driver.switchTo().window(currWindow);
	}

	@Then("^New \"([^\"]*)\" is displayes in Transmittal History list$")
	public void new_is_displayes_in_Transmittal_History_list(String type) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    myprojects.waitForResultsLoadingComplete();
		history.switchToHistoryDialog();
		Row row=history.getFirstHistoryTypeRow(type);
		Assert.assertEquals(row.getColumnText("Type"), type);
		TestBaseProvider.getTestBase().getContext().setProperty("type", type);
		TestBaseProvider.getTestBase().getContext().setProperty("row", row);
	}

	@Then("^'Last Modified' value for the New  Transmittal Form is today$")
	public void last_Modified_value_for_the_New_Transmittal_Form_is_today() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    Row row=(Row) TestBaseProvider.getTestBase().getContext().getProperty("row");
	    String historyDate=row.getColumnText("Last Modified");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		Calendar cal = Calendar.getInstance();
		   String currDate=dateFormat.format(cal.getTime()).toString();
		   Assert.assertEquals(historyDate, currDate);
	}

	@Then("^'Mechs' value for New  Transmittal Form matches \"([^\"]*)\"$")
	public void mechs_value_for_New_Transmittal_Form_matches(String mechValue) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    Row row=(Row) TestBaseProvider.getTestBase().getContext().getProperty("row");
	   Assert.assertEquals(row.getColumnText("Mechs").toUpperCase(),mechValue.toUpperCase());
	}

	@Then("^'Proofs' value for New Transmittal Form matches \"([^\"]*)\"$")
	public void proofs_value_for_New_Transmittal_Form_matches(String proofsValue) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    Row row=(Row) TestBaseProvider.getTestBase().getContext().getProperty("row");
	   Assert.assertEquals(proofsValue.replaceAll("\\s", "").toUpperCase(),row.getColumnText("Proofs").replaceAll("\\s", "").toUpperCase());
	}

	@Then("^'Transmittal Generated By' value for New  Transmittal Form is user name$")
	public void transmittal_Generated_By_value_for_New_Transmittal_Form_is_user_name() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    Row row=(Row) TestBaseProvider.getTestBase().getContext().getProperty("row");
	   String historyName= row.getColumnText("Transmittal Generated by");
	   myprojects.waitForResultsLoadingComplete();
	    WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	    driver.switchTo().defaultContent();
	   UserHomePage userHomePage = new UserHomePage();
//		WaitUtils.waitForDisplayed(userHomePage.getUserName());
		Assert.assertEquals(historyName, userHomePage.getUserNameText());
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The list of Transmittals include \"([^\"]*)\"$")
	public void the_list_of_Transmittals_include(String headersInfo) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		ItemGrid itemgrid=history.getHistoryGrid();
		 List<String> headers = new Gson().fromJson(headersInfo, List.class);
		 for (String header : headers) {
			AssertUtils.assertDisplayed(itemgrid.getHeader(header));
		}	
	}

	@SuppressWarnings("static-access")
	@Then("^The number on Transmittals Forms on the list for \"([^\"]*)\" matches the amount of Transmittals Forms in database$")
	public void the_number_on_Transmittals_Forms_on_the_list_for_matches_the_amount_of_Transmittals_Forms_in_database(String isbn13) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		ItemGrid itemgrid=history.getHistoryGrid();
		int rowsNum=itemgrid.getRows().size();
		DBConnection dbcon=new DBConnection();
		int dbcount = 0;
		ResultSet rs=dbcon.dbConnection("SELECT COUNT(*) AS TOTAL_ROWS FROM TRANSMITTAL_HISTORY WHERE ISBN_13='"+isbn13+"'");
		while (rs.next()) {
			dbcount=rs.getInt("TOTAL_ROWS");
		}
		Assert.assertEquals(dbcount, rowsNum);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The Transmittals List should display \"([^\"]*)\"$")
	public void the_Transmittals_List_should_display(String editHeaders) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		ItemGrid itemgrid=history.getHistoryGrid();
		 List<String> headers = new Gson().fromJson(editHeaders, List.class);
		 for (String header : headers) {
			AssertUtils.assertDisplayed(itemgrid.getHeader(header));
		}
	}
	
	@Then("^The Edit icon is displayed only for the most recent \"([^\"]*)\" listed$")
	public void the_Edit_icon_is_displayed_only_for_the_most_recent_listed(String type) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    myprojects.waitForResultsLoadingComplete();
		history.switchToHistoryDialog();
		Row row1=history.getFirstHistoryTypeRow(type);
		ItemGrid itemgrid=history.getHistoryGrid();
		for(Row nextRow:history.getHistoryTypeRows(type))
		{
			if(nextRow==row1)
			{
				AssertUtils.assertDisplayed(row1.getHistoryTableButtons("Edit"));

			}
			AssertUtils.assertNotDisplayed(nextRow.getHistoryTableButtons("Edit"));
		}
	}
	
	@Then("^The Delete icon is displayed only for the most recent \"([^\"]*)\" listed$")
	public void the_Delete_icon_is_displayed_only_for_the_most_recent_listed(String type) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    myprojects.waitForResultsLoadingComplete();
		history.switchToHistoryDialog();
		Row row1=history.getFirstHistoryTypeRow(type);
		ItemGrid itemgrid=history.getHistoryGrid();
		for(Row nextRow:history.getHistoryTypeRows(type))
		{
			if(nextRow==row1)
			{
				AssertUtils.assertDisplayed(row1.getHistoryTableButtons("Delete"));

			}
			AssertUtils.assertNotDisplayed(nextRow.getHistoryTableButtons("Delete"));
		}
	}
	

@When("^User selects the \"([^\"]*)\" icon for most recent \"([^\"]*)\"$")
public void user_selects_the_icon_for_most_recent(String delete, String type) throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	Row row=history.getFirstHistoryTypeRow(type);
	row.getColumn(delete).click();
	TestBaseProvider.getTestBase().getContext().setProperty("transmittal.type", type);
}

@When("^User dismiss the confirmation message$")
public void user_dismiss_the_confirmation_message() throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	history.getpopAcceptButton().click();
}

@Then("^Transmittal Form with \"([^\"]*)\" and \"([^\"]*)\" is NOT displayed in the Transmittals List$")
public void transmittal_Form_with_and_is_NOT_displayed_in_the_Transmittals_List(String mechs, String proofs) throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    String type=(String) TestBaseProvider.getTestBase().getContext().getProperty("transmittal.type");
    Row row=history.getFirstHistoryTypeRow(type);
    Assert.assertNotEquals(row.getColumnText("Mechs"),mechs);
    Assert.assertNotEquals(row.getColumnText("Proofs"),proofs);
}

@SuppressWarnings("static-access")
@Then("^Transmittal Form with \"([^\"]*)\" and \"([^\"]*)\" is removed from database for \"([^\"]*)\"$")
public void transmittal_Form_with_and_is_removed_from_database_for(String mechs, String proofs, String isbn13) throws Throwable {
   	 String type=(String) TestBaseProvider.getTestBase().getContext().getProperty("transmittal.type");
	 DBConnection dbcon=new DBConnection();
	 ResultSet rs=dbcon.dbConnection("select * from TRANSMITTAL_HISTORY where isbn_13='"+isbn13+"' and TYPE like '%"+type+"%' ORDER BY DATE_CREATED DESC");
	 String dbMechs = null;
	 String dbProofs = null;
	 while(rs.next())
	 {
		dbMechs =rs.getString("Mechs");
		dbProofs=rs.getString("Proofs");
	 }
	 Assert.assertNotEquals(dbMechs,mechs);
	 Assert.assertNotEquals(dbProofs,proofs);
}

@When("^User 'Edit' the most recent \"([^\"]*)\" with \"([^\"]*)\" and \"([^\"]*)\"$")
public void user_Edit_the_most_recent_with_and(String type, String otherMech, String otherProof) throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	Row row=history.getFirstHistoryTypeRow(type);
	row.getColumn("Edit").click();
	TransmittalForm tf=new TransmittalForm();
	tf.switchToTransmittalForm();
    tf.getFormMechOther().click();
    tf.getFormMechOtherInput().sendKeys(otherMech);
    String proofs=(String) TestBaseProvider.getTestBase().getContext().getProperty("proofs");
    tf.getTransmittalFormButtons(proofs).click();
    tf.getProofTypeOtherCheckbox().click();
    tf.getProofTypeOtherInput().sendKeys(otherProof);
    tf.getFormUpdateBtn().click();
    WebDriver driver=TestBaseProvider.getTestBase().getDriver();
   String currWindow= driver.getWindowHandle();
   myprojects.switchToWindow();
   driver.switchTo().window(currWindow);
}

@Then("^The Edit icon is displayed only for this Transmittal Form$")
public void the_Edit_icon_is_displayed_only_for_this_Transmittal_Form() throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
	String type=(String) TestBaseProvider.getTestBase().getContext().getProperty("type");
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	Row row1=history.getFirstHistoryTypeRow(type);
	ItemGrid itemgrid=history.getHistoryGrid();
	for(Row nextRow:history.getHistoryTypeRows(type))
	{
		if(nextRow==row1)
		{
			AssertUtils.assertDisplayed(row1.getHistoryTableButtons("Edit"));

		}
		AssertUtils.assertNotDisplayed(nextRow.getHistoryTableButtons("Edit"));
	}
}

@Then("^The list of Transmittals is sorted in ascending order by 'Type'$")
public void the_list_of_Transmittals_is_sorted_in_ascending_order_by_Type() throws Throwable {
	 MyProjectsPage myprojects=new MyProjectsPage();
     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
     myprojects.waitForResultsLoadingComplete();
     history.switchToHistoryDialog();
     ItemGrid itemgrid=history.getHistoryGrid();
     ArrayList<String> colText=new ArrayList<>();
     for(Row row:itemgrid.getRows())
     {
    	 colText.add(row.getColumnText("Type"));
     }
    Assert.assertTrue(itemgrid.isSortedAscend(colText, "Type"));
}

@Then("^Each type \"([^\"]*)\" is sorted in descending order by 'Last Modified' date$")
public void each_type_is_sorted_in_descending_order_by_Last_Modified_date(String types) throws Throwable {
     MyProjectsPage myprojects=new MyProjectsPage();
     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
     ItemGrid itemgrid=history.getHistoryGrid();
	 List<String> transmittalTypes = new Gson().fromJson(types, List.class);
     for(String type:transmittalTypes)
     {
     ArrayList<Row> rows=history.getHistoryTypeRows(type);
     ArrayList<String> colText=new ArrayList<>();
     for(Row row:rows)
     {
    	 colText.add(row.getColumnText("Last Modified"));
     }
    Assert.assertTrue(history.sortLatestDate(colText));
//    Assert.assertTrue(itemgrid.isSortedDesc(colText, "Date"));
     }
}

@Then("^\"([^\"]*)\" on the list displays 'Proofs' separated by comma$")
public void on_the_list_displays_Proofs_separated_by_comma(String type) throws Throwable {

	  MyProjectsPage myprojects=new MyProjectsPage();
	     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	     ItemGrid itemgrid=history.getHistoryGrid();
	     ArrayList<Row> rows=history.getHistoryTypeRows(type);
	     ArrayList<String> colText=new ArrayList<>();
	     colText.add(rows.get(0).getColumnText("Proofs"));
	     String Str =colText.get(0);
	     
	  
	     
	     /* for(Row row:rows)
	     {
	      colText.add(row.getColumnText("Proofs"));
	     }
	     for (String colValue : colText) {
	         System.out.println(colValue);
    	 }
       
     
       */
       
       
}




@When("^User edits most recent \"([^\"]*)\" with multiple proofs$")
public void user_edits_most_recent_Cover_Transmittal_Form_with_multiple_proofs(String type) throws Throwable {
	
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	Row row=history.getFirstHistoryTypeRow(type);
	row.getColumn("Edit").click();
	TransmittalForm tf=new TransmittalForm();
	tf.switchToTransmittalForm();
	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	driver.findElement(By.xpath("//*[@id='chRandom']")).click();
	driver.findElement(By.xpath("//*[@id='chAdditionalProof']")).click();
	tf.getFormUpdateBtn().click();
   String currWindow= driver.getWindowHandle();
   myprojects.switchToWindow();
   driver.switchTo().window(currWindow);
  
     Thread.sleep(8000);
     Row row1=history.getFirstHistoryTypeRow(type);
     String s= row1.getColumn("Proofs").getText();
	//WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	//String s=driver.findElement(By.xpath("//*[@id='table_comments']/tbody/tr[1]/td[4]")).getText();

	
   }
/*
@Then("^\"([^\"]*)\" on the list displays 'Proofs' separated by comma$")
public void cover_Transmittal_Form_on_the_list_displays_Proofs_separated_by_comma(String Proofs,String type) throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
	TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	 Row row1=history.getFirstHistoryTypeRow(type);
     String s= row1.getColumn("Proofs").getText();
	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	

	System.out.println("the text is"  +s);
	
}*/
@When("^User submits  New \"([^\"]*)\"  with \"([^\"]*)\" and \"([^\"]*)\"$")
public void user_submits_New_with_and(String type, String mechs, String proofs) throws Throwable {

   MyProjectsPage myprojects=new MyProjectsPage();
   TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
   history.getFormButton().click();
   TransmittalForm tf=new TransmittalForm();
   tf.switchToTransmittalForm();
   tf.getTransmittalFormButtons(type).click();
   Thread.sleep(1000);
   tf.getTransmittalFormButtons(mechs).click();
   Thread.sleep(2000);
   tf.getTransmittalFormButtons(proofs).click();
   TestBaseProvider.getTestBase().getContext().setProperty("proofs", proofs);
   WebDriver driver=TestBaseProvider.getTestBase().getDriver();
  // String currWindow= driver.getWindowHandle();
   tf.getSubmitButton().click();
   Thread.sleep(8000);
   if(driver.getCurrentUrl().contains("pdf"))
	{
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	}
	else
	{
		System.out.println("#########################");
		/*
		myprojects.getTransmittalHistoryDialog();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='table_comments']/tbody/tr[2]/td[7]/div/div")).click();*/
		
		/*Row row=history.getFirstHistoryTypeRow(type);
		row.getColumn("Edit").click();*/
		
	}
  // WebDriver driver=TestBaseProvider.getTestBase().getDriver();
//  String currWindow= driver.getWindowHandle();
//  myprojects.switchToWindow();
  //driver.switchTo().window(currWindow);
 
 // System.out.println("@@@@@@@@@@@@" +s);
   
   
   
	
}

@When("^User edits most recent 'Interior Transmittal Form' with multiple proofs$")
public void user_edits_most_recent_Interior_Transmittal_Form_with_multiple_proofs() throws Throwable {
   
}

@When("^User edits most recent 'Jacket Transmittal Form' with multiple proofs$")
public void user_edits_most_recent_Jacket_Transmittal_Form_with_multiple_proofs() throws Throwable {
    
}

@Then("^'Jacket Transmittal Form' on the list displays 'Proofs' separated by comma$")
public void jacket_Transmittal_Form_on_the_list_displays_Proofs_separated_by_comma() throws Throwable {
   
}



}
