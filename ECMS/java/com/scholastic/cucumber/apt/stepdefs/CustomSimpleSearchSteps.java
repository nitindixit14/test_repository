package com.scholastic.cucumber.apt.stepdefs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.HeaderSection;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class CustomSimpleSearchSteps {

	@When("^the user view the \"([^\"]*)\"$")
	public void the_user_view_the(String page) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    if(page.equals("My Projects"))
	    {
	    	WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			driver.switchTo().defaultContent();
	    	HeaderSection headerSection = new HeaderSection();
	    	WaitUtils.waitForEnabled(headerSection.getMyProjectsBreadCrumb());
	    	myprojects.clickElementByJS(headerSection.getMyProjectsBreadCrumb());
			myprojects.waitForResultsLoadingComplete();
	    }
	    if(page.equals("Search Results"))
	    {
	    	String ISBN = TestBaseProvider.getTestBase().getContext().getString("search.isbnsearch.isbn");
	    	myprojects.searchFor(ISBN);
	    }
	}

	@SuppressWarnings("unchecked")
	@Then("^user should be presented with \"([^\"]*)\" search options$")
	public void user_should_be_presented_with_search_options(String searchCheckboxes) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    List<String> items = new Gson().fromJson(searchCheckboxes, List.class);
		for (String searchType : items) {
			AssertUtils.assertDisplayed(myprojects.getSearchTypeCheckbox(searchType));
		}
	}

	@When("^the user selects \"([^\"]*)\" search option$")
	public void the_user_selects_search_option(String searchType) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    WaitUtils.waitForDisplayed(myprojects.getSearchTypeCheckbox(searchType));
	   myprojects.clickElementByJS(myprojects.getSearchTypeCheckbox(searchType));
	}

	@When("^conducts a search for \"([^\"]*)\"$")
	public void conducts_a_search_for(String searchTerm) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.searchFor(searchTerm);
	}

	@Then("^each of the results should contain the \"([^\"]*)\" in the \"([^\"]*)\" column$")
	public void each_of_the_results_should_contain_the_in_the_column(String searchTerm, String searchOption) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	   ArrayList<String> columnText= itemgrid.allColumnsText(searchOption);
	   for(String value:columnText)
	   {
		   Assert.assertTrue(value.toUpperCase().contains(searchTerm.toUpperCase()));
	   }
	}

	@Then("^the count of results should match the results returned from the \"([^\"]*)\" query$")
	public void the_count_of_results_should_match_the_results_returned_from_the_query(String sqlQuery) throws Throwable {
		int expected_count = 0;
		ResultSet rs3 = DBConnection.dbConnection(sqlQuery);
		while (rs3.next()) {
			expected_count = rs3.getInt("TOTAL_COUNT");
		}
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int actual_count = Integer.parseInt(myprojects.getLastProjectNum());
		Assert.assertTrue(actual_count==expected_count);
	}
	
	@Then("^the search options should reset$")
	public void the_search_options_should_reset() throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    Assert.assertFalse(myprojects.getSearchTypeCheckbox("Title").isSelected());
	    Assert.assertFalse(myprojects.getSearchTypeCheckbox("ISBN").isSelected());
	}
	
	@Then("^the selected search option \"([^\"]*)\" remains checked$")
	public void the_selected_search_option_remains_checked(String option) throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
		 myprojects.waitForResultsLoadingComplete();
		 Assert.assertTrue(myprojects.getSearchTypeCheckbox(option).isSelected());
	}
	
	@Then("^each of the results should contain the \"([^\"]*)\" in one of the \"([^\"]*)\" column$")
	public void each_of_the_results_should_contain_the_in_one_of_the_column(String searchTerm, String searchOption) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	   List<String> items = new Gson().fromJson(searchOption, List.class);
	   boolean navNextPage = false;
		do {
			if (navNextPage) {
				myprojects.clickNextButon();
				myprojects.waitForResultsLoadingComplete();
			}
			  ItemGrid itemgrid=myprojects.getProjectGrid();
		for(Row row:itemgrid.getRows())
		{
			Assert.assertTrue(row.getColumnText(items.get(0)).toUpperCase().contains(searchTerm.toUpperCase()) || row.getColumnText(items.get(1)).toUpperCase().contains(searchTerm.toUpperCase()));
		}
		navNextPage = true;
		} while (myprojects.getNextButton().isEnabled());

	}
	
	@Then("^the user should be presented with a error message$")
	public void the_user_should_be_presented_with_a_error_message() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		List<WebElement> errors = projectsPage.getErrorMessages();
		assertThat(errors.size(), greaterThan(0));
	}

	@Then("^the error message should read \"([^\"]*)\"$")
	public void the_error_message_should_read(String expectederror) throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String errorMessage=driver.findElement(By.id("fb_message_label")).getText();
		Assert.assertEquals(expectederror, errorMessage);
	}
	
}