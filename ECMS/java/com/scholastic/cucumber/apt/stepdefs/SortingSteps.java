package com.scholastic.cucumber.apt.stepdefs;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SortingSteps {

	@Given("^My Project List is sorted descending order by \"([^\"]*)\"$")
	public void my_Project_List_is_sorted_descending_order_by(String option) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		itemgrid.showAllColumns();
		boolean sortedList = true;
		ArrayList<String> col_text = itemgrid.allColumnsText(option);
		int i=1;
		while (sortedList) {
			if (itemgrid.isSortedDesc(col_text, option)) {
				sortedList = false;
				i++;
			} else {
				myProjects.clickElementByJS(itemgrid.getHeader(option));
				WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
				col_text = itemgrid.allColumnsText(option);
				i++;
			}
			if(i==3)
			{
				sortedList = false;
			}
		}
	}

	@Then("^projected listed are sorted in ascending order by \"([^\"]*)\"$")
	public void projected_listed_are_sorted_in_ascending_order_by(String option) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		// Thread.sleep(1000);
		ArrayList<String> col_text = itemgrid.allColumnsText(option);
		Assert.assertTrue(itemgrid.isSortedAscend(col_text, option));
	}

	@Then("^first project on the page continues previous page sorting with \"([^\"]*)\"$")
	public void first_project_on_the_page_continues_previous_page_sorting_with(String option) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		myProjects.clickElementByJS(myProjects.getFirstButton());
		ItemGrid itemgrid = myProjects.getProjectGrid();
		// Thread.sleep(1000);
		ArrayList<String> col_text = itemgrid.allColumnsText(option);
		WaitUtils.waitForEnabled(myProjects.getNextButton());
		myProjects.clickElementByJS(myProjects.getNextButton());
		WaitUtils.waitForEnabled(myProjects.getDisplaypageOption());
		myProjects.clickElementByJS(myProjects.getDisplaypageOption());
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		int projectsPerPage = Integer.parseInt(myProjects.getSelectedPagingOption().getText());
		myProjects.waitForResultsLoadingComplete();
		int currentPage = Integer.parseInt(myProjects.getCurrentPageNum());
		int number = (currentPage * projectsPerPage) - projectsPerPage + 1;
		Assert.assertTrue((itemgrid.getRows().get(0).getColumnText(option).trim().contains(col_text.get(number - 1).trim())));
	}

	@Given("^My Project List is sorted ascending order by \"([^\"]*)\"$")
	public void my_Project_List_is_sorted_ascending_order_by(String option) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		itemgrid.showAllColumns();
		boolean sortedList = true;
		ArrayList<String> col_text = itemgrid.allColumnsText(option);
		int i=1;
		while (sortedList) {
			if (itemgrid.isSortedAscend(col_text, option)) {
				while (itemgrid.allItemsSame(col_text, option)) {
					myProjects.clickElementByJS(itemgrid.getHeader(option));
					break;
				}
				sortedList = false;
				i++;
			} else {
				myProjects.clickElementByJS(itemgrid.getHeader(option));
				myProjects.waitForResultsLoadingComplete();
				WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
				col_text = itemgrid.allColumnsText(option);
				i++;
			}
			if(i==2)
			{
				sortedList = false;
			}
		}
		

	}

	@Then("^projected listed are sorted in descending order by \"([^\"]*)\"$")
	public void projected_listed_are_sorted_in_descending_order_by(String option) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		// Thread.sleep(1000);
		ArrayList<String> col_text = itemgrid.allColumnsText(option);
		Assert.assertTrue(itemgrid.isSortedDesc(col_text, option));
	}

	@Given("^user searches for small search results$")
	public void user_searches_for_small_search_results() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		// Thread.sleep(1000);
		String searchkey = TestBaseProvider.getTestBase().getString("searchtestdata.search.BETWEEN_106_120");
		myprojects.searchFor(searchkey);
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		;
		// Thread.sleep(1000);
	}

	@When("^user selects \"([^\"]*)\" on the Projects list$")
	public void user_selects_on_the_Projects_list(String option) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		myprojects.clickElementByJS(itemgrid.getHeader(option));
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
	}

	@Given("^user searches for big result set with searchkey$")
	public void user_searches_for_big_result_set_with_searchkey() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		String searhKey = TestBaseProvider.getTestBase().getString("searchforbigresult.searchkey");
		myprojects.searchFor(searhKey);
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
	}

}
