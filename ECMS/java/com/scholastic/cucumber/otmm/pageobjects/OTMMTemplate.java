/**
 * 
 */
package com.scholastic.cucumber.otmm.pageobjects;

import com.scholastic.torque.common.BaseTestPage;

/**
 * @author chiragjayswal
 *
 */
public class OTMMTemplate extends BaseTestPage<OTMMSignInPage> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.scholastic.torque.common.BaseTestPage#openPage()
	 */
	@Override
	protected void openPage() {

	}

	@Override
	public boolean isPageActive() {
		return getHeader().isPresent();
	}
	
	public OTMMHeader getHeader() {
		return new OTMMHeader();
	}
}
