Feature: OTBPM - Transmittal History
  @TestCase5 @Offshore1
  Scenario Outline: 5- User unassigns project to himself so is not displayed in My Projects List
    Given user is logged in OTBPM as "APT_Art_Director"
    And user selects a "<PROJECT>" from My Projects and go to Assign page
    When User unassigns himself as 'Art Director' and saves unassign selection
    Then user should seee "<PROJECT>" is not listed in My Projects page
    And User can Search for "<PROJECT>"
    Then "<PROJECT>" Art Director value is empty
    And user selects a "<PROJECT>" from My Projects and go to Assign page
    When User selects a "<VALUE>" for "<FIELD>" from the Autosuggested list

    Examples: 
      | PROJECT           | VALUE         | FIELD        |
      | 978-0-545-75190-2 | John Williams | Art Director |
      
   @ORPHAN @ECIMT720 @P3 @nitin  @Offshore1
  Scenario Outline: 10- User Views a Cover Transmittal Form from the Transmittal History

    Given user is logged in OTBPM
   	When User views Transmittal History for "<PROJECT>"
    When User selects the 'View' icon for a Cover Transmittal Form
    Then Transmittal Form is displayed as a pdf
   
    Examples:
      | PROJECT           |Title Prefix |
      | 978-1-338-04475-1 |Production/Manufacturing Transmittal Form for Cover|
      
      
       @ORPHAN @ECIMT720 @P3 @nitin  @Test123 @Offshore1
  Scenario Outline: 12- User Views a Interior Transmittal Form from the Transmittal History

    Given user is logged in OTBPM
    And User views Transmittal History for "<PROJECT>"
    When User selects the 'View' icon for a Interior Transmittal Form
    Then Transmittal Form is displayed as a pdf

    Examples:
      | PROJECT           |Title Prefix |
      | 978-1-338-04475-1 |Production/Manufacturing Transmittal Form for Interior |
      
#  Nilesh Scenarios:
 @ORPHAN @EAPT1313 @P2   @nitin @Offshore1
  Scenario Outline: 8- Advanced Search Multi fields
    Given user is logged in OTBPM as "ANOTHER_ART_DIRECTOR"
    And the user is on the Advanced Search page
    When the user conducts advanced search of multiple fields as "<type1>" "<value1>","<type2>" "<value2>","<type3>" "<value3>" 
    Then the returned results should contain at least one record with "<value1>" in the "<type1>" column,"<value2>" in the "<type2>" column,"<value3>" in the "<type3>" column 
    
    Examples:
      | type1  | value1       | type2   | value2     | type3  | value3      |
      | ISBN  | 978-0-545-36253-5   | Author  | Jenne Simon| Title  | ALEX TOYS: HELPING HANDS    |
      | ISBN  | 978-0-545-36252-8   | Author  | Jenne Simon| Title  | ALEX TOYS: MOODY MONSTERS    |
      | ISBN  | 978-0-545-70844-9   | Author  | Daisy Meadows| Title  | BABY ANIMAL RESCUE FAIRIES, THE #1: MAE THE PANDA FAIRY    |
      
 
  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2  @nitin @Offshore1
  Scenario Outline: 10- Cover transmittal Form displays all user provided information for Online Transmittal Forms

    Given user is logged in OTBPM
   When user searches for "<project>"
   When User opens Project Details for "<project>"
    And User opens a New Transmittal Form for a project
    And User enters data into the "<List Colors>", "<List Matched Ink>", "<Files Posted to>" and "<Special Instruction fields>"
    When User submits New "<TYPE_OF_TRANSMITTAL_FORM>" with "<MECHS>" and "<PROOFS>"
   
    Examples:
      | type     | List Colors | List Matched Ink|Files Posted to|Special Instruction fields |project |TYPE_OF_TRANSMITTAL_FORM |MECHS |PROOFS |
      | Cover    | Colors      | Ink			 |User           | Instruction        |978-1-338-04475-1 |Cover | Art released to Mfg |Case Sample |
      
      
 @ORPHAN @ECIMT597 @P3 @NitinJ @Offshore @Offshore1
  Scenario Outline: 13- Navigates to Assign view from Project Details

    Given user is logged in OTBPM
    When user searches for "<PROJECT>" 
    Then User opens Project Details for "<PROJECT>"
    And user clicks on 'Assign'
    Then 'Assign' view is displayed in a new window
    And User selects a "<VALUE>" for "<FIELD>" from the Autosuggested list on Assing Dialog
    Then the "<VALUE>" match information in Project Details view
      
    Examples:
      | PROJECT           |FIELD             |VALUE                |
      | 978-0-545-90883-2 | Art Director     |Cordys Internal|
 
      