@FileName
Feature: filename
       
@P1 @FileNameFormat
Scenario Outline: File name format in myprojects page and projects details page
Given user is logged in OTBPM
When user is in 'My Projects' view
And user searches for "<searchKey>"
Then the file Name on the myprojects and projects details page are the same
And the file name should have the prefix of the Record Number followed by an underscore
And the file name should have the first 12 alphanumeric values of the title followed by the text "_4cc.eps"
Examples:
|searchKey|
|978-0-439-66542-1|
|978-0-545-43749-3|
 
@P2
 Scenario Outline: File name rules for with no record number
    #Given the "<searchKey>" does not have a record number
    And user is logged in OTBPM
    When user is in 'My Projects' view
    And user searches for "<searchKey>"
    Then the file Name on the myprojects and projects details page are the same
    And the file name should have the prefix of the unformatted ISBN_13 followed by an underscore
    And the file name should have the first 12 alphanumeric values of the title followed by the text "_4cc.eps"

    Examples: 
      | searchKey         |
      | 978-1-338-09913-3 |

#*need to find titles without record number
 
@P2
Scenario Outline:  File name rules where title is less than 13 alphanumeric values the title of "<searchKey>" has
Given the title of "<searchKey>" has 13 alphanumeric values or less  
And user is logged in OTBPM
When user is in 'My Projects' view
And user searches for "<searchKey>"
Then the file Name on the myprojects and projects details page are the same
And the file name should have the prefix of the unformatted ISBN_13 followed by an underscore
And the file name should have the alphanumeric values of the title followed by the text "_4cc.eps"
Examples:
|searchKey|
|                            |
|*APPLE                            |
#*need to find titles where the title has less than 12 alphanumeric character
 