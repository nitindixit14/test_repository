Feature: OTBPM - Transmittal History
@ORPHAN @ECIMT597 @P3 @NitinJ @Offshore
  Scenario Outline: 13- Navigates to Assign view from Project Details
    Given user is logged in OTBPM
    When user searches for "<PROJECT>" 
    Then User opens Project Details for "<PROJECT>"
    And user clicks on 'Assign'
    Then 'Assign' view is displayed in a new window
    And User selects a "<VALUE>" for "<FIELD>" from the Autosuggested list on Assing Dialog
    Then the "<VALUE>" match information in Project Details view
      
    Examples:
      | PROJECT           |FIELD             |VALUE                |
      | 978-0-545-90883-2 | Art Director     |Cordys Internal|


 @ORPHAN @ECIMT598 @P3 @Aparna @Offshore
 Scenario Outline: 23- Specific comments can be located in the EPIC Comments section

     Given user is logged in OTBPM
   #  When user searches for "<PROJECT>" 
    #Then User opens Project Details for "<PROJECT>"
   When User opens Project Details for '<project>'
    And user expands 'EPIC comments'
   Then "<specific_comments>" are displayed when available
    And label for "<specific_comments>" is displayed as "<label>" when comments are available
     Examples:
       | project           | specific_comments | label             |
#       | 978-0-545-03517-0 | Cover             | Cover Comments    |
      # | 978-0-545-03517-0 | Jacket            | Jacket Comments   |
      # | 978-0-545-03517-0 | Insert            | Insert Comments   |
      # | 978-0-545-44868-0 | Cover             | Cover Comments    |
       | 978-0-545-44868-0 | Interior          | Interior Comments |
#       | 978-0-545-44868-0 | Jacket            | Jacket Comments   |

  @ORPHAN @ECIMT720 @P3 @nitin @Offshore 
  Scenario Outline: 10- User Views a Cover Transmittal Form from the Transmittal History

    Given user is logged in OTBPM
   	When User views Transmittal History for "<PROJECT>"
    When User selects the 'View' icon for a Cover Transmittal Form
    Then Transmittal Form is displayed as a pdf
   
    Examples:
      | PROJECT           |Title Prefix |
      | 978-1-338-04475-1 |Production/Manufacturing Transmittal Form for Cover|
      
      
@ORPHAN @ECIMT858 @AdvancedSearchByDateAllFields @RONAK @Chrome @Offshore
  Scenario Outline: 16- User can enter a date manually or using the calendar widget in the In Date: From: To: field

    Given user is logged in OTBPM as "APT_Art_Director"
    And user is in 'My Projects' view
    And the user is on the Advanced Search page
    When User selects a "<dateRangeFrom>" and "<dateRangeTo>" for "<label>"
    Then 'Advanced Search' view is dismissed
    Then 'Search Results' in "<tableLables>" view displays matching results for any of the following database fields "<databaseFields>" 
Examples:
|dateRangeFrom|dateRangeTo|databaseFields|		label|			tableLables|
|02/10/16|02/21/16|	['FIRST_USE_DATE']	|First Use Date:|['First Use Date']|
|01/01/15|01/01/16|['COVER_TARGET_DUE_MFG','JACKET_TARGET_DUE_MFG','INTERIOR_TARGET_DUE_MFG']|Target Due to Mfg|['Cover Target Due to Mfg.','Jacket Target Due to Mfg.','Interior Target Due to Mfg.']|
#|02/10/16|02/21/16|['PUBLICATION_DATE']|	Pub Date|['Pub Date']|
#|02/10/16|02/21/16|['TRADE_IN_STORE_DATE']|		In Store|['In Store']|

   	 @ORPHAN @ECIMT629 @P2 @multipleOptionsInPdf @P3InExcel @RONAK @Offshore
  Scenario Outline: 16- Proofs Types selection of multiple values distinct than other persist on the Transmittal form pdf
    Given user is logged in OTBPM
 		And the user selects the Transmittal Form of a project
    When the user selects "<pdfType>"
    And selects the required fields "<fields>"
    And user selects 'Proofs Types' first "<checkbox>" options
    And user submits the form
 	  And generates the transmittal form
    Then The selected 'Proofs Types' "<checkbox>","","" options are is displayed in the Transmittal Form
   
    Examples:
	   |pdfType|fields|checkbox|
		 |Cover|['CDs','Mechanicals released to Mfg']|['Edox','Epson']|
#		 |Cover|['CDs','Mechanicals released to Mfg']|['Edox','Epson','Soft Proof (pdf)']|
	#	 |Cover|['CDs','Mechanicals released to Mfg']|['Edox','Epson','Soft Proof (pdf)','Wet Proof','Random','Printer Proofs']|
