@OTBPM-Routing @OTBPM-Routing(ECIMT-1063-1022)
Feature: OTBPM - Routing (ECIMT-1063)

  @ORPHAN @ECIMT1063 @P3 @RONAK
  Scenario Outline: 1- Verify the Transmittal Covers form PDF filename is in expected format
    Given user is logged in OTBPM
    When User views Transmittal History for "<PROJECT>"
    And User selects the "View" icon for most recent "<TYPE_OF_TRANSMITTAL_FORM>"
    Then Verify PDF file naming convention for this Transmittal Form should be like "<PDF_NAME>"

    Examples: 
      | PROJECT     | TYPE_OF_TRANSMITTAL_FORM | PDF_NAME                     |
      | -812-52060- | Cover                    | transmittalcover_81252060    |
      | -812-52060- | Interior                 | transmittalinterior_81252060 |
      | -812-52060- | Jacket                   | transmittaljacket_81252060   |

  @ORPHAN @ECIMT1063 @P3 @RONAK
  Scenario Outline: 1- Verify the Transmittal Covers form PDF filename is in expected format
    Given user is logged in OTBPM
    # When user is in 'My Projects' view
    #And user searches for "<PROJECT>"
    When user selects "<routingType>" Routing for project "<PROJECT>"
    Then Verify PDF file naming convention for this Transmittal Form should be like "<PDF_NAME>"

    Examples: 
      | PROJECT     | routingType      | PDF_NAME                 |
      | -812-52060- | Cover Routing    | routingcover_81252060    |
      | -812-52060- | Interior Routing | routinginterior_81252060 |

  @ORPHAN @ECIMT1022 @P3 @RONAK @Chrome
  Scenario: 7- verify the cancel button is displayed and functional on the transmittal form
    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    Then cancel button is displayed on the online transmittal form
    And selecting the cancel button will dismiss the online transmittal form
    