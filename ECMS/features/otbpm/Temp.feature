 @OTBPM-TransmittalHistory
Feature: OTBPM - Transmittal History

	@ORPHAN @ECIMT628 @P3
  Scenario Outline: 1- The list of stored Transmittals is color coded by Type of Transmittal Form

    Given user is logged in OTBPM
    When User views Transmittal History for "<PROJECT>"
    Then All the 'Interior' Transmittals listed have text in 'Yellowish Brown' color
    And All the 'Cover' Transmittals listed have text in 'Purple' color
    And All the 'Jacket' Transmittals listed have text in 'Green' color
    Examples:
      | PROJECT           |
      | 978-1-338-04475-1 |
      | 978-0-545-86184-7 |
      

       @ORPHAN @ECIMT628 @P2
  Scenario Outline: 2- The list of stored Transmittals displays information box when hoovering over the icons

    Given user is logged in OTBPM
    When User views Transmittal History for "<PROJECT>"
   Then User hoovers over an "<ACTION_ICON>" and verifies "<INFO_TEXT>"
   
      Examples:
      | PROJECT           | ACTION_ICON | INFO_TEXT          |
      | 978-1-338-04475-1 | img_View        | View Transmittal   |
      | 978-1-338-04475-1 | img_Edit        | Edit Transmittal   |
      | 978-1-338-04475-1 | img_Del     	 | Delete Transmittal |
      | 978-0-545-86184-7 | View        	 | View Transmittal   |
     | 978-0-545-86184-7 | Edit        	 | Edit Transmittal   |
      | 978-0-545-86184-7 | Delete          | Delete Transmittal |

  @ORPHAN @ECIMT720 @P3 @nitin
  Scenario Outline: 10- User Views a Cover Transmittal Form from the Transmittal History

    Given user is logged in OTBPM
   	When User views Transmittal History for "<PROJECT>"
    When User selects the 'View' icon for a Cover Transmittal Form
    Then Transmittal Form is displayed as a pdf
    And 'Date' in Transmittal Form matches 'Last Modified' value in Transmittal History
    And 'Title Prefix' in Transmittal Form is 'Production/Manufacturing Transmittal Form for Cover'
    And 'Title Sufix' in Transmittal Form matches 'Mechs' value in Transmittal History
    Examples:
      | PROJECT           |
      | 978-1-338-04475-1 |
      | 978-0-545-90368-4 |

  @ORPHAN @ECIMT720 @P3
  Scenario Outline: 11- User Views a Jacket Transmittal Form from the Transmittal History

    Given user is logged in OTBPM
    And User views Transmittal History for "<PROJECT>"
    When User selects the 'View' icon for a Jacket Transmittal Form
    Then Transmittal Form is displayed as a pdf
    And 'Date' in Transmittal Form matches 'Last Modified' value in Transmittal History
    And 'Title Prefix' in Transmittal Form is 'Production/Manufacturing Transmittal Form for Jacket'
    And 'Title Sufix' in Transmittal Form matches 'Mechs' value in Transmittal History
    Examples:
      | PROJECT |
      |978-1-338-04475-1         |
      |         |



  @ORPHAN @ECIMT720 @P3
  Scenario Outline: 12- User Views a Interior Transmittal Form from the Transmittal History

    Given User is logged in OTBPM
    And User is in 'Transmittal History' view for a '<PROJECT>'
    When User selects the 'View' icon for a Interior Transmittal Form
    Then Transmittal Form is displayed as a pdf
    And 'Date' in Transmittal Form matches 'Last Modified' value in Transmittal History
    And 'Title Prefix' in Transmittal Form is 'Production/Manufacturing Transmittal Form for Interior'
    And 'Title Sufix' in Transmittal Form matches 'Mechs' value in Transmittal History
    Examples:
      | PROJECT           |
      | 978-1-338-04475-1 |
      | 978-0-545-90368-4 |

   	 @ORPHAN @ECIMT629 @P2 @multipleOptionsInPdf @P3InExcel
  Scenario Outline: 16- Proofs Types selection of multiple values distinct than other persist on the Transmittal form pdf

    Given user is logged in OTBPM
 	And the user selects the Transmittal Form of a project
    When the user selects "<pdfType>"
   	And user selects 'Proofs Types' first "<multiple_options>" options
#    And user selects 'Proofs Types' first '<multiple_options>' options
    When user Submits the Transmittal Form
    Then The selected 'Proofs Types' options are is displayed in the Transmittal Form
    Examples:
      |pdfType| multiple_options |
     |Cover|['Mech','Proofs','Herewith']   |['Art released to Mfg','Edox','Epson','CDs','Transparencies']|
	 |Cover|['Mech','Proofs','Herewith']   |['Art released to Mfg','Edox','Epson','Soft Proof (pdf)','Slides','CDs','Transparencies']|
	 |Cover|['Mech','Proofs','Herewith']   |['Art released to Mfg','Edox','Epson','Soft Proof (pdf)','Wet Proof','4/c Flat Art','Slides','CDs','Transparencies']|


	@ORPHAN @ECIMT629 @P2
  Scenario Outline: 19- User can select an additional Proofs Types from the available list

    Given user is logged in OTBPM
    And 'Transmittal Form' page is displayed for '<project>'
    When user selects option "Additional Proof Types"
    Then user select an "<option>" from the list
 	 Examples: 
  	|option|
  	|Artwork| 
 
   
  @ORPHAN @ECIMT629 @P3
  Scenario Outline: 29- Optional fields input test

    Given user is logged in OTBPM
    And 'Transmittal Form' page is displayed for '<project>'
    When "<content>" is entered into 'List Matched Inks'
    And "<content>" is entered into 'List Colors'
    And 'Special Instructions' is displayed
    And "<content>" is entered into 'Files posted to'
    And "<content>" is entered into 'Special Instructions'
    And user submits the form
  
    Examples:
      | content            |
      | GENERIC CONTENT    |
      | generic content    |
      | Generic Content    |
      | 1234567890         |
      | !@#$%^&*()`~       |
      | <>,./?;':"[]{}-=_+ |	
      | (line break)       |
  	
  	
  	  @ORPHAN @ECIMT957 @P3
  Scenario: 2- 'Other' option label should have a capitalized O.

    Given user is logged in OTBPM
    When 'Transmittal Form' page is displayed for '<project>'
    Then 'Other' label text has a capitalized "o" under the Mechs, Proof Types and Herewith sections
    
 #  Nilesh Scenarios:   
     @ORPHAN @EAPT1313 @P2 @nilesh
  Scenario Outline: 7- Advanced Search Names with multiple terms comma separated
    Given user is logged in OTBPM as "ANOTHER_ART_DIRECTOR"
    And the user is on the Advanced Search page
    When the user conducts a "<type>" advanced search of "<value>"
    Then the returned results should contain at least one of the "<value>" in the "<type>" column
    
    Examples:
      | type         | value             |
      | Art Director | John Williams,Rick DeMonico    |
      
 
#  Nilesh Scenarios:
 @ORPHAN @EAPT1313 @P2   @nilesh
  Scenario Outline: 8- Advanced Search Multi fields
    Given user is logged in OTBPM as "ANOTHER_ART_DIRECTOR"
    And the user is on the Advanced Search page
    When the user conducts advanced search of multiple fields as "<type1>" "<value1>","<type2>" "<value2>","<type3>" "<value3>" 
    Then the returned results should contain at least one record with "<value1>" in the "<type1>" column,"<value2>" in the "<type2>" column,"<value3>" in the "<type3>" column 
    
    Examples:
      | type1  | value1       | type2   | value2     | type3  | value3      |
      | ISBN  | 978-0-545-36253-5   | Author  | Jenne Simon| Title  | ALEX TOYS: HELPING HANDS    |
      | ISBN  | 978-0-545-36252-8   | Author  | Jenne Simon| Title  | ALEX TOYS: MOODY MONSTERS    |
      | ISBN  | 978-0-545-70844-9   | Author  | Daisy Meadows| Title  | BABY ANIMAL RESCUE FAIRIES, THE #1: MAE THE PANDA FAIRY    |
      

