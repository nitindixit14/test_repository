 @nitin
Feature: OTBPM - Transmittal History

	@ORPHAN @ECIMT628 @P3 @nitin
  Scenario Outline: 1- The list of stored Transmittals is color coded by Type of Transmittal Form

    Given user is logged in OTBPM
    When User views Transmittal History for "<PROJECT>"
    Then All the 'Interior' Transmittals listed have text in 'Yellowish Brown' color
    And All the 'Cover' Transmittals listed have text in 'Purple' color
    And All the 'Jacket' Transmittals listed have text in 'Green' color
    Examples:
      | PROJECT           |
      | 978-1-338-04475-1 |
#      | 978-0-545-86184-7 |
      

   @ORPHAN @ECIMT628 @P2 @nitin
  Scenario Outline: 2- The list of stored Transmittals displays information box when hoovering over the icons

    Given user is logged in OTBPM
    When User views Transmittal History for "<PROJECT>"
   Then User hoovers over an "<ACTION_ICON>" and verifies "<INFO_TEXT>"
   
      Examples:
      | PROJECT           | ACTION_ICON | INFO_TEXT          |
      | 978-1-338-04475-1 | img_View        | View Transmittal   |
     | 978-1-338-04475-1 | img_Edit        | Edit Transmittal   |
      | 978-1-338-04475-1 | img_Del     	 | Delete Transmittal |
#      | 978-0-545-86184-7 | View        	 | View Transmittal   |
#     | 978-0-545-86184-7 | Edit        	 | Edit Transmittal   |
#      | 978-0-545-86184-7 | Delete          | Delete Transmittal |

  @ORPHAN @ECIMT720 @P3 @nitin @Offshore @Offshore1
  Scenario Outline: 10- User Views a Cover Transmittal Form from the Transmittal History

    Given user is logged in OTBPM
   	When User views Transmittal History for "<PROJECT>"
    When User selects the 'View' icon for a Cover Transmittal Form
    Then Transmittal Form is displayed as a pdf
   
    Examples:
      | PROJECT           |Title Prefix |
      | 978-1-338-04475-1 |Production/Manufacturing Transmittal Form for Cover|


  @ORPHAN @ECIMT720 @P3 @nitin 
  Scenario Outline: 11- User Views a Jacket Transmittal Form from the Transmittal History

    Given user is logged in OTBPM
    And User views Transmittal History for "<PROJECT>"
    When User selects the 'View' icon for a Jacket Transmittal Form
    Then Transmittal Form is displayed as a pdf
   And 'Date' in Jacket Transmittal Form matches 'Last Modified' value in Transmittal History
   And "<Title Prefix>"in Transmittal Form is 'Production/Manufacturing Transmittal Form for Jacket'
    And 'Title Sufix' in Jacket Transmittal Form matches 'Mechs' value in Transmittal History
    Examples:
      | PROJECT 			|Title Prefix |
      |978-1-338-04475-1    |Production/Manufacturing Transmittal Form for Jacket |
 
  @ORPHAN @ECIMT720 @P3 @nitin  @Test123 @Offshore
  Scenario Outline: 12- User Views a Interior Transmittal Form from the Transmittal History

    Given user is logged in OTBPM
    And User views Transmittal History for "<PROJECT>"
    When User selects the 'View' icon for a Interior Transmittal Form
    Then Transmittal Form is displayed as a pdf

    Examples:
      | PROJECT           |Title Prefix |
      | 978-1-338-04475-1 |Production/Manufacturing Transmittal Form for Interior |


	@ORPHAN @ECIMT629 @P2 @nitin
  Scenario Outline: 19- User can select an additional Proofs Types from the available list

    Given user is logged in OTBPM
    And 'Transmittal Form' page is displayed for '<project>'
    When user selects option "Additional Proof Types"
    Then user select an "<option>" from the list
 	 Examples: 
  	|option|
  	|Artwork| 
 
   
  @ORPHAN @ECIMT629 @P3 @nitin
  Scenario Outline: 29- Optional fields input test

    Given user is logged in OTBPM
    And 'Transmittal Form' page is displayed for '<project>'
    When "<content>" is entered into 'List Matched Inks'
    And "<content>" is entered into 'List Colors'
    And 'Special Instructions' is displayed
    And "<content>" is entered into 'Files posted to'
    And "<content>" is entered into 'Special Instructions'
    And user submits the form
  
    Examples:
      | content            |
      | GENERIC CONTENT    |
#      | generic content    |
#      | Generic Content    |
#      | 1234567890         |
#      | !@#$%^&*()`~       |
#      | <>,./?;':"[]{}-=_ |	
#      | (line break)       |
#  	
#  	
  	@ORPHAN @ECIMT957 @P3 @nitin
  Scenario: 2- 'Other' option label should have a capitalized O.

    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    Then 'Other' label text has a capitalized "o" under the Mechs, Proof Types and Herewith sections
    
# #  Nilesh Scenarios:   
     @ORPHAN @EAPT1313 @P2 @nitin
  Scenario Outline: 7- Advanced Search Names with multiple terms comma separated
    Given user is logged in OTBPM as "ANOTHER_ART_DIRECTOR"
    And the user is on the Advanced Search page
    When the user conducts a "<type>" advanced search of "<value>"
    Then the returned results should contain at least one of the "<value>" in the "<type>" column
    
    Examples:
      | type         | value             |
      | Art Director | John Williams,Rick DeMonico    |
      
 
#  Nilesh Scenarios:
 @ORPHAN @EAPT1313 @P2   @nitin @Offshore
  Scenario Outline: 8- Advanced Search Multi fields
    Given user is logged in OTBPM as "ANOTHER_ART_DIRECTOR"
    And the user is on the Advanced Search page
    When the user conducts advanced search of multiple fields as "<type1>" "<value1>","<type2>" "<value2>","<type3>" "<value3>" 
    Then the returned results should contain at least one record with "<value1>" in the "<type1>" column,"<value2>" in the "<type2>" column,"<value3>" in the "<type3>" column 
    
    Examples:
      | type1  | value1       | type2   | value2     | type3  | value3      |
      | ISBN  | 978-0-545-36253-5   | Author  | Jenne Simon| Title  | ALEX TOYS: HELPING HANDS    |
      | ISBN  | 978-0-545-36252-8   | Author  | Jenne Simon| Title  | ALEX TOYS: MOODY MONSTERS    |
      | ISBN  | 978-0-545-70844-9   | Author  | Daisy Meadows| Title  | BABY ANIMAL RESCUE FAIRIES, THE #1: MAE THE PANDA FAIRY    |



  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2  @nitin @Offshore
  Scenario Outline: 10- Cover transmittal Form displays all user provided information for Online Transmittal Forms

    Given user is logged in OTBPM
   When user searches for "<project>"
   When User opens Project Details for "<project>"
    And User opens a New Transmittal Form for a project
    And User enters data into the "<List Colors>", "<List Matched Ink>", "<Files Posted to>" and "<Special Instruction fields>"
    When User submits New "<TYPE_OF_TRANSMITTAL_FORM>" with "<MECHS>" and "<PROOFS>"
   
    Examples:
      | type     | List Colors | List Matched Ink|Files Posted to|Special Instruction fields |project |TYPE_OF_TRANSMITTAL_FORM |MECHS |PROOFS |
      | Cover    | Colors      | Ink			 |User           | Instruction        |978-1-338-04475-1 |Cover | Art released to Mfg |Case Sample |

      
   @ORPHAN @EAPT1313 @P1 @AdvancedSearchFullName@Dipak
  Scenario Outline: 1- Verify new Advanced Search UI
  Given user is logged in OTBPM
     And user is in 'My Projects' view
    When the user is on the Advanced Search page
    Then the user conducts a "<type>" advanced search of "<value>"
    And the user is on the Advanced Search page
    And User selects a "<dateRangeFrom>" and "<dateRangeTo>" for "<label>"
    Examples:
      | type         | value               |dateRangeFrom|dateRangeTo|		label|	
      | Art Director | rick demonico       |02/10/16    |02/21/16	 | First Use Date:|
      | Designer     | patti ann harris    |02/10/16    |02/21/16	 | Pub Date:      |
      | Author       | Timothy V. Rasinski |02/10/16    |02/21/16	 |Target Due to Mfg: |
      | Illustrator  | Tom LaPadula        |02/10/16    |02/21/16	 | In Store Date: |
  


 
  @ORPHAN @EAPT1313 @P1 @AdvancedSearchFullName@Dipak
  Scenario Outline: User can perform an advanced search using the By date: In Store Date: fields
    Given user is logged in OTBPM
     When user is in 'My Projects' view
    Then the user is on the Advanced Search page
    And User selects a "<dateRangeFrom>" and "<dateRangeTo>" for "<label>"

  Examples:
     |dateRangeFrom|dateRangeTo  | label   |
       |02/15/16     |02/17/16	 | In Store Date:|

@ORPHAN @EAPT1313 @P1 @AdvancedSearchFullName@Dipak
  Scenario Outline: User can perform an advanced search using the By date: Pub Date: fields
    Given user is logged in OTBPM
     When user is in 'My Projects' view
    Then the user is on the Advanced Search page
    And User selects a "<dateRangeFrom>" and "<dateRangeTo>" for "<label>"

  Examples:
     |dateRangeFrom|dateRangeTo  |label   |
       |02/15/16     |02/17/16	 | Pub Date:|


@ORPHAN @EAPT1313 @P1 @AdvancedSearchFullName@Dipak
  Scenario Outline: User can perform an advanced search using the By date: First Use: fields
    Given user is logged in OTBPM
     When user is in 'My Projects' view
    Then the user is on the Advanced Search page
    And User selects a "<dateRangeFrom>" and "<dateRangeTo>" for "<label>"

  Examples:
     |dateRangeFrom|dateRangeTo  | label   |
       |02/15/16     |02/17/16	 |  First Use Date:|


@ORPHAN @EAPT1313 @P1 @AdvancedSearchFullName @Dipak
  Scenario Outline: User can perform an advanced search using the By date: Target Due to Mfg: fields
    Given user is logged in OTBPM
     When user is in 'My Projects' view
    Then the user is on the Advanced Search page
    And User selects a "<dateRangeFrom>" and "<dateRangeTo>" for "<label>"

  Examples:
     |dateRangeFrom|dateRangeTo  | label   |
       |02/15/16     |02/17/16	 | Target Due to Mfg: |
       
 @ORPHAN @ECIMT597 @P3 @NitinJ @Offshore @Offshore1
  Scenario Outline: 13- Navigates to Assign view from Project Details

    Given user is logged in OTBPM
    When user searches for "<PROJECT>" 
    Then User opens Project Details for "<PROJECT>"
    And user clicks on 'Assign'
    Then 'Assign' view is displayed in a new window
    And User selects a "<VALUE>" for "<FIELD>" from the Autosuggested list on Assing Dialog
    Then the "<VALUE>" match information in Project Details view
      
    Examples:
      | PROJECT           |FIELD             |VALUE                |
      | 978-0-545-90883-2 | Art Director     |Cordys Internal|
  
  @ORPHAN @ECIMT597 @P3 @Aparna    
  Scenario Outline: 2- Fields for Assign Art Director, Cover and Interior Designer are aligned
 
    Given user is logged in OTBPM
    When user searches for "<PROJECT>" 
    Then User opens Project Details for "<PROJECT>"
    And user clicks on 'Assign'
    Then 'Assign' view is displayed in a new window
    Then "<SELECTABLE>" field is displayed labeled as "<LABEL>"
    And "<SELECTABLE>" field is populated with all the Active "<users>" with "<ROLE>"
 
   Examples:
    
      | SELECTABLE        | LABEL             | ROLE                        |users|
      | Art Director      | Art Director      | ['Art Director']            |Joe QAtester|
#     | Cover Designer    | Cover Designer    | ['Art Director','Designer'] |Yaffa Jaskoll|
    #  | Interior Designer | Interior Designer | ['Art Director','Designer'] |Joe QAtester|

  @ORPHAN @ECIMT598 @P3 @Aparna @Offshore1
 Scenario Outline: 23- Specific comments can be located in the EPIC Comments section

     Given user is logged in OTBPM
   #  When user searches for "<PROJECT>" 
    #Then User opens Project Details for "<PROJECT>"
   When User opens Project Details for '<project>'
    And user expands 'EPIC comments'
   Then "<specific_comments>" are displayed when available
    And label for "<specific_comments>" is displayed as "<label>" when comments are available
     Examples:
       | project           | specific_comments | label             |
#       | 978-0-545-03517-0 | Cover             | Cover Comments    |
      # | 978-0-545-03517-0 | Jacket            | Jacket Comments   |
      # | 978-0-545-03517-0 | Insert            | Insert Comments   |
      # | 978-0-545-44868-0 | Cover             | Cover Comments    |
       | 978-0-545-44868-0 | Interior          | Interior Comments |
#       | 978-0-545-44868-0 | Jacket            | Jacket Comments   |
       

  @ORPHAN @ECIMT598 @P3 @Aparna     
 Scenario:  11 - Only Art Director's projects display when user with Art Director role selects Show My Team's Projects link with no designers added to that Art Director's team
  
   Given user is logged in OTBPM
   When the user selects Manage my Team
   And there currently is no designers added to the Art Director team
    When user selects the Show My Team Projects link on the My Projects screen   
    Then only Art Director projects will display.
       