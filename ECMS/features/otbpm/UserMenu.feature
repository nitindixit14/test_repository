Feature: Home page configuration for different role(s) 
# Description: As an APT user, I only want to see the options available to me in the system, so I am not confused by 
#              navigation elements which don't apply to me such as SCCG or Administration
# 
# Associated Function: OTBPM Menu
# Stories covered: ECIMT-760 (It crosses functionality with ECIMT-024 which includes menu/submenu behavior), ECIMT-730, ECIMT-744 
# Note: APT sub-menu behavior is not covered here as it is covered in ECIMT-024

@ECIMT760 @Smoke760 @Smoke
Scenario Outline: 1- Menu default behavior 
# Iterated for 'Default' as: APT Production Editor|APT Editor/Mgmt/Guest|APT Manufacturing|APT Vendor
	Given user with "<ROLE>" role 
	When user with "<ROLE>" role signing in 
	Then "<REQUIRED _MENUS>" menus should be available 
	And "<NOT_REQUIRED _MENUS>" menus should not be available 
	
	Examples:
		| ROLE											| REQUIRED _MENUS						| NOT_REQUIRED _MENUS 					|
#		| Default										| ['APT']			| ['Home','TBD','SCCG','Administrator']	|
		| ['APT Production Editor']						| ['APT']			| ['Home','TBD','SCCG','Administrator']	|
		| ['APT Manufacturing']							| ['APT']			| ['Home','TBD','SCCG','Administrator']	|
		| ['APT Vendor']								| ['APT']			| ['Home','TBD','SCCG','Administrator']	|
#		| Guest											| ['APT']			| ['Home','TBD','SCCG','Administrator']	|
		| ['APT Designer']								| ['APT']			| ['Home','TBD','SCCG','Administrator']	|	  
		| ['APT Art Director']							| ['APT']			| ['Home','TBD','SCCG','Administrator']	|
		| ['APT Art Director','APT Production Editor']	| ['APT']			| ['Home','TBD','SCCG','Administrator']	|
		| ['APT Art Director', 'APT Designer']			| ['APT']			| ['Home','TBD','SCCG','Administrator']	|
#		| SCCGReprintEditor								| ['APT','SCCG']	| ['Home','TBD','Administrator']		|
#		| SCCGManufacturing								| ['APT','SCCG']	| ['Home','TBD','Administrator']		|
		| ['APT Designer','SCCG Manufacturing']			| ['APT','SCCG']	| ['Home','TBD','Administrator']		|