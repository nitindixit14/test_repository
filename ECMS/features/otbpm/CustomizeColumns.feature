Feature: Customize the MyProject List Table 
As an APT User, 
I want the ability to customize and sort the MyProject List table, 
such that I can see the information that is most important to me

@SortAscendingOrderUpArrow @Smoke702 @ECIMT702 @Smoke @SmokeMyProject @SmokeMyProjectSoring
Scenario Outline:  - Project list can be sorted in ascending order when unsorted by the selected criteria 									
	Given user is logged in OTBPM								
	And user is in 'My Projects' view 								
	When user selects "<option>" on the Projects list in the header								
	Then projects are sorted in ascending by the selected option "<option>"								
	And 'arrow icon' on "<option>" indicates column is sorted in ascending order 							
Examples:  									
|option					|																
|Author						|								
#|Illustrator				|								
#|File Name					|								
#|Pub Date					|								
#|ISBN						|								
|Trim						|								
#|# of Pages					|								
#|Trade Price				|								
#|Cover Designer				|							
|Interior Designer			|								
|Art Director				|								
#|Cover Target Due to Mfg.  	|									
#|Jacket Target Due to Mfg. 	|									
#|Interior Target Due to Mfg.	|									
#|First Use Date				|		

@SortDescendingDownArrow @ECIMT702
Scenario Outline: - Project list can be sorted in descending order when sorted  in ascending order by the selected criteria									
	Given user is logged in OTBPM								
	And user is in 'My Projects' view 								
	And My Project List is sorted ascending order by "<OPTION>"			
	When user selects "<OPTION>" on the Projects list								
  	Then projected listed are sorted in descending order by "<OPTION>"
  	And 'arrow icon' on "<OPTION>" indicates column is sorted in descending order								
Examples:  																		
|OPTION						|									
#|Title						|								
|Author						|								
|Illustrator				|								
#|File Name					|								
#|Pub Date					|								
#|ISBN						|								
#|Trim						|								
#|# of Pages					|								
#|Trade Price				|								
|Cover Designer				|							
#|Interior Designer			|								
#|Art Director				|								
#|Cover Target Due to Mfg.  	|									
#|Jacket Target Due to Mfg. 	|									
#|Interior Target Due to Mfg.	|									
#|First Use Date				|							

@SortAscendingArrowUpMoreProjects 	@ECIMT702								
Scenario Outline: - Project list can be sorted in ascending order when sorted in descending order by the selected criteria  									
	Given user is logged in OTBPM								
	And user is in 'My Projects' view 								
	And My Project List is sorted descending order by "<OPTION>"						
	When user selects "<OPTION>" on the Projects list								
	Then projected listed are sorted in ascending order by "<OPTION>"									
	And 'arrow icon' on "<OPTION>" indicates column is sorted in ascending order 								
Examples:  									
									
|OPTION						|									
#|Title						|								
#|Author						|								
#|Illustrator				|								
|File Name					|								
|Pub Date					|								
#|ISBN						|								
|Trim						|								
|# of Pages					|								
|Trade Price				|								
|Cover Designer				|							
#|Interior Designer			|								
#|Art Director				|								
#|Cover Target Due to Mfg.  	|									
#|Jacket Target Due to Mfg. 	|									
#|Interior Target Due to Mfg.	|									
#|First Use Date				|		
								
@HideColumns @ECIMT702 @Smoke702 @Smoke
Scenario Outline: - Remove project options (hide columns) from MyProject List 									
	Given user is logged in OTBPM								
	And "<column_name>" is displayed  in the Projects List 								
	When user hides  "<column_name>" 								
	Then the column "<column_name>" is NOT displayed in Project list								
Examples:									
|column_name			|									
|Cover|
|Barcode|
|Title|
|Author|
|Illustrator|
|Pub Date|
|ISBN|
#|File Name|
#|Trim|
#|# of Pages|
#|Trade Price|
#|Cover Designer|
#|Interior Designer|
#|Art Director|
#|Transmittal History|
#|Cover Routing|
#|Interior Routing|
#|Cover Target Due to Mfg.|
#|Jacket Target Due to Mfg.|
#|Interior Target Due to Mfg.|
#|First Use Date|				

@ShowColumns @ECIMT702 @Smoke702 @Smoke
Scenario Outline: 4- Add project options (show columns) to MyProject List									
	Given user is logged in OTBPM								
	And "<column_name>" is not displayed  in the Project List 								
	When user chose the  "<column_name>" to show								
	Then the column "<column_name>" is displayed in Project list								
	And the "<column_name>" value is populated for all the projects on the list								
Examples:									
|column_name				|									
#|Cover|
#|Barcode|
#|Title|
#|Author|
#|Illustrator|
#|Pub Date|
#|ISBN|
#|File Name|
#|Trim|
#|# of Pages|
#|Trade Price|
#|Cover Designer|
#|Interior Designer|
#|Art Director|
|Transmittal History|
|Cover Routing|
|Interior Routing|
|Cover Target Due to Mfg.|
|Jacket Target Due to Mfg.|
|Interior Target Due to Mfg.|
|First Use Date|
					
@ShowHideAtSameTime @ECIMT702
Scenario Outline: - Show/Hide multiple columns in MyProject List at the same time									
	Given user is logged in OTBPM								
	When user select the  "<columns_show>"  to show								
	And user unselect the  "<columns_hide>"  to hide								
	Then the columns "<columns_hide>" are NOT displayed in Project list								
	And the columns "<columns_show>" are displayed in Project list								
	And the columns "<columns_show>" value is populated for all the projects on the list								
Examples:									
|columns_show																									  |	columns_hide|								
|['Checkboxes', 'Cover', 'Barcode','Title', 'Author', 'Illustrator', 'Pub Date', 'ISBN', 'File Name']|['Trim','# of Pages', 'Trade Price', 'Cover Designer', 'Interior Designer','Art Director', 'Transmittal History', 'Cover Routing', 'Interior Routing', 'Cover Target Due to Mfg.', 'Jacket Target Due to Mfg.', 'Interior Target Due to Mfg.', 'First Use Date']|							
																
@ShowHideColumnsSessionPersistence		@ECIMT702					
Scenario Outline: 5- Show/Hide multiple columns in MyProject List with session persistence									
	Given user is logged in OTBPM								
	And user is in 'My Projects' view 								
	And user select the  "<COLUMNS_TO_SHOW>"  to show								
	And user unselect the  "<COLUMNS_TO_HIDE>"  to hide								
	When User logs out 								
	And logs in for a new session								
	Then the columns "<COLUMNS_TO_HIDE>" are NOT displayed in Project list								
	And the columns "<COLUMNS_TO_SHOW>" are displayed in Project list
Examples:									
|COLUMNS_TO_SHOW				|COLUMNS_TO_HIDE					|		
|['Cover', 'Title', 'Illustrator', 'ISBN', 'Trim', 'Trade Price', 'Interior Designer', 'Transmittal History', 'Interior Routing', 'Transmittal Form', 'Cover Target Due to Mfg.', 'Interior Target Due to Mfg.']|[ 'Barcode', 'Author', 'Pub Date', 'File Name','# of Pages', 'Cover Designer', 'Art Director', 'Cover Routing', 'Jacket Target Due to Mfg.', 'First Use Date']|
#|['Checkboxes', 'Cover', 'Barcode','Title', 'Author', 'Illustrator', 'Pub Date', 'ISBN', 'File Name']|['Trim','# of Pages', 'Trade Price', 'Cover Designer', 'Interior Designer','Art Director', 'Transmittal History', 'Cover Routing', 'Interior Routing','Transmittal Form', 'Cover Target Due to Mfg.', 'Jacket Target Due to Mfg.', 'Interior Target Due to Mfg.', 'First Use Date']|
#|['Trim','# of Pages', 'Trade Price', 'Cover Designer', 'Interior Designer','Art Director', 'Transmittal History', 'Cover Routing', 'Interior Routing','Transmittal Form', 'Cover Target Due to Mfg.', 'Jacket Target Due to Mfg.', 'Interior Target Due to Mfg.', 'First Use Date']|['Checkboxes', 'Cover', 'Barcode','Title', 'Author', 'Illustrator', 'Pub Date', 'ISBN', 'File Name']|		

@ResizeColumns @ECIMT702
Scenario Outline: - Resize columns in MyProject List 									
	Given user is logged in OTBPM								
	And "<COLUMN_NAME>" is displayed  in the Projects List 								
	When user changes  "<COLUMN_NAME>" size by moving header's right border	of "<width>"							
	Then the column "<COLUMN_NAME>" width is displayed with the selected size 	"<width>"							
Examples:									
|COLUMN_NAME 			|	width|
|Title|100px|
|Author|100px|
|Illustrator|100px|
|Pub Date|100px|
|ISBN|100px|
|File Name|200px|
|Trim|200px|
|# of Pages|200px|
|Trade Price|200px|
|Cover Designer|200px|
|Interior Designer|200px|
|Art Director|200px|
|Cover Target Due to Mfg.|200px|
|Jacket Target Due to Mfg.|200px|
|Interior Target Due to Mfg.|200px|
|First Use Date|				200px|				

@ResizeColumnWithSessionPersistence	@ECIMT702
Scenario Outline: 6- Resize column width in MyProject List with session persistence									
	Given user is logged in OTBPM		
	And "column_name" is displayed  in the Projects List						
	When user changes  "<columns>"  by  "<width>"								
	And User logs out of the session							
	And logs in for a new session								
	Then the "<columns>" columns are displayed in the selected size	"<width>"							
Examples:									
|columns                                                  |width                               |									
#|['Title', 'Illustrator', 'File Name']	            |['120px', '180px', '150px']                     |	
#|['Cover Designer', 'Interior Designer',  'Interior Target Due to Mfg.']|['30px', '120px', '200px']                | 	
|['File Name',  'Pub Date', 'Trim', '# of Pages', 'Trade Price', 'ISBN', 'Author', 'Art Director'] |['25px', '75px', '20px', '90px', '40px', '300px', '15px', '38px']|	

@ReorderColumns		@NotWorking					
Scenario Outline: - Reorder columns in MyProject List 									
	Given user is logged in OTBPM								
	And "<column_name>" is displayed  in the Projects List 								
	When user changes  "<column_name>"  position by dragging and dropping the column header								
	Then the column "<column_name>" is displayed inserted in the selected position								
Examples:
|column_name				|									
#|Checkboxes|
#|Cover|
#|Barcode|
#|Title|
#|Author|
|Illustrator|
#|Pub Date|
#|ISBN|
#|File Name|
#|Trim|
#|# of Pages|
#|Trade Price|
#|Cover Designer|
#|Interior Designer|
#|Art Director|
#|Transmittal History|
#|Cover Routing|
#|Interior Routing|
#|Cover Target Due to Mfg.|
#|Jacket Target Due to Mfg.|
#|Interior Target Due to Mfg.|
#|First Use Date|

@ReOrderColumnsSessionPersistence  @NotWorking
Scenario Outline: 7- Reorder columns in MyProject List with session persistence									
	Given user is logged in OTBPM								
	And all columns are displayed  in the Project List 								
	When user changes  "<columns>"  order to  "<sequence>"								
	And User logs out of the session							
	And logs in for a new session									
	Then the columns  are displayed in the selected order								
Examples:									
|columns                                                                                                     |sequence                                  |									
|['Transmittal History', 'Jacket Target Due to Mfg' ]														 | [12, 1]	                                |
|['Title', 'Illustrator', 'File Name', 'Interior Target Due to Mfg']								             | [1, 7, 8, 20]	                        |
|['Cover Designer', 'Interior Designer',  'Interior Target Due to Mfg', 'Transmittal Form', 'Cover Routing'] | [22, 10, 8, 2, 6]                        |	
|['File Name',  'Pub Date', 'Trim', '# of Pages', 'Trade Price', 'ISBN', 'First Use date', 'Author', 'cover image', ' barcode', 'Interior Routing', 'Checkboxes', 'Art Director']					 | [9, 8, 7, 6, 22, 21, 20, 19, 1, 2, 13, 7]|	

@DefaultColumnsView								
Scenario: - Set Project List to default view for columns/size/order									
Given user is logged in OTBPM								
And user customize Project List size of view of columns								
When user selects to set default Project List								
Then the default columns are displayed in default size and order

@DefaultColumnSessionPersistence 
Scenario Outline: - Select Default view for columns/size/order persist between sessions									
	Given user is logged in OTBPM								
	And user is in 'My Projects' view 								
	When user changes  "<columns>"  by  "<width>"			
	And user select the  "<COLUMNS_TO_SHOW>"  to show								
	And user unselect the  "<COLUMNS_TO_HIDE>"  to hide
	And user selects 'Set grid to default' view						
	And User logs out 								
	And logs in for a new session								
	Then the default columns are displayed in default size and order	
	Examples:
	|columns                          |width                               	|columns_show																									  |	columns_hide|																						
|['Title', 'Illustrator', 'File Name']	   |['20px', '80px', '50px']|['Checkboxes', 'Cover', 'Barcode','Title', 'Author', 'Illustrator', 'Pub Date', 'ISBN', 'File Name']|['Trim','# of Pages', 'Trade Price', 'Cover Designer', 'Interior Designer','Art Director', 'Transmittal History', 'Cover Routing', 'Interior Routing', 'Cover Target Due to Mfg.', 'Jacket Target Due to Mfg.', 'Interior Target Due to Mfg.', 'First Use Date']|	