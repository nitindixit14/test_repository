Feature: OTBPM - Transmittal History
 #  Nilesh Scenarios:
 @ORPHAN @EAPT1313 @P2   @nitin @Offshore1
  Scenario Outline: 8- Advanced Search Multi fields
    Given user is logged in OTBPM as "ANOTHER_ART_DIRECTOR"
    And the user is on the Advanced Search page
    When the user conducts advanced search of multiple fields as "<type1>" "<value1>","<type2>" "<value2>","<type3>" "<value3>" 
    Then the returned results should contain at least one record with "<value1>" in the "<type1>" column,"<value2>" in the "<type2>" column,"<value3>" in the "<type3>" column 
    
    Examples:
      | type1  | value1       | type2   | value2     | type3  | value3      |
      | ISBN  | 978-0-545-36253-5   | Author  | Jenne Simon| Title  | ALEX TOYS: HELPING HANDS    |
      | ISBN  | 978-0-545-36252-8   | Author  | Jenne Simon| Title  | ALEX TOYS: MOODY MONSTERS    |
      | ISBN  | 978-0-545-70844-9   | Author  | Daisy Meadows| Title  | BABY ANIMAL RESCUE FAIRIES, THE #1: MAE THE PANDA FAIRY    |
      
 