@OTBPM-Transmittal @OTBPM-Transmittal(ECIMT-629)
Feature: OTBPM - Transmittal (ECIMT-629)
 
  @ORPHAN @ECIMT629 @P1 @TypeSelectorInForm
  Scenario Outline: 1- Cover/Jacket/Interior selector is displayed on Transmittal Form

    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    Then "<transmittalType>" 'Cover/Jacket/Interior' radiobuttons are displayed
	Examples:
	|transmittalType|
	|['Cover','Jacket','Interior']|

  @ORPHAN @ECIMT629 @P2 @NotInteriorDesigner
  Scenario Outline: 2- Cover/Jacket/Interior selector is set to Cover when user is NOT the assigned Interior Designer

    Given "<user>" is logged in OTBPM
   When User searches for a "<project>" in My Projects view
    And the user selects the Transmittal Form of a project
    Then "Cover" radiobutton is selected
    Examples:
      | user 			    | project           |searchType|
      | NotInteriorDesigner | 978-0-545-60007-1 |ISBN|

  @ORPHAN @ECIMT629 @P2 @InteriorDesigner
  Scenario Outline: 3- Cover/Jacket/Interior selector is set to Interior when user is the assigned Interior Designer

    Given "<user>" is logged in OTBPM
    When User searches for a "<project>" in My Projects view
    And the user selects the Transmittal Form of a project
    Then "Interior" radiobutton is selected
    Examples:
      | user     			| project           |searchType|
      | InteriorDesigner | 978-0-545-60007-1 |ISBN|


  @ORPHAN @ECIMT629 @P2 @UpdateFormOnType
  Scenario Outline: 4- Transmittal Form is updated based on Jacket/Cover/Interior selection

   Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    And the user selects "<transmittalType>"
    Then Transmittal Form "<title>" is updated for "<transmittalType>"
    And "<typeOfColors>" label is updated for "<transmittalType>"
    Examples:
      | transmittalType |typeOfColors|								title|
      | Jacket   		|Jacket Colors:|Production/Manufacturing Transmittal Form for Jacket|
      | Cover   	  |Cover # of Colors:|Production/Manufacturing Transmittal Form for Cover|
      | Interior  	|Interior # of Colors:|Production/Manufacturing Transmittal Form for Interior|


  @ORPHAN @ECIMT629_2 @P2 @TransmittalFormPdf
  Scenario Outline: 5- Transmittal Form is generated based on Jacket/Cover/Interior selection

    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
     And the user selects "<type>"
    And selects the required fields "<fields>"
    And user submits the form
    And generates the transmittal form
    Then Transmittal Form for "<type>" is displayed as a pdf that opens in a new tab
    Examples:
      | type 	  |fields|
      | Jacket    |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
      | Cover     |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
      | Interior  |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|


  @ORPHAN @ECIMT629 @P3 @ARCDefault
  Scenario: 6- Advanced Reader Copy is unselected by default

    Given user is logged in OTBPM
   When the user selects the Transmittal Form of a project
    Then 'ARC'checkbox is unchecked
    
    @ORPHAN @P2 @DuplicateInTransmittalpdfForm7
  Scenario Outline: 7- ARC is not displayed in Transmittal Form when it is not selected in Transmittal Form

    Given user is logged in OTBPM
      And the user selects the Transmittal Form of a project
    When the user selects "<type>"
     And user does not select 'Advanced Reader Copy'
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then 'ARC' is not displayed in the "<type>" Transmittal Form pdf
    Examples:
      | type     |fields|
      | Cover    |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Jacket   |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Interior |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|

  @ORPHAN @ECIMT629_1 @P2 @DuplicateInTransmittalpdfForm8
  Scenario Outline: 8- ARC:Yes is displayed in print when checked in Transmittal Form

     Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    When user checks 'Advanced Reader Copy'
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then 'ARC:YES' is displayed in the "<type>" Transmittal Form pdf
    Examples:
      | type     |fields|
      | Cover    |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Jacket   |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Interior |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|


  @ORPHAN @ECIMT629 @P3 @ProductInformation
  Scenario Outline: 9- Product information is displayed and non editable

    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    Then project "<information>" is displayed
    And project "<information>" value is not editable
    Examples:
      | information       |
        | ['Title','ISBN 13','Art Director','Pub Date','ISBN:','Cover Designer','Trim Size','Record #','Interior Designer','Page Count','Trade Price','Production Editor','Can. Price','Editor','BF Price','Mfg. Manager','# of Colors','Product Type','Binding'] |

 @ORPHAN @ECIMT629 @P3 @InformationBoxInForm
  Scenario Outline: 10- An information box is displayed in Transmittal Form

  Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
    Then grouped "<elements>" are displayed inside a container border
    Examples:
      | elements |
 | ['ISBN 13','Art Director','Pub Date','ISBN:','Cover Designer','Trim Size','Record #','Interior Designer','Page Count','Trade Price','Production Editor','Can. Price','Editor','BF Price','Mfg. Manager'] |
	
	
	 @ORPHAN @ECIMT629 @P3 @OptionalFieldsInForm
  Scenario Outline: 11- Optional fields are displayed

    Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
    Then "<label>" is displayed
    Examples:
    |label|
    |['Title','List Matched Inks','Files Posted to','Special Instructions']|
	
	 @ORPHAN @ECIMT629_2 @P3 @MechSelectNoOther
  Scenario Outline: 12- Mech selection distinct than other persist on the Transmittal form pdf

    Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<fields>"
    And user selects 'Mech' option "<option>"
    And user submits the form
 	 And generates the transmittal form
    Then The selected 'Mech' option in "<value>" is displayed in the Transmittal Form
    Examples:
      | type     |fields|option|value|
#	| Interior |['Edox','CDs']|['Art released to Mfg']|Art released to Mfg|
#	| Interior |['Edox','CDs']|['Mechanicals released to Mfg']|Mechanicals released to Mfg|
	| Interior |['Edox','CDs']|['1st']|1st  Proof|
	| Interior |['Edox','CDs']|['2nd']|2nd Proof|
	| Interior |['Edox','CDs']|['3rd']|3rd  Proof|
	| Interior |['Edox','CDs']|['4th']|4th  Proof|
	| Interior |['Edox','CDs']|['5th']|5th  Proof|
	
	 @ORPHAN @ECIMT629 @P2 @CustomValueMechType
  Scenario Outline: 13- User can select a custom value for Mech

    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And user selects 'Mech' option "<option>"
    Then a text field is displayed for user to type for 'Mech'
    Examples:
    | type    |option|
    | Interior |['Other']|
    
   @ORPHAN @ECIMT629_1 @P3 @MechSelectOther
  Scenario Outline: 14- Mech selection as other persist on the Transmittal form pdf

    Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<fields>"
     And user selects "Other" custom "<value>"
    And user submits the form
 	 And generates the transmittal form
   Then The selected 'Mech' option in "<value>" is displayed in the Transmittal Form 
    Examples:	
	| type     |fields|			value|
	| Interior |['Edox','CDs']|test|
	
	 @ORPHAN @ECIMT629_1 @P1  @SelectOneDistinctPdf
  Scenario Outline: 15- Proofs Types/HerWith selection of one value distinct than other persist on the Transmittal form pdf
    # Allow the user to select one or more Proof checkboxes.
    Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
      When the user selects "<pdfType>"
    And selects the required fields "<eachFieldSingleOption>"
     And user submits the form
   And generates the transmittal form
    Then The selected "<type>" option in "<eachFieldSingleOption>" is displayed in the Transmittal Form
    Examples:
  | pdfType|type   |eachFieldSingleOption|
   |Jacket|['Mech','Proofs','Herewith']   |['Art released to Mfg','Edox','CDs']|
   |Cover |['Mech','Proofs','Herewith']  |['Art released to Mfg','Epson','Transparencies']|
   |Interior |['Mech','Proofs','Herewith']  |['Art released to Mfg','Soft Proof (pdf)','Slides']|
#   |Cover |['Mech','Proofs','Herewith']  |['Art released to Mfg','Wet Proof','4/c Flat Art']|
#   |Cover |['Mech','Proofs','Herewith']  |['Art released to Mfg','Random','1/c Flat Art']|
#   |Cover |['Mech','Proofs','Herewith']  |['Art released to Mfg','Printer Proofs','CDs']|
   	
   	 @ORPHAN @ECIMT629 @P2 @multipleOptionsInPdf @P3InExcel @RONAK @Offshore1
  Scenario Outline: 16- Proofs Types selection of multiple values distinct than other persist on the Transmittal form pdf
    Given user is logged in OTBPM
 		And the user selects the Transmittal Form of a project
    When the user selects "<pdfType>"
    And selects the required fields "<fields>"
    And user selects 'Proofs Types' first "<checkbox>" options
    And user submits the form
 	  And generates the transmittal form
    Then The selected 'Proofs Types' "<checkbox>","","" options are is displayed in the Transmittal Form
   
    Examples:
	   |pdfType|fields|checkbox|
		 |Cover|['CDs','Mechanicals released to Mfg']|['Edox','Epson']|
		 |Cover|['CDs','Mechanicals released to Mfg']|['Edox','Epson','Soft Proof (pdf)']|
	#	 |Cover|['CDs','Mechanicals released to Mfg']|['Edox','Epson','Soft Proof (pdf)','Wet Proof','Random','Printer Proofs']|

	 
	 
     @ORPHAN @ECIMT629 @P2 @CustomValueProofType
  Scenario Outline: 17- User can select a custom value for Proofs Types

    Given user is logged in OTBPM
     When the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And user selects 'Proofs Types' first "<checkbox>" options
    Then a text field is displayed for user to type
    Examples:
   | type|checkbox|
    |Cover|['OtherProofType']|
    
    
      @ORPHAN @ECIMT629_3 @P3 @OtherProofInPdf
  Scenario Outline: 18- Proofs Types selection as other persist on the Transmittal form pdf

    Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
      When the user selects "<type>"
    And selects the required fields "<fields>"
    And user selects "OtherProofType" custom "<value>"
    And user submits the form
 	 And generates the transmittal form
    Then The custom value "<value>" is displayed in the Transmittal Form pdf "<label>"
	Examples:
	| type     |fields|value|label|
	| Interior |['Mechanicals released to Mfg']|OtherTest|Proofs:|
	
#	@ORPHAN @ECIMT629 @P2
#  Scenario: 19- User can select an additional Proofs Types from the available list
#
#    Given user is logged in OTBPM
#    And 'Transmittal Form' page is displayed for '<project>'
#    When user selects 'Proofs Types' option 'Additional Proof Types'
#    Then a list is displayed for user to select an option
#    
    
     @ORPHAN @ECIMT629_3 @P2 @PersistAdditionalProofs
  Scenario Outline: 20- Proofs Types selection as additional persist on the Transmittal form pdf

   Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<fields>"
    And user selects option "Additional Proof Types"
    And user select an "<option>" from the list
 	And user submits the form
 	 And generates the transmittal form
    Then The additional "<option>" is displayed in the Transmittalpdf Form
	Examples:
	      | type     |fields|option|
	      | Interior |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Mechanicals released to Mfg']|Artwork|
	

  @ORPHAN @ECIMT629_3 @P2 @MultipleProofTypes
  Scenario Outline: 21- Proofs Types selection of multiple values including other persist on the Transmittal form pdf

    # Allow the user to select one or more Herewith checkboxes.
    Given user is logged in OTBPM
      And the user selects the Transmittal Form of a project
      When the user selects "<type>"
    And selects the required fields "<fields>"
    And user selects 'Proofs Types' first "<checkbox>" options
    And user selects "OtherProofType" custom "<value>"
     And user selects option "Additional Proof Types"
    And user select an "<option>" from the list
    And user submits the form
 	 And generates the transmittal form
    Then The selected 'Proofs Types' "<checkbox>","<value>","<option>" options are is displayed in the Transmittal Form
    Examples:
       | type     |fields|									option|			 checkbox|value|
	   | Interior |['CDs','Mechanicals released to Mfg']|Files				|['Edox']|test|
	   | Interior |['CDs','Mechanicals released to Mfg']|Laser Proofs		|['Edox','Epson']|test|
	   | Interior |['CDs','Mechanicals released to Mfg']|Proofing Sample	|['Edox','Epson','Soft Proof (pdf)']|test|
	   | Interior |['CDs','Mechanicals released to Mfg']|Embossing Sample	|['Edox','Epson','Soft Proof (pdf)','Wet Proof']|test|
	   | Interior |['CDs','Mechanicals released to Mfg']|Proofing Sample	|['Edox','Epson','Soft Proof (pdf)','Wet Proof','Random']|test|
	   | Interior |['CDs','Mechanicals released to Mfg']|Ink Draw Down		|['Edox','Epson','Soft Proof (pdf)','Wet Proof','Random','Printer Proofs']|test|
    
#      @ORPHAN @ECIMT629 @P2  @CombinedInSelectOneDistinctPdf
#  Scenario Outline: 22- Herewith selection of one value distinct than other persist on the Transmittal form pdf
#
#    Given user is logged in OTBPM
#    And 'Transmittal Form' page is displayed for '<project>'
#    And user selects 'Herewith' option '<position>'
#    When user Submits the Transmittal Form
#    Then The selected 'Herewith' option in '<positon>' is displayed in the Transmittal Form
#    Examples:
#      | position |
#      | 1        |
#      | 2        |
#      | 3        |
#      | 4        |
#      | 5        |
  
  @ORPHAN @ECIMT629_2 @P2 @MultipleHerewithNoOther
  Scenario Outline: 23- Herewith selection of multiple values distinct than other persist on the Transmittal form pdf

   Given user is logged in OTBPM
      When the user selects the Transmittal Form of a project
      And the user selects "<type>"
    And selects the required fields "<fields>"
    And user selects 'HereWiths' first "<checkbox>" options
    And user submits the form
 	 And generates the transmittal form
    Then The selected 'Herewith' "<checkbox>" options are is displayed in the Transmittal Form
    Examples:
     | type     |fields|										 checkbox|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs']|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies']|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides']|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides','4/c Flat Art']|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art']|  
             
    
     @ORPHAN @ECIMT629_3 @P2  @CustomValueHerewithType
  Scenario Outline: 24- User can select a custom value for Herewith

    Given user is logged in OTBPM as "APT_Art_Director"
     When the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And user selects "OtherHereWith" custom ""
    Then a 'Herewith' text field is displayed for user to type
     Examples:
     |type|
   |Jacket|

  
 	
  @ORPHAN @ECIMT629_3 @P2 @OtherHereWithInPdf
  Scenario Outline: 25- Herewith selection as other persist on the Transmittal form pdf

    Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
      When the user selects "<type>"
    And selects the required fields "<fields>"
    And user selects "OtherHereWith" custom "<value>"
    And user submits the form
 	 And generates the transmittal form
    Then The custom value "<value>" is displayed in the Transmittal Form pdf "<label>"
	Examples:
	| type     |fields|value|label|
	| Interior |['Mechanicals released to Mfg','Edox']|OtherTest|Herewith|
 

 	 
	   @ORPHAN @ECIMT629_2 @P2 @MultipleHerewithWithOther
  Scenario Outline: 26- Herewith selection of multiple values including other persist on the Transmittal form pdf

    Given user is logged in OTBPM
      When the user selects the Transmittal Form of a project
      And the user selects "<type>"
    And selects the required fields "<fields>"
    And user selects 'HereWiths' first "<checkbox>" options
    And user selects "OtherHereWith" custom "<value>"
     And user submits the form
 	 And generates the transmittal form
    Then The selected 'Herewith' "<checkbox>","<value>" options are is displayed in the Transmittal Form
    Examples:
     | type     |fields|										 checkbox|value|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs']|test|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies']|test|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides']|test|
     | Interior |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides','4/c Flat Art']|test|
     | Cover |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art']|test|
	 

  @ORPHAN @ECIMT629_3 @P3 @CancelTransmittalForm
  Scenario Outline: 27- User can cancel the current online transmittal form after submit it

    Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
      When the user selects "<type>"
    And selects the required fields "<fields>"
    When user Cancels the Transmittal Form
    Then 'Transmittal Form' window closes
    And the 'Transmittal Form' of "<type>" and "<fields>" is not listed in the transmittal history
	Examples:
	| type     |fields|
	| Interior |['Mechanicals released to Mfg','Edox','Epson']|


  @ORPHAN @ECIMT629_1 @P1 @SubmitTransmittalForm
  Scenario Outline: 28- User submits Transmittal form providing all mandatory fields

     Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
      When the user selects "<type>"
    And selects the required fields "<fields>"
     And user submits the form
	Then Transmittal Form for "<type>" is displayed as a pdf that opens in a new tab
     And the 'Transmittal Form' of "<type>" and "<fields>" are listed in the transmittal history
	Examples:
	| type     |fields|
	| Interior |['Mechanicals released to Mfg','Edox']|
	
	
	  @ORPHAN @ECIMT629_2 @P2 @MultipleHerewithNoOther @RONAK
  Scenario Outline: 11- The herewith list should be listed in the first column of the generated PDF

   Given user is logged in OTBPM
   When the user selects the Transmittal Form of a project
   And the user selects "<type>"
    And selects the required fields "<fields>"
    And user selects 'HereWiths' first "<checkbox>" options
    And user submits the form
 	 And generates the transmittal form
    Then The selected 'Herewith' "<checkbox>" options are is displayed in the Transmittal Form
    Examples:
     | type     |fields|										 checkbox|
     | Jacket |['Edox','Mechanicals released to Mfg']|['CDs']|
     | Jacket |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies']|
     | Jacket |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides']|
     | Jacket |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides','4/c Flat Art']|
     | Jacket |['Edox','Mechanicals released to Mfg']|['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art']|  
             
    
	
	
#  @ORPHAN @ECIMT629 @P3
#  Scenario Outline: 29- Optional fields input test
#
#    Given user is logged in OTBPM
#    And 'Transmittal Form' is selected for '<project>'
#    When <content> is entered into 'List Matched Inks'
#    And <content> is entered into 'List Colors'
#    And 'Special Instructions' is displayed
#    And <content> is entered into 'Files posted to'
#    And <content> is entered into 'Special Instructions'
#    And required options are selected
#    And Transmittal form is submitted
#    Then the generated PDF form should display as expected
#    Examples:
#      | content            |
#      | GENERIC CONTENT    |
#      | generic content    |
#      | Generic Content    |
#      | 1234567890         |
#      | !@#$%^&*()`~       |
#      | <>,./?;':"[]{}-=_+ |
#      | (line break)       |
#
#
#  @ORPHAN @ECIMT629
#  Scenario: 30 - Error message display when clicking submit on the online Transmittal Form without selecting a Mech Radio button
#
#    Given user is logged in OTBPM									
#    And 'Transmittal Form' page is displayed for '<project>'									
#    And user provides all mandatory fields except for Mech Radio button									
#    When user Submits the Transmittal Form									
#    Then an error message displays with the text: Select a "Mechs" for the transmittal form.									
#
#
#  @ORPHAN @ECIMT629
#  Scenario: 31 - Error message display when user selects the Mech Radio button of "Other" but does not enter any text into the "Other" field and submits online transmittal form
#
#    Given user is logged in OTBPM											
#    And 'Transmittal Form' page is displayed for '<project>'											
#    And user provides all mandatory fields											
#    And user selects Mech Radio button of "Other" without entering text into "Other" field											
#    When user Submits the Transmittal Form											
#    Then an error message displays with the text: Enter a value for "Other" Mechs for the transmittal form.											
#
#
#  @ORPHAN @ECIMT629
#  Scenario: 32 - Error message display when clicking submit on the online Transmittal Form without selecting at least one Proof checkbox
#
#    Given user is logged in OTBPM											
#    And 'Transmittal Form' page is dispalyed for '<project>'											
#    And user provides all mandatory fields without checking at least one "Proof" checkbox											
#    When user Submits the Transmittal Form											
#    Then an error message displays with the text: Select at least one "Proof Type" for the transmittal form.											
#
#
#  @ORPHAN @ECIMT629
#  Scenario: 33 - Error message display when user selects the Proof checkbox of "Other" but does not enter any text into the "Other" field and submits online transmittal form
#
#    Given user is logged in OTBPM											
#    And 'Transmittal Form' page is displayed for '<project>'											
#    And user provides all mandatory fields											
#    And user selects Proof checkbox of "Other" but does not enter any text into the "Other" field											
#    When user Submits the Transmittal Form											
#    Then an error message displays with the text: Enter a value for "Other" Proof Type for the transmittal form.											
#
#
#  @ORPHAN @ECIMT629
#  Scenario: 34 - Error message display when user selects the Herewith checkbox of "Other" but does not enter any text into the "Other" field and submits online transmittal form
#
#    Given user is logged in OTBPM											
#    And 'Transmittal Form' page is displayed for '<project>'											
#    And user provides all mandatory fields											
#    And user selects Herewith checkbox of "Other" but does not enter any text into the "Other" field											
#    When user Submits the Transmittal Form											
#    Then an error message displays with the text: Enter a value for "Other" Herewith for the transmittal form.											

