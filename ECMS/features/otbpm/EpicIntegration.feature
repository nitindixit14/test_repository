Feature: Epic Integration


@EpicIntegration @ECIMT634
Scenario Outline: create/edit epic record and check it in the database and check it in the OTBPM
Given user login to epic
When user searches for title
And user selects the owner
And user fills the mandatory fields
And user clicks save button
And user clicks on recenttabs
And user request for isbn
#And user finalize
#And User searches for New record by ISBN 13
And read the product details values
And User Query product information for "<INFO_IN_OTBPM>" from OTBPM database
And "<INFO_IN_OTBPM>" values from OTBPM database matches displayed "<PRODUCT_INFO>" in EPIC
And user logout of epic
Given user is logged in OTBPM
When User searches in My Projects view using isbn
Then INFO_IN _MY_PROJECTS values for the record matches OTBPM DATABASE_MP_FIELDS
#And user selects a project by 'Title'
#Then <INFO_IN _PRODUCT_DETAILS> values for the record matches OTBPM <DATABASE_PD_FIELDS>
Then <INFO_IN_TRANSMITTAL_FORM> values for the record matches OTBPM <DATABASE_TF_FIELDS>
And user sign out 
Given user login to epic
When User searches for New record by ISBN 13
And user edits the record information   
And read the product details values
And User Query product information for "<INFO_IN_OTBPM>" from OTBPM database
And "<INFO_IN_OTBPM>" values from OTBPM database matches displayed "<PRODUCT_INFO>" in EPIC
Given A valid user
When User searches in My Projects view using isbn
Then Search results matches for the project field that displays text
Examples:
	|PRODUCT_INFO|INFO_IN_OTBPM|
	|['Millennium Title:','Author','Illustrator','ISBN','ISBN 13','UPC:','UPN:','Type','Pub Date','Trim Size','Trade Strict On Sale Date:','Internal Editor','Managing Editor','Text Pages','First Use','Millennium RCDNO','In Store Date','SCHEDULE','Trade','Canada','Cover Comments','Jacket Comments','Text Comments:','Insert Comments','Binding','Cover Colors','Jacket Colors','Text Colors']|['TITLE_NAME', 'AUTHOR', 'ILLUSTRATOR', 'ISBN', 'ISBN_13', 'UPC', 'RECORD_NO', 'TRIM_SIZE', 'EDITOR', 'MANAGING_EDITOR', 'NO_OF_PAGES', 'FIRST_USE_DATE', 'TRADE_IN_STORE_DATE', 'PRODUCTION_SCHEDULE_CODE', 'TRADE_PRICE', 'CANADAR_PRICE', 'JACKET_COMMENTS', 'COVER_COMMENTS', 'BOOK_INTERIOR_TEXT_COMMENTS', 'INSERT_COMMENTS', 'PUBLICATION_DATE', 'PRODUCT_TYPE', 'BINDING', 'COVER_COLOR_COUNT', 'BOOK_INTERIOR_COLOR_COUNT', 'BOOK_INTERIOR_COLOR', 'COVER_COLOR', 'JACKET_COLOR']|																																																																																														