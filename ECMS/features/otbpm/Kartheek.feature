@Kartheek
Feature: OTBPM -

  @TestCase1
  Scenario Outline: 1 -Error display when Assign Designer button clicked without selecting at least one designer from the All Designers list
    Given user is logged in OTBPM as "<value>"
    When the user is on the Manage my Team page
    Then an error message with the title "Manage My Team" displays with the text:Select an available Designer to add to your team

    Examples: 
      | value            |
      | APT_Art_Director |

  @TestCase2
  Scenario Outline: 10 - Error display when Remove Designer button clicked without selecting at least one designer from the "Designers who report to me" list
    Given user is logged in OTBPM as "<value>"
    When the user is on the Manage my Team page
    When the user clicks the Remove Designer button without selecting at least one designer from the "Designers who report to me" list
    Then an error message with the title "Manage My Team" displays with the text: Select a Designer to remove from your team.

    Examples: 
      | value            |
      | APT_Art_Director |

  @TestCase3
  Scenario Outline: 11 -- Project Details - Managing Editor is displayed as Production Editor
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    And user is in 'Project Details' page for "<project>"
    Then 'Production Editor' label is displayed

    # And 'Managing Editor' label is not displayed
    Examples: 
      | project           |
      | 978-0-545-44868-0 |

  @TestCase4
  Scenario Outline: 7 - Designer selection is persistent
    Given user is logged in OTBPM as "APT_Art_Director"
    And the user is on the Manage my Team page
    And the user assigns a Designer "<designer>" to the Team
    When User logs outs and login as "APT_Art_Director" user
    And the user returns to the Manage my Team page
    Then the same Designers "<designer>" still remain in the Team

    Examples: 
      | designer         |
      | ['Albey QaTest'] |

  @TestCase5 @Offshore
  Scenario Outline: 5- User unassigns project to himself so is not displayed in My Projects List
    Given user is logged in OTBPM as "APT_Art_Director"
    And user selects a "<PROJECT>" from My Projects and go to Assign page
    When User unassigns himself as 'Art Director' and saves unassign selection
    Then user should seee "<PROJECT>" is not listed in My Projects page
    And User can Search for "<PROJECT>"
    Then "<PROJECT>" Art Director value is empty
    And user selects a "<PROJECT>" from My Projects and go to Assign page
    When User selects a "<VALUE>" for "<FIELD>" from the Autosuggested list

    Examples: 
      | PROJECT           | VALUE         | FIELD        |
      | 978-0-545-75190-2 | John Williams | Art Director |

  @TestCase6
  Scenario Outline: 1- Selected Project information is displayed in Assign view
    Given user is logged in OTBPM
    When user selects a "<PROJECT>" and go to  Assign page
    Then A table is displayed with the following "<COLUMNS>"
      | value             |
      | Title             |
      | ISBN              |
      | Art Director      |
      | Cover Designer    |
      | Interior Designer |
    And the values are populated and matched information with "<Title>""<ISBN>""<Art Director>""<Cover Designer>""<Interior Designer>" for "<PROJECT>"

    Examples: 
      | PROJECT                                         | COLUMNS                                                        | Title                                                                                                                        | ISBN                                            | Art Director                              | Cover Designer           | Interior Designer |
      | 978-0-545-91372-0                               | [Title,ISBN,Art Director,Cover Designer,Interior Designer]     | BACKSTORIES: SUPERMAN: THE MAN OF TOMORROW  EBA                                                                              | 978-0-545-91372-0                               | Sudheer Mogi                              | Hiral Desai              | SunTest           |
      | 978-0-545-91372-0 978-1-443-15633-2             | [Title, ISBN, Art Director, Cover Designer, Interior Designer] | BACKSTORIES: SUPERMAN: THE MAN OF TOMORROW  EBA,BATMAN/SUPERMAN KIDS EC PPK                                                  | 978-0-545-91372-0,978-1-443-15633-2             | Sudheer Mogi,Sudheer Mogi                 | Hiral Desai,Hiral Desai  | SunTest,SunTest   |
      | 978-0-545-91372-0 978-1-443-15633-2 SC-01715658 | [Title, ISBN, Art Director, Cover Designer, Interior Designer] | BACKSTORIES: SUPERMAN: THE MAN OF TOMORROW  EBA,BATMAN/SUPERMAN KIDS EC PPK,240 CUT-UP LETTER CHUNKS FOR WORKING WITH WORD F | 978-0-545-91372-0,978-1-443-15633-2,SC-01715658 | Sudheer Mogi,Sudheer Mogi,Cordys Internal | Hiral Desai,Hiral Desai, | SunTest,SunTest,  |

  @TestCase7
  Scenario Outline: 3- The listed Transmittals displays multiple Proofs separated by comma
    Given user is logged in OTBPM
    When User views Transmittal History for "<PROJECT>"
    Then "<TYPE_OF_TRANSMITTAL_FORM>" on the list displays 'Proofs' separated by comma

    Examples: 
      | PROJECT           | TYPE_OF_TRANSMITTAL_FORM |
      | 978-1-338-04475-1 | Cover                    |
      | 978-1-338-04475-1 | Interior                 |
      | 978-0-545-90368-4 | Cover                    |
      | 978-0-545-90368-4 | Interior                 |
      | 978-0-545-86184-7 | Cover                    |

  @TestCase8
  Scenario Outline: 3- Edited Transmittal Form displays multiple Proofs separated by comma
    Given user is logged in OTBPM
    When User views Transmittal History for "<PROJECT>"
    When User submits  New "<TYPE_OF_TRANSMITTAL_FORM>"  with "<MECHS>" and "<PROOFS>"
    When User edits most recent "<TYPE_OF_TRANSMITTAL_FORM>" with "<PROOF1>""<PROOF2>"
    Then "<TYPE_OF_TRANSMITTAL_FORM>" on the list displays 'Proofs' separated by comma

    Examples: 
      | PROJECT           | TYPE_OF_TRANSMITTAL_FORM | MECHS               | PROOFS | PROOF1    | PROOF2 |
      | 978-0-545-90883-2 | Cover                    | Art released to Mfg | Edox   | Wet Proof | Random |
      | 978-0-545-90883-2 | Jacket                   | Art released to Mfg | Edox   | Wet Proof | Random |
      | 978-0-545-90883-2 | Interior                 | Art released to Mfg | Edox   | Wet Proof | Random |

  @ORPHAN @ECIMT510 @P2 @SpecialEffectsLabelInPdf @TestCase9
  Scenario Outline: 8- Display Special Effects label with room for designers to write in notes in the Cover/Jacket Routing Print Form
    Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then Special Effects label with room to write in notes is displayed on the printable PDF form

    Examples: 
      | routingType      |
      | Cover Routing    |
      | Interior Routing |

  @TestCase10 @P2
  Scenario Outline: File name rules for with no record number
    #Given the "<searchKey>" does not have a record number
    And user is logged in OTBPM
    When user is in 'My Projects' view
    And user searches for "<searchKey>"
    Then the file Name on the myprojects and projects details page are the same
    And the file name should have the prefix of the unformatted ISBN_13 followed by an underscore
    And the file name should have the first 12 alphanumeric values of the title followed by the text "_4cc.eps"

    Examples: 
      | searchKey         |
      | 978-1-338-09913-3 |

  @ORPHAN @ECIMT597 @P3 @TestCase11
  Scenario Outline: 3- Title of window is displayed as Assign
    Given user is logged in OTBPM
    When user selects a "<PROJECT>" and go to Assign page
    Then 'Assign' Title is displayed

    Examples: 
      | PROJECT           |
      | 978-0-545-91372-0 |

  @ORPHAN @ECIMT598 @P2 @CoverRoutingButtonDetails @TestCase13
  Scenario Outline: 14- Cover Routing button is displayed in Project Details page
    Given user is logged in OTBPM
    When user searches for "<project>"
    And User opens Project Details for "<project>"
    Then 'Cover Routing button' is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |

  @ORPHAN @ECIMT598 @P2 @TestCase14
  Scenario Outline: 15- Cover Routing Form can be launched from Project detail
    Given user is logged in OTBPM
    When User opens Project Details for "<project>"
    And user opens 'Cover Routing' Form
    Then 'Cover Routing Form' is displayed as pdf

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |

  @ORPHAN @ECIMT598 @P2 @TestCase15
  Scenario Outline: 17- Interior Routing can be launched from Project detail
    Given user is logged in OTBPM
    When User opens Project Details for "<project>"
    And user opens 'Interior Routing' Form
    Then 'Interior Routing Form' is displayed as pdf

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |

  @ORPHAN @ECIMT598 @P2 @TestCase16
  Scenario Outline: 16- Interior Routing button is displayed in Project Details page
    Given user is logged in OTBPM
    When user searches for "<project>"
    And User opens Project Details for "<project>"
    Then 'Interior Routing button' is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |

  @ORPHAN @ECIMT598 @P2 @TestCase17
  Scenario Outline: 18- Assign button is displayed in Project Details page for Art Director or Designer users
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    Then 'Assign button'is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |

  @ORPHAN @ECIMT598 @P3 @TestCase18
  Scenario Outline: 21- Production Tracking information shows information when expanded
    Given user is logged in OTBPM
    And user searches for "<project>"
    When User opens Project Details for "<project>"
    And user expands the 'Production Tracking information'
    Then 'Production Tracking Information Coming Soon......' text is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |

  #| 978-0-545-83652-4 |
  @ORPHAN @ECIMT598 @P3 @NotAutomated @TestCase19
  Scenario Outline: No Available barcode message is displayed for project in Project Details page
    Given user is logged in OTBPM
    When User opens Project Details for "<project>"
    And User selects the Barcode icon
    Then No Available barcode message is displayed

    Examples: 
      | project           |
      | 978-0-000-00513-7 |

  # | 978-0-590-30861-8 |
  #| 978-0-794-43317-8 |
  @ORPHAN @ECIMT598 @P3 @TestCase20
  Scenario Outline: 13- User opens New Transmittal Form from Project Details for NOT 'Editor/Mgmt/Guest' users
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    When User opens 'New Transmittal Form'
    Then 'New Transmittal Form' view is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |

  #     | 978-0-545-83652-4 |
  @ORPHAN @ECIMT598 @P3 @NotAutomated @TestCase21
  Scenario Outline: Multiple barcodes can be donwloaded  for project in Project Details page
    Given user is logged in OTBPM
    And user searches for "<project>"
    When User opens Project Details for "<project>"
    And User selects the Barcode icon
    Then list of Available barcodes are displayed in  a new window
    And the number of barcodes listed is "<barcodes_count>"

    Examples: 
      | project           | barcodes_count |
      | 978-0-439-35764-7 |              2 |

  #    | 978-0-545-44868-0 |             2 |
  #    | 978-0-545-86184-7 |             2 |
  @ORPHAN @ECIMT598 @P3 @TestCase22
  Scenario Outline: 20- Production Tracking information is displayed
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    Then 'Production tracking information' is displayed
    And 'Production tracking information' is collapsed
    And 'Production tracking information' is expandable
    And 'Production tracking information' is collapsable

    Examples: 
      | project           |
      | 978-0-545-44868-0 |

  # | 978-0-545-83652-4 |
  @ORPHAN @ECIMT598 @P2 @TestCase23
  Scenario Outline: 22- EPIC comments section is displayed
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    Then 'EPIC comments section' is displayed
    And 'EPIC comments section' is collapsed
    And 'EPIC comments section' is expandable
    And 'EPIC comments section' is collapsable

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |

  @ORPHAN @ECIMT598 @P3 @NoTransmittalForm @TestCase24
  Scenario Outline: 12- Online Transmittal Form is not displayed in Project Details for 'Editor/Mgmt/Guest' users
    Given user is logged in OTBPM as "APT_Editor_Mgmt_Guest"
    When user searches for "<project>"
    And User opens Project Details for "<project>"
    Then 'New Transmittal button' is not displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |

  @ORPHAN @ECIMT1183 @P3 @TestCase25
  Scenario Outline: 2- Fields for Assign Art Director, Cover and Interior Designer are aligned
    Given user is logged in OTBPM
    When user selects a "<PROJECT>" and go to Assign page
    Then Select fields for 'Art Director','Cover Designer' and'Interior Designer' are aligned

    Examples: 
      | PROJECT           |
      | 978-0-545-44868-0 |

  @ORPHAN @ECIMT597 @P3 @TestCase26
  Scenario Outline: 5- Autosuggest Art Director is enabled when user types on the field
    Given user is logged in OTBPM
    And user selects a "<PROJECT>" and go to Assign page
    When user types "<CHARACTER_SEQUENCE>" in "<Art Director>" field
    Then A list of 'Art Directors' containing the typed "<CHARACTER_SEQUENCE>" is displayed
    And The "<Art Director>" list is updated each time user types an  additional character
      | value   |
      | j       |
      | ja      |
      | jay     |
      | jayswal |
      | john    |

    Examples: 
      | PROJECT           | CHARACTER_SEQUENCE | Art Director                   |
      | 978-0-545-91372-0 | j                  | Provide value for Art Director |
      | 978-0-545-91372-0 | ja                 | Provide value for Art Director |
      | 978-0-545-91372-0 | jay                | Provide value for Art Director |
      | 978-0-545-91372-0 | jayswal            | Provide value for Art Director |
      | 978-0-545-91372-0 | john               | Provide value for Art Director |

  @ORPHAN @ECIMT1183 @P3 @TestCase27
  Scenario: 3- 'Other' option label should have a capitalized O.
    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    Then 'Other' label text has a capitalized "o" under the Mechs, Proof Types and Herewith sections

  @ORPHAN @ECIMT598 @P3 @TestCase29
  Scenario Outline: 1- Columns in Assign list are displayed in expected order
    Given user is logged in OTBPM
    When user selects a "<PROJECT>" and go to Assign page
    When User is in 'Assign' view
    Then The list of projects to assign include "<INFORMATION_HEADERS>" in that order

    Examples: 
      | INFORMATION_HEADERS                                                  | PROJECT           |
      | ['Title','ISBN','Art Director','Cover Designer','Interior Designer'] | 978-0-545-03517-0 |

  @ORPHAN @ECIMT510 @P2 @DisplayCircleHeadersInPdf @Testcase30
  Scenario Outline: 9- Display header for the five circles in the Interior Routing Print Form
    Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then header is displayed for the five circles on printable PDF form

    Examples: 
      | routingType      |
      #|Cover Routing|
      | Interior Routing |

  @ORPHAN @ECIMT597 @P3 @TestCase31
  Scenario Outline: 8- Autosuggest is NOT enabled when user types invalid characters
    Given user is logged in OTBPM
    And user selects a "<PROJECT>" and go to Assign page
    When user types "<CHARACTER_SEQUENCE>" in "<SELECT_FIELD>"
    Then No autosuggest list is displayed

    Examples: 
      | PROJECT           | CHARACTER_SEQUENCE | SELECT_FIELD                           |
      | 978-0-545-91372-0 | #$%                | Provide value for Art Director         |
      | 978-0-545-91372-0 |              12345 | Provide value for Art Director         |
      # | 978-0-545-91372-0 | United States      | Provide value for Art Director     |
      | 978-0-545-91372-0 | #$%                | Provide value for Cover Designer       |
      | 978-0-545-91372-0 |              12345 | Provide value for Cover Designer       |
      # | 978-0-545-91372-0 | United States      | Provide value for Cover Designer    |
      | 978-0-545-91372-0 | #$%                | Provide a value for Interior Designer. |
      | 978-0-545-91372-0 |              12345 | Provide a value for Interior Designer. |

  # | 978-0-545-91372-0 | United States      | Provide a value for Interior Designer. |
  @ORPHAN @ECIMT597 @P3 @TestCase33
  Scenario Outline: 6- Autosuggest Cover Designer is enabled when user types on the field
    Given user is logged in OTBPM
    And user selects a "<PROJECT>" and go to Assign page
    When user types "<CHARACTER_SEQUENCE>" in "<Cover Designer>" field
    Then A list of 'Designers' containing the typed "<CHARACTER_SEQUENCE>" is displayed
    And The "<Cover Designer>" list is updated each time user types an  additional character
      | value |
      | j     |
      | ja    |
      | jay   |

    Examples: 
      | PROJECT           | CHARACTER_SEQUENCE | Cover Designer                   |
      | 978-0-545-91372-0 | j                  | Provide value for Cover Designer |
      | 978-0-545-91372-0 | ja                 | Provide value for Cover Designer |
      | 978-0-545-91372-0 | jay                | Provide value for Cover Designer |
      | 978-0-545-91372-0 | jayswal            | Provide value for Cover Designer |
      | 978-0-545-91372-0 | ann                | Provide value for Cover Designer |

  @ORPHAN @ECIMT597 @P3 @TestCase34
  Scenario Outline: 7- Autosuggest Interior Designer is enabled when user types on the field
    Given user is logged in OTBPM
    And user selects a "<PROJECT>" and go to Assign page
    When user types "<CHARACTER_SEQUENCE>" in "<Interior Designer>" field
    Then A list of 'InteriorDesigners' containing the typed "<CHARACTER_SEQUENCE>" is displayed
    And The "<Interior Designer>" list is updated each time user types an  additional character
      | value |
      | j     |
      | ja    |
      | jay   |

    Examples: 
      | PROJECT           | CHARACTER_SEQUENCE | Interior Designer                      |
      | 978-0-545-91372-0 | j                  | Provide a value for Interior Designer. |
      | 978-0-545-91372-0 | ja                 | Provide a value for Interior Designer. |
      | 978-0-545-91372-0 | jay                | Provide a value for Interior Designer. |
      | 978-0-545-91372-0 | jayswal            | Provide a value for Interior Designer. |
      | 978-0-545-91372-0 | ann                | Provide a value for Interior Designer. |

  @Myproject @Testcase35
  Scenario: 3- Verify the "MyProject" label in the MyProject view is displayed completely in the MyProjects View
    #Given user is on the home page
    Given user is logged in OTBPM
    When user is in 'My Projects' view
    Then "MyProject" label is displayed completely

  @ORPHAN @ECIMT597 @AssignWithoutSelecting
  Scenario: Error display when no product selection made in My Projects or Search results table and Assign button is clicked on
    Given user is logged in OTBPM
    And user is in My Projects view or Search Results view
    When user clicks on Assign button without selecting a product
    Then an error message is displayed with the text: Select at least one product to Assign.

  @ORPHAN @ECIMT597 @NoChangesAssign
  Scenario: Error display in Assign view when no change made to the assignments
    Given user is logged in OTBPM
    And user selects a "978-0-545-91372-0" and go to Assign page
    When user clicks save without making any change to assignments
    Then an error message is displayed with the text: Select or remove an Art Director or Designer.

  @SessionPersistPaging
  Scenario Outline: 1- Validate the persistence of the Show per Page option on My Projects page
    Given user is logged in OTBPM
    And user is in 'My Projects' view
    When 'Projects per page' values is set to "<option>"
    When User logs out
    And logs in for a new session
    Then the Show per Page option should display as "<option>"

    Examples: 
      | option |
      |     50 |

  #|100	|
  #|150	|
  #|50	|
  @PagingPersistOtherPages
  Scenario Outline: 2-Validate the persistence of the Show per Page option when returning to the MyProjects Page
    Given user is logged in OTBPM
    And user is in 'My Projects' view
    When 'Projects per page' values is set to "<option>"
    And the user updates the Show per Page option to "<other_option>"
    And the user conducts any valid search
    And the user navigates to the My Project
    Then the Show per Page option should display as "<option>"

    Examples: 
      | option | other_option |
      |     50 |          100 |

  #|100	|150		|
  #|150	|50		|
  #|50	|100		|
  #Validate highlight of column header when sorting is triggered for a column, on my Projects Page
  @ColorOfSortedColumn
  Scenario Outline: Validate highlight of column header when sorting is triggered for a column, on my Projects Page
    Given user is logged in OTBPM as "APT_Art_Director"
    And user is in 'My Projects' view
    And My Project List is sorted ascending order by "<OPTION>"
    Then projected listed are sorted in ascending order by "<OPTION>"
    And the column header "<OPTION>" should be highlighted
    When user selects "<OPTION>" on the Projects list
    Then projected listed are sorted in descending order by "<OPTION>"
    And the column header "<OPTION>" should be highlighted

    Examples: 
      | OPTION |
      #  |Title|
      | Author |

  #  |illustrator|
  #  |pub date|
  #  |isbn|
  #  |file name|
  #  |trim|
  #  |# of pages|
  #  |trade price|
  #  |cover designer|
  #  |interior designer|
  #  |art director|
  #  |cover target due to mfg.|
  #  |jacket target due to mfg.|
  #  |interior target due to mfg.|
  #  |first use date|
  #Verify the "MyProject" label in the MyProject view is displayed completely in the MyProjects View
  @Myprojecttitleverify
  Scenario: Verify the "MyProject" label in the MyProject view is displayed completely in the MyProjects View
    Given user is logged in OTBPM as "APT_Art_Director"
    When user is in 'My Projects' view
    Then "My Projects" label is displayed completely

  #Verify the user can navigate to other page by editing the page details
  @EditPageDetails
  Scenario Outline: Verify the user can navigate to other page by editing the page details
    Given user is logged in OTBPM
    When user selects the page "<PAGE_NUMBER>"
    Then user will navigate to the specified page "<PAGE_NUMBER>"

    Examples: 
      | PAGE_NUMBER |
      |           2 |
