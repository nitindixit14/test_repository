Feature: Search with text 

@Search @en_US @P1 @ECIMT-1343 
Scenario Outline: Regular search using text 
	Given User is logged into OTBPM 
	When User searches using "<serchText>" 
	Then each of the results returned should contain the same "<serchText>" in any of the column 
	
	Examples:
		| serchText		|
		| bookname		|
		| isbn			|
#		| designername	|
		

@OTBPM @en_US
Scenario: Search for same keyword 
	Given User is logged into OTBPM 
	And User searches for any text 
	And User presented with serach results 
	When User searches for same text again 
	Then User should presented with the same serach results 		


@Search @OTBPM @en_US	@SearchNull	
Scenario: Search Filter Notification 
	Given User is logged into OTBPM 
	When User searches without serach text 
	Then There should be warning message to provide serach filter 
		

@Search @OTBPM @en_US @ECIMT-1344 
Scenario: Regular Search with ISBN 
	Given User is logged into OTBPM 
	When User searches for ISBN 
	Then There should be only one result with ISBN in ISBN column 
	

@OTBPM @en_US @ECIMT-1345 @ECIMT-1346
Scenario Outline: Advanced Search by option 
	Given User is logged into OTBPM 
	When User searches by "<searchBy>" with all words in Advanced Search 
	Then The result should have all projects having "<searchBy>" in "<columnHeader>" column 
	Examples: 
		| searchBy		| columnHeader		|
		| Designer		| Interior Designer |
		| Art Director	| Art Director		|
			
			