Feature: Update My Projects List with additional data elements & UI changes

@AdvancedSearchOption @Smoke595 @ECIMT595	@Smoke			
Scenario: 1- 'Advanced Search' option is available in Project List view						
       Given user is logged in OTBPM						
       When user is in 'My Projects' view						
       Then 'Advanced Search' option is displayed
       						
@AssignOption @Smoke595 @ECIMT595	@Smoke		
Scenario Outline: 2- 'Assign' option is available for 'Art Director' or 'Designer' users in Project List view						
       Given user is logged in OTBPM as "<ARTDIRECTOR_DESIGNER>"					
       When user is in 'My Projects' view						
       Then 'Assign' option is displayed only for "<ARTDIRECTOR_DESIGNER>"
       Examples:
       |ARTDIRECTOR_DESIGNER|
       |APT_Art_Director|	
       |APT_Designer|				
       	
@AssignOptionNotDisplayed @Smoke595 @ECIMT595		@Smoke				
Scenario Outline: 3- 'Assign' option is NOT available for NOT 'Art Director' or 'Designer' users in Project List view						
       Given user is logged in OTBPM as "<not_director_designer_users>"						
       When user is in 'My Projects' view						
       Then 'Assign' option is not displayed   						
       Examples:
       |not_director_designer_users|
       |APT_Production_Editor|
       |APT_Editor_Mgmt_Guest|
       |APT_Manufaturing|
       
@Assignwindow  @ECIMT595
Scenario Outline: 4- 'Art Director' or 'Designer' users can select a project to assign						
       Given user is logged in OTBPM as "<other_user>"							
       When Project List is populated						
       And user selects a project to 'Assign' 						
       Then 'Assign' window is displayed	
       Examples:
       |other_user|
       |APT_Art_Director|	
       |APT_Designer|				
       	
@AssignMultipleProjets	@ECIMT595					
Scenario Outline: 5- 'Art Director' or 'Designer' users can select multiple projects to assign						
       Given user is logged in OTBPM as "<other_user>"						
       When Project List is populated						
       When user selects more than one project 'to Assign'						
       Then 'Assign' window is displayed						
       Examples:
       |other_user|
       |APT_Art_Director|	
       |APT_Designer|
       
@VerifyBarcode @Smoke595 @ECIMT595 @Smoke
Scenario: 6- Small barcode image is displayed for products in Project List						
       Given user is logged in OTBPM						
       When Project List is populated					
       Then 'barcode small icon' is displayed for the listed projects						
						
@SelectTitle 	@ECIMT595			
Scenario: 8- User view project information detail when selecting a project by Title						
       Given user is logged in OTBPM						
       And Project List is populated						
       When user selects a project by 'Title'						
       Then 'Product Detail' window is displayed   
          						
@SelectCover	@ECIMT595			
Scenario: 9- User view project information detail when selecting a project by Cover image						
       Given user is logged in OTBPM						
       And Project List is populated						
       When user selects a project from by 'Cover Image'						
       Then 'Product Detail' window is displayed      			
       			
@TransmittalHistory	@Smoke595 @ECIMT595	@Smoke			
Scenario: 10- 'Transmittal History' is included in the project list 						
       Given user is logged in OTBPM						
       When user is in 'My Projects' view						
       Then 'Transmittal History' is displayed on the Project List	
       					
@TransmittalHistoryIcon	@ECIMT595					
Scenario: 11- User can access a project 'Transmittal History' from the Project View						
       Given user is logged in OTBPM						
       When Project List is populated						
       Then 'Transmittal History icon' is displayed for the listed projects		
       				
@TransmittalHistoryStoredTransmittals	@ECIMT595					
Scenario: 12- User views 'Transmittal History' for a project with stored transmittals						
       Given user is logged in OTBPM						
       And Project List is populated						
       When user views the 'Transmittal History' for the project						
       Then 'Transmittal History' window is displayed 					
       	
@TransmittalHistoryNoStoredTransmittals	@ECIMT595					
Scenario: 13- User view 'Transmittal History' for a project on the list with NO stored transmittals						
       Given user is logged in OTBPM						
       When Project List is populated						
       When user views the 'Transmittal History' for the project						
       Then 'Online Transmittal Form' window is displayed 			
       			
@Routing @Smoke595	@ECIMT595		@Smoke		
Scenario Outline: 14- 'Cover Routing & Interior Routing' is included in the project list 						
       Given user is logged in OTBPM						
       When user is in 'My Projects' view						
       Then "<Routing>" is displayed on the Project List	
       Examples:
       |Routing|
       |Cover Routing|	
       |Interior Routing|				
       
@CoverRoutingIcon	@ECIMT595					
Scenario Outline: 15- User can access a project 'Cover Routing & Interior Routing' from the Project View						
       Given user is logged in OTBPM						
       When Project List is populated					
       Then "<Routing_icon>" is displayed for the listed project	
        Examples:
       |Routing_icon|
       |Cover Routing|	
       |Interior Routing|					
       
@Routingpdf	@Smoke595	@Smoke			
Scenario Outline: 16- User views 'Cover Routing & Interior routing' for a project						
       Given user is logged in OTBPM						
       And Project List is populated						
       When user views the "<Routing>" for the project						
       Then "<Routing_form>" is displayed as pdf  
       	 Examples:
       |Routing			|Routing_form		|
       |Cover Routing	|Cover/Jacket/Case	|	
       |Interior Routing|Interior			|						   						

@TransmittalFormUsers @Smoke595 @ECIMT595	@Smoke				
Scenario Outline: 20- 'Transmittal Form' is included in the project list for NOT 'Editor/Mgmt/Guest' users						
       Given user is logged in OTBPM as "<not_Editor/Mgmt/Guest>"					
       When user is in 'My Projects' view						
       Then 'Transmittal Form' is displayed on the Project List		
       Examples:
       |not_Editor/Mgmt/Guest|
       |APT_Art_Director|
       |APT_Production_Editor|	
       |APT_Designer|
       |APT_Manufaturing|
            				
@NotTransmittalFormUsers @Smoke595	@ECIMT595	@Smoke				
Scenario Outline: 21- 'Transmittal Form' is NOT included in the project list for 'Editor/Mgmt/Guest' users						
       Given user is logged in OTBPM as "<user_Editor/Mgmt/Guest>"						
       When user is in 'My Projects' view						
       Then 'Transmittal Form' is NOT included on the Project List	
       Examples:
       |user_Editor/Mgmt/Guest|
       |APT_Editor_Mgmt_Guest|		
       		
@TransmittalForm @ECIMT595_2						
Scenario: 22- User can access a project 'Online Transmittal Form' from the Project View						
       Given user is logged in OTBPM						
       When Project List is populated						
       Then 'Transmittal Form icon' is displayed for the listed projects   	
       					
@TransmittalFormWindow	@ECIMT595_2				
Scenario: 23-  User views  'Online Transmittal Form' from a project						
       Given user is logged in OTBPM						
       And Project List is populated						
       When user views the 'Transmittal Form' for the project						
       Then 'Transmittal Form' window is displayed
					
@SimpleSearch 			@ECIMT595_2			
Scenario Outline: 24- Users executes a simple Search   						
       Given user is logged in OTBPM						
       When user searches for "<value>"						
       Then Project List is updated	with "<value>"					
Examples:			
|value|		
|909090909090909090909090|	
|0-545-91372-1|						
|0545913721|		
|978-0-545-91372-0|						
|9780545913720|																	
#|545|						
|SUPERMAN: THE MAN OF TOMORROW|		
#|SUPERMAN|										
#|Nandhakumar Shanmugam|
					
@ShowMyTeamsProject	@Smoke595	@ECIMT595_2		@Smoke
Scenario Outline: 25- 'Show My Teams Project' link is displayed for 'Art Director' users 						
        Given user is logged in OTBPM as "<Art_Director>"					
       When user is in 'My Projects' view						
       Then 'Show My Teams Project' option is displayed	
       Examples:
       |Art_Director|
       |APT_Art_Director|
       					
@DisableShowMyTeamsProject	@Smoke595	@ECIMT595_2	@Smoke			
Scenario Outline: 26- 'Show My Teams Project' link is NOT displayed for NOT 'Art Director' users 						
       Given user is logged in OTBPM as "<Not_Art_Director>"						
       When user is in 'My Projects' view						
       Then 'Show My Teams Project' link is not displayed	
       Examples:
       |Not_Art_Director|	
       |APT_Designer|					 						
       |APT_Production_Editor|
       |APT_Editor_Mgmt_Guest|
       |APT_Manufaturing|
       
@ChangeMyTeamToMyProjects	@ECIMT595_2				
Scenario Outline: 27- 'Art Director' user can change to 'My Team Projects' and back to 'My Projects'  						
      Given user is logged in OTBPM as "<Art_Director>"	
       When user selects 'Show My Team Project'					
      Then 'Show My Team Project' link is not displayed						
    And 'Show My Projects' link is displayed	
    Examples:
       |Art_Director|
       |APT_Art_Director|
       					
@ChangeMyProjectsToMyTeam	@ECIMT595_2				
Scenario Outline: 28- 'Art Director' user can change to 'My Projects' and back to 'My Team Projects'  						
       Given user is logged in OTBPM as "<Art_Director>"							
       When user selects 'Show My Projects'						
    Then 'Show My Projects' link is not displayed						
       And 'Show My Teams Project' link is displayed	
        Examples:
       |Art_Director|
       |APT_Art_Director|					

@IsbnDisplay @Smoke595	@ECIMT595_2		@Smoke			
Scenario: 29- Project 'ISBN 13' is displayed for listed projects as ISBN						
       Given user is logged in OTBPM						
       When Project List is populated						
       Then project 'ISBN 13' is displayed as ISBN						
				
@ProjectListDefaultView	@ECIMT595_2					
Scenario Outline: 30 Project List default view with hidden data 						
       Given user is logged in OTBPM						
       When user is in 'My Projects' view	
       And My Projects grid view is set to default 					
       Then "<column_name>" is displayed in the Project List							
       Examples:
       |column_name|
       |['Checkboxes', 'Cover', 'Barcode','Title', 'Author', 'Illustrator', 'Pub Date', 'ISBN', 'File Name', 'Trim','# of Pages', 'Trade Price', 'Cover Designer', 'Interior Designer','Art Director', 'Transmittal History', 'Cover Routing', 'Interior Routing','Transmittal Form', 'Cover Target Due to Mfg.', 'Jacket Target Due to Mfg.', 'Interior Target Due to Mfg.', 'First Use Date']|
                
       					
@TargetDuetoMfg @Smoke595 @ECIMT595_2 @Smoke
Scenario Outline: 31- 'Target Due to Mfg' is included in the project list 						
       Given user is logged in OTBPM						
       When user is in 'My Projects' view						
       Then "<target>" target is displayed on the Project List	
       Examples:
       |target|
       |Cover Target Due to Mfg.|
       |Jacket Target Due to Mfg.|
       |Interior Target Due to Mfg.|									