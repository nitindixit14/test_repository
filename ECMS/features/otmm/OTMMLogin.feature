Feature: OTMM Login
	Proves 
	- OTMM login
	- OTDS authentication
	- AD integration with OTMM
	- DB availability

@Login @en_US @SMOKE @C1
Scenario: Log into Search ELB OTMM URL 
	Given User is on OTMM sing in page
	When user signing in to OTMM using tester AD credential
	Then Login Succeeds 
