/**
 * 
 */
package com.scholastic.cucumber.otmm.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;

/**
 * @author chirag.jayswal
 *
 */
public class OTMMSignInPage extends BaseTestPage<TestPage> {
	
	@FindBy(locator = "otmm.username")
	private WebElement userNameInputBox;

	@FindBy(locator = "otmm.password")
	private WebElement passwordInputBox;

	@FindBy(locator = "otmm.signin")
	private WebElement signInButton;
	
	@Override
	protected void openPage() {
		getDriver().get(getContext().getString("otmm.url"));
	}
	
	public void signIn(String userID, String password) {
		userNameInputBox.sendKeys(userID);
		passwordInputBox.sendKeys(password);

		signInButton.click();
		//return new UserHomePage();
	}

}
