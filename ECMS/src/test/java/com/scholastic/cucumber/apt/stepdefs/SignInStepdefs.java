package com.scholastic.cucumber.apt.stepdefs;

import static com.scholastic.torque.common.AssertUtils.assertDisplayed;
import static com.scholastic.torque.common.AssertUtils.assertTextMatches;
import static com.scholastic.torque.common.StringMatchers.containsIgnoreCase;
import static com.scholastic.torque.common.TestBaseProvider.getTestBase;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.data.model.User;
import com.scholastic.cucumber.apt.data.model.UserHelper;
import com.scholastic.cucumber.apt.pageobjects.HeaderSection;
import com.scholastic.cucumber.apt.pageobjects.HeaderSection.APTMenu;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.cucumber.apt.pageobjects.SignOutPage;
import com.scholastic.cucumber.apt.pageobjects.UserHomePage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SignInStepdefs{

	@Given("^A valid user$")
	public void a_Valid_User() throws Throwable {
		OTBPMSigninPage signinPage = new OTBPMSigninPage();
		signinPage.launchPage();
	}

	@When("^user signing in$")
	public void user_Signing_in() throws Throwable {
		OTBPMSigninPage signinPage = new OTBPMSigninPage();
		// signinPage.launchPage();
		signinPage.signIn(getTestBase().getString("userID"), getTestBase().getString("password"));
	}

	@Then("^user should be taken to dashboard$")
	public void user_Should_Be_Taken_To_Dashboard() {
		UserHomePage userHomePage = new UserHomePage();
		assertDisplayed(userHomePage.getPageInfoLabels());

		assertTextMatches(userHomePage.getPageInfoLabels(), containsIgnoreCase("My Projects"));

	}

	@Then("^user name should displayed in header$")
	public void user_name_should_displayed_in_header() {
		UserHomePage userHomePage = new UserHomePage();
		WaitUtils.waitForDisplayed(userHomePage.getUserName());
		assertDisplayed(userHomePage.getUserName());
		String firstName = getTestBase().getString("fullname.firstname");
		String lastName = getTestBase().getString("fullname.lastname");
		assertTextMatches(userHomePage.getUserName(), equalToIgnoringCase(firstName + " " + lastName));

	}

	@When("^user sign out$")
	public void user_Sign_Out() {
		UserHomePage userHomePage = new UserHomePage();
		WebElement logOutButton = userHomePage.getLogOut();
		WebDriver driver = getTestBase().getDriver();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", logOutButton);
	}

	@Then("^user signed out$")
	public void user_signed_out() {
		SignOutPage signOutPage = new SignOutPage();
		assertDisplayed(signOutPage.getGoToSignInPageButton());
	}

	@SuppressWarnings("unchecked")
	@Given("^user with \"([^\"]*)\" role$")
	public void user_with_role(String roleStr) throws Throwable {
		// String user =
		// getTestBase().getString(role.toLowerCase().trim()+".user.name");
		List<String> roles = new Gson().fromJson(roleStr, List.class);
		User user = UserHelper.getUserWithRoles(roles);
		if (null == user) {
			throw new PendingException("Required data for user with role [" + roleStr + "]");
		}
	}

	@SuppressWarnings("unchecked")
	@When("^user with \"([^\"]*)\" role signing in$")
	public void user_with_role_signing_in(String roleStr) throws Throwable {
		// Configuration user =
		// getTestBase().getContext().subset(role.toLowerCase().trim());
		List<String> roles = new Gson().fromJson(roleStr, List.class);
		User user = UserHelper.getUserWithRoles(roles);
		OTBPMSigninPage signinPage = new OTBPMSigninPage();
		signinPage.launchPage();
		signinPage.signIn(user.getName(), user.getPassword());
	}

	@Then("^Art Production Tracking Menu should be avialable$")
	public void art_production_tracking_menu_should_be_avialable() {
		HeaderSection headerSection = new HeaderSection();
		headerSection.waitForPresent();
		WaitUtils.waitForDisplayed(headerSection.getWrappedElement());
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loadHolder")));
		try {
			APTMenu.APT.waitForPresent();
		} catch (Exception e) {
			throw new AssertionError("Expected "+ APTMenu.APT.getTitle() + " should be present on page");
		}
		WebElement aptLink = APTMenu.APT.getElement();
		assertThat("Art production menu on page", aptLink, notNullValue());
		assertDisplayed(aptLink);
	}

	@SuppressWarnings("unchecked")
	@Then("^user should have access to \"([^\"]*)\"$")
	public void user_should_have_access_to_apt_menu(String s) throws InterruptedException {
		UserHomePage userHomePage = new UserHomePage();
		List<String> subMenus = new Gson().fromJson(s, List.class);

		// ensure each menu item present, don't see navigation as of now to do
		// more validation
		for (int i = 0; i < subMenus.size(); i++) {
			String subMenu = subMenus.get(i);
			WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loadHolder")));
			Thread.sleep(1000);
			WebElement aptLink = APTMenu.title(subMenu).getElement();
			assertThat(subMenu + " menu on page", aptLink, notNullValue());
			assertDisplayed(aptLink);
		}

		// ensure it is no more than expected, if it is less get failed from
		// above assertions
		assertThat("Art production Traking accesible sub menus to user", APTMenu.APT.getSubMenus().size(),
				equalTo(subMenus.size()));

		// ensure the navigation
		for (int i = 0; i < subMenus.size(); i++) {
			String subMenu = subMenus.get(i);
			APTMenu aptSubMenu = APTMenu.title(subMenu);
			aptSubMenu.open();

			// ensure page
			assertDisplayed(userHomePage.getPageInfoLabels());
			assertTextMatches(userHomePage.getPageInfoLabels(), containsIgnoreCase(aptSubMenu.getTitle()));

			// double check menu items at this page as well (this is additional
			// check)
			assertThat("Art production Traking accesible sub menus to user", APTMenu.APT.getSubMenus().size(),
					equalTo(subMenus.size()));
		}
	}

	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" menus should be available$")
	public void should_be_available(String menuStr) throws Throwable {
		List<String> menus = new Gson().fromJson(menuStr, List.class);

		for (String menu : menus) {
			try {
				APTMenu.title(menu).waitForPresent();
			} catch (Exception e) {
				throw new AssertionError("Expected " + menu + " should be present on page");
			}

			WebElement menuEle = APTMenu.title(menu).getElement();
			assertDisplayed(menuEle);
		}
	}

	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" menus should not be available$")
	public void should_not_be_available(String menuStr) throws Throwable {
		List<String> menus = new Gson().fromJson(menuStr, List.class);
		for (String menu : menus) {
			WebElement menuEle = APTMenu.title(menu).getElement();
			assertThat(menu + " menu on page", menuEle, nullValue());
		}
	}
	
	@When("^user can view the menu$")
	public void user_can_view_the_menu() throws Throwable {
	    WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	    driver.switchTo().defaultContent();
	    UserHomePage userHomePage = new UserHomePage();
	}

}
