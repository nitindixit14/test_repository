package com.scholastic.cucumber.apt.pageobjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.support.FindBy;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.Keys.TransmittalFormLocators;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;
import com.scholastic.torque.webdriver.ExtendedElement;

public class TransmittalForm extends BaseTestPage<TestPage> implements TransmittalFormLocators{

	@FindBy(locator = TRANSMITTAL_FORM_SUBMIT_BUTTON)
	private WebElement submit;
	
	@FindBy(locator = TRANSMITTAL_FORM_FILES_POSTED_INPUT)
	private WebElement filesPostedToInput;
	
	@FindBy(locator = TRANSMITTAL_FORM_CHECKBOXES)
	private WebElement transmittalFormCheckBoxes;
	
	@FindBy(locator = TRANSMITTAL_FORM_CANCEL_BUTTON)
	private WebElement cancel;
	
	@FindBy(locator = TRANSMITTAL_FORM_HEREWITH_OTHER_CHECKBOX)
	private WebElement herewithOtherCheckbox;
	
	@FindBy(locator = TRANSMITTAL_FORM_PROOFTYPE_OTHER_CHECKBOX)
	private WebElement proofTypeOtherCheckbox;
	
	@FindBy(locator = PROOFTYPE_OTHER_INPUT)
	private WebElement proofTypeOtherInput;
	
	
	@FindBy(locator = HEREWITH_OTHER_INPUT)
	private ExtendedElement herewithOtherInput;
	
	@FindBy(locator = TRANSMITTAL_FORM_SPECIAL_INSTRUCTIONS_INPUT)
	private WebElement specialInstructionsInput;
	
	@FindBy(locator= TRANSMITTAL_FORM_MECH_OTHER)
	private WebElement formMechOther;
	
	@FindBy(locator= INFORMATIONBOX_CONTAINER)
	private WebElement informationbox;
	
	@FindBy(locator= MECH_OTHER_INPUT)
	private WebElement formMechOtherInput;
	
	
	@FindBy(locator= FORM_UPDATE_BUTTON)
	private WebElement formUpdateBtn;
	
	@FindBy(locator= LIST_COLORS_INPUT)
	private WebElement listcolorInput;
	
	@FindBy(locator= LIST_MATCHED_INKS_INPUT)
	private WebElement listmatchedinksinput;
	
	@FindBy(locator= FILES_POSTED_TO_INPUT)
	private WebElement filespostedtoinput;
	
	@FindBy(locator= SPECIAL_INSTRUCTION_INPUT)
	private WebElement specialinstructioninput;
	
	public WebElement getSpecialinstructioninput() {
		return specialinstructioninput;
	}

	public WebElement getListcolorInput() {
		return listcolorInput;
	}

	public WebElement getListmatchedinksinput() {
		return listmatchedinksinput;
	}

	public WebElement getFilespostedtoinput() {
		return filespostedtoinput;
	}

	public WebElement formPopupButtons(String btnName)
	{
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String xpathExpression=String.format(FORM_POPUP_BUTTONS,btnName);
	return	driver.findElement(LocatorUtils.getBy(xpathExpression));
	}
	

	public WebElement getCancelButton()
	{
		return cancel;
	}
	
	public void clickFormpopUp(String label)
	{
		
	switchToTransmittalForm();
	WaitUtils.waitForDisplayed(formPopupButtons(label));
		formPopupButtons(label).click();
	}
	
	public WebElement getFormUpdateBtn() {
		return formUpdateBtn;
	}
	
	
	public WebElement getFormMechOtherInput() {
		return formMechOtherInput;
	}
	
	public WebElement getInformationbox()
	{
		return informationbox;
	}

	public WebElement getFormMechOther()
	{
		return formMechOther;
	}
	
	public WebElement getSpecialInstructionsInput()
	{
		return specialInstructionsInput;
	}
	
	
	public WebElement getProofTypeOtherCheckbox()
	{
		return proofTypeOtherCheckbox;
	}
	
	public WebElement getProofTypeOtherInput()
	{
		return proofTypeOtherInput;
	}
	
	
	public WebElement getHerewithOtherCheckbox()
	{
		return herewithOtherCheckbox;
	}
	
	public ExtendedElement getHerewithOtherInput()
	{
		return herewithOtherInput;
	}
	
	public void getAdditionalDropdown(String option)
	{
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.findElement(By.xpath(".//*[@id='additionalTypeSelect']/div[2]/input")).click();
		driver.switchTo().defaultContent();
		for(int i=1;i<=14;i++)
		{
			WebElement elm=driver.findElement(By.xpath(String.format(".//*[@id='___eventCaptureLayer']/div/div[%d]", i)));
			if(elm.getText().equals(option))
			{
				elm.click();
				break;
			}
		}
	}
	
	public WebElement getTransmittalFormButtons(String label)
	{
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String xpathExpression=String.format(TRANSMITTAL_FORM_CHECKBOXES,label);
	return	driver.findElement(LocatorUtils.getBy(xpathExpression));
	}
	
	public WebElement getTransmittalFormTextFields(String label)
	{
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String xpathExpression=String.format(TRANSMITTAL_FORM_TEXTFIELDS_INPUT,label);
	return	driver.findElement(LocatorUtils.getBy(xpathExpression));
	}
	
	public WebElement getLabel(String label) {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String xpathExpression=String.format(TRANSMITTAL_FORM_ALL_LABELS,label);
	return	driver.findElement(LocatorUtils.getBy(xpathExpression));
	}
	
	public WebElement getSubmitButton()
	{
		return submit;
	}
	
	public WebElement getFilesPostedToInput() {
		return filesPostedToInput;
	}
	
	public WebElement getValueElement(String lable) {
		String xpath = String.format(VALUE_ELEMENT, lable);
		WaitUtils.waitForPresent(getDriver(), By.xpath(xpath));
		return TestBaseProvider.getTestBase().getDriver().findElement(By.xpath(xpath));
	}
	
	public Row findallAssignments()
	{
		MyProjectsPage myprojects=new MyProjectsPage();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		int i=0;
		Row row1 = null;
		 boolean navNextPage = false;
			do {
				if (navNextPage) {
					myprojects.getNextButton().click();
					myprojects.waitForResultsLoadingComplete();
				}
		   for (Row row:itemgrid.getRows()) {
			String coverDesignerName=itemgrid.getRows().get(i).getColumnText("Cover Designer").trim();
			String interiorDesignerName=itemgrid.getRows().get(i).getColumnText("Interior Designer").trim();
			String artDirectorName=itemgrid.getRows().get(i).getColumnText("Art Director").trim();
			
			if(coverDesignerName.equals(interiorDesignerName) && interiorDesignerName.equals(artDirectorName))
			{
				row1=row;
				return row;
				
			}
			else
			{
				continue;
					}
		   }
			navNextPage = true;
			} while (myprojects.getNextButton().isEnabled());
		
			
			return row1;
			
	}
	
	public Dialog getDialog() {
		return new Dialog(APP_COMMON_DIALOG);
	}
	
	public void switchToTransmittalForm()
	{
		getDialog();
		WaitUtils.waitForDisplayed(getDialog().getDialogTitleElement());
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().frame(0);
	}
	
	public ArrayList<String> splitAndSortString(String values)
	{
		values=values.replaceAll("\\s+","");
		String[] arrValues=values.split(",",values.length() );
		ArrayList<String> selectedOptions=new ArrayList<>();
		for(int i=1;i<arrValues.length;i++)
		{
			selectedOptions.add(arrValues[i]);
		}
		Collections.sort(selectedOptions);
		return selectedOptions;
	}
	
	public void fillTransmittalForm(String fields) {
		TransmittalForm transmittals=new TransmittalForm();
		   List<String> items = new Gson().fromJson(fields, List.class);
			for (String item : items) { 	  
		   if(item.equals("OtherHereWith"))
		   {
			   transmittals.getHerewithOtherCheckbox().click();
			   WaitUtils.waitForDisplayed(transmittals.getHerewithOtherInput());
			   transmittals.getHerewithOtherInput().clear();
			   String herewithInput="otherHereWith";
			   transmittals.getHerewithOtherInput().sendKeys(herewithInput);
			   getContext().setProperty("herewith.input", herewithInput);
		   }
		   
		   else  if(item.equals("OtherProofType"))
		   {
			   transmittals.getProofTypeOtherCheckbox().click();
			   WaitUtils.waitForDisplayed(transmittals.getProofTypeOtherInput());
			   transmittals.getProofTypeOtherInput().clear();
			   String proofInput="otherProof";
			   transmittals.getProofTypeOtherInput().sendKeys(proofInput);
			   getContext().setProperty("prooftypes.input", proofInput);
		   }
		   
		   else if(item.contains("Files"))
			{
				String filesText=getContext().getString("transmittal.form.filesPosted.input");
				 transmittals.getFilesPostedToInput().clear();
				 transmittals.getFilesPostedToInput().sendKeys(filesText);
				 getContext().setProperty("files.posted.to", filesText);
			}
		   
		   else{
			 transmittals.getTransmittalFormButtons(item).click();
		   }
			}
	}

	@Override
	protected void openPage() {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
