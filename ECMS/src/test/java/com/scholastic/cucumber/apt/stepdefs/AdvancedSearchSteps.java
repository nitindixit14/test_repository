package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.AdvancedSearchPage;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AdvancedSearchSteps {

	@Given("^the user is on the Advanced Search page$")
	public void the_user_is_on_the_Advanced_Search_page() throws Throwable {
		Thread.sleep(1000);
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.clickAdvancedSearchLink();
	    AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
	    advancedSearch.switchToAdvSearchDialog();
	    
	}
	
	@And("^\"(.*)\" has Title/AD/Designer/author/pubdate \"(.*)\"$")
	public void has_ISBN_Title_AD_Designer_author_pubdate(String isbn, String values)
			throws ClassNotFoundException, SQLException {
		String DB_Designer = null;
		String DB_Author = null;
		String DB_Title = null;
		String DB_Publication = null;
		String DB_ArtDirector = null;
		String title = values.split(",")[0];
		String AD = values.split(",")[1];
		String Designer = values.split(",")[2];
		String author = values.split(",")[3];
		String pubDate = values.split(",")[4];

		ResultSet rs = DBConnection.dbConnection(
				"select DESIGNER, AUTHOR,TITLE_NAME,PUBLICATION_DATE,ART_DIRECTOR_NAME  from APT_PROJECTS_VIEW where ISBN_13 LIKE '%978-0-439-02548-5%'");
		while (rs.next()) {
			// DB_Designer = rs.getObject(1).toString();
			DB_Author = rs.getObject(2).toString();
			DB_Title = rs.getObject(3).toString();
			DB_Publication = rs.getObject(4).toString();
			DB_ArtDirector = rs.getObject(5).toString();
		}
		// Assert.assertTrue(DB_Designer.contains(Designer));
		Assert.assertTrue(DB_Author.contains(author));
		Assert.assertTrue(DB_Title.toUpperCase().contains(title.toUpperCase()));
		Assert.assertTrue(DB_Publication.contains(pubDate));
		Assert.assertTrue(DB_ArtDirector.toUpperCase().contains(AD.toUpperCase()));

	}
	
	@And("^conducts an ISBN/Title/AD/Designer/author/pubdate search of \"(.*)\"$")
	public void conducts_an_ISBN_Title_AD_Designer_author_pubdate_search_of(String value) {
		AdvancedSearchPage advancedSearch = new AdvancedSearchPage();

		String isbn = value.split(",")[0];
		String title = value.split(",")[1];
		String AD = value.split(",")[2];
		String Designer = value.split(",")[3];
		String author = value.split(",")[4];
		String pubDate = value.split(",")[5];
		String fromDate = pubDate.split("~")[0];
		String toDate = pubDate.split("~")[1];
		advancedSearch.search("ISBN", isbn);
		advancedSearch.search("Title", title);
		advancedSearch.search("Art Director", AD);
		advancedSearch.search("Designer", Designer);
		advancedSearch.search("Author", author);
		advancedSearch.searchByDate("Pub Date", fromDate, toDate);
		advancedSearch.clickAdvSearchBtn();
	}

	@Then("^the application should not return any results$")
	public void the_application_should_not_return_any_results() {
		AdvancedSearchPage advancedSearch = new AdvancedSearchPage();
		Assert.assertTrue(advancedSearch.getErrorNoDataFound().isDisplayed());
	}

	@When("^the user conducts a \"([^\"]*)\" advanced search of \"([^\"]*)\"$")
	public void the_user_conducts_a_advanced_search_of(String label, String searchText) throws Throwable {
		 AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		 advancedSearch.search(label, searchText);
		 advancedSearch.clickAdvSearchBtn();
	}

	//Added by Nilesh
	@When("^the user conducts advanced search of multiple fields as \"([^\"]*)\" \"([^\"]*)\",\"([^\"]*)\" \"([^\"]*)\",\"([^\"]*)\" \"([^\"]*)\"$")
	 public void the_user_conducts_advanced_search_of_multiple_fields_as(String type1, String value1,String type2, String value2,String type3, String value3) throws Throwable {
	  AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
	  advancedSearch.search(type1, value1);
	  advancedSearch.search(type2, value2);
	  advancedSearch.search(type3, value3);
	  advancedSearch.clickAdvSearchBtn();
	 }
	//Added by Nilesh
	@Then ("^the returned results should contain at least one record with \"([^\"]*)\" in the \"([^\"]*)\" column,\"([^\"]*)\" in the \"([^\"]*)\" column,\"([^\"]*)\" in the \"([^\"]*)\" column$")
	  public void the_returned_result_should_contain_at_least_one_record_with(String value1,String type1,String value2,String type2,String value3,String type3) throws InterruptedException{
	   MyProjectsPage myprojects=new MyProjectsPage();
	      myprojects.waitForResultsLoadingComplete();
	      ItemGrid itemgrid=myprojects.getProjectGrid();
	      itemgrid.showAllColumns();
	      Thread.sleep(2000);
	      if(!(type1.equals(null)||type2.equals(null)||type3.equals(null)))
	      {
	       
	        ArrayList<String> columnType1=itemgrid.getColumnValues(type1);
	        Thread.sleep(2000);
	        Assert.assertTrue(columnType1.contains(value1.trim()), value1+" is not present in the "+ type1 +" column");
	        ArrayList<String> columnType2=itemgrid.getColumnValues(type2);
	        Thread.sleep(2000);
	        Assert.assertTrue(columnType2.contains(value2.trim()), value2+"is not present in the "+ type2 +" column");
	        ArrayList<String> columnType3=itemgrid.getColumnValues(type3);
	        Thread.sleep(2000);
	        Assert.assertTrue(columnType3.contains(value3.trim()), value3+"is not present in the "+ type3 +" column");
	      }else
	      {
	       Assert.fail("Blank Column Name");
	      }
	     
	  }
	
//Added by Nilesh
	
	@Then ("^the returned results should contain at least one of the \"([^\"]*)\" in the \"([^\"]*)\" column$")
	  public void the_returned_result_should_contain_at_least_one_of_the_search_values_in_the_type(String value,String type) throws InterruptedException{
	   MyProjectsPage myprojects=new MyProjectsPage();
	    AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
	      myprojects.waitForResultsLoadingComplete();
	      ItemGrid itemgrid=myprojects.getProjectGrid();
	      itemgrid.showAllColumns();
	      if(!type.equals(null))
	      {
	       ArrayList<String> columnType=itemgrid.getColumnValues(type);
	       
	       Assert.assertTrue(columnType.contains(value.split(",")[0]), value.split(",")[0] + "is not present in the "+ type +" column");
	       Assert.assertTrue(columnType.contains(value.split(",")[1]), value.split(",")[1] + "is not present in the "+ type +" column");
	      }
	      else{
	       Assert.fail("Blank Column Name");
	      }
	  }
	
	@Then("^the returned results should contain all of the search values \"([^\"]*)\" in the \"([^\"]*)\" column$")
	public void the_returned_results_should_contain_all_of_the_search_values_in_the_column(String searchText, String column) throws Throwable {
		  MyProjectsPage myprojects=new MyProjectsPage();
		  AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    if(column.equals("Designer"))
		    {
		    	ArrayList<String> designers=itemgrid.getdesignerValues("Cover Designer");
	    		ArrayList<String> interiorDesigner=itemgrid.getdesignerValues("Interior Designer");
	    		designers.addAll(interiorDesigner);
		    	for(String designer:designers)
		    	{
		    		Assert.assertTrue(designer.toUpperCase().contains(searchText.toUpperCase().trim()));
		    	}
		    }
		    
		    else if(column.equals("Editor"))
		    {
		    	for(String text:advancedSearch.getEditorValue("Editor"))
		    	{
		    		Assert.assertTrue(text.toUpperCase().contains(searchText.toUpperCase().trim()));
		    	}
		    }
		    
		    else
		    { 
		    ArrayList<String> completeColumnText=itemgrid.allColumnsText(column);
		    String[] searchkeys=searchText.split(",");
		  		    for(String rowText:completeColumnText)
		    {
		    	for (String searchValue:searchkeys) {
					Assert.assertTrue(rowText.toUpperCase().contains(searchValue.toUpperCase().trim()));
		    	}
		  	}
		    }
	}

	@When("^the user conducts a \"([^\"]*)\" advanced search for fifty isbns \"([^\"]*)\"$")
	public void the_user_conducts_a_advanced_search_for_fifty_isbns(String label, String searchText) throws Throwable {
		 AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		 String searchValues=TestBaseProvider.getTestBase().getString("advancedSearch.isbns."+searchText);
		 advancedSearch.search(label, searchValues);
		 advancedSearch.clickAdvSearchBtn();
		 TestBaseProvider.getTestBase().getContext().setProperty("search.50isbns", searchValues);
	}

	@Then("^all the returned titles should all an ISBN from \"([^\"]*)\"$")
	public void all_the_returned_titles_should_all_an_ISBN_from(String isbn13Values) throws Throwable {
		String isbns=TestBaseProvider.getTestBase().getString("advancedSearch.isbns."+isbn13Values);
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		ArrayList<String> projectUIisbns=itemgrid.allColumnsText("ISBN");
		ArrayList<String> isbn13=new ArrayList<>();
		String[] isbn=isbns.split(" ");
		for (String string : isbn) {
			isbn13.add(string);
		}
		Collections.sort(projectUIisbns);
		Collections.sort(isbn13);
		Assert.assertEquals(projectUIisbns, isbn13);
	}
	
	@Then("^the application should present an error message$")
	public void the_application_should_present_an_error_message() throws Throwable {
		 AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		 Assert.assertTrue(advancedSearch.getErrorMessages().size()>0);
	}
	
	@Then("^results displayed on the Search Results for \"([^\"]*)\" should match the results from the query results of \"([^\"]*)\" with \"([^\"]*)\"$")
	public void results_displayed_on_the_Search_Results_for_should_match_the_results_from_the_query_results_of_with(String column, String query, String dbName) throws Throwable {
	    	 MyProjectsPage myprojects=new MyProjectsPage();
		  AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    DBConnection dbcon=new DBConnection();
		    ResultSet rs= dbcon.dbConnection(query);
		    ArrayList<String> dbvalues=new ArrayList<>();
		    ArrayList<String> completeColumnText=new ArrayList<>();
		    if(dbName.equals("DESIGNER"))
		    {
		    	ArrayList<String> designers=itemgrid.getdesignerValues("Cover Designer");
	    		ArrayList<String> interiorDesigner=itemgrid.getdesignerValues("Interior Designer");
	    		designers.addAll(interiorDesigner);
	    		while(rs.next())
	    		{
	    			dbvalues.add(rs.getString("COVER_DESIGNER_NAME"));
	    			dbvalues.add(rs.getString("INTERIOR_DESIGNER_NAME"));
	    		}
	    		dbvalues.removeAll(Collections.singleton(null));
	    		designers.removeAll(Collections.singleton(""));
	    		Assert.assertEquals(designers, dbvalues);
		    }
		    else
		    {
		    	if(dbName.equals("EDITOR"))
		    	{
		    		completeColumnText=advancedSearch.getEditorValue(column);
		    	}
		    	else
		    	{
		    	completeColumnText=itemgrid.allColumnsText(column);
		    	}
		    	while(rs.next())
	    		{
		    		dbvalues.add(rs.getString(dbName));
	    		}
		    	Assert.assertEquals(completeColumnText, dbvalues);
		    }  
	}
	
	@SuppressWarnings("static-access")
	@Then("^'Search Results' in \"([^\"]*)\" view displays matching results for any of the following database fields \"([^\"]*)\"$")
	public void search_Results_in_view_displays_matching_results_for_any_of_the_following_database_fields(String tableLabels, String databaseFields) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		  AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		String fromDate = (String) TestBaseProvider.getTestBase().getContext().getProperty("from.date");
		String toDate = (String) TestBaseProvider.getTestBase().getContext().getProperty("to.date");
		ArrayList<String> labelsText=new ArrayList<>();
		ArrayList<String> databaseResults=new ArrayList<>();
		List<String> items = new Gson().fromJson(tableLabels, List.class);
		List<String> dbitems = new Gson().fromJson(databaseFields, List.class);
		for (String item : items) {
			if(item.equals("In Store"))
			{
				ArrayList<String>dates=advancedSearch.getEditorValue("In Store");
				for(String date:dates)
				{
					labelsText.add(myprojects.setFormatFromYY(date));
				}
			}
			else{
				labelsText.addAll(itemgrid.allColumnsText(item));
			}
		}
		DBConnection dbcon=new DBConnection();
		String query="";
		if(tableLabels.contains("Target"))
		{
			query="select * from APT_PROJECTS_VIEW " + "where "
					+ "(COVER_TARGET_DUE_MFG >=  '" + fromDate
					+ "' and COVER_TARGET_DUE_MFG < ='" + toDate + "' ) " + 
					"OR (JACKET_TARGET_DUE_MFG >=  '" + fromDate + "' and"
							+ " JACKET_TARGET_DUE_MFG < = '" + toDate
					+ "' )" + "OR (INTERIOR_TARGET_DUE_MFG >=  '" + fromDate + "' and INTERIOR_TARGET_DUE_MFG < = '" + toDate + "' )";
		}
		else
		{
			for (String dbItem : dbitems) {
				query="select * from APT_PROJECTS_VIEW " + "where "+ dbItem +" >='" + fromDate+"' and "+dbItem+" <= '" + toDate + "'";
			}
		}
		ResultSet rs=dbcon.dbConnection(query);
		
		while (rs.next()) {
			for (String dbItem : dbitems) {
					String date = rs.getString(dbItem);
					databaseResults.add(myprojects.changeDateFormat(date));
				} 
			}
		
		Collections.sort(labelsText);
		Collections.sort(databaseResults);
		System.out.println("advanced searchUI by firstusedate:"+labelsText);
		System.out.println("advanced searchDB by firstusedate:"+databaseResults);
		Assert.assertEquals(labelsText, databaseResults);
		
		}
	
	@Then("^the returned results should contain at least one of the search values in the \"([^\"]*)\" column$")
	public void the_returned_results_should_contain_at_least_one_of_the_search_values_in_the_column(String arg1) throws Throwable {
	    
		  MyProjectsPage myprojects=new MyProjectsPage();
		  //AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    itemgrid.showAllColumns();
		    Thread.sleep(6000);
		    
		    WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		    driver.switchTo().defaultContent();
		    driver.switchTo().frame("display_frame");
		    
		//String[] Expected={"John Williams",""};
		String  S=  driver.findElement(By.xpath("//*[@id='tbmyProjects']/tbody/tr[2]/td[19]/div/input")).getAttribute("value");
		Assert.assertTrue(S.contains("John Williams"),"FAIL-Test didn't match");
		
		
	}

	
}
