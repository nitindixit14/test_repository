package com.scholastic.cucumber.apt.pageobjects;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.torque.common.Section;
import com.torque.automation.core.TestDataUtils;

public class Dialog extends Section {

	public Dialog(String loc) {
		super(loc);
	}

	@FindBy(locator = "dialog.title")
	private WebElement dialogtitle;

	public WebElement getDialogTitleElement() {
		return dialogtitle;
	}

	public void switchToDialog() {
		try {
			TestDataUtils.getDriver().switchTo().frame(0);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
