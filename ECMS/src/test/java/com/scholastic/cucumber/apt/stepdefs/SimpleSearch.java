package com.scholastic.cucumber.apt.stepdefs;

import static org.hamcrest.MatcherAssert.assertThat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import org.apache.log4j.helpers.DateTimeDateFormat;
import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.components.ItemList;
import com.scholastic.cucumber.apt.pageobjects.AdvanceSearchDialog;
import com.scholastic.cucumber.apt.pageobjects.AdvancedSearchPage;
import com.scholastic.cucumber.apt.pageobjects.HeaderSection;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.UserHomePage;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SimpleSearch {

	@When("^User searches for a \"([^\"]*)\" in My Projects view$")
	public void user_searches_for_a_in_My_Projects_view(String searchText) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.searchFor(searchText);
	}

	@Then("^Search results matches \"([^\"]*)\" for at least one project field that displays text or numeric value$")
	public void search_results_matches_for_at_least_one_project_field_that_displays_text_or_numeric_value(
			String textToMatch) throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		boolean navNextPage = false;
		do {
			if (navNextPage) {
				projectsPage.getNextButton().click();
				projectsPage.waitForResultsLoadingComplete();
			}
			ItemGrid results = projectsPage.getProjectGrid();

			for (Row row : results.getRows()) {
				String rowText = row.getText();

				assertThat(rowText.toUpperCase(), Matchers.containsString(textToMatch.toUpperCase()));
			}
			navNextPage = true;
		} while (projectsPage.getNextButton().isEnabled());

	}

	@Then("^the count of search results should be greaeter or equal to \"([^\"]*)\"$")
	public void the_count_of_search_results_should_be_greaeter_or_equal_to(String sqlQuery) throws Throwable {
		int expected_count = 0;
		ResultSet rs3 = DBConnection.dbConnection(sqlQuery);
		while (rs3.next()) {
			expected_count = rs3.getInt("TOTAL_COUNT");
		}
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int actual_count = Integer.parseInt(myprojects.getLastProjectNum());
		Assert.assertTrue(actual_count >= expected_count);
	}

	@Then("^No projects are displayed for fake Search Results$")
	public void no_projects_are_displayed_for_fake_Search_Results() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		Assert.assertTrue(itemgrid.getRows().isEmpty());
	}

	@Then("^Page Title is displayed as 'Search Results'$")
	public void page_Title_is_displayed_as_Search_Results() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		Assert.assertEquals(myprojects.getMyProjectsTitle().getText(), "Search Results");
	}

	@When("^User searches for a \"([^\"]*)\" in Search Results view$")
	public void user_searches_for_a_in_Search_Results_view(String searchText) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.searchFor(searchText);
		myprojects.searchFor(searchText);
	}

	@Then("^'Show Only My Projects' link is NOT displayed$")
	public void show_Only_My_Projects_link_is_NOT_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		AssertUtils.assertNotDisplayed(myprojects.getshowTeamProjLink());
	}

	@Then("^'Show My Teams Projects' link is NOT displayed$")
	public void show_My_Teams_Projects_link_is_NOT_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		AssertUtils.assertNotDisplayed(myprojects.getshowTeamProjLink());
	}

	@Then("^Breadcrumbs is displayed as 'Scholastic> My Projects'$")
	public void breadcrumbs_is_displayed_as_Scholastic_My_Projects() throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		WebElement breadcrumb = driver.findElement(By.xpath(".//*[@id='text1']/div/div/div[3]"));
		AssertUtils.assertDisplayed(breadcrumb);
	}

	@When("^User selects 'My Projects' breadcrumb$")
	public void user_selects_My_Projects_breadcrumb() throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		HeaderSection headerSection = new HeaderSection();
		headerSection.getMyProjectsBreadCrumb().click();
	}

	@Then("^My Projects view is displayed$")
	public void my_Projects_view_is_displayed() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		Assert.assertEquals("My Projects", myProjects.getMyProjectsTitle().getText());
	}

	@Then("^project 'Pub Date' is displayed as \\(MM/DD/YYYY\\)$")
	public void project_Pub_Date_is_displayed_as_MM_DD_YYYY() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		String dateRegex = "^(0[1-9]|1[0-2])\\/(0[1-9]|1\\d|2\\d|3[01])\\/(19|20)\\d{2}$";
		ArrayList<String> pubDates = itemgrid.allColumnsText("Pub Date");
		for (int i = 0; i < pubDates.size(); i++) {
			if ((pubDates.get(i).toString() != null) && (!"".equals(pubDates.get(i).toString()))) {
				Pattern pattern = Pattern.compile(dateRegex);
				Matcher matcher = pattern.matcher(pubDates.get(i));
				Assert.assertTrue(matcher.matches());
			}
		}
	}

	@Then("^project 'First Use Date' is displayed as \\(MM/DD/YYYY\\)$")
	public void project_First_Use_Date_is_displayed_as_MM_DD_YYYY() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		String dateRegex = "^(0[1-9]|1[0-2])\\/(0[1-9]|1\\d|2\\d|3[01])\\/(19|20)\\d{2}$";
		ArrayList<String> firstUseDate = itemgrid.allColumnsText("First Use Date");
		for (int i = 0; i < firstUseDate.size(); i++) {
			if ((firstUseDate.get(i).toString() != null) && (!"".equals(firstUseDate.get(i).toString()))) {
				Pattern pattern = Pattern.compile(dateRegex);
				Matcher matcher = pattern.matcher(firstUseDate.get(i));
				Assert.assertTrue(matcher.matches());
			}
		}
	}

	@When("^the user navigates to the My Projects page$")
	public void the_user_navigates_to_the_My_Projects_page() throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		HeaderSection headerSection = new HeaderSection();
		MyProjectsPage myprojects=new MyProjectsPage();
		WaitUtils.waitForDisplayed(headerSection.getMyProjectsBreadCrumb());
		myprojects.clickElementByJS(headerSection.getMyProjectsBreadCrumb());
	}

	@When("^User selects 'Advanced Search'$")
	public void user_selects_Advanced_Search() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.getAdvancedSearchLink().click();
		AdvanceSearchDialog advanceSearchDialog = myprojects.getAdvanceSearchDialog();
		advanceSearchDialog.waitForPresent();
		WaitUtils.waitForDisplayed(advanceSearchDialog.getWrappedElement());
	}

	@Then("^'Advanced Search' view is open in a new window$")
	public void advanced_Search_view_is_open_in_a_new_window() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.getAdvanceSearchDialog();
		WebElement advancedSearchDialog = myprojects.getDialog().getDialogTitleElement();
		Assert.assertEquals(advancedSearchDialog.getText(), "Advanced Search");
		myprojects.getcloseButton().click();
		myprojects.waitForResultsLoadingComplete();
	}

	@When("^User is in 'Advanced Search' view$")
	public void user_is_in_Advanced_Search_view() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		AdvanceSearchDialog advanceSearchDialog = projectsPage.getAdvanceSearchDialog();
		projectsPage.waitForResultsLoadingComplete();
		WaitUtils.waitForDisplayed(projectsPage.getAdvancedSearchLink());
		projectsPage.clickElementByJS(projectsPage.getAdvancedSearchLink());
		advanceSearchDialog.waitForPresent();
		WaitUtils.waitForDisplayed(advanceSearchDialog.getWrappedElement());
	}

	@Then("^Page Title is displayed as 'Advanced Search'$")
	public void page_Title_is_displayed_as_Advanced_Search() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.getAdvanceSearchDialog();
		WebElement advancedSearchDialog = myprojects.getDialog().getDialogTitleElement();
		Assert.assertEquals(advancedSearchDialog.getText(), "Advanced Search");
	}

	@Then("^\"([^\"]*)\" search option is displayed in the date area$")
	public void search_option_is_displayed_in_the_date_area(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		AdvanceSearchDialog advanceSearchDialog = myprojects.getAdvanceSearchDialog();
		advanceSearchDialog.switchToDialog();
		String byDateGroupBoxTitle = advanceSearchDialog.getGroupBox().getAttribute("title");
		String groupText = advanceSearchDialog.getGroupBox().getText();
		Assert.assertTrue(groupText.contains("Target Due to Mfg:"));
		Assert.assertTrue(byDateGroupBoxTitle.contains("By date"));
	}

	@SuppressWarnings("unchecked")
	@When("^User selects a \"([^\"]*)\" and \"([^\"]*)\" for \"([^\"]*)\"$")
	public void user_selects_a_and_for(String fromDate, String toDate, String label) throws Throwable {
	 	MyProjectsPage myprojects = new MyProjectsPage();
		 AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		 advancedSearch.searchByDate(label, fromDate, toDate);
		 Thread.sleep(2000);
		 advancedSearch.clickAdvSearchBtn();
		TestBaseProvider.getTestBase().getContext().setProperty("from.date", fromDate);
		TestBaseProvider.getTestBase().getContext().setProperty("to.date", toDate);
	}

	@Then("^'Advanced Search' view is dismissed$")
	public void advanced_Search_view_is_dismissed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		AssertUtils.assertNotDisplayed(myprojects.getAdvanceSearchDialog().getWrappedElement());
	}

	@Then("^'Search Results' view displays matching results for any of the following database fields \"([^\"]*)\"$")
	public void search_Results_view_displays_matching_results_for_any_of_the_following_database_fields(String label)
			throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		ArrayList<String> searchArray = new ArrayList<String>();
		searchArray.addAll(itemgrid.allColumnsText("Cover Target Due to Mfg."));
		searchArray.addAll(itemgrid.allColumnsText("Jacket Target Due to Mfg."));
		searchArray.addAll(itemgrid.allColumnsText("Interior Target Due to Mfg."));
		ArrayList<String> targetArray = dbGetTargetColumns();
		Collections.sort(searchArray);
		Collections.sort(targetArray);
		Assert.assertTrue(targetArray.equals(searchArray));
	}

	public ArrayList<String> dbGetTargetColumns() throws ClassNotFoundException, SQLException, ParseException {
		String fromDate = (String) TestBaseProvider.getTestBase().getContext().getProperty("from.date");
		String toDate = (String) TestBaseProvider.getTestBase().getContext().getProperty("to.date");
		String sqlQuery = "select * from PRODUCT_CUSTOM_INFO " + "where (COVER_TARGET_DUE_MFG between '" + fromDate
				+ "' and '" + toDate + "' ) " + "OR (JACKET_TARGET_DUE_MFG between '" + fromDate + "' and '" + toDate
				+ "' )" + "OR (INTERIOR_TARGET_DUE_MFG between '" + fromDate + "' and '" + toDate + "' )";
		ArrayList<String> datesArray = new ArrayList<String>();
		MyProjectsPage myprojects = new MyProjectsPage();
		ResultSet rs3 = DBConnection.dbConnection(sqlQuery);
		while (rs3.next()) {
			datesArray.add(rs3.getString("COVER_TARGET_DUE_MFG"));
			datesArray.add(rs3.getString("JACKET_TARGET_DUE_MFG"));
			datesArray.add(rs3.getString("INTERIOR_TARGET_DUE_MFG"));
		}
		ArrayList<String> newDates = new ArrayList<String>();
		for (String string : datesArray) {
			newDates.add(myprojects.changeDateFormat(string));
		}
		return newDates;
	}

}