package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.scholastic.cucumber.apt.components.ItemList;
import com.scholastic.cucumber.apt.pageobjects.HeaderSection;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.ManageMyTeamPage;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.UserHomePage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ManageMyTeamSteps {

	@Given("^the user selected the Show My Team’s Project link$")
	public void the_user_selected_the_Show_My_Team_s_Project_link() throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.clickShowTeamProj();
	}

	@Given("^the link reads ‘Show only my Projects’$")
	public void the_link_reads_Show_only_my_Projects() throws Throwable {
		  MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    Assert.assertEquals(myprojects.getshowTeamProjLink().getText(),"Show Only My Projects");
	}

	@Then("^the link should by default read ‘Show My Teams Projects’$")
	public void the_link_should_by_default_read_Show_My_Teams_Projects() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    Assert.assertEquals(myprojects.getshowTeamProjLink().getText(),"Show My Team's Projects");
	}
	
	@When("^the user selects the ‘Show only my Projects’ link$")
	public void the_user_selects_the_Show_only_my_Projects_link() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.clickShowTeamProj(); 
	    Thread.sleep(1000);
	}

	@Then("^the ‘Show only my Projects’ link should display as ‘Show My Team’s Projects’$")
	public void the_Show_only_my_Projects_link_should_display_as_Show_My_Team_s_Projects() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    Assert.assertEquals(myprojects.getshowTeamProjLink().getText(),"Show My Team's Projects");
	}
	
	
//	@Given("^user can see the projects$")
//	public void user_can_see_the_projects() throws Throwable {
//		 MyProjectsPage myprojects=new MyProjectsPage();
//		    myprojects.waitForResultsLoadingComplete();
//		    ItemGrid itemgrid=myprojects.getProjectGrid();
//	}
	
	@When("^the user selects Manage my Team$")
	public void the_user_selects_Manage_my_Team() throws Throwable {
	ManageMyTeamPage myteam=new ManageMyTeamPage();
	myteam.clickManageMyTeam();
	}

	@Then("^the Manage my Team view should display$")
	public void the_Manage_my_Team_view_should_display() throws Throwable {
	   MyProjectsPage myprojects=new MyProjectsPage();
	   myprojects.waitForResultsLoadingComplete();
	   WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		HeaderSection headerSection = new HeaderSection();
	   Assert.assertEquals(headerSection.getMyProjectsBreadCrumb().getText(),"Manage My Team");
	}

	@Then("^the list on the left should contain a list of all the active designers not already assign to Art Directors team$")
	public void the_list_on_the_left_should_contain_a_list_of_all_the_active_designers_not_already_assign_to_Art_Directors_team() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ManageMyTeamPage myteam=new ManageMyTeamPage();
		   ItemList itemlist=myteam.getAllDesigners();
		   Assert.assertTrue(itemlist.getWrappedElement().getText().contains("All designers"));
	}

	@Then("^the list on the right should contain a list of all the active designers assigned to the Art Directors Team$")
	public void the_list_on_the_right_should_contain_a_list_of_all_the_active_designers_assigned_to_the_Art_Directors_Team() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ManageMyTeamPage myteam=new ManageMyTeamPage();
		   ItemList itemlist=myteam.getDesignersReportToMe();
		   Assert.assertTrue(itemlist.getWrappedElement().getText().contains("Designers who report to me"));
	}
	
	@Given("^a ‘Designer’ has active project assignments where user is not the art director$")
	public void a_Designer_has_active_project_assignments_where_user_is_not_the_art_director() throws Throwable {
		UserHomePage userHomePage = new UserHomePage();
		WaitUtils.waitForDisplayed(userHomePage.getUserName());
		TestBaseProvider.getTestBase().getContext().setProperty("designer.name", userHomePage.getUserNameText());
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		ArrayList<String> designers=itemgrid.allColumnsText("Cover Designer");
		ArrayList<String> interiorDesigners=itemgrid.allColumnsText("Interior Designer");
		designers.addAll(interiorDesigners);
		ArrayList<String> artDirectors=itemgrid.allColumnsText("Art Director");
		TestBaseProvider.getTestBase().getContext().setProperty("designers", designers);
		TestBaseProvider.getTestBase().getContext().setProperty("artDirectors", artDirectors);
	}

	@When("^that ‘Designer’ is assigned to the team$")
	public void that_Designer_is_assigned_to_the_team() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ManageMyTeamPage myteam=new ManageMyTeamPage();
		   myteam.removeAllDesigners();
		   Thread.sleep(1000);
		  String designer= (String) TestBaseProvider.getTestBase().getContext().getProperty("designer.name");
		  myteam.setDesigner(designer);
	}

	@When("^the user navigates to the ‘Show my Teams Project’ page$")
	public void the_user_navigates_to_the_Show_my_Teams_Project_page() throws Throwable {
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		myteam.clickAptMyProjects();
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.clickShowTeamProj();
	}

	@Then("^the projects of the ’Designer’ should also be included in the ‘Show my Teams Projects’ page$")
	public void the_projects_of_the_Designer_should_also_be_included_in_the_Show_my_Teams_Projects_page() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		ArrayList<String> designers=itemgrid.allColumnsText("Cover Designer");
		ArrayList<String> interiorDesigners=itemgrid.allColumnsText("Interior Designer");
		ArrayList<String> titles=itemgrid.allColumnsText("Title");
		TestBaseProvider.getTestBase().getContext().setProperty("titles", titles);
		designers.addAll(interiorDesigners);
		ArrayList<String> designersRolesList=(ArrayList<String>) TestBaseProvider.getTestBase().getContext().getProperty("designers");
		Collections.sort(designers);
		Collections.sort(designersRolesList);
		System.out.println(designersRolesList);
		System.out.println(designers);
		Assert.assertTrue(designers.containsAll(designersRolesList));
	}
	
	@Given("^User logs outs and login as \"([^\"]*)\" user$")
	public void user_logs_outs_and_login_as_user(String user) throws Throwable {
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		myteam.loginAsOtherUser(user);
	}
	

	@When("^that ‘Designer’ is removed from the team$")
	public void that_Designer_is_removed_from_the_team() throws Throwable {
    MyProjectsPage myprojects=new  MyProjectsPage();
    myprojects.waitForResultsLoadingComplete();
    ManageMyTeamPage myteam=new ManageMyTeamPage();
    String designerName= (String) TestBaseProvider.getTestBase().getContext().getProperty("designer.name");
    myteam.removeDesigner(designerName);
	}

	@Then("^the projects of the ’Designers’ does not display in the ‘Show my Teams Projects’ page$")
	public void the_projects_of_the_Designers_does_not_display_in_the_Show_my_Teams_Projects_page() throws Throwable {
		 MyProjectsPage myprojects=new  MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    ArrayList<String> projects=(ArrayList<String>) TestBaseProvider.getTestBase().getContext().getProperty("titles");
		    ArrayList<String> currProjects=itemgrid.allColumnsText("Title");
		    Assert.assertNotEquals(projects, currProjects);
	}
	

	@When("^the user is on the Manage my Team page$")
	public void the_user_is_on_the_Manage_my_Team_page() throws Throwable {
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		Thread.sleep(4000);
		myteam.clickManageMyTeam();
	}
	
	@Then("^And all the projects in the project list should be assigned to the user$")
	public void and_all_the_projects_in_the_project_list_should_be_assigned_to_the_user() throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.switchToDefault();
	    UserHomePage homepage=new UserHomePage();
	    String userName=homepage.getUserNameText();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	   for(Row row: itemgrid.getRows())
	   {
		   String artDirector=row.getColumnText("Art Director");
		   String coverDesigner= row.getColumnText("Cover Designer");
		   String interiorDesigner=row.getColumnText("Interior Designer");
		   String roleNames=artDirector+coverDesigner+interiorDesigner;
		   Assert.assertTrue(roleNames.contains(userName));
	   }
	}
	
	@When("^a ‘Designer’ \"([^\"]*)\" is \"([^\"]*)\" to the Team$")
	public void a_Designer_is_to_the_Team(String designerName, String action) throws Throwable {
	    ManageMyTeamPage myteam=new ManageMyTeamPage();
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    String userName="";
	    if(action.equals("assigned"))
	    {
	    	userName=myteam.username();
		    myprojects.waitForResultsLoadingComplete();
			   myteam.removeAllDesigners();
			   Thread.sleep(1000);
			myteam.setDesigner(designerName);
			 Thread.sleep(2000);
	    }
	    if(action.equals("removed"))
	    {
	    	userName=myteam.username();
	    	 myprojects.waitForResultsLoadingComplete();
	    	myteam.removeDesigner(designerName);
	    }
	    if(action.equals("reassigned from a different Art Director"))
	    {
	    	myteam.loginAsOtherUser("ANOTHER_ART_DIRECTOR");
	    	userName=myteam.username();
	    	myteam.clickManageMyTeam();
	    	MyProjectsPage projects=new MyProjectsPage();
	    	projects.waitForResultsLoadingComplete();
	    	myteam.removeAllDesigners();
	    	Thread.sleep(1000);
		    	myteam.setDesigner(designerName);	
		    	Thread.sleep(2000);
	    }
	    TestBaseProvider.getTestBase().getContext().setProperty("username", userName);
	}

	@SuppressWarnings("static-access")
	@Then("^the update \"([^\"]*)\" on \"([^\"]*)\" should reflect immediately in the database$")
	public void the_update_on_should_reflect_immediately_in_the_database(String action, String designerName) throws Throwable {
	    DBConnection dbcon=new DBConnection();
	   String username= (String) TestBaseProvider.getTestBase().getContext().getProperty("username");
	    ResultSet rs=dbcon.dbConnection("select TOP 1 * from ROLE_MAPPING ORDER BY ROLE_MAP_ID DESC");
	    String dbvalue="";
	    String primaryName="";
	    while(rs.next())
	    {
	    	dbvalue=rs.getString("SECONDARY_USER");
	    	primaryName=rs.getString("PRIMARY_USER");
	    }
	    if(action.equals("assigned"))
	    {
	    	for(int i=0;i<20;i++){
		    	if(dbvalue!=designerName)
		    		Thread.sleep(500);
		    	else
		    		break;
		    	}
	    	Assert.assertEquals(dbvalue, designerName);
	    }
	    if(action.equals("removed"))
	    {
	    	Thread.sleep(2000);
	    	if(primaryName==null || primaryName=="")
	    	{
	    		Assert.assertTrue(primaryName==null || primaryName=="");
	    	}
	    	if(username.contains(primaryName))
	    	{
	    		Assert.assertFalse(dbvalue==designerName);
	    	}
	    	else
	    	{
	    		Assert.assertFalse(username.contains(primaryName));
	    	}
	    }
	    if(action.contains("reassigned"))
	    {
	    	Assert.assertEquals(dbvalue, designerName);
	    	Assert.assertTrue(username.contains(primaryName));
	    }
	}
	
	@Then("^an error message with the title \"([^\"]*)\" displays with the text:Select an available Designer to add to your team$")
	public void an_error_message_with_the_title_displays_with_the_text_No_Designers_are_assigned_to_your_team(String arg1) throws Throwable {
	    
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		
		MyProjectsPage myprojects = new MyProjectsPage();
		
		myprojects.waitForResultsLoadingComplete();
		//driver.switchTo().frame(driver.findElement(By.cssSelector("iframe#display_frame.container")));
		Thread.sleep(2000);
		WaitUtils.waitForEnabled(myteam.getAssignDesignerButton());
		
		myprojects.clickElementByJS(myteam.getAssignDesignerButton());
		
		String msg=myteam.getselectDesignerMsgPopup().getText();
		
		Assert.assertEquals(msg,"Select an available Designer to add to your team.", "FAIL-error msg text not matched");
		
		}	

	@When("^the user clicks the Remove Designer button without selecting at least one designer from the \"([^\"]*)\" list$")
	public void the_user_clicks_the_Remove_Designer_button_without_selecting_at_least_one_designer_from_the_list(String arg1) throws Throwable {
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();	
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		//driver.switchTo().frame(driver.findElement(By.cssSelector("iframe#display_frame.container")));
		Thread.sleep(4000);
		WaitUtils.waitForEnabled(myteam.getRemoveDesignerButton());
		//myteam.getRemoveDesignerButton().click();
		
		myprojects.clickElementByJS(myteam.getRemoveDesignerButton());
		
	}

	@Then("^an error message with the title \"([^\"]*)\" displays with the text: Select a Designer to remove from your team\\.$")
	public void an_error_message_with_the_title_displays_with_the_text_Select_a_Designer_to_remove_from_your_team(String arg1) throws Throwable {
	    
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();	
		String msg=myteam.getselectDesignerMsgPopup().getText();
		
		//String msg= driver.findElement(By.xpath("//*[@id='fb_message_label']")).getText();
		Assert.assertEquals(msg,"Select a Designer to remove from your team.", "FAIL-error msg text not matched");
		
		
	 }

	
	@Given("^the user assigns a Designer \"([^\"]*)\" to the Team$")
	public void the_user_assigns_a_Designer_to_the_Team(String arg1) throws Throwable {
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		//driver.switchTo().frame(driver.findElement(By.cssSelector("iframe#display_frame.container")));
		WaitUtils.waitForDisplayed(myteam.getParticularDesignerselect());
		myteam.getParticularDesignerselect().click();
		//WaitUtils.waitForDisplayed(driver.findElement(By.xpath(".//h4[contains(text(),'Albey QaTest')]")));
	    myteam.removeDesigner("Albey QaTest");
		//driver.findElement(By.xpath(".//h4[contains(text(),'Albey QaTest')]")).click();
		myteam.getAssignDesignerButton().click();
		
	}
	@When("^the user returns to the Manage my Team page$")
	public void the_user_returns_to_the_Manage_my_Team_page() throws Throwable {
		
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		myteam.clickManageMyTeam();
		
	}
	
	@Then("^the same Designers \"([^\"]*)\" still remain in the Team$")
	public void the_same_Designers_still_remain_in_the_Team(String arg1) throws Throwable {
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().frame(driver.findElement(By.cssSelector("iframe#display_frame.container")));
		//WaitUtils.waitForDisplayed(driver.findElement(By.xpath(".//h4[contains(text(),'Albey QaTest')]")));
		WaitUtils.waitForDisplayed(myteam.getParticularDesignerselect());
		AssertUtils.assertDisplayed(myteam.getParticularDesignerselect());
	    
	}
	
	@Then ("^there currently is no designers added to the Art Director team$")
    public void no_designers_added_to_the_Art_Director() throws InterruptedException{
           ManageMyTeamPage manageMyTeamPage=new ManageMyTeamPage();
           Thread.sleep(5000);
           try{
           manageMyTeamPage.removeAllDesigners();
           }
           catch(Exception e){
           }
           }
    
    @When("^user selects the Show My Team Projects link on the My Projects screen$")
    public void  user_selects_Show_My_Team_Projects_link() throws InterruptedException{
           ManageMyTeamPage manageMyTeamPage=new ManageMyTeamPage();
           Thread.sleep(5000);
           manageMyTeamPage.clickAptMyProjects();
           Thread.sleep(5000);
    }
    @Then("^only Art Director projects will display\\.$")
    public void only_Art_Director_projects_will_display() throws Throwable {
           UserHomePage homePage =new UserHomePage();
           Thread.sleep(2000);
String actual=homePage.getUserNameText();
           System.out.println(homePage.getUserNameText());
           MyProjectsPage projectspage = new MyProjectsPage();
       projectspage.waitForResultsLoadingComplete();
     projectspage.waitForResultsLoadingComplete();
          ItemGrid ig=projectspage.getProjectGrid();
          ig.showAllColumns();
          projectspage.waitForResultsLoadingComplete();
                 try{
                        ItemGrid itemgrid=projectspage.getProjectGrid();
                        
                               
                               List<String> colText=itemgrid.allColumnsText("Art Director");
                              for(String str: colText){
                                     Assert.assertEquals(actual, str);
                                     
                               }
                        
                        
           //     Thread.sleep(10000);
                 }
           catch(Exception e){
                  
           }
           
    
    }
	
	
	
}
