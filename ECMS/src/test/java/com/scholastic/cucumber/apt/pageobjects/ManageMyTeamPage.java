/**
 * 
 */
package com.scholastic.cucumber.apt.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.ManageMyTeamPageLocators;
import com.scholastic.cucumber.apt.components.ItemList;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

/**
 * @author chirag.jayswal
 *
 */
public class ManageMyTeamPage extends BaseTestPage<TestPage> implements ManageMyTeamPageLocators {

	@FindBy(locator = ASSIGN_DESIGNER_BUTTON)
	private WebElement assignDesignerButton;

	@FindBy(locator = REMOVE_DESIGNER_BUTTON)
	private WebElement removeDesignerButton;
	
	@FindBy(locator = SELECT_DESIGNER_MSG_POPUP)
	private WebElement selectDesignerMsgPopup;
	
	@FindBy(locator = PARTICULAR_DESIGNER_SELECT)
	private WebElement ParticularDesignerselect;
	
	
/*	
	public WebElement getParticularDesignerselect(String label) {
		String xpath = String.format(PARTICULAR_DESIGNER_SELECT, label);
	return	getDriver().findElement(By.xpath(xpath));	
	}*/
	


	public WebElement getParticularDesignerselect() {
		return ParticularDesignerselect ;
	}
	

	@Override
	protected void openPage() {

	}
	
	public WebElement getselectDesignerMsgPopup() {
		return selectDesignerMsgPopup;
	}

	public WebElement getAssignDesignerButton() {
		return assignDesignerButton;
	}

	public void setAssignDesignerButton(WebElement assignDesignerButton) {
		this.assignDesignerButton = assignDesignerButton;
	}

	public WebElement getRemoveDesignerButton() {
		return removeDesignerButton;
	}

	public void setRemoveDesignerButton(WebElement removeDesignerButton) {
		this.removeDesignerButton = removeDesignerButton;
	}

	public ItemList getAllDesigners() {
		return new ItemList(ALL_DESIGNERS_CONTAINER);
	}

	public ItemList getDesignersReportToMe() {
		return new ItemList(DESIGNERS_REPORT_TO_ME_CONTAINER);
	}
	
	public void removeAllDesigners() throws InterruptedException
	{
		WebDriver driver= 	TestBaseProvider.getTestBase().getDriver();
		ItemList itemlist=getDesignersReportToMe();
		WaitUtils.waitForDisplayed(itemlist.getWrappedElement());
		String text= itemlist.getWrappedElement().getText();
		String[] t=text.split("\\n");
		for(int i=1;i<t.length;i++)
		{
			
			WebElement option=driver.findElement(By.xpath(String.format("//div[contains(@title,'%s')]",t[i])));
			itemlist.selectItem(option.getText());
			getRemoveDesignerButton().click();
		}
	}
	
	public void removeDesigner(String name)
	{
		
		ItemList allDesignersList=getAllDesigners();
		ItemList itemlist=getDesignersReportToMe();
		if(!allDesignersList.hasItem(name))
		{
		itemlist.selectItem(name);
		getRemoveDesignerButton().click();
		}
	}
	
	public void setDesigner(String name)
	{
		ItemList allDesignersList=getAllDesigners();
		ItemList selectedDesignersList=getDesignersReportToMe();
		if(!selectedDesignersList.hasItem(name))
		{
			allDesignersList.selectItem(name);
			getAssignDesignerButton().click();	
		}	
	}
	
	public void clickManageMyTeam() throws InterruptedException
	{
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.switchToDefault();
//		WebElement parent=HeaderSection.APTMenu.APT.getElement();
//		parent.click();
		WebElement child=HeaderSection.APTMenu.ManageMyTeam.getElement();
		child.click();
	}
	
	public void clickAptMyProjects() throws InterruptedException
	{
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.switchToDefault();
//		WebElement parent=HeaderSection.APTMenu.APT.getElement();
//		parent.click();
		WebElement child=HeaderSection.APTMenu.MYPROJECT.getElement();
		child.click();
	}
	
	public void loginAsOtherUser(String user) throws InterruptedException
	{
		MyProjectsPage myprojects=new MyProjectsPage();
		SignOutPage signout = new SignOutPage();
		OTBPMSigninPage signin = new OTBPMSigninPage();
		myprojects.waitForResultsLoadingComplete();
		Thread.sleep(1000);
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		UserHomePage homepage = new UserHomePage();
		HeaderSection headersection = homepage.getHeaderSection();
		clickElementByJS(headersection.getLogoutButton());
		clickElementByJS(signout.getGoToSignInPageButton());
		String username = TestBaseProvider.getTestBase().getString("signin.signinuser." + user + ".name");
		String password = TestBaseProvider.getTestBase().getString("signin.signinuser." + user + ".password");
		signin.signIn(username, password);
	}
	
	public void clickElementByJS(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click();", element);
	}
	
	public String username()
	{
		 MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    myprojects.switchToDefault();
		    UserHomePage homepage=new UserHomePage();
		    String userName=homepage.getUserNameText();
		    myprojects.waitForResultsLoadingComplete();
		    return userName;
	}
	
}
