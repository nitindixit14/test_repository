package com.scholastic.cucumber.apt.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.Keys.AssignDialogLocators;
import com.scholastic.cucumber.apt.Keys.BarcodeDialogLocators;
import com.scholastic.cucumber.apt.Keys.MyProjectsPageLocators;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;
import com.scholastic.torque.webdriver.ExtendedElement;

public class BarcodeDialog extends BaseTestPage<TestPage> implements BarcodeDialogLocators {

	
/*	public BarcodeDialog(String loc) {
		super(loc);
	}
	*/
	@FindBy(locator = BARCODES_TABLE)
	private WebElement BarcodesTable;
	
	public WebElement getBarcodesTable()
	{
		return BarcodesTable;
	}
	
	@FindBy(locator = BARCODES_COUNT)
	private WebElement BarcodesCount;
	
	
	public WebElement getBarcodesCount()
	{
		return BarcodesCount;
	}
	
	
	
	public Dialog getDialog() {
		return new Dialog(BARCODES_DIALOG);
	}
	
	public void switchToBarcodeDialog()
	{
		getDialog();
		WaitUtils.waitForDisplayed(getDialog().getDialogTitleElement());
		getDriver().switchTo().frame(0);
	}
	
	
	@Override
	protected void openPage() {
		// TODO Auto-generated method stub
		
	}
	
	
}
