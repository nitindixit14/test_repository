package com.scholastic.cucumber.apt.pageobjects;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.EPICHomePageLocators;
import com.scholastic.cucumber.apt.Keys.EPICKeyInfoLocators;
import com.scholastic.cucumber.apt.stepdefs.DBConnection;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

public class EpicKeyInfoPage extends BaseTestPage<TestPage> implements EPICKeyInfoLocators {

	@FindBy(locator = CLONE_BUTTON)
	private WebElement cloneButton;

	@FindBy(locator = OWNER_DROPDOWN)
	private WebElement ownerDropdown;

	@FindBy(locator = BOTH_RADIOBUTTON)
	private WebElement bothRadioButton;

	@FindBy(locator = INCLUDE_RADIOBUTTON)
	private WebElement includeRadioButton;

	@FindBy(locator = FIRST_USE_DATE)
	private WebElement firstUseDate;

	@FindBy(locator = PROPERTIES_OPTION)
	private WebElement propertiesOption;

	@FindBy(locator = PROPERTIES_BUTTON)
	private WebElement propertiesButton;

	@FindBy(locator = PUB_DATE)
	private WebElement pubDate;

	@FindBy(locator = TRADE_INSTORE_DATE)
	private WebElement tradeInStoreDate;

	@FindBy(locator = EDITORAIL_AGE_DROPDOWN1)
	private WebElement editorialAge1;

	@FindBy(locator = EDITORAIL_AGE_DROPDOWN2)
	private WebElement editorialAge2;

	@FindBy(locator = MAJOR_CATEGORY)
	private WebElement majorCategory;

	@FindBy(locator = PRIMARY_CATEGORY)
	private WebElement primaryCategory;

	@FindBy(locator = PRIMARY_CATEGORY_CHECKBOX)
	private WebElement primaryCategoryCheckbox;

	@FindBy(locator = PRIMARY_CATEGORY_OK_BUTTON)
	private WebElement primaryCategoryOkButton;

	@FindBy(locator = SAVE_BUTTON)
	private WebElement saveButton;

	@FindBy(locator = CONTRACT_SAVE_BUTTON)
	private WebElement contractSaveButton;

	@FindBy(locator = RECENT_ITEMS_TAB)
	private WebElement recentItemsTab;

	@FindBy(locator = FIRST_RECENT_ITEM)
	private WebElement firstRecentItem;

	@FindBy(locator = REQUEST_ISBN)
	private WebElement requestIsbn;

	@FindBy(locator = REQUEST_SCHOLASTIC_ISBN)
	private WebElement requestScholasticIsbn;

	@FindBy(locator = REQUEST_SCHOLASTIC_ISBN_SUBMIT_BUTTON)
	private WebElement requestScholasticIsbnSubmitButton;

	@FindBy(locator = CONTINUE_BUTTON)
	private WebElement continueButton;

	@FindBy(locator = CONTINUE_ADDING_BUTTON)
	private WebElement continueAddingButton;

	@FindBy(locator = UNSCHEDULED)
	private WebElement unscheduled;

	@FindBy(locator = TRADE_GROUP)
	private WebElement tradeGroup;

	@FindBy(locator = BUDGET_CATEGORY)
	private WebElement budgetCategory;

	public WebElement getUnscheduled() {
		return unscheduled;
	}

	public WebElement getTradeGroup() {
		return tradeGroup;
	}

	public WebElement getBudgetCategory() {
		return budgetCategory;
	}

	public WebElement getcontinueAddingButton() {
		return continueAddingButton;
	}

	public WebElement getcontinueButton() {
		return continueButton;
	}

	public WebElement getpropertiesOption() {
		return propertiesOption;
	}

	public WebElement getpropertiesButton() {
		return propertiesButton;
	}

	public WebElement getpubDate() {
		return pubDate;
	}

	public WebElement gettradeInStoreDate() {
		return tradeInStoreDate;
	}

	public WebElement geteditorialAge1() {
		return editorialAge1;
	}

	public WebElement geteditorialAge2() {
		return editorialAge2;
	}

	public WebElement getmajorCategory() {
		return majorCategory;
	}

	public WebElement getprimaryCategory() {
		return primaryCategory;
	}

	public WebElement getprimaryCategoryCheckbox() {
		return primaryCategoryCheckbox;
	}

	public WebElement getprimaryCategoryOkButton() {
		return primaryCategoryOkButton;
	}

	public WebElement getsaveButton() {
		return saveButton;
	}

	public WebElement getcontractSaveButton() {
		return contractSaveButton;
	}

	public WebElement getrecentItemsTab() {
		return recentItemsTab;
	}

	public WebElement getfirstRecentItem() {
		return firstRecentItem;
	}

	public WebElement getrequestIsbn() {
		return requestIsbn;
	}

	public WebElement getrequestScholasticIsbn() {
		return requestScholasticIsbn;
	}

	public WebElement getrequestScholasticIsbnSubmitButton() {
		return requestScholasticIsbnSubmitButton;
	}

	public WebElement getFirstUseDate() {
		return firstUseDate;
	}

	public WebElement getBothRadioButton() {
		return bothRadioButton;
	}

	public WebElement getCloneButton() {
		return cloneButton;
	}

	public WebElement getOwnerDropdown() {
		return ownerDropdown;
	}

	public WebElement getIncludeRadioButton() {
		return includeRadioButton;
	}

	public String ProductList(String option) {

		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebElement owner = driver.findElement(By.xpath("html/body/table[3]/tbody/tr/td[2]/table/tbody/tr[2]/td[5]"));
		ItemGrid itemgrid;
		String arr = new String();
		int j = 1;
		int d = 1;

		for (int b = 1; b <= 3; b = b + 2) {
			j = 1;

			String num_tables = "html/body/form/table[3]/tbody/tr/td[1]/table";
			List<WebElement> num_tables_size = driver.findElements(By.xpath(String.format(num_tables)));

			for (int c = 1; c <= num_tables_size.size(); c++) {
				String product_xpath_size = "html/body/form/table[3]/tbody/tr/td[%d]/table[%d]/tbody/tr[2]/td/table/tbody/tr";
				List<WebElement> product_table1_size = driver
						.findElements(By.xpath(String.format(product_xpath_size, b, c)));

				for (int a = 1; a <= 4; a = a + 2) {
					String product_xpath = product_xpath_size + "[%d]/td[%d]";
					for (int i = 1; i <= product_table1_size.size(); i++) {
						List<WebElement> table1 = driver
								.findElements(By.xpath(String.format(product_xpath, b, c, i, a)));
						for (WebElement element : table1) {
							if (option.equals("Trade")) {
								try {
									WebElement tradePrice = driver.findElement(By.xpath(
											"html/body/form/table[2]/tbody/tr/td[3]/table[5]/tbody/tr[2]/td/table/tbody/tr[1]/td[2]"));
									arr = tradePrice.getText();
									return arr;
								} catch (Exception e) {
									arr = " ";
									return arr;
								}
							}
							if (option.equals("Canada")) {
								try {
									WebElement canadaPrice = driver.findElement(By.xpath(
											"html/body/form/table[2]/tbody/tr/td[3]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[2]"));
									arr = canadaPrice.getText();
									return arr;
								} catch (Exception e) {
									arr = " ";
									return arr;
								}

							}
							if (option.equals("BF Price")) {
								try {
									WebElement bfPrice = driver.findElement(By.xpath(
											"html/body/form/table[2]/tbody/tr/td[1]/table[10]/tbody/tr[9]/td/table/tbody/tr[4]/td/table/tbody/tr[2]/td[2]"));
									arr = bfPrice.getText();
									return arr;
								} catch (Exception e) {
									arr = " ";
									return arr;
								}

							}

							if (element.getText().contains(option)) {
								WebElement table1_value = driver
										.findElement(By.xpath(String.format(product_xpath, b, c, i, a + 1)));
								arr = table1_value.getText();
								j = j + 1;
								break;
							}
						}
						if (j != 1)
							break;
					}
					if (j != 1)
						break;
				}
				if (j != 1)
					break;
			}
			if (j != 1)
				break;
		}
		if (j == 1 && d == 1) {

			WebElement detailedSpecsCosts = driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[1]/a[1]/img"));
			detailedSpecsCosts.click();

			//
			int a = 1;
			String deatiledSpecsTable = "html/body/table[5]/tbody/tr[1]/td[1]/table/tbody/tr[2]/td/table[1]/tbody/tr[%d]/td[%d]";
			for (int i = 2; i < 26; i++) {
				if (i == 10 || i == 16) {
					continue;
				} else {
					WebElement element = driver.findElement(By.xpath(String.format(deatiledSpecsTable, i, a)));
					if (element.getText().contains(option)) {
						WebElement table4_value = driver
								.findElement(By.xpath(String.format(deatiledSpecsTable, i, a + 1)));
						// arr.add(table4_value.getText());
						arr = table4_value.getText();
						driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[1]/a[1]/img")).click();

						d++;

						break;
					}
					if (option.contains("SCHEDULE")) {

						WebElement schedule = driver.findElement(By
								.xpath("html/body/table[4]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[8]/td[2]"));
						String[] scheduleCompleteText = schedule.getText().split("-");
						String scheduleCode = scheduleCompleteText[0];
						arr = scheduleCode.trim();
						driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[1]/a[1]/img")).click();
						break;
					}

				}

			}

		}
		// for(int i=0;i<arr.size();i++){
		// System.out.print(arr.get(i));
		// JOptionPane.showMessageDialog(null, arr.get(i));
		// }
		return arr;
	}

	@SuppressWarnings("static-access")
	public String readDataFromEpic(String label, String xpathExpression) throws InterruptedException {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String value = "";
		try {
			if (label.equals("Trade")) {
				try {
					WebElement tradePrice = driver.findElement(By.xpath(
							"html/body/form/table[2]/tbody/tr/td[3]/table[5]/tbody/tr[2]/td/table/tbody/tr[1]/td[2]"));
					value = tradePrice.getText();
					return value;
				} catch (Exception e) {
					value = " ";
					return value;
				}
			}
			if (label.equals("Canada")) {
				try {
					WebElement canadaPrice = driver.findElement(By.xpath(
							"html/body/form/table[2]/tbody/tr/td[3]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[2]"));
					value = canadaPrice.getText();
					return value;
				} catch (Exception e) {
					value = " ";
					return value;
				}
			}
			if (label.contains("Schedule")) {
				WebElement schedule = driver.findElement(By.xpath(xpathExpression.format(xpathExpression, label)));
				WaitUtils.waitForDisplayed(schedule);
				String[] scheduleCompleteText = schedule.getText().split("-");
				String scheduleCode = scheduleCompleteText[0];
				value = scheduleCode.trim();
				Thread.sleep(1000);
			} else {
				WebElement KeyInfovalue = driver.findElement(By.xpath(xpathExpression.format(xpathExpression, label)));
				value = KeyInfovalue.getText();
			}

		} catch (Exception e) {

		}
		return value;
	}

	// if(label.contains("Canada"))
	// {
	// WebElement
	// value=driver.findElement(By.xpath("html/body/form/table[2]/tbody/tr/td[3]/table[5]/tbody/tr[2]/td/table/tbody/tr[2]/td[2]"));
	// return value.getText();
	// }

	@Override
	protected void openPage() {
		// TODO Auto-generated method stub

	}

}
