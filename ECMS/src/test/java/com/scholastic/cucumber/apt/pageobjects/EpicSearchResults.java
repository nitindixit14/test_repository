package com.scholastic.cucumber.apt.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;

public class EpicSearchResults extends BaseTestPage<TestPage> {

	@FindBy(xpath = "/html/body/form/table[2]/tbody/tr/td/table[2]/tbody/tr[2]/td[3]/a")
	private WebElement result;

	public WebElement getResult() {
		return result;
	}

	@Override
	protected void openPage() {
		// TODO Auto-generated method stub

	}

}
