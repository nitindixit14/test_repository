package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.EpicHomePage;
import com.scholastic.cucumber.apt.pageobjects.InternalRoutingForm;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.cucumber.apt.pageobjects.TransmittalPdf;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransmittalPdfFormSteps {
	
	

	@Then("^'Date' in Cover Transmittal Form matches 'Last Modified' value in Transmittal History$")
	public void date_in_Cover_Transmittal_Form_matches_Last_Modified_value_in_Transmittal_History() throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String lastModifiedDate=(String) TestBaseProvider.getTestBase().getContext().getProperty("Cover.last.modified.date");
		TransmittalPdf pdf=new TransmittalPdf();
		MyProjectsPage myprojects=new MyProjectsPage();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element = wait.until(ExpectedConditions.visibilityOf(pdf.getValueElement("Date:")));
		String date=pdf.getValueElement("Date:").getText();
		String formattedModifiedDate=myprojects.setFormatFromYY(lastModifiedDate);
		Assert.assertEquals(date, formattedModifiedDate);
	}
	
	@Then("^'Date' in Jacket Transmittal Form matches 'Last Modified' value in Transmittal History$")
	public void date_in_Jacket_Transmittal_Form_matches_Last_Modified_value_in_Transmittal_History() throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String lastModifiedDate=(String) TestBaseProvider.getTestBase().getContext().getProperty("Jacket.last.modified.date");
		TransmittalPdf pdf=new TransmittalPdf();
		MyProjectsPage myprojects=new MyProjectsPage();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element = wait.until(ExpectedConditions.visibilityOf(pdf.getValueElement("Date:")));
		String date=pdf.getValueElement("Date:").getText();
		String formattedModifiedDate=myprojects.setFormatFromYY(lastModifiedDate);
		Assert.assertEquals(date, formattedModifiedDate);
	}
	
	@Then("^'Date' in Interior Transmittal Form matches 'Last Modified' value in Transmittal History$")
	public void date_in_Interior_Transmittal_Form_matches_Last_Modified_value_in_Transmittal_History() throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String lastModifiedDate=(String) TestBaseProvider.getTestBase().getContext().getProperty("Interior.last.modified.date");
		TransmittalPdf pdf=new TransmittalPdf();
		MyProjectsPage myprojects=new MyProjectsPage();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element = wait.until(ExpectedConditions.visibilityOf(pdf.getValueElement("Date:")));
		String date=pdf.getValueElement("Date:").getText();
		String formattedModifiedDate=myprojects.setFormatFromYY(lastModifiedDate);
		Assert.assertEquals(date, formattedModifiedDate);
	}
	
	

	@Then("^'Title Sufix' in Cover Transmittal Form matches 'Mechs' value in Transmittal History$")
	public void title_Sufix_in_Cover_Transmittal_Form_matches_Mechs_value_in_Transmittal_History() throws Throwable {
		Thread.sleep(10000);
		TransmittalPdf pdf=new TransmittalPdf();
		String titleSuffix=pdf.getValueElement("-").getText();
		String suffix=(String) TestBaseProvider.getTestBase().getContext().getProperty("Cover.mechs.value");
	 	Assert.assertEquals(titleSuffix.trim(), suffix);
	 	
}
	
	@Then("^'Title Sufix' in Jacket Transmittal Form matches 'Mechs' value in Transmittal History$")
	public void title_Sufix_in_Jacket_Transmittal_Form_matches_Mechs_value_in_Transmittal_History() throws Throwable {
		Thread.sleep(10000);
		TransmittalPdf pdf=new TransmittalPdf();
		String titleSuffix=pdf.getValueElement("-").getText();
		String suffix=(String) TestBaseProvider.getTestBase().getContext().getProperty("Jacket.mechs.value");
	 	Assert.assertEquals(titleSuffix.trim(), suffix);
	}
	
	@Then("^'Title Sufix' in Interior Transmittal Form matches 'Mechs' value in Transmittal History$")
	public void title_Sufix_in_Interior_Transmittal_Form_matches_Mechs_value_in_Transmittal_History() throws Throwable {
		Thread.sleep(10000);
		TransmittalPdf pdf=new TransmittalPdf();
		String titleSuffix=pdf.getValueElement("-").getText();
		String suffix=(String) TestBaseProvider.getTestBase().getContext().getProperty("Interior.mechs.value");
	 	Assert.assertEquals(titleSuffix.trim(), suffix);
}
	
	@When("^User selects the 'View' icon for a Jacket Transmittal Form$")
	public void user_selects_the_View_icon_for_a_Jacket_Transmittal_Form() throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		  MyProjectsPage projectsPage = new MyProjectsPage();
		  TransmittalHistoryDialog transmittalHistoryDialog = projectsPage.getTransmittalHistoryDialog();
		  ItemGrid historyResults = transmittalHistoryDialog.getHistoryGrid();
		  Thread.sleep(10000);
		  transmittalHistoryDialog.waitForPresent();
		  transmittalHistoryDialog.switchToDialog();
		  
		  WebElement Header=historyResults.getHeader("Type");
		  Thread.sleep(2000);
		  Header.click();
		  Thread.sleep(2000);
		  Header.click();
		  Thread.sleep(10000);
		  
		  String LastModifieddate = transmittalHistoryDialog.getFirstHistoryTypeRow("Jacket").getColumnText("Last Modified");
		  TestBaseProvider.getTestBase().getContext().setProperty("Jacket.last.modified.date",LastModifieddate);
		  
		  String Mechs = transmittalHistoryDialog.getFirstHistoryTypeRow("Jacket").getColumnText("Mechs");
		  TestBaseProvider.getTestBase().getContext().setProperty("Jacket.mechs.value",Mechs);
		  Thread.sleep(2000);
		  Row Row = transmittalHistoryDialog.getFirstHistoryTypeRow("Jacket");
		  WebElement input = Row.findElement(By.id("img_View"));
		  WebDriverWait wait = new WebDriverWait(driver, 300);
		  WebElement element = wait.until(ExpectedConditions.visibilityOf(input));
		  //driver.findElement(By.xpath(".//tr[4]//td[preceding-sibling::td/div/input[@id='out_Mechs']]//div/div/img[@id='img_View']")).click();
		  projectsPage.clickElementByJS(input);
		  Thread.sleep(10000L);
		  
	}
	
	@Then("^\"([^\"]*)\"in Transmittal Form is 'Production/Manufacturing Transmittal Form for Jacket'$")
	public void title_Prefix_in_Transmittal_Form_is_Production_Manufacturing_Transmittal_Form_for_Jacket(String title) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(10000);
		TransmittalPdf pdf=new TransmittalPdf();
		String titlePrefix=pdf.getLabelElement("Production").getText().trim();
		
		Assert.assertEquals(titlePrefix, title.trim());
	}
	
	@Then("^\"([^\"]*)\"in Transmittal Form is 'Production/Manufacturing Transmittal Form for Interior'$")
	public void title_Prefix_in_Transmittal_Form_is_Production_Manufacturing_Transmittal_Form_for_Interior(String title) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(10000);
		TransmittalPdf pdf=new TransmittalPdf();
		String titlePrefix=pdf.getLabelElement("Production").getText().trim();
		
		Assert.assertEquals(titlePrefix, title.trim());
	}
	
	@When("^User selects the 'View' icon for a Interior Transmittal Form$")
	public void user_selects_the_View_icon_for_a_Interior_Transmittal_Form() throws Throwable {
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		  MyProjectsPage projectsPage = new MyProjectsPage();
		  TransmittalHistoryDialog transmittalHistoryDialog = projectsPage.getTransmittalHistoryDialog();
		  ItemGrid historyResults = transmittalHistoryDialog.getHistoryGrid();
		  Thread.sleep(10000);
		  transmittalHistoryDialog.waitForPresent();
		  transmittalHistoryDialog.switchToDialog();
		  Thread.sleep(10000);
		  //Scroll to right
		  JavascriptExecutor js = (JavascriptExecutor)driver; 
		  js.executeScript("window.scrollBy(200,0)", "");
		  
		  WebElement Header=historyResults.getHeader("Type");
		  Header.click();
		  Thread.sleep(2000);
		  Header.click();
		  Thread.sleep(10000);
		  
		  String LastModifieddate = transmittalHistoryDialog.getFirstHistoryTypeRow("Interior").getColumnText("Last Modified");
		  TestBaseProvider.getTestBase().getContext().setProperty("Interior.last.modified.date",LastModifieddate);
		  Thread.sleep(2000);
		  String Mechs = transmittalHistoryDialog.getFirstHistoryTypeRow("Interior").getColumnText("Mechs");
		  TestBaseProvider.getTestBase().getContext().setProperty("Interior.mechs.value",Mechs);
		  Thread.sleep(2000);
  
		  Row row=transmittalHistoryDialog.getFirstHistoryTypeRow("Interior");
		  WebElement input = row.findElement(By.id("img_View"));
		  
		  try{
			  WebDriverWait wait = new WebDriverWait(driver, 300);
			  WebElement element = wait.until(ExpectedConditions.visibilityOf(input));
			  input.click();
			  Thread.sleep(10000L);	
			  }catch(Exception e){
						  
				  projectsPage.clickElementByJS(input);
			  }
		}
		 



	@Then("^The current system date is displayed in the top right corner$")
	public void the_current_system_date_is_displayed_in_the_top_right_corner() throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		try{
			driver.switchTo().defaultContent();
		if(driver.getCurrentUrl().contains("pdf"))
		{
			driver.switchTo().defaultContent();
		}
		else
		{
			MyProjectsPage myprojects=new MyProjectsPage();
			myprojects.switchToWindow();
			driver.switchTo().defaultContent();
		}
		}
		catch(Exception e)
		{
			driver.switchTo().defaultContent();
		}
		String routingFormDate=routingform.getValueElement("Date").getText();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		   //get current date
		TimeZone tz = TimeZone.getTimeZone("EST");
		Calendar cal = Calendar.getInstance(tz);
		   Assert.assertEquals(dateFormat.format(cal.getTime()).toString(), routingFormDate.trim());
	}
	
	@SuppressWarnings("unchecked")
	@Then("^information \"([^\"]*)\" is displayed in \"([^\"]*)\" Transmittal Form$")
	public void information_is_displayed_in_Transmittal_Form(String fields, String arg2) throws Throwable {
//		TestBaseProvider.getTestBase().getDriver().get("http://tg-ecms-otbpm-s.digital.scholastic.com/transmittal_forms/transmittalcover_9780439665421_2016-05-13_T10-50-17.pdf");
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		ArrayList<String> pdfValues=new ArrayList<>();
		List<String> items = new Gson().fromJson(fields, List.class);
		for (String item : items) {
	//		WaitUtils.waitForDisplayed(transmittalpdf.getValueElement(item));
			WebDriver driver=TestBaseProvider.getTestBase().getDriver();
			try{
				driver.switchTo().defaultContent();
			if(driver.getCurrentUrl().contains("pdf"))
			{
				driver.switchTo().defaultContent();
			}
			else
			{
				MyProjectsPage myprojects=new MyProjectsPage();
				myprojects.switchToWindow();
				driver.switchTo().defaultContent();
			}
			}
			catch(Exception e)
			{
				driver.switchTo().defaultContent();
			}
			WebElement element=driver.findElement(By.xpath(String.format("//div[contains(text(),':')][starts-with(text(),'%s')]/following::div[1]",item)));
		String transmittalpdfvalue=element.getText();
		if(transmittalpdf.getAllLabelsInPdf().contains(transmittalpdfvalue))
		{
			pdfValues.add("");
		}
		else if(item.contains("Price") && transmittalpdfvalue.contains("$"))
		{
			pdfValues.add(transmittalpdfvalue.replace("$", ""));
		}
		else
		{
			pdfValues.add(transmittalpdfvalue);
			if(item.equals("ISBN"))
			{
				TestBaseProvider.getTestBase().getContext().setProperty("isbn", transmittalpdfvalue);
			}
		}
		}
		TestBaseProvider.getTestBase().getContext().setProperty("pdf.values", pdfValues);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Then("^information \"([^\"]*)\" value matches database value for a project$")
	public void information_value_matches_database_value_for_a_project(String type) throws Throwable {
		ArrayList<String>pdfValues=(ArrayList<String>) TestBaseProvider.getTestBase().getContext().getProperty("pdf.values");
		DBConnection dbcon=new DBConnection();
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalPdf tp=new TransmittalPdf();
		EpicHomePage homepage=new EpicHomePage();
		String databaseFields=TestBaseProvider.getTestBase().getString("database."+type+".fields");
		String isbn=(String) TestBaseProvider.getTestBase().getContext().getProperty("isbn");
		ArrayList<String> databaseResults=new ArrayList<>();
		ResultSet rs=dbcon.dbConnection("SELECT TOP 1 * FROM dbo.APT_PROJECTS_VIEW,dbo.TRANSMITTAL_HISTORY WHERE APT_PROJECTS_VIEW.ISBN=TRANSMITTAL_HISTORY.ISBN AND APT_PROJECTS_VIEW.ISBN='"+ isbn + "' ORDER BY DATE_CREATED DESC");
		List<String> dbitems = new Gson().fromJson(databaseFields, List.class);
		while (rs.next()) {
			for (String item : dbitems) {
				if (item.contains("DATE")) {
					String date = rs.getString(item);
					databaseResults.add(myprojects.changeDateFormat(date));
				} else {
					databaseResults.add(rs.getString(item));
				}
			}
		}
		tp.assertResults(pdfValues,databaseResults);
	}
	
	@When("^user does not select 'Advanced Reader Copy'$")
	public void user_does_not_select_Advanced_Reader_Copy() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		 WebElement transmittalType=transmittals.getTransmittalFormButtons("Advanced Reader Copy");
		 if( transmittalType.isSelected())
		  {
			  transmittalType.click();
		  }
	}

	@Then("^'ARC' is not displayed in the \"([^\"]*)\" Transmittal Form pdf$")
	public void arc_is_not_displayed_in_the_Transmittal_Form_pdf(String arg1) throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		Assert.assertFalse(transmittalpdf.getAllLabelsInPdf().contains("ARC:"));
	}
	
	@When("^user checks 'Advanced Reader Copy'$")
	public void user_checks_Advanced_Reader_Copy() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		 WebElement transmittalType=transmittals.getTransmittalFormButtons("Advanced Reader Copy");
		 if( transmittalType.isSelected())
		  {
		  }
		 else
		 {
			 transmittalType.click();
		 }
	}

	@Then("^'ARC:YES' is displayed in the \"([^\"]*)\" Transmittal Form pdf$")
	public void arc_YES_is_displayed_in_the_Transmittal_Form_pdf(String arg1) throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		 Thread.sleep(1000);
		AssertUtils.assertDisplayed(transmittalpdf.getLabelElement("ARC:"));
		String arcValue=routingform.getValueElement("ARC:").getText();
		Assert.assertEquals("Yes", arcValue);
	}
	
	@SuppressWarnings("unchecked")
	@When("^User enters data into the \"([^\"]*)\" field$")
	public void user_enters_data_into_the_field(String fields) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		WebElement inputFields;
		ArrayList<String> formValues=new ArrayList<>();
		String inputData=TestBaseProvider.getTestBase().getString("transmittalFormInput.data");
		  List<String> items = new Gson().fromJson(fields, List.class);
		  List<String> dataItems=new Gson().fromJson(inputData, List.class);
		  for (int i = 0; i < items.size(); i++) {
				String item = items.get(i);
				String dataItem = dataItems.get(i);
				formValues.add(dataItem.trim());
				if(item.contains("Files"))
				{
				inputFields=transmittals.getFilesPostedToInput();
				}
				else if(item.contains("Special"))
				{
					inputFields=transmittals.getSpecialInstructionsInput();
				}
				else
				{
		inputFields=transmittals.getTransmittalFormTextFields(item);
				}
		inputFields.clear();
		inputFields.sendKeys(dataItem);
			}
		  TestBaseProvider.getTestBase().getContext().setProperty("form.values", formValues);
	}

	@SuppressWarnings("unchecked")
	@Then("^the data entered into the \"([^\"]*)\" feild matches the user's input$")
	public void the_data_entered_into_the_feild_matches_the_user_s_input(String fields) throws Throwable {
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		ArrayList<String> formValues=(ArrayList<String>) TestBaseProvider.getTestBase().getContext().getProperty("form.values");
		ArrayList<String> pdfValues=new ArrayList<>();
		List<String> items = new Gson().fromJson(fields, List.class);
		 for(String item:items)
		 {
			String transmittalpdfvalue= transmittalpdf.getValueElement(item).getText().trim();
			 if(transmittalpdf.getAllLabelsInPdf().contains(transmittalpdfvalue))
				{
					pdfValues.add("");
				}
			 else
			 {
			 pdfValues.add(transmittalpdfvalue);
			 }
		 }
		 Assert.assertEquals(formValues, pdfValues);
		
	}

	@SuppressWarnings("unchecked")
	@Then("^is displayed with the label as \"([^\"]*)\"$")
	public void is_displayed_with_the_label_as(String fields) throws Throwable {
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		List<String> items = new Gson().fromJson(fields, List.class);
		 for(String item:items)
		 {
			 String routingValue=transmittalpdf.getLabelElement(item).getText().trim();
			 Assert.assertEquals(routingValue, item);
		 }
	}

	@SuppressWarnings("unchecked")
	@Then("^The selected option \"([^\"]*)\" is displayed in the \"([^\"]*)\" Transmittal Form$")
	public void the_selected_option_is_displayed_in_the_Transmittal_Form(String options, String arg2) throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		ArrayList<String> routingValues=new ArrayList<>();
		ArrayList<String> formValues=new ArrayList<>();
		routingValues.add(routingform.getValueElement("Mech").getText().trim());
		routingValues.add(routingform.getValueElement("Herewith").getText().trim());
		routingValues.add(routingform.getValueElement("Proofs").getText().trim());
		List<String> items = new Gson().fromJson(options, List.class);
		 for(String item:items)
		 {
			 formValues.add(item.trim()); 
		 }
		 Collections.sort(routingValues);
		 Collections.sort(formValues);
		 Assert.assertEquals(formValues, routingValues); 
	}
	
	@Then("^'Spine' information is not displayed on the printed Form$")
	public void spine_information_is_not_displayed_on_the_printed_Form() throws Throwable {
		InternalRoutingForm routingform=new InternalRoutingForm();
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		Assert.assertFalse(transmittalpdf.getAllLabelsInPdf().contains("Spine"));
	}
	

	
	@Then("^A header is displayed as \"([^\"]*)\" followed by the selected \"([^\"]*)\" value$")
	public void header_is_displayed_as_name_followed_by_mechs_value(String name,String mechs) throws Throwable {
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String headerelm = transmittalpdf.getLabelElement(name).getText();
		String headerelm1=transmittalpdf.getLabelElement(mechs).getText();
		String header=headerelm.trim()+headerelm1.trim();
		Assert.assertTrue(header.equals(name+mechs));
}
	
	
	@Then("^Transmittal Form is displayed as a pdf$")
	public void transmittal_Form_is_displayed_as_a_pdf() throws Throwable {
	       WebDriver driver=TestBaseProvider.getTestBase().getDriver();
           String curWindow = driver.getWindowHandle();
           Set<String> windows = driver.getWindowHandles();
           TestBaseProvider.getTestBase().getContext().setProperty("cover.windows", windows.size());
           for (String window : windows) {
                  System.err.println("window " + window);
                  if (!window.equalsIgnoreCase(curWindow))
                        ;
                  driver.switchTo().window(window);
                  Assert.assertNotSame("same window", curWindow, window);
                  System.out.println("Pdf file opened");
           }
    }

	

	
//	@Then("^Transmittal Form is displayed as a pdf$")
//	public void transmittal_Form_is_displayed_as_a_pdf() throws Throwable {
//	    // Write code here that turns the phrase above into concrete actions
//		 WebDriver driver=TestBaseProvider.getTestBase().getDriver();
//		 TransmittalPdf transmittalpdf=new TransmittalPdf();
//		 String transmittalFormWindow = driver.getWindowHandle();
//		 TestBaseProvider.getTestBase().getContext().setProperty("transmittal.window", transmittalFormWindow);
//			Set<String> windows = driver.getWindowHandles();
//			Assert.assertTrue(windows.size()>1);
//			for (String window : windows) {
//				System.err.println("window " + window);
//				if (!window.equalsIgnoreCase(transmittalFormWindow))
//					;
//				driver.switchTo().window(window);
//					}
//			Thread.sleep(20000);
//			Assert.assertTrue(driver.getTitle().contains("pdf"));
//			/* String pdfValue=transmittalpdf.getLabelElement("Cover").getText();
//			 Assert.assertTrue(pdfValue.contains("Cover"));*/
//		  
//		  
//	}
	
	
	@Then("^'Date' in Transmittal Form matches 'Last Modified' value in Transmittal History$")
	public void date_in_Transmittal_Form_matches_Last_Modified_value_in_Transmittal_History() throws Throwable {
		Thread.sleep(10000);
		String lastModifiedDate=(String) TestBaseProvider.getTestBase().getContext().getProperty("last.modified.date");
		TransmittalPdf pdf=new TransmittalPdf();
		MyProjectsPage myprojects=new MyProjectsPage();
		String date=pdf.getValueElement("Date:").getText();
		String formattedModifiedDate=myprojects.setFormatFromYY(lastModifiedDate);

		Assert.assertEquals(date, formattedModifiedDate);
	}


	@Then("^\"([^\"]*)\"in Transmittal Form is 'Production/Manufacturing Transmittal Form for Cover'$")
	public void title_Prefix_in_Transmittal_Form_is_Production_Manufacturing_Transmittal_Form_for_Cover(String title) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(10000);
		TransmittalPdf pdf=new TransmittalPdf();
		String titlePrefix=pdf.getLabelElement("Production").getText().trim();
		
		Assert.assertEquals(titlePrefix, title.trim());

	}

	@Then("^'Title Sufix' in Transmittal Form matches 'Mechs' value in Transmittal History$")
	public void title_Sufix_in_Transmittal_Form_matches_Mechs_value_in_Transmittal_History() throws Throwable {
		Thread.sleep(10000);
		TransmittalPdf pdf=new TransmittalPdf();
		String titleSuffix=pdf.getValueElement("-").getText();
		String suffix=(String) TestBaseProvider.getTestBase().getContext().getProperty("mechs.value");
	 	
		Assert.assertEquals(titleSuffix.trim(), suffix);

	
}
	
}
