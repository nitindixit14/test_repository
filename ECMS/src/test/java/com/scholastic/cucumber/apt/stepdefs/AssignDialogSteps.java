package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.AssignDialog;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.ProjectDetails;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AssignDialogSteps {
	
	
	
	@When("^User selects a \"([^\"]*)\" for \"([^\"]*)\" from the Autosuggested list on Assing Dialog$")
	public void user_selects_a_for_from_the_Autosuggested_list_on_assign_dialog(String value, String field)
			throws Throwable {
		AssignDialog assign = new AssignDialog();
		assign.switchToAssignDialog();
		assign.selectAssignAutoSuggest(field.trim(), value);
		assign.clickAssignSaveBtn();
	}
	
	@Then("^the \"([^\"]*)\" match information in Project Details view$")
    public void the_match_information_in_Project_Details_view(String artDirector) throws Throwable {
           Thread.sleep(3000);
           MyProjectsPage myProj = new MyProjectsPage();
           myProj.waitForResultsLoadingComplete();
           ProjectDetails projectDetails = new ProjectDetails();
           projectDetails.switchToDetailsDialog();
           String detailsValue =TestBaseProvider.getTestBase().getDriver().findElement(By.cssSelector("#output_artDirector")).getAttribute("value");
           Assert.assertEquals(detailsValue, artDirector);
    }
	
	@Then("^A list of 'Art Directors' containing the typed \"([^\"]*)\" is displayed$")
	public void a_list_of_Art_Directors_containing_the_typed_is_displayed(String CHARACTER_SEQUENCE) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		//List<WebElement> l=assign.getassignAutoSuggestElements();
		
		List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		
       for(WebElement names:l)
		{
			System.out.println(names.getText());
			String Artdirectors=names.getText();
			Assert.assertTrue(Artdirectors.toLowerCase().trim().contains(CHARACTER_SEQUENCE), "FAIL-required suggestion doesn't contains");
		}
		
	 }
	
	@Then("^A list of 'Designers' containing the typed \"([^\"]*)\" is displayed$")
	public void a_list_of_Designers_containing_the_typed_is_displayed(String CHARACTER_SEQUENCE) throws Throwable {
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		//List<WebElement> l=	assign.getassignAutoSuggestElements();
		List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		for(WebElement names:l)
		{
			System.out.println(names.getText());
			
			String designers=names.getText();
			Assert.assertTrue(designers.toLowerCase().trim().contains(CHARACTER_SEQUENCE), "FAIL-required suggestion doesn't contains");
		}
		
	 }
		
	@When("^user types \"([^\"]*)\" in \"([^\"]*)\" field$")
	public void user_types_in_field(String CHARACTER_SEQUENCE, String input) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		assign.getAssignInputs(input).clear();
		
		assign.getAssignInputs(input).sendKeys(CHARACTER_SEQUENCE);
		
		/*driver.findElement(By.xpath("//*[@id='inpInteriorDesigner']")).clear();
		driver.findElement(By.xpath("//*[@id='inpInteriorDesigner']")).sendKeys(CHARACTER_SEQUENCE);*/
		Thread.sleep(6000);
		
	}
	
	@Then("^No autosuggest list is displayed$")
	public void no_autosuggest_list_is_displayed() throws Throwable {
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		Assert.assertFalse(assign.getAssignAutoSuggest().isDisplayed(),"FAIL-");
		
		
		//Assert.assertFalse(driver.findElement(By.id("__AutoSuggest_suggestionNode")).isDisplayed(), "FAIL-");
	   
	}
	
	@When("^user selects a \"([^\"]*)\" and go to Assign page$")
	public void user_selects_a_and_go_to_Assign_page(String searchText) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    AssignDialog assign=new AssignDialog();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.searchFor(searchText);
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    	WaitUtils.waitForEnabled(itemgrid.getCheckbox().get(1));
	    	myprojects.clickElementByJS(itemgrid.getCheckbox().get(1));
	    myprojects.clickAssignButton();
	    assign.switchToAssignDialog();
	}
	
	@When("^User selects a Art Director, Cover Designer and Interior Designer from the Autosuggested list$")
	public void user_selects_a_Art_Director_Cover_Designer_and_Interior_Designer_from_the_Autosuggested_list() {
		AssignDialog assign = new AssignDialog();
		assign.selectAutoSuggestValues();

	}

	@Then("^\"([^\"]*)\" label and input field are displayed$")
	public void label_and_input_field_are_displayed(String labels) throws Throwable {
	    AssignDialog assign=new AssignDialog();
	    List<String> labelsList = new Gson().fromJson(labels, List.class);
	    for(String label:labelsList)
	    {
	    AssertUtils.assertDisplayed(assign.getAssignLabels(label));
	    AssertUtils.assertDisplayed(assign.getAssignInputs(label));
	    }
	}
	
	@When("^User selects a \"([^\"]*)\" for \"([^\"]*)\" from the Autosuggested list$")
	public void user_selects_a_for_from_the_Autosuggested_list(String value, String field) throws Throwable {
		AssignDialog assign=new AssignDialog();
		  assign.selectAssignAutoSuggest(field, value);
		  Thread.sleep(2000);
		  assign.clickAssignSaveBtn();
	}

	@Then("^the selected value \"([^\"]*)\" is displayed in the \"([^\"]*)\" column for all the \"([^\"]*)\" projects in the table$")
	public void the_selected_value_is_displayed_in_the_column_for_all_the_projects_in_the_table(String value, String field, String projects) throws Throwable {
	   	MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.getProjectGrid().showAllColumns();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    for(int i=0;i<projectsNo;i++)
	    {
	    	String coltext=itemgrid.getRows().get(i).getColumnText(field);
	    	Assert.assertTrue(coltext.contains(value));
	    }
	}
	
	@When("^user can view the \"([^\"]*)\" and \"([^\"]*)\" in the table for \"([^\"]*)\" project$")
	public void user_can_view_the_and_in_the_table_for_project(String selectedFields, String remainingFields, String projectsNo) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
	    ArrayList<String> selectedFieldsvalues=itemgrid.getColumnTextForList(selectedFields,projectsNo);
	    ArrayList<String> remainingFieldsvalues= itemgrid.getColumnTextForList(remainingFields,projectsNo);
	    getContext().setProperty("selected.fields", selectedFieldsvalues);
	    getContext().setProperty("remaining.fields",remainingFieldsvalues);
	}
	
	private Configuration getContext() {
		return TestBaseProvider.getTestBase().getContext();
	}

	@When("^User selects \"([^\"]*)\" for \"([^\"]*)\" from the Autosuggested list$")
	public void user_selects_for_from_the_Autosuggested_list(String values, String selectedValuesList) throws Throwable {
		AssignDialog assign=new AssignDialog(); 
		List<String> selected = new Gson().fromJson(selectedValuesList, List.class);
		List<String> inputs = new Gson().fromJson(values, List.class);
		    for(int i=0;i<selected.size();i++)
		    {
				assign.selectAssignAutoSuggest(selected.get(i), inputs.get(i));
		    }
	}

	@When("^User Saves Assign selection$")
	public void user_Saves_Assign_selection() throws Throwable {
		 AssignDialog assign=new AssignDialog();
		 assign.clickAssignSaveBtn();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Then("^Assigned values \"([^\"]*)\" for \"([^\"]*)\" are stored in database for \"([^\"]*)\" project$")
	public void assigned_values_for_are_stored_in_database_for_project(String values, String dbselected, String projects) throws Throwable {
		   DBConnection dbcon=new DBConnection();
		   MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ItemGrid itemgrid=myprojects.getProjectGrid();
		    List<String> dboptions = new Gson().fromJson(dbselected, List.class);
		    List<String> selectedValue = new Gson().fromJson(values, List.class);
		    int projectsNo=Integer.parseInt(projects);
		    String dbvalue="";
		 for(int i=0;i<projectsNo;i++)
		    {
			 String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
		    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
		    	while(rs.next())
		    	{
		    	for(int j=0;j<dboptions.size();j++)
		    	{
		    		dbvalue=rs.getString(dboptions.get(j));
		    		if(null==dbvalue)
		    		{
		    			dbvalue="";
		    		}
		    		Assert.assertEquals(dbvalue, selectedValue.get(j));
		    	}
		    	}
		    }
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Then("^Initial project values remains unchanged in database \"([^\"]*)\" for \"([^\"]*)\" projects$")
	public void initial_project_values_remains_unchanged_in_database_for_projects(String dbremaining, String projects) throws Throwable {
	   ArrayList<String> remainingValues= (ArrayList<String>) getContext().getProperty("remaining.fields");
	    List<String> dboptions = new Gson().fromJson(dbremaining, List.class);
	    DBConnection dbcon=new DBConnection();
		   MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    ArrayList<String> dbvalues=new ArrayList<>();
	    for(int i=0;i<projectsNo;i++)
	    {
		 String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
	    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
	    	while(rs.next())
	    	{
	    	for(int j=0;j<dboptions.size();j++)
	    	{
	    		String dbvalue=rs.getString(dboptions.get(j));
	    		if(null==dbvalue)
	    		{
	    			dbvalue="";
	    		}
	    		dbvalues.add(dbvalue.trim());
	    	}
	    	}
	    }
	    Assert.assertEquals(dbvalues, remainingValues);
	}

	@SuppressWarnings("static-access")
	@Then("^Values displayed in Project List for \"([^\"]*)\" matches database values$")
	public void values_displayed_in_Project_List_for_matches_database_values(String projects) throws Throwable {
		DBConnection dbcon=new DBConnection();
		   MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    ArrayList<String> dbvalues=new ArrayList<>();
	    ArrayList<String> col_text=new ArrayList<>();
	    for(int i=0;i<projectsNo;i++)
	    {
		 String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
	    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
	    	while(rs.next())
	    	{
	    		dbvalues.add(rs.getString("ART_DIRECTOR_NAME").trim());
	    		dbvalues.add(rs.getString("COVER_DESIGNER_NAME").trim());
	    		dbvalues.add(rs.getString("INTERIOR_DESIGNER_NAME").trim());

	    	}
	    	Row row=itemgrid.getRows().get(i);
	    	col_text.add(row.getColumnText("Art Director").trim());
	    	col_text.add(row.getColumnText("Cover Designer").trim());
	    	col_text.add(row.getColumnText("Interior Designer").trim());
	    }
	    Collections.replaceAll(dbvalues, null, "");	    
	    Assert.assertEquals(dbvalues, col_text);
	}
	@When("^User unassigns himself as 'Art Director' and saves unassign selection$")
	public void user_unassigns_himself_as_Art_Director_and_saves_unassign_selection() throws Throwable {
		
		MyProjectsPage myprojects=new MyProjectsPage();
		AssignDialog assign=new AssignDialog();
		//assign.switchToAssignDialog();
		Thread.sleep(2000);
		assign.clickRemoveADBtn();
		assign.clickAssignSaveBtn();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		//WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		//driver.findElement(By.xpath("")).click();
	    //driver.findElement(By.xpath("//*[@id='savebutton']")).click();
		
	}
	
	@When("^User is in 'Assign' view$")
	public void user_is_in_Assign_view() throws Throwable {MyProjectsPage myprojects=new MyProjectsPage();
	WebDriver driver = TestBaseProvider.getTestBase().getDriver();
	driver.switchTo().defaultContent();
	myprojects.waitForResultsLoadingComplete();
	//driver.switchTo().frame("display_frame");
	WebElement W=driver.findElement(By.xpath("//*[@id='fb_title']"));
	AssertUtils.assertDisplayed(W);
	Assert.assertEquals(W.getText(),"Assign");
	}

	@Then("^Select fields for 'Art Director','Cover Designer' and'Interior Designer' are aligned$")
	public void select_fields_for_Art_Director_Cover_Designer_and_Interior_Designer_are_aligned() throws Throwable {
		AssignDialog assign=new AssignDialog();
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		
		Thread.sleep(4000);
		int AD_loc=assign.getAssignInputs("Art Director").getLocation().x;
		int CD_loc=assign.getAssignInputs("Cover Designer").getLocation().x;
		int ID_loc=assign.getAssignInputs("Interior Designer").getLocation().x;
		
	   Assert.assertEquals(AD_loc,CD_loc);
	    Assert.assertEquals(CD_loc,ID_loc);
	    
		/*int AD_Y  = driver.findElement(By.xpath("//*[@id='inpArtDirector']")).getLocation().y;
		int CD_Y =  driver.findElement(By.xpath("//*[@id='inpCoverDesigner']")).getLocation().y;
		int ID_Y=   driver.findElement(By.xpath("//*[@id='inpInteriorDesigner']")).getLocation().y;
				*/
		
		
		}
	
	@When("^user types \"([^\"]*)\" in Art Director field$")
	public void user_types_in_Art_Director_field(String CHARACTER_SEQUENCE) throws Throwable {
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.findElement(By.xpath("")).sendKeys(CHARACTER_SEQUENCE);
		
	
	}

	@Then("^A list of 'Art Directors' containing the typed character sequence is displayed$")
	public void a_list_of_Art_Directors_containing_the_typed_character_sequence_is_displayed(String CHARACTER_SEQUENCE) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		
		for(int i=0;i<=l.size();i++)
		{
			
	String suggestion=l.get(i).getText();
	Assert.assertTrue(suggestion.contains(CHARACTER_SEQUENCE));
		}
		
		
	}


	@Then("^Initial project values remains unchanged in database for \"([^\"]*)\" project$")
	public void initial_project_values_remains_unchanged_in_database(String ISBN)
			throws ClassNotFoundException, SQLException {
		ResultSet rs = DBConnection.dbConnection(
				"select ART_DIRECTOR_NAME,COVER_DESIGNER_NAME,INTERIOR_DESIGNER_NAME  from APT_PROJECTS_VIEW where ISBN_13 LIKE '%"
						+ ISBN + "%'");
		String artDirectorDB = null;
		String coverDesignerDB = null;
		String interiorDesignerDB = null;
		while (rs.next()) {
			if (rs.getObject(1) != null) {
				artDirectorDB = rs.getObject(1).toString();
				Assert.assertTrue(artDirectorDB.trim()
						.equals(TestBaseProvider.getTestBase().getContext().getProperty("initial.art.director").toString().trim()));

			} else {
				Assert.assertTrue(
						TestBaseProvider.getTestBase().getContext().getProperty("initial.art.director").equals(""));

			}
			if (rs.getObject(2) != null) {
				coverDesignerDB = rs.getObject(2).toString();
				Assert.assertTrue(coverDesignerDB.trim()
						.equals(TestBaseProvider.getTestBase().getContext().getProperty("initial.cover.designer").toString().trim()));

			} else {
				Assert.assertTrue(
						TestBaseProvider.getTestBase().getContext().getProperty("initial.cover.designer").equals(""));

			}
			if (rs.getObject(3) != null) {
				interiorDesignerDB = rs.getObject(3).toString();
				Assert.assertTrue(interiorDesignerDB.trim()
						.equals(TestBaseProvider.getTestBase().getContext().getProperty("initial.interior.designer").toString().trim()));

			} else {
				Assert.assertTrue(TestBaseProvider.getTestBase().getContext().getProperty("initial.interior.designer")
						.equals(""));

			}
		}
	}

	@Then("^Assigned values are stored in database for \"([^\"]*)\" project$")
	public void assigned_values_are_stored_in_database_for_project(String ISBN) throws ClassNotFoundException, SQLException {
		ResultSet rs = DBConnection.dbConnection(
				"select ART_DIRECTOR_NAME,COVER_DESIGNER_NAME,INTERIOR_DESIGNER_NAME  from APT_PROJECTS_VIEW where ISBN_13 LIKE '%"
						+ ISBN + "%'");
		String artDirectorDB = null;
		String coverDesignerDB = null;
		String interiorDesignerDB = null;
		while (rs.next()) {
			if (rs.getObject(1) != null) {
				artDirectorDB = rs.getObject(1).toString();
				Assert.assertTrue(artDirectorDB.toLowerCase()
						.contains(TestBaseProvider.getTestBase().getContext().getProperty("given.art.director").toString().toLowerCase()));

			} else {
				Assert.assertTrue(
						TestBaseProvider.getTestBase().getContext().getProperty("given.art.director").equals(""));

			}
			if (rs.getObject(2) != null) {
				coverDesignerDB = rs.getObject(2).toString();
				Assert.assertTrue(coverDesignerDB.toLowerCase()
						.contains(TestBaseProvider.getTestBase().getContext().getProperty("given.cover.designer").toString().toLowerCase()));

			} else {
				Assert.assertTrue(
						TestBaseProvider.getTestBase().getContext().getProperty("given.cover.designer").equals(""));

			}
			if (rs.getObject(3)!= null) {
				interiorDesignerDB = rs.getObject(3).toString();
				Assert.assertTrue(interiorDesignerDB.toLowerCase()
						.contains(TestBaseProvider.getTestBase().getContext().getProperty("given.interior.designer").toString().toLowerCase()));

			} else {
				Assert.assertTrue(TestBaseProvider.getTestBase().getContext().getProperty("given.interior.designer")
						.equals(""));

			}
		}
	

	}

	@And("^Assigned values are displayed in Project List$")
	public void assigned_values_are_displayed_in_Project_List() {
		ItemGrid itmeGrid = new MyProjectsPage().getProjectGrid();
		String projectListArtDirector = itmeGrid.getRows().get(0)
				.getColumnText(itmeGrid.getColumnIndex("Art Director"));
		String projectListCoverDesigner = itmeGrid.getRows().get(0)
				.getColumnText(itmeGrid.getColumnIndex("Cover Designer"));
		String projectListInterioDesigner = itmeGrid.getRows().get(0)
				.getColumnText(itmeGrid.getColumnIndex("Interior Designer"));
		Assert.assertTrue(projectListArtDirector.toLowerCase()
				.contains(TestBaseProvider.getTestBase().getContext().getProperty("given.art.director").toString().toLowerCase()));

		Assert.assertTrue(projectListCoverDesigner.toLowerCase()
				.contains(TestBaseProvider.getTestBase().getContext().getProperty("given.cover.designer").toString().toLowerCase()));

		Assert.assertTrue(projectListInterioDesigner.toLowerCase()
				.contains(TestBaseProvider.getTestBase().getContext().getProperty("given.interior.designer").toString().toLowerCase()));
	}

	@Then("^Initial project values are displayed unchanged in Project List$")
	public void initial_project_values_are_displayed_unchanged_in_Project_List() {
		ItemGrid itmeGrid = new MyProjectsPage().getProjectGrid();
		String projectListArtDirector = itmeGrid.getRows().get(0)
				.getColumnText(itmeGrid.getColumnIndex("Art Director"));
		String projectListCoverDesigner = itmeGrid.getRows().get(0)
				.getColumnText(itmeGrid.getColumnIndex("Cover Designer"));
		String projectListInterioDesigner = itmeGrid.getRows().get(0)
				.getColumnText(itmeGrid.getColumnIndex("Interior Designer"));
		Assert.assertTrue(projectListArtDirector
				.equals(TestBaseProvider.getTestBase().getContext().getProperty("initial.art.director")));

		Assert.assertTrue(projectListCoverDesigner
				.equals(TestBaseProvider.getTestBase().getContext().getProperty("initial.cover.designer")));

		Assert.assertTrue(projectListInterioDesigner
				.equals(TestBaseProvider.getTestBase().getContext().getProperty("initial.interior.designer")));
	}
	
	
  @Then("^The \"([^\"]*)\" list is updated each time user types an  additional character$")
	public void the_list_is_updated_each_time_user_types_an_additional_character(String input,DataTable table) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		
		System.out.println(table);
		List<List<String>> data= table.raw();
		System.out.println(data.get(1).get(0));
		
		System.out.println(data.get(3).get(0));
		
		
	
		AssignDialog assign=new AssignDialog();
		assign.getAssignInputs(input).clear();
		assign.getAssignInputs(input).sendKeys(data.get(1).get(0));
		
		Thread.sleep(5000);
		List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		//List<WebElement> l=	assign.getassignAutoSuggestElements();
		Thread.sleep(5000);
		int x=l.size();
		
		
		assign.getAssignInputs(input).clear();
		assign.getAssignInputs(input).sendKeys(data.get(3).get(0));
		
		Thread.sleep(5000);
		List<WebElement> l2=driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		//List<WebElement> l2=assign.getassignAutoSuggestElements();
		Thread.sleep(5000);
		int y=l2.size();
		
		if(x==y)
		{
			
			Assert.fail();
		}
	}
  
  @When("^user types \"([^\"]*)\" in \"([^\"]*)\"$")
	public void user_types_in(String CHARACTER_SEQUENCE, String SELECT_FIELD) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		assign.getAssignInputs(SELECT_FIELD).sendKeys(CHARACTER_SEQUENCE);
		Thread.sleep(3000);
		
		
  }
	@Then("^A list of 'InteriorDesigners' containing the typed \"([^\"]*)\" is displayed$")
	public void a_list_of_InteriorDesigners_containing_the_typed_is_displayed(String CHARACTER_SEQUENCE) throws Throwable {
	 
     WebDriver driver=TestBaseProvider.getTestBase().getDriver();
     AssignDialog assign=new AssignDialog();
     List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div")); 
	
		for(WebElement names:l)
		{
			System.out.println(names.getText());
			String designers=names.getText();
			Assert.assertTrue(designers.toLowerCase().trim().contains(CHARACTER_SEQUENCE), "FAIL-required suggestion doesn't contains");
		}
		
	}
  
  
}
