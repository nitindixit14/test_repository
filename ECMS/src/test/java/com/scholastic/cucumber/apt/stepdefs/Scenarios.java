package com.scholastic.cucumber.apt.stepdefs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.scholastic.cucumber.apt.pageobjects.AssignDialog;
import com.scholastic.cucumber.apt.pageobjects.HeaderSection;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class Scenarios {

	@Given("^user is in My Projects view or Search Results view$")
	public void user_is_in_My_Projects_view_or_Search_Results_view() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		Thread.sleep(1000);
	}

	@When("^user clicks on Assign button without selecting a product$")
	public void user_clicks_on_Assign_button_without_selecting_a_product() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.clickAssignButton();
	}

	@Then("^an error message is displayed with the text: Select at least one product to Assign\\.$")
	public void an_error_message_is_displayed_with_the_text_Select_at_least_one_product_to_Assign() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String errorMessage=driver.findElement(By.id("fb_message_label")).getText();
		Assert.assertEquals("Select at least one product to Assign.", errorMessage);
	}
	
	@When("^user clicks save without making any change to assignments$")
	public void user_clicks_save_without_making_any_change_to_assignments() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
	    AssignDialog assign=new AssignDialog();
	    assign.switchToAssignDialog();
	    assign.clickAssignSaveBtn();
	}

	@Then("^an error message is displayed with the text: Select or remove an Art Director or Designer\\.$")
	public void an_error_message_is_displayed_with_the_text_Select_or_remove_an_Art_Director_or_Designer() throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String errorMessage=driver.findElement(By.id("fb_message_label")).getText();
		Assert.assertEquals("Select or remove an Art Director or Designer.", errorMessage);
	}
	
	@Then("^the Show per Page option should display as \"([^\"]*)\"$")
	public void the_Show_per_Page_option_should_display_as(String option) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.clickPagingOption();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		String pagingOption = myprojects.getSelectedPagingOption().getText();
		Assert.assertEquals(pagingOption, option);
	}
	
	@When("^the user updates the Show per Page option to \"([^\"]*)\"$")
	public void the_user_updates_the_Show_per_Page_option_to(String projectsPerPage) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.getPagingOption(projectsPerPage);
	}

	@When("^the user conducts any valid search$")
	public void the_user_conducts_any_valid_search() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.searchFor("batman");
	}

	@When("^the user navigates to the My Project$")
	public void the_user_navigates_to_the_My_Project() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		HeaderSection headerSection = new HeaderSection();
		myprojects.clickElementByJS(headerSection.getMyProjectsBreadCrumb());
		myprojects.waitForResultsLoadingComplete();
		Thread.sleep(2000);
	}
	
	@Then("^the column header \"([^\"]*)\" should be highlighted$")
	public void the_column_header_should_be_highlighted(String option) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myProjects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		System.out.println(itemgrid.getHeader(option).getAttribute("style"));
		System.out.println(itemgrid.getHeader(option).getAttribute("background-color"));
		System.out.println(itemgrid.getHeader(option).getCssValue("color"));
		Assert.assertEquals(itemgrid.getHeader(option).getCssValue("color").trim(),("rgba(72, 116, 173, 1)"));
	}
	
	@When("^user searches without entering input$")
	public void user_searches_without_entering_input() throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.searchFor("");
	}

	@Then("^Error Message displays$")
	public void error_Message_displays() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		List<WebElement> errors = projectsPage.getErrorMessages();
		assertThat(errors.size(), greaterThan(0));
	}

	@Then("^Error Message text is: Enter a search value\\.$")
	public void error_Message_text_is_Enter_a_search_value() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String errorMessage=driver.findElement(By.id("fb_message_label")).getText();
		Assert.assertEquals("Enter a search value.", errorMessage);
	}
	
	@Then("^\"([^\"]*)\" is displayed correctly$")
	public void is_displayed_correctly(String arg1) throws Throwable {
		TransmittalForm transmittalForm=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittalForm.getLabel("Cover # of Colors:"));
		Assert.assertTrue(transmittalForm.getLabel("Cover # of Colors:").getText().contains("Cover # of Colors"));
	}
	
	@Then("^user will navigate to the specified page \"([^\"]*)\"$")
	public void user_will_navigate_to_the_specified_page(String pageNum) throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		Assert.assertEquals(projectsPage.getPageNavigator().getAttribute("value"),pageNum);
	}
	
	@Then("^\"([^\"]*)\" label is displayed completely$")
	public void label_is_displayed_completely(String arg1) throws Throwable {
	 
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		WaitUtils.waitForDisplayed(myprojects.getMyProjectsTitle());
		AssertUtils.assertDisplayed(myprojects.getMyProjectsTitle());
		
		
	}
	
}
