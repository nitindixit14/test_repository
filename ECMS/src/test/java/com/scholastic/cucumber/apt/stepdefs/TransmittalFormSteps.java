package com.scholastic.cucumber.apt.stepdefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.cucumber.apt.pageobjects.ProjectDetails;
//import com.scholastic.cucumber.apt.pageobjects.PendingException;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.cucumber.apt.pageobjects.TransmittalPdf;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransmittalFormSteps {

	@Then("^'New Transmittal Form' view is displayed$")
	public void new_Transmittal_Form_view_is_displayed() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		
		AssertUtils.assertDisplayed(myprojects.getDialog().getDialogTitleElement());
		
	}
	
	@When("^User generates a \"([^\"]*)\" Transmittal Form for a project, using any \"([^\"]*)\" and \"([^\"]*)\" value$")
	public void user_generates_a_Transmittal_Form_for_a_project_using_any_and_value(String type, String Mechs, String proofs) throws Throwable {
	   
		TransmittalForm tf=new TransmittalForm();
		
		   tf.switchToTransmittalForm();
		   WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		   tf.getTransmittalFormButtons(Mechs).click();
		   tf.getTransmittalFormButtons(proofs).click();
		   //driver.findElement(By.xpath("//*[@id='radio1_1']")).click();
		   Thread.sleep(2000);
		   //driver.findElement(By.xpath("//*[@id='chEpson']")).click();
		   tf.getSubmitButton().click();
		   Thread.sleep(4000);
		
	}

	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" 'Cover/Jacket/Interior' radiobuttons are displayed$")
	public void cover_Jacket_Interior_radiobuttons_are_displayed(String transmittalTypes) throws Throwable {
	 
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		 List<String> types = new Gson().fromJson(transmittalTypes, List.class);
			for (String transmittal : types) { 	  
		 WebElement transmittalType=transmittals.getTransmittalFormButtons(transmittal);
		 AssertUtils.assertDisplayed(transmittalType);
			}
	}
	
	@Given("^\"([^\"]*)\" is logged in OTBPM$")
	public void is_logged_in_OTBPM(String user) throws Throwable {
		OTBPMSigninPage signin = new OTBPMSigninPage();
		signin.launchPage();
		signin.signIn(TestBaseProvider.getTestBase().getContext().getString("transmittalFormUsers."+user+".name"),
				TestBaseProvider.getTestBase().getString("transmittalFormUsers."+user+".password"));
	}

	@Then("^\"([^\"]*)\" radiobutton is selected$")
	public void radiobutton_is_selected(String transmittal) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		 WebElement transmittalType=transmittals.getTransmittalFormButtons(transmittal);
		 AssertUtils.assertSelected(transmittalType);
	}
	
	@When("^the user selects the Transmittal Form$")
	public void the_user_selects_the_Transmittal_Form() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex("Transmittal Form");		
	}

	@Then("^Transmittal Form \"([^\"]*)\" is updated for \"([^\"]*)\"$")
	public void transmittal_Form_is_updated_for(String title, String transmittal) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		myprojects.getDialog();
		myprojects.switchToFrame();
		Assert.assertTrue(myprojects.getDialog().getDialogTitleElement().getText().trim().contains(title));
	}

	@Then("^\"([^\"]*)\" label is updated for \"([^\"]*)\"$")
	public void label_is_updated_for(String typeOfColors, String transmittalType) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.switchToFrame();
		TransmittalForm transmittalForm=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittalForm.getLabel(typeOfColors));
		Assert.assertTrue(transmittalForm.getLabel(typeOfColors).getText().contains(transmittalType));
	}
	
	@Then("^Transmittal Form for \"([^\"]*)\" is displayed as a pdf that opens in a new tab$")
	public void transmittal_Form_for_is_displayed_as_a_pdf_that_opens_in_a_new_tab(String transmitalType) throws Throwable {
		 WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		 String transmittalFormWindow = driver.getWindowHandle();
		 TestBaseProvider.getTestBase().getContext().setProperty("transmittal.window", transmittalFormWindow);
			Set<String> windows = driver.getWindowHandles();
			Assert.assertTrue(windows.size()>1);
			for (String window : windows) {
				System.err.println("window " + window);
				if (!window.equalsIgnoreCase(transmittalFormWindow))
					;
				driver.switchTo().window(window);
			}
			String pdfValue=transmittalpdf.getLabelElement(transmitalType).getText();
			Assert.assertTrue(pdfValue.contains(transmitalType));
		   Assert.assertTrue(driver.getTitle().contains("pdf"));
		   
	}
	
//	@When("^user selects 'Mech' option \"([^\"]*)\"$")
//	public void user_selects_Mech_option(String other) throws Throwable {
//		MyProjectsPage myprojects=new MyProjectsPage();
//		TransmittalForm transmittals=new TransmittalForm();
//		myprojects.getDialog();
//		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
//		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
//		driver.switchTo().frame(0);
//		transmittals.getTransmittalFormButtons(other).click();
//	}
//
//	@Then("^a text field is displayed for user to type$")
//	public void a_text_field_is_displayed_for_user_to_type() throws Throwable {
//	    TransmittalForm transmittalform=new TransmittalForm();
//	    AssertUtils.assertDisplayed(transmittalform.getFormMechOther());
//	}
	
	@When("^user selects 'Mech/ProofsTypes' option \"([^\"]*)\"$")
	public void user_selects_Mech_ProofsTypes_option(String other) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().frame(0);
		transmittals.getTransmittalFormButtons(other).click();
		transmittals.getProofTypeOtherCheckbox().click();
		transmittals.getHerewithOtherCheckbox().click();
	}

	@Then("^a text field is displayed for user to type for 'Mech/ProofsTypes'$")
	public void a_text_field_is_displayed_for_user_to_type_for_Mech_ProofsTypes() throws Throwable {
		  TransmittalForm transmittalform=new TransmittalForm();
		    AssertUtils.assertDisplayed(transmittalform.getFormMechOther());
		    AssertUtils.assertDisplayed(transmittalform.getProofTypeOtherInput());
		    AssertUtils.assertDisplayed(transmittalform.getHerewithOtherInput());
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The selected \"([^\"]*)\" option in \"([^\"]*)\" is displayed in the Transmittal Form$")
	public void the_selected_option_in_is_displayed_in_the_Transmittal_Form(String types, String selectedOptions) throws Throwable {
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		 List<String> itemTypes = new Gson().fromJson(types, List.class);
		 List<String> options = new Gson().fromJson(selectedOptions, List.class);
			for(int i=0;i<itemTypes.size();i++)
			{
				Assert.assertEquals(transmittalpdf.getTransmittalPdfValue(itemTypes.get(i)), options.get(i));
			}
	}
	
	@Then("^'ARC'checkbox is unchecked$")
	public void arc_checkbox_is_unchecked() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.switchToTransmittalForm();
		WebElement transmittalType=transmittals.getTransmittalFormButtons("Advanced Reader Copy");
		Assert.assertFalse(transmittalType.isSelected());
	}
	
	@When("^user selects option \"([^\"]*)\"$")
	public void user_selects_option(String option) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.switchToTransmittalForm();
		transmittals.getTransmittalFormButtons(option).click();
		/*WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		Thread.sleep(10000L);
		System.out.println("Checkbox detail is:" +(driver.findElement(By.xpath(".//*[@id='chAdditionalProof']"))));
		*/
	
	}

	@When("^user select an \"([^\"]*)\" from the list$")
	public void user_select_an_from_the_list(String option) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.getAdditionalDropdown(option);
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		transmittals.switchToTransmittalForm();
	}

	@Then("^The additional \"([^\"]*)\" is displayed in the Transmittalpdf Form$")
	public void the_additional_is_displayed_in_the_Transmittalpdf_Form(String option) throws Throwable {
	    TransmittalPdf transmittalpdf=new TransmittalPdf();
	   String pdfValue= transmittalpdf.getValueElement("Proofs:").getText().trim();
	   Assert.assertEquals(pdfValue,option);
	}
	
	

	@When("^user selects \"([^\"]*)\" custom \"([^\"]*)\"$")
	public void user_selects_custom(String option, String value) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		transmittals.switchToTransmittalForm();
		 if(option.equals("OtherHereWith"))
		   {
			   transmittals.getHerewithOtherCheckbox().click();
			   WaitUtils.waitForPresent(transmittals.getHerewithOtherInput());
			   transmittals.getHerewithOtherInput().clear();
			   transmittals.getHerewithOtherInput().sendKeys(value);
		   }
		   
		   else  if(option.equals("OtherProofType"))
		   {
			   transmittals.getProofTypeOtherCheckbox().click();
			   WaitUtils.waitForDisplayed(transmittals.getProofTypeOtherInput());
			   transmittals.getProofTypeOtherInput().clear();
			   transmittals.getProofTypeOtherInput().sendKeys(value);
		   }
		   else
		   {
				transmittals.getTransmittalFormButtons(option).click();
				transmittals.getFormMechOtherInput().clear();
				transmittals.getFormMechOtherInput().sendKeys(value);
		   }
	}

	@SuppressWarnings("unchecked")
	@Then("^The selected 'Proofs Types' \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" options are is displayed in the Transmittal Form$")
	public void the_selected_Proofs_Types_options_are_is_displayed_in_the_Transmittal_Form(String checkboxes, String option, String value) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		List<String> types = new Gson().fromJson(checkboxes, List.class);
		String selectedValues="";
		for (String proofType : types) { 	  
			if(selectedValues.isEmpty())
			{
				selectedValues=proofType;
			}
			else{
			selectedValues=selectedValues+", "+proofType;
			}
		}
		if(!option.trim().isEmpty()){
			selectedValues=selectedValues+", "+option;	
		}
		if(!value.trim().isEmpty()){
			selectedValues=selectedValues+", "+value;
		}
		
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String pdfValues= transmittalpdf.getTransmittalPdfValue("Proofs:");
		ArrayList<String> pdf=transmittalform.splitAndSortString(pdfValues);
		ArrayList<String> form=transmittalform.splitAndSortString(selectedValues);
		Assert.assertEquals(form, pdf);
	}
	
	@Then("^The custom value \"([^\"]*)\" is displayed in the Transmittal Form pdf \"([^\"]*)\"$")
	public void the_custom_value_is_displayed_in_the_Transmittal_Form_pdf(String value, String label) throws Throwable {
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String pdfValue=transmittalpdf.getTransmittalPdfValue(label);
		Assert.assertEquals(pdfValue,value);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The selected 'Herewith' \"([^\"]*)\" options are is displayed in the Transmittal Form$")
	public void the_selected_Herewith_options_are_is_displayed_in_the_Transmittal_Form(String options) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		List<String> types = new Gson().fromJson(options, List.class);
		String selectedValues="";
		for (String herewith : types) { 	  
			if(selectedValues.isEmpty())
			{
				selectedValues=herewith;
			}
			else{
			selectedValues=selectedValues+", "+herewith;
			}
		}
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String pdfValues= transmittalpdf.getTransmittalPdfValue("Herewith:");
		ArrayList<String> pdf=transmittalform.splitAndSortString(pdfValues);
		ArrayList<String> form=transmittalform.splitAndSortString(selectedValues);
		Assert.assertEquals(form, pdf);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The selected 'Herewith' \"([^\"]*)\",\"([^\"]*)\" options are is displayed in the Transmittal Form$")
	public void the_selected_Herewith_options_are_is_displayed_in_the_Transmittal_Form(String checkboxes, String value) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		List<String> types = new Gson().fromJson(checkboxes, List.class);
		String selectedValues="";
		for (String herewith : types) { 
			if(selectedValues.isEmpty())
			{
				selectedValues=herewith;
			}
			else{
			selectedValues=selectedValues+", "+herewith;
			}
		}
		selectedValues=selectedValues+", "+value;
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String pdfValues= transmittalpdf.getTransmittalPdfValue("Herewith:");
		ArrayList<String> pdf=transmittalform.splitAndSortString(pdfValues);
		ArrayList<String> form=transmittalform.splitAndSortString(selectedValues);
		Assert.assertEquals(form, pdf);
	}
	
	@When("^user Cancels the Transmittal Form$")
	public void user_Cancels_the_Transmittal_Form() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.getcloseButton().click();
		TransmittalForm form=new TransmittalForm();
		form.clickFormpopUp("No");
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		myprojects.waitForResultsLoadingComplete();
	}

	@Then("^'Transmittal Form' window closes$")
	public void transmittal_Form_window_closes() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	   AssertUtils.assertNotDisplayed(myprojects.getDialog().getDialogTitleElement());
	}

	@SuppressWarnings("unchecked")
	@Then("^the 'Transmittal Form' of \"([^\"]*)\" and \"([^\"]*)\" is not listed in the transmittal history$")
	public void the_Transmittal_Form_of_and_is_not_listed_in_the_transmittal_history(String type, String fields) throws Throwable {
	   	MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalHistoryDialog historyDialog = myprojects.getTransmittalHistoryDialog();
		historyDialog.openTransmittalHistory(0);
		historyDialog.waitForPresent();
		historyDialog.switchToDialog();
		Row row=historyDialog.getFirstHistoryTypeRow(type);
		String mechs=row.getColumnText("Mechs");
		String proofs=row.getColumnText("Proofs");
		List<String> types = new Gson().fromJson(fields, List.class);
		String selectedValues="";
		for (String values : types) { 
			if(selectedValues.isEmpty())
			{
				selectedValues=values;
			}
			else{
			selectedValues=selectedValues+", "+values;
			}
		}
		Assert.assertNotEquals(selectedValues, mechs+", "+proofs);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^the 'Transmittal Form' of \"([^\"]*)\" and \"([^\"]*)\" are listed in the transmittal history$")
	public void the_Transmittal_Form_of_and_are_listed_in_the_transmittal_history(String type, String fields) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String window=(String) TestBaseProvider.getTestBase().getContext().getProperty("transmittal.window");
		driver.switchTo().window(window);
		TransmittalHistoryDialog historyDialog = myprojects.getTransmittalHistoryDialog();
		myprojects.waitForResultsLoadingComplete();
		historyDialog.switchToHistoryDialog();
		Row row=historyDialog.getFirstHistoryTypeRow(type);
		String mechs=row.getColumnText("Mechs");
		String proofs=row.getColumnText("Proofs");
		List<String> types = new Gson().fromJson(fields, List.class);
		String selectedValues="";
		for (String values : types) { 
			if(selectedValues.isEmpty())
			{
				selectedValues=values;
			}
			else{
			selectedValues=selectedValues+", "+values;
			}
		}
		selectedValues=selectedValues.replaceAll("\\s+","");
		String options=mechs+","+proofs;
		options=options.replaceAll("\\s+","");
		Assert.assertEquals(selectedValues,options );
	}
	
	@SuppressWarnings("unchecked")
	@Then("^grouped \"([^\"]*)\" are displayed inside a container border$")
	public void grouped_are_displayed_inside_a_container_border(String fields) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		transmittalform.switchToTransmittalForm();
		List<String> types = new Gson().fromJson(fields, List.class);
		for (String values : types) { 
			AssertUtils.assertDisplayed(transmittalform.getInformationbox());
			Assert.assertTrue(transmittalform.getInformationbox().getText().contains(values));
		}
	}
	
	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" is displayed$")
	public void is_displayed(String fields) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		transmittalform.switchToTransmittalForm();
		List<String> types = new Gson().fromJson(fields, List.class);
		for (String values : types) { 
			AssertUtils.assertDisplayed(transmittalform.getLabel(values));
		}
	}
	
	@Then("^The selected 'Mech' option in \"([^\"]*)\" is displayed in the Transmittal Form$")
	public void the_selected_Mech_option_in_is_displayed_in_the_Transmittal_Form(String option) throws Throwable {
		TransmittalPdf transmittalpdf=new TransmittalPdf();
			String pdfValues= transmittalpdf.getTransmittalPdfValue("Mech:");
			pdfValues=pdfValues.replaceAll("\\s+","");
			option=option.replaceAll("\\s+","");
			Assert.assertEquals(option, pdfValues);
		}
	
	@Then("^a text field is displayed for user to type for 'Mech'$")
	public void a_text_field_is_displayed_for_user_to_type_for_Mech() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittals.getFormMechOtherInput());
	}
	
	@Then("^a text field is displayed for user to type$")
	public void a_text_field_is_displayed_for_user_to_type() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittals.getProofTypeOtherInput());
	}
	
	@Then("^a 'Herewith' text field is displayed for user to type$")
	public void a_Herewith_text_field_is_displayed_for_user_to_type() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittals.getHerewithOtherInput());
	}
	
	//Added by Nitin
	@Given("^'Transmittal Form' page is displayed for '<project>'$")
	public void transmittal_Form_page_is_displayed_for_project() throws Throwable {
	MyProjectsPage myprojects = new MyProjectsPage();
	myprojects.waitForResultsLoadingComplete();
	Thread.sleep(2000);
	ItemGrid myprojectsgrid = myprojects.getProjectGrid();
	myprojectsgrid.showAllColumns();
	myprojects.waitForResultsLoadingComplete();
	ItemGrid itemgrid = myprojects.getProjectGrid();
	int index = itemgrid.getColumnIndex("Transmittal Form");
	try{
	WaitUtils.waitForDisplayed(itemgrid.getRows().get(0).getColumn(index));
	itemgrid.getRows().get(0).getColumn(index).click();
	}
	catch(Exception e)
	{
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
    	final WebElement details = driver.findElement(By.xpath(".//*[@id='tbmyProjects']/tbody/tr[2]/td[14]/div/input"));
    	details.click();
	}
}
	
	//Added by Nitin
	@When("^user selects 'Proofs Types' option 'Additional Proof Types'$")
	public void user_selects_Proofs_Types_option_Additional_Proof_Types(String option) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		TransmittalForm tf=new TransmittalForm();
		 tf.switchToTransmittalForm();
		transmittals.getTransmittalFormButtons(option).click();
	}
	
	
	@When("\"([^\"]*)\" is entered into 'List Matched Inks'$")
	public void generic_CONTENT_is_entered_into_List_Matched_Inks(String option ) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		TransmittalForm transmittals=new TransmittalForm();
		TransmittalForm tf=new TransmittalForm();
		 tf.switchToTransmittalForm();
		 tf.getListmatchedinksinput().sendKeys(option);
		 tf.getListmatchedinksinput().sendKeys(Keys.TAB);
		Thread.sleep(5000L);
	}
	
	@When("\"([^\"]*)\" is entered into 'List Colors'$")
	public void generic_CONTENT_is_entered_into_List_Colors(String option) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		TransmittalForm transmittals=new TransmittalForm();
		TransmittalForm tf=new TransmittalForm();
		Thread.sleep(5000L);
		tf.getListcolorInput().sendKeys(option);
		tf.getListcolorInput().sendKeys(Keys.TAB);
		Thread.sleep(5000L);
	}
	
	@When("^'Special Instructions' is displayed$")
	public void special_Instructions_is_displayed() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		TransmittalForm transmittals=new TransmittalForm();
		TransmittalForm tf=new TransmittalForm();
		tf.getSpecialinstructioninput().isDisplayed();
		}
	
	@When("\"([^\"]*)\" is entered into 'Files posted to'$")
	public void generic_CONTENT_is_entered_into_Files_posted_to(String option) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		TransmittalForm transmittals=new TransmittalForm();
		TransmittalForm tf=new TransmittalForm();
		tf.getFilespostedtoinput().sendKeys(option);
		tf.getFilespostedtoinput().sendKeys(Keys.TAB);
		Thread.sleep(5000L);
	}
	
	@When("\"([^\"]*)\" is entered into 'Special Instructions'$")
	public void generic_CONTENT_is_entered_into_Special_Instructions(String option) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		TransmittalForm transmittals=new TransmittalForm();
		TransmittalForm tf=new TransmittalForm();
		tf.getSpecialinstructioninput().sendKeys(option);
		tf.getSpecialinstructioninput().sendKeys(Keys.TAB);
		Thread.sleep(5000L);
	}
	
	@Then("^'Other' label text has a capitalized \"([^\"]*)\" under the Mechs, Proof Types and Herewith sections$")
	public void other_label_text_has_a_capitalized_under_the_Mechs_Proof_Types_and_Herewith_sections(String option) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		TransmittalForm transmittals=new TransmittalForm();
		TransmittalForm tf=new TransmittalForm();
		 tf.switchToTransmittalForm();
//		WebElement txt =transmittals.getLabel("Other");
//		String Lable = txt.getText().trim();
		char Expected = 'O';
		String Actual = transmittals.getLabel("Other").getText();
		Assert.assertEquals(Expected, Actual.charAt(0), "message");
		
	
	}
	
	@Then("^Product \"([^\"]*)\" is displayed inside a border section$")
	public void product_information_is_displayed_inside_a_border_section(String fields) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		TransmittalForm transmittalform=new TransmittalForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		transmittalform.switchToTransmittalForm();
		List<String> types = new Gson().fromJson(fields, List.class);
		for (String values : types) { 
			AssertUtils.assertDisplayed(transmittalform.getInformationbox());
			Assert.assertTrue(transmittalform.getInformationbox().getText().contains(values));
		
	}
	
	}
	
	@Given("^User opens a New Transmittal Form for a project$")
	public void user_opens_a_New_Transmittal_Form_for_a_project() throws Throwable {
			ProjectDetails details=new ProjectDetails();
		  details.switchToDetailsDialog();
		  details.getButton("New Transmittal").click();

	}
	
	@Given("^User enters data into the \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_data_into_the_List_Colors_List_Matched_Ink_Files_Posted_to_and_Special_Instruction_fields(String Label1, String Label2,String Label3,String Label4) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.switchToTransmittalForm();
		driver.findElement(By.id("outListColors")).sendKeys(Label1);
		driver.findElement(By.id("outListColors")).sendKeys(Keys.TAB);
		Thread.sleep(5000L);
		driver.findElement(By.id("out_LsMatchLinks")).sendKeys(Label2);
		driver.findElement(By.id("out_LsMatchLinks")).sendKeys(Keys.TAB);
		Thread.sleep(5000L);
		driver.findElement(By.id("InpFilesPostedTo")).sendKeys(Label3);
		driver.findElement(By.id("InpFilesPostedTo")).sendKeys(Keys.TAB);
		Thread.sleep(5000L);
		driver.findElement(By.id("InpInstruction")).sendKeys(Label4);
		driver.findElement(By.id("InpInstruction")).sendKeys(Keys.TAB);
		Thread.sleep(5000L);
	}
	
	@When("^User submits New \"([^\"]*)\" with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_submits_New_with_and(String type, String mechs, String proofs) throws Throwable {
	   
		MyProjectsPage myprojects=new MyProjectsPage();
		   TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		   history.getFormButton().click();
		   TransmittalForm tf=new TransmittalForm();
		  // tf.switchToTransmittalForm();
		   tf.getTransmittalFormButtons(type).click();
		   Thread.sleep(1000);
		   tf.getTransmittalFormButtons(mechs).click();
		   Thread.sleep(2000);
		   tf.getTransmittalFormButtons(proofs).click();
		   TestBaseProvider.getTestBase().getContext().setProperty("proofs", proofs);
		   WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		  
		   tf.getSubmitButton().click();
		   Thread.sleep(10000);
		   System.out.println("New Transform form has been submitted");
		 
		}
	

	@Then("^the \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" into these feilds should matches the user's inputs$")
	public void the_data_entered_into_these_feilds_should_matches_the_user_s_inputs(String Label1, String Label2,String Label3,String Label4) throws Throwable {
	   	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		MyProjectsPage projectsPage = new MyProjectsPage();
		String windowString1=TestBaseProvider.getTestBase().getContext().getString("project.execution.window.title");
		TestBaseProvider.getTestBase().getDriver().switchTo().window(windowString1);
		projectsPage.switchToFrame();
		driver.findElement(By.id("fb_closeImg")).click();
		System.out.println("Closed project dialog");
		String windowString=TestBaseProvider.getTestBase().getContext().getString("execution.window.title");
		TestBaseProvider.getTestBase().getDriver().switchTo().window(windowString);
		
		 MyProjectsPage myprojects=new MyProjectsPage();
		    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		    myprojects.waitForResultsLoadingComplete();
		    myprojects.searchFor("978-1-338-04475-1");
		    try{
		    ItemGrid itemgrid=myprojects.getProjectGrid();	
		    itemgrid.getRows().get(0).getColumn("Transmittal History").click();
		    }
		    catch(Exception e)
		    {
		    //	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		    	final WebElement historybtn = driver.findElement(By.xpath(".//*[@id='tbmyProjects']/tbody/tr[2]/td[13]/div/img"));
		    	historybtn.click();
		    }
		    history.switchToHistoryDialog();
		    Row Row = history.getFirstHistoryTypeRow("Cover");
			WebElement input = Row.findElement(By.id("img_Edit"));
			input.click();
			Thread.sleep(10000L);
			 TransmittalForm tf=new TransmittalForm();
			 tf.switchToTransmittalForm();
			 //JOptionPane.showInputDialog(Label1);
			// JOptionPane.showInputDialog(driver.findElement(By.id("outListColors")).getAttribute("value"));
			 Assert.assertEquals(Label1,driver.findElement(By.id("outListColors")).getAttribute("value"));
			Assert.assertEquals(Label2,driver.findElement(By.id("out_LsMatchLinks")).getAttribute("value"));
			Assert.assertEquals(Label3,driver.findElement(By.id("InpFilesPostedTo")).getAttribute("value"));
			Assert.assertEquals(Label4,driver.findElement(By.id("InpInstruction")).getAttribute("value"));
		}
	
	

}
