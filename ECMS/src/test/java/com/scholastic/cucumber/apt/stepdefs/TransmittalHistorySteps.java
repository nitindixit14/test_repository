package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.ProjectDetails;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.cucumber.apt.pageobjects.UserHomePage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransmittalHistorySteps {

	//Added by Nitin
	@When("^User selects the 'View' icon for a Cover Transmittal Form$")
	public void user_selects_the_View_icon_for_a_Cover_Transmittal_Form() throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		  MyProjectsPage projectsPage = new MyProjectsPage();
		  TransmittalHistoryDialog transmittalHistoryDialog = projectsPage.getTransmittalHistoryDialog();
		  //TestBaseProvider.getTestBase().getContext().setProperty("execution.window.title",);
		  ItemGrid historyResults = transmittalHistoryDialog.getHistoryGrid();
		  Thread.sleep(10000);
		  transmittalHistoryDialog.waitForPresent();
		  transmittalHistoryDialog.switchToDialog();
		  String LastModifieddate = transmittalHistoryDialog.getFirstHistoryTypeRow("Cover").getColumnText("Last Modified");
		  TestBaseProvider.getTestBase().getContext().setProperty("Cover.last.modified.date",LastModifieddate);
		  
		  String Mechs = transmittalHistoryDialog.getFirstHistoryTypeRow("Cover").getColumnText("Mechs");
		  TestBaseProvider.getTestBase().getContext().setProperty("Cover.mechs.value",Mechs);
		  
		  Row Row = transmittalHistoryDialog.getFirstHistoryTypeRow("Cover");
		  WebElement input = Row.findElement(By.id("img_View"));
		  try{
		  WebDriverWait wait = new WebDriverWait(driver, 300);
		  WebElement element = wait.until(ExpectedConditions.visibilityOf(input));
		  input.click();
		  Thread.sleep(10000L);	
		  }catch(Exception e){
					  
			  projectsPage.clickElementByJS(input);
		  }
	}
	@When("^User views Transmittal History for \"([^\"]*)\"$")
		public void user_views_Transmittal_History_for(String project) throws Throwable {
		    MyProjectsPage myprojects=new MyProjectsPage();
		    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		    myprojects.waitForResultsLoadingComplete();
		    Thread.sleep(10000L);
		    myprojects.searchFor(project);
		    try{
		    ItemGrid itemgrid=myprojects.getProjectGrid();	
		    itemgrid.getRows().get(0).getColumn("Transmittal History").click();
		    String title=itemgrid.getRows().get(0).getColumn("Title").getText();
			TestBaseProvider.getTestBase().getContext().setProperty("my.project.title",title);
		    
		    }
		    catch(Exception e)
		    {
		    	myprojects.getMyProjectsIcon().click();
		    }
		    history.switchToHistoryDialog();
		}

	@Then("^Window Title is displayed as 'Transmittal History'$")
	public void window_Title_is_displayed_as_Transmittal_History() throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		Assert.assertEquals("Transmittal History", history.getDialog().getDialogTitleElement().getText().trim());
	}
	
	@Then("^Cover Image is displayed$")
	public void cover_Image_is_displayed() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
	     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	     AssertUtils.assertDisplayed(history.getCoverImage());
	}
	
	@Then("^\"([^\"]*)\" is displayed with \"([^\"]*)\"$")
	public void is_displayed_with(String productInfo, String label) throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
	     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	     Assert.assertEquals(label,history.getLabel(productInfo).getText());
	}

	@SuppressWarnings({ "static-access" })
	@Then("^\"([^\"]*)\" values are populated from database \"([^\"]*)\" for \"([^\"]*)\"$")
	public void values_are_populated_from_database_for(String infoLabel, String dblabel, String isbn) throws Throwable {
	 	DBConnection dbcon=new DBConnection();
	 	MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	 	String dbvalue="";
		ResultSet rs=dbcon.dbConnection("select * from APT_PROJECTS_VIEW WHERE ISBN_13='"+isbn+"'");
		while(rs.next())
		{
			dbvalue=rs.getString(dblabel);
		}	
		Assert.assertEquals(dbvalue,history.getValueElement(infoLabel));
	}

	@Then("^\"([^\"]*)\" values match values displayed in My Project page$")
	public void values_match_values_displayed_in_My_Project_page(String infoLabel) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	   String historyInfoValue= history.getValueElement(infoLabel);
	   myprojects.waitForResultsLoadingComplete();
	   myprojects.getcloseButton().click();
	   myprojects.waitForResultsLoadingComplete();
	   ItemGrid itemgrid=myprojects.getProjectGrid();
	   String projectsValue="";
	   Row row=itemgrid.getRows().get(0);
	  if(infoLabel.equals("ISBN 13"))
	  {
		  projectsValue=row.getColumnText("ISBN"); 
	  }
	  else if(infoLabel.equals("ISBN"))
	  {
			myprojects.waitForResultsLoadingComplete();
			ItemGrid ig=myprojects.getProjectGrid();
			ig.getRows().get(0).getColumn("Title").click();
			ProjectDetails pd=new ProjectDetails();
			pd.switchToDetailsDialog();
			projectsValue=pd.getValueElement(infoLabel).getAttribute("value");
	  }
	  else
	  {
		   projectsValue=row.getColumnText(infoLabel);
	  }
	  Assert.assertEquals(historyInfoValue, projectsValue);
	}
	
	@When("^User submits a New \"([^\"]*)\"  with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_submits_a_New_with_and(String type, String mechs, String proofs) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    history.getFormButton().click();
	    TransmittalForm tf=new TransmittalForm();
	    tf.switchToTransmittalForm();
	    tf.getTransmittalFormButtons(type).click();
	    Thread.sleep(1000);
	    tf.getTransmittalFormButtons(mechs).click();
	    Thread.sleep(2000);
	    tf.getTransmittalFormButtons(proofs).click();
	    TestBaseProvider.getTestBase().getContext().setProperty("proofs", proofs);
	    tf.getSubmitButton().click();
	    WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	   String currWindow= driver.getWindowHandle();
	   myprojects.switchToWindow();
	   driver.switchTo().window(currWindow);
	}

	@Then("^New \"([^\"]*)\" is displayes in Transmittal History list$")
	public void new_is_displayes_in_Transmittal_History_list(String type) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    myprojects.waitForResultsLoadingComplete();
		history.switchToHistoryDialog();
		Row row=history.getFirstHistoryTypeRow(type);
		Assert.assertEquals(row.getColumnText("Type"), type);
		TestBaseProvider.getTestBase().getContext().setProperty("type", type);
		TestBaseProvider.getTestBase().getContext().setProperty("row", row);
	}

	@Then("^'Last Modified' value for the New  Transmittal Form is today$")
	public void last_Modified_value_for_the_New_Transmittal_Form_is_today() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    Row row=(Row) TestBaseProvider.getTestBase().getContext().getProperty("row");
	    String historyDate=row.getColumnText("Last Modified");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		TimeZone tz = TimeZone.getTimeZone("EST");
		Calendar cal = Calendar.getInstance(tz);
		String currDate=dateFormat.format(cal.getTime()).toString();
		   Assert.assertEquals(historyDate, currDate);
	}

	@Then("^'Mechs' value for New  Transmittal Form matches \"([^\"]*)\"$")
	public void mechs_value_for_New_Transmittal_Form_matches(String mechValue) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    Row row=(Row) TestBaseProvider.getTestBase().getContext().getProperty("row");
	   Assert.assertEquals(row.getColumnText("Mechs").toUpperCase(),mechValue.toUpperCase());
	}

	@Then("^'Proofs' value for New Transmittal Form matches \"([^\"]*)\"$")
	public void proofs_value_for_New_Transmittal_Form_matches(String proofsValue) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    Row row=(Row) TestBaseProvider.getTestBase().getContext().getProperty("row");
	   Assert.assertEquals(proofsValue.replaceAll("\\s", "").toUpperCase(),row.getColumnText("Proofs").replaceAll("\\s", "").toUpperCase());
	}

	@Then("^'Transmittal Generated By' value for New  Transmittal Form is user name$")
	public void transmittal_Generated_By_value_for_New_Transmittal_Form_is_user_name() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    Row row=(Row) TestBaseProvider.getTestBase().getContext().getProperty("row");
	   String historyName= row.getColumnText("Transmittal Generated by");
	   myprojects.waitForResultsLoadingComplete();
	    WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	    driver.switchTo().defaultContent();
	   UserHomePage userHomePage = new UserHomePage();
//		WaitUtils.waitForDisplayed(userHomePage.getUserName());
		Assert.assertEquals(historyName, userHomePage.getUserNameText());
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The list of Transmittals include \"([^\"]*)\"$")
	public void the_list_of_Transmittals_include(String headersInfo) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		ItemGrid itemgrid=history.getHistoryGrid();
		 List<String> headers = new Gson().fromJson(headersInfo, List.class);
		 for (String header : headers) {
			AssertUtils.assertDisplayed(itemgrid.getHeader(header));
		}	
	}

	@SuppressWarnings("static-access")
	@Then("^The number on Transmittals Forms on the list for \"([^\"]*)\" matches the amount of Transmittals Forms in database$")
	public void the_number_on_Transmittals_Forms_on_the_list_for_matches_the_amount_of_Transmittals_Forms_in_database(String isbn13) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		ItemGrid itemgrid=history.getHistoryGrid();
		int rowsNum=itemgrid.getRows().size();
		DBConnection dbcon=new DBConnection();
		int dbcount = 0;
		ResultSet rs=dbcon.dbConnection("SELECT COUNT(*) AS TOTAL_ROWS FROM TRANSMITTAL_HISTORY WHERE ISBN_13='"+isbn13+"'");
		while (rs.next()) {
			dbcount=rs.getInt("TOTAL_ROWS");
		}
		Assert.assertEquals(dbcount, rowsNum);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The Transmittals List should display \"([^\"]*)\"$")
	public void the_Transmittals_List_should_display(String editHeaders) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
		ItemGrid itemgrid=history.getHistoryGrid();
		 List<String> headers = new Gson().fromJson(editHeaders, List.class);
		 for (String header : headers) {
			AssertUtils.assertDisplayed(itemgrid.getHeader(header));
		}
	}
	
	@Then("^The Edit icon is displayed only for the most recent \"([^\"]*)\" listed$")
	public void the_Edit_icon_is_displayed_only_for_the_most_recent_listed(String type) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    myprojects.waitForResultsLoadingComplete();
		history.switchToHistoryDialog();
		Row row1=history.getFirstHistoryTypeRow(type);
		ItemGrid itemgrid=history.getHistoryGrid();
		for(Row nextRow:history.getHistoryTypeRows(type))
		{
			if(nextRow==row1)
			{
				AssertUtils.assertDisplayed(row1.getHistoryTableButtons("Edit"));

			}
			AssertUtils.assertNotDisplayed(nextRow.getHistoryTableButtons("Edit"));
		}
	}
	
	@Then("^The Delete icon is displayed only for the most recent \"([^\"]*)\" listed$")
	public void the_Delete_icon_is_displayed_only_for_the_most_recent_listed(String type) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	    myprojects.waitForResultsLoadingComplete();
		history.switchToHistoryDialog();
		Row row1=history.getFirstHistoryTypeRow(type);
		ItemGrid itemgrid=history.getHistoryGrid();
		for(Row nextRow:history.getHistoryTypeRows(type))
		{
			if(nextRow==row1)
			{
				AssertUtils.assertDisplayed(row1.getHistoryTableButtons("Delete"));

			}
			AssertUtils.assertNotDisplayed(nextRow.getHistoryTableButtons("Delete"));
		}
	}
	

@When("^User selects the \"([^\"]*)\" icon for most recent \"([^\"]*)\"$")
public void user_selects_the_icon_for_most_recent(String delete, String type) throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	Row row=history.getFirstHistoryTypeRow(type);
	row.getColumn(delete).click();
	TestBaseProvider.getTestBase().getContext().setProperty("transmittal.type", type);
}

@When("^User dismiss the confirmation message$")
public void user_dismiss_the_confirmation_message() throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	history.getpopAcceptButton().click();
}

@Then("^Transmittal Form with \"([^\"]*)\" and \"([^\"]*)\" is NOT displayed in the Transmittals List$")
public void transmittal_Form_with_and_is_NOT_displayed_in_the_Transmittals_List(String mechs, String proofs) throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    String type=(String) TestBaseProvider.getTestBase().getContext().getProperty("transmittal.type");
    Row row=history.getFirstHistoryTypeRow(type);
    Assert.assertNotEquals(row.getColumnText("Mechs"),mechs);
    Assert.assertNotEquals(row.getColumnText("Proofs"),proofs);
}

@SuppressWarnings("static-access")
@Then("^Transmittal Form with \"([^\"]*)\" and \"([^\"]*)\" is removed from database for \"([^\"]*)\"$")
public void transmittal_Form_with_and_is_removed_from_database_for(String mechs, String proofs, String isbn13) throws Throwable {
   	 String type=(String) TestBaseProvider.getTestBase().getContext().getProperty("transmittal.type");
	 DBConnection dbcon=new DBConnection();
	 ResultSet rs=dbcon.dbConnection("select * from TRANSMITTAL_HISTORY where isbn_13='"+isbn13+"' and TYPE like '%"+type+"%' ORDER BY DATE_CREATED DESC");
	 String dbMechs = null;
	 String dbProofs = null;
	 while(rs.next())
	 {
		dbMechs =rs.getString("Mechs");
		dbProofs=rs.getString("Proofs");
	 }
	 Assert.assertNotEquals(dbMechs,mechs);
	 Assert.assertNotEquals(dbProofs,proofs);
}

@When("^User 'Edit' the most recent \"([^\"]*)\" with \"([^\"]*)\" and \"([^\"]*)\"$")
public void user_Edit_the_most_recent_with_and(String type, String otherMech, String otherProof) throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	Row row=history.getFirstHistoryTypeRow(type);
	row.getColumn("Edit").click();
	TransmittalForm tf=new TransmittalForm();
	tf.switchToTransmittalForm();
    tf.getFormMechOther().click();
    tf.getFormMechOtherInput().sendKeys(otherMech);
    String proofs=(String) TestBaseProvider.getTestBase().getContext().getProperty("proofs");
    tf.getTransmittalFormButtons(proofs).click();
    tf.getProofTypeOtherCheckbox().click();
    tf.getProofTypeOtherInput().sendKeys(otherProof);
    tf.getFormUpdateBtn().click();
    WebDriver driver=TestBaseProvider.getTestBase().getDriver();
   String currWindow= driver.getWindowHandle();
   myprojects.switchToWindow();
   driver.switchTo().window(currWindow);
}

@Then("^The Edit icon is displayed only for this Transmittal Form$")
public void the_Edit_icon_is_displayed_only_for_this_Transmittal_Form() throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
	String type=(String) TestBaseProvider.getTestBase().getContext().getProperty("type");
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	Row row1=history.getFirstHistoryTypeRow(type);
	ItemGrid itemgrid=history.getHistoryGrid();
	for(Row nextRow:history.getHistoryTypeRows(type))
	{
		if(nextRow==row1)
		{
			AssertUtils.assertDisplayed(row1.getHistoryTableButtons("Edit"));

		}
		AssertUtils.assertNotDisplayed(nextRow.getHistoryTableButtons("Edit"));
	}
}

@Then("^The list of Transmittals is sorted in ascending order by 'Type'$")
public void the_list_of_Transmittals_is_sorted_in_ascending_order_by_Type() throws Throwable {
	 MyProjectsPage myprojects=new MyProjectsPage();
     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
     myprojects.waitForResultsLoadingComplete();
     history.switchToHistoryDialog();
     ItemGrid itemgrid=history.getHistoryGrid();
     ArrayList<String> colText=new ArrayList<>();
     for(Row row:itemgrid.getRows())
     {
    	 colText.add(row.getColumnText("Type"));
     }
    Assert.assertTrue(itemgrid.isSortedAscend(colText, "Type"));
}

@Then("^Each type \"([^\"]*)\" is sorted in descending order by 'Last Modified' date$")
public void each_type_is_sorted_in_descending_order_by_Last_Modified_date(String types) throws Throwable {
     MyProjectsPage myprojects=new MyProjectsPage();
     TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
     ItemGrid itemgrid=history.getHistoryGrid();
	 List<String> transmittalTypes = new Gson().fromJson(types, List.class);
     for(String type:transmittalTypes)
     {
     ArrayList<Row> rows=history.getHistoryTypeRows(type);
     ArrayList<String> colText=new ArrayList<>();
     for(Row row:rows)
     {
    	 colText.add(row.getColumnText("Last Modified"));
     }
    Assert.assertTrue(history.sortLatestDate(colText));
//    Assert.assertTrue(itemgrid.isSortedDesc(colText, "Date"));
     }
}


@Then("^Verify PDF file naming convention for this Transmittal Form should be like \"([^\"]*)\"$")
public void transmittal_pdf_file_name(String name) throws Throwable {
	MyProjectsPage myprojects=new MyProjectsPage();
	myprojects.switchToWindow();
	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
    String currentUrl=driver.getCurrentUrl();
    currentUrl=currentUrl.substring(currentUrl.lastIndexOf("/")+1);
    Pattern p = Pattern.compile(name+"_\\d\\d\\d\\d-\\d\\d-\\d\\d_T\\d\\d-\\d\\d-\\d\\d.pdf");
    Matcher m = p.matcher(currentUrl);
    Assert.assertTrue(m.matches());
}

@Then("^\"([^\"]*)\" on the list displays 'Proofs' separated by comma$")
public void on_the_list_displays_Proofs_separated_by_comma(String type) throws Throwable {

	  MyProjectsPage myprojects=new MyProjectsPage();
	  TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
	  myprojects.waitForResultsLoadingComplete();
	  history.switchToHistoryDialog();
	     ItemGrid itemgrid=history.getHistoryGrid();
	     WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	     ArrayList<Row> rows=history.getHistoryTypeRows(type);
	     ArrayList<String> colText=new ArrayList<>();
	     colText.add(rows.get(0).getColumnText("Proofs"));
	     String Str =colText.get(0);
	     System.out.println("The proofs values are  "+Str);
	     
	     List<String> ProofsList = Arrays.asList(Str.split(","));
	     
	     for(int i=0;i<ProofsList.size();i++)
	     {
	    	 System.out.println("proofs value is  "+ProofsList.get(i));
	     }
	     
	     
	}

@When("^User edits most recent \"([^\"]*)\" with \"([^\"]*)\"\"([^\"]*)\"$")
public void user_edits_most_recent_with(String type, String proof1, String proof2) throws Throwable {
    
	MyProjectsPage myprojects=new MyProjectsPage();
    TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
    myprojects.waitForResultsLoadingComplete();
	history.switchToHistoryDialog();
	Row row=history.getFirstHistoryTypeRow(type);
	row.getColumn("Edit").click();
	TransmittalForm tf=new TransmittalForm();
	tf.switchToTransmittalForm();
	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	tf.getTransmittalFormButtons(proof1).click();
	tf.getTransmittalFormButtons(proof2).click();
	tf.getFormUpdateBtn().click();
	Thread.sleep(2000);
	//String currWindow= driver.getWindowHandle();
	
	  /* String currWindow= driver.getWindowHandle();
	   myprojects.switchToWindow();
	   driver.switchTo().window(currWindow);*/
}


@When("^User submits  New \"([^\"]*)\"  with \"([^\"]*)\" and \"([^\"]*)\"$")
public void user_submits_New_with_and(String type, String mechs, String proofs) throws Throwable {

   MyProjectsPage myprojects=new MyProjectsPage();
   TransmittalHistoryDialog history=myprojects.getTransmittalHistoryDialog();
   history.getFormButton().click();
   TransmittalForm tf=new TransmittalForm();
   tf.switchToTransmittalForm();
   tf.getTransmittalFormButtons(type).click();
   Thread.sleep(1000);
   tf.getTransmittalFormButtons(mechs).click();
   Thread.sleep(1000);
   tf.getTransmittalFormButtons(proofs).click();
   TestBaseProvider.getTestBase().getContext().setProperty("proofs", proofs);
   WebDriver driver=TestBaseProvider.getTestBase().getDriver();
  
   tf.getSubmitButton().click();
   Thread.sleep(2000);
 
   }


}
