package com.scholastic.cucumber.apt.pageobjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.TransmittalPdfLocators;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

import junit.framework.Assert;

public class TransmittalPdf extends BaseTestPage<TestPage> implements TransmittalPdfLocators{
	@FindBy(locator = PAGE)
	private WebElement page;
	
	public WebElement getLabelElement(String label) throws InterruptedException
	{
		Thread.sleep(10000L);
		String xpath = String.format(LABEL_ELEMENT, label);
		WaitUtils.waitForPresent(TestBaseProvider.getTestBase().getDriver(), By.xpath(xpath));
		return page.findElement(By.xpath(xpath));
	}

	public WebElement getValueElement(String lable) throws InterruptedException {
		Thread.sleep(10000L);
		String xpath = String.format(VALUE_ELEMENT, lable);
		System.out.println("Xpath is: " +xpath);
		WaitUtils.waitForPresent(TestBaseProvider.getTestBase().getDriver(), By.xpath(xpath));
		return page.findElement(By.xpath(xpath));
	}
	
	public ArrayList<String> getAllLabelsInPdf()
	{
		 ArrayList<String> labels=new ArrayList<>();
		List<WebElement> pageLabels=page.findElements(By.xpath(ALL_PDF_LABELS));
		for(WebElement label:pageLabels)
		{
			labels.add(label.getText().trim());
		}
		return labels;
	}
	
	public String getTransmittalPdfValue(String label)
	{
		String text="";
		String xpath = String.format(COMPLETE_TEXT, label);
		WaitUtils.waitForPresent(TestBaseProvider.getTestBase().getDriver(), By.xpath(xpath));
		List<WebElement> elements=page.findElements(By.xpath(xpath));
		for(WebElement elm:elements)
		{
			if(getAllLabelsInPdf().contains(elm.getText().trim()))
			{
				break;
			}
			else
			{
			text=text+elm.getText()+" ";
			}
		}
		return text.trim();
	}
	
	public void assertResults(ArrayList<String> pdf,ArrayList<String> db)
	{
		ArrayList<String> dbArr=new ArrayList<>();
		ArrayList<String> pdfArr=new ArrayList<>();
		db.removeAll(Collections.singleton(null));
		pdf.removeAll(Collections.singleton("  "));
		pdf.removeAll(Collections.singleton(""));
		pdf.removeAll(Collections.singleton(" "));
		db.removeAll(Collections.singleton("  "));
		db.removeAll(Collections.singleton(""));
		db.removeAll(Collections.singleton(" "));
		db.removeAll(Collections.singleton(null));
		
		for (String string : db) {
			string = string.replaceAll("\\s", "");
			dbArr.add(string);
			}
		for (String string : pdf) {
			string = string.replaceAll("\\s", "");
			pdfArr.add(string);
			}
		
//			else
//			{
//				arr.add(string);
//			}
//		}
		Assert.assertEquals(pdfArr, dbArr);
	}

	@Override
	protected void openPage() {
		// TODO Auto-generated method stub
		
	}

}
