/**
 * 
 */
package com.scholastic.cucumber.apt.components;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.ItemListLocators;
import com.scholastic.torque.common.Section;
import com.scholastic.torque.common.TestBaseProvider;

/**
 * @author chirag.jayswal
 *
 */
public class ItemList extends Section implements ItemListLocators {

	@FindBy(locator = GROUP_HEADER_LOC)
	WebElement groupHeader;

	@FindBy(locator = ITEM_LOC)
	private List<WebElement> items;

	public ItemList(String loc) {
		super(loc);
	}

	public boolean hasItem(String name) {
		String loc = String.format(ITEM_BY_NAME_LOC_FORMAT, name);

		return !findElements(By.xpath(loc)).isEmpty();
	}

	public boolean isItemSelected(String name) {
		return getItem(name).getAttribute("class").toLowerCase().contains("selected");
	}

	public void selectItem(String name) {
		getItem(name).click();
	}

	public void selectItem(String... items) {
		Actions actions = new Actions(TestBaseProvider.getTestBase().getDriver());
		actions = actions.keyDown(Keys.CONTROL);
		for (String item : items) {
			actions = actions.click(getItem(item));
		}
		actions.keyUp(Keys.CONTROL).build().perform();
	}

	private WebElement getItem(String name) {
		String loc = String.format(ITEM_BY_NAME_LOC_FORMAT, name);
		return findElement(By.xpath(loc));
	}
}
