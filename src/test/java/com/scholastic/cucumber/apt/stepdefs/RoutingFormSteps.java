package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.EpicHomePage;
import com.scholastic.cucumber.apt.pageobjects.InternalRoutingForm;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.TransmittalPdf;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class RoutingFormSteps {

	@When("^user selects \"([^\"]*)\" Routing for a project$")
	public void user_selects_Routing_for_a_project(String routingType) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    itemgrid.getRows().get(5).getColumn(routingType).click();
	}

	@Then("^pdf form is displayed in a printable format with the light blue background in the new tab\\.$")
	public void pdf_form_is_displayed_in_a_printable_format_with_the_light_blue_background_in_the_new_tab() throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.switchToWindow();
	    
	}

	@Then("^can be printed on (\\d+)\\.(\\d+)x(\\d+) page for all the supported browsers\\.$")
	public void can_be_printed_on_x_page_for_all_the_supported_browsers(int arg1, int arg2, int arg3) throws Throwable {
	    
	}
	
	@Then("^Printable form is displayed in the new tab\\.$")
	public void printable_form_is_displayed_in_the_new_tab() throws Throwable {
	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String curWindow = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		TestBaseProvider.getTestBase().getContext().setProperty("cover.windows", windows.size());
		for (String window : windows) {
			System.err.println("window " + window);
			if (!window.equalsIgnoreCase(curWindow))
				;
			driver.switchTo().window(window);
			Assert.assertNotSame("same window", curWindow, window);
		}
	}

	@Then("^no cover online forms are displayed$")
	public void no_cover_online_forms_are_displayed() throws Throwable {
		int totalWindows=(int) TestBaseProvider.getTestBase().getContext().getProperty("cover.windows");
		Assert.assertTrue(totalWindows==2);
	}
	
	@Then("^PDF file header displays Internal Routing Form Cover/Jacket/Case$")
	public void pdf_file_header_displays_Internal_Routing_Form_Cover_Jacket_Case() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		myprojects.switchToWindow();
		String headerelm = transmittalpdf.getLabelElement("Internal Routing Form").getText();
		String headerelm1=transmittalpdf.getLabelElement("Cover/Jacket/Case").getText();
		String header=headerelm+headerelm1;
		Assert.assertTrue(header.equals("Internal Routing Form Cover/Jacket/Case"));
	}
	
	@Then("^internal Routing Form Interior is displayed on header$")
	public void internal_Routing_Form_Interior_is_displayed_on_header() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		myprojects.switchToWindow();
		String headerelm = transmittalpdf.getLabelElement("Internal Routing Form").getText();
		String headerelm1=transmittalpdf.getLabelElement("Interior").getText();
		String header=headerelm+headerelm1;
		Assert.assertTrue(header.equals("Internal Routing Form Interior"));
	}
	
	@Then("^date label is displayed without populating date on the printable PDF form$")
	public void date_label_is_displayed_without_populating_date_on_the_printable_PDF_form() throws Throwable {
		InternalRoutingForm routingForm = new InternalRoutingForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.switchToWindow();
		String dateValue=routingForm.getCompleteTextInPdf("Date:");
		Assert.assertTrue(dateValue.isEmpty());
	}
	
	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" are displayed in the Cover/Jacket Routing Print Form$")
	public void are_displayed_in_the_Cover_Jacket_Routing_Print_Form(String dataFields) throws Throwable {
		InternalRoutingForm routingForm = new InternalRoutingForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		ArrayList<String> dataArray=new ArrayList<>();
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		myprojects.switchToWindow();
		List<String> items = new Gson().fromJson(dataFields, List.class);
		for (String item : items) {
			AssertUtils.assertDisplayed(routingForm.getLabelElement(item));
			if(item.equals("ISBN:"))
			{
				dataArray.add(routingForm.getValueElement(item).getText());	
				TestBaseProvider.getTestBase().getContext().setProperty("routingform.isbn", routingForm.getValueElement(item).getText());
			}
			else if(item.equals("Editor"))
			{
				String editor=driver.findElement(By.xpath(".//*[.='Editor:']/following-sibling::*[1]")).getText();
				if(routingForm.getAllLabelsInPdf().contains(editor))
				{
					dataArray.add("");
				}
				else
				{
				dataArray.add(editor);
				}
			}
			else
			{
				dataArray.add(routingForm.getCompleteTextInPdf(item));
			}
		}
		TestBaseProvider.getTestBase().getContext().setProperty("routingform.datafields", dataArray);
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Then("^verify values on the PDF field should match with the OTBPM database \"([^\"]*)\"\\.$")
	public void verify_values_on_the_PDF_field_should_match_with_the_OTBPM_database(String databaseFields) throws Throwable {
	  DBConnection dbCon=new DBConnection();
	  MyProjectsPage myprojects=new MyProjectsPage();
	  String isbn= (String) TestBaseProvider.getTestBase().getContext().getProperty("routingform.isbn");
	  ArrayList<String> dataFields= (ArrayList<String>) TestBaseProvider.getTestBase().getContext().getProperty("routingform.datafields");
	  ArrayList<String> databaseArray=new ArrayList<>();
	  List<String> items = new Gson().fromJson(databaseFields, List.class);
	   ResultSet rs=dbCon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE ISBN='" + isbn+ "'");
	   while (rs.next()) {
			for (String item : items) {
				
				if (item.contains("DATE")) {
					String date = rs.getString(item);
					databaseArray.add(myprojects.changeDateFormat(date));
					
				} 
				else if(item.contains("PRICE"))
				{
					String price = rs.getString(item);
					if(null==price)
					{
					
					}
					else
					{
						double priceNum=Double.parseDouble(price);
						String formattedPrice="$"+Double.toString(priceNum);
						databaseArray.add(formattedPrice);
					}
				}
				else {
					databaseArray.add(rs.getString(item));
				}
			}
			break;	
		}
	   EpicHomePage homepage=new EpicHomePage();
	   homepage.assertResults(databaseArray, dataFields);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" is displayed in the Cover/Jacket Routing Print Form$")
	public void is_displayed_in_the_Cover_Jacket_Routing_Print_Form(String labels) throws Throwable {
	    InternalRoutingForm routingForm=new InternalRoutingForm();
	    MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.switchToWindow();
		List<String> items = new Gson().fromJson(labels, List.class);
		for (String item : items) {
	    Assert.assertEquals(routingForm.getpdfCheckboxes(item).getText(),"❏");
		
		}
	}
	
	@Then("^header is displayed for the five circles on printable PDF form$")
	public void header_is_displayed_for_the_five_circles_on_printable_PDF_form() throws Throwable {
		 InternalRoutingForm routingForm=new InternalRoutingForm();
		 MyProjectsPage myprojects=new MyProjectsPage();
		 myprojects.switchToWindow();
		 Thread.sleep(3000);
		AssertUtils.assertDisplayed(routingForm.getLabelElement("Corrections • Comments • Changes"));
		 
	}
	
	@Then("^Special Effects label with room to write in notes is displayed on the printable PDF form$")
	public void special_Effects_label_with_room_to_write_in_notes_is_displayed_on_the_printable_PDF_form() throws Throwable {
		 InternalRoutingForm routingForm=new InternalRoutingForm();
		 MyProjectsPage myprojects=new MyProjectsPage();
			myprojects.switchToWindow();
	
		AssertUtils.assertDisplayed(routingForm.getLabelElement("Special Effects:"));
		AssertUtils.assertDisplayed(routingForm.getValueElement("Special Effects:"));
	}
	@Then("^'Cover Routing Form' is displayed as pdf$")
	public void cover_Routing_Form_is_displayed_as_pdf() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		 myprojects.switchToWindow();
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		
		Assert.assertTrue(driver.getCurrentUrl().contains("pdf"));
		 
	}
	@Then("^'Interior Routing Form' is displayed as pdf$")
	public void interior_Routing_Form_is_displayed_as_pdf() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		 myprojects.switchToWindow();
		 WebDriver driver=TestBaseProvider.getTestBase().getDriver();
			Assert.assertTrue(driver.getCurrentUrl().contains("pdf"));
		
	}
	
}
