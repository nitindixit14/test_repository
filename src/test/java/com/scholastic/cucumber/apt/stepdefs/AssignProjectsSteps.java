package com.scholastic.cucumber.apt.stepdefs;

import java.sql.Driver;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.AssignDialog;
import com.scholastic.cucumber.apt.pageobjects.EpicHomePage;
import com.scholastic.cucumber.apt.pageobjects.EpicKeyInfoPage;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.ProjectDetails;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class AssignProjectsSteps {

	@When("^\"([^\"]*)\" is in OTBPM Art Directors List$")
	public void is_in_OTBPM_Art_Directors_List(String arg1) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
		ItemGrid itemGrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getcheckboxesHeader());
		itemGrid.showAllColumns();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	   Assert.assertTrue(itemgrid.allColumnsText("Art Director").size()>0);
	    
	}

	@When("^New EPIC record exists in OTBPM$")
	public void new_EPIC_record_exists_in_OTBPM() throws Throwable {
		  MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
	}

	@SuppressWarnings("static-access")
	@When("^User Query \"([^\"]*)\" for New record from OTBPM database$")
	public void user_Query_for_New_record_from_OTBPM_database(String arg1) throws Throwable {
	    DBConnection dbcon=new DBConnection();
	    String isbn=(String) TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
	    ResultSet rs=dbcon.dbConnection("SELECT ART_DIRECTOR_NAME FROM APT_PROJECTS_VIEW WHERE ISBN_13='" + isbn+ "'");
	    while(rs.next())
	    {
	    	TestBaseProvider.getTestBase().getContext().setProperty("db.artdirector.name",rs.getString("ART_DIRECTOR_NAME"));
	    }
	}

	@Then("^Art Director value from OTBPM database matches displayed \"([^\"]*)\" in EPIC$")
	public void art_Director_value_from_OTBPM_database_matches_displayed_in_EPIC(String artDirector) throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		String name="";
		if (artDirector.contains(",")) {
			String[] fullname= artDirector.split(",");
			name=fullname[1].replace(",", "").trim()+fullname[0];
		}
		else
		{
			name=artDirector;
		}
		name=name.replaceAll("\\s", "");
		dbValue=dbValue.replaceAll("\\s", "");
		Assert.assertEquals(dbValue.toUpperCase(), name.toUpperCase());
	}
	
	@Then("^\"([^\"]*)\" value displayed on Project List matches OTBPM database value for Art Director$")
	public void value_displayed_on_Project_List_matches_OTBPM_database_value_for_Art_Director(String arg1) throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		 MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    int index=itemgrid.getColumnIndex("Art Director");
		   String myProjectsValue= itemgrid.getRows().get(0).getColumnText(index);
		   Assert.assertEquals(myProjectsValue.toUpperCase(), dbValue.toUpperCase());
	}

	@Then("^\"([^\"]*)\" value displayed on Project Details page matches OTBPM database value for Art Director$")
	public void value_displayed_on_Project_Details_page_matches_OTBPM_database_value_for_Art_Director(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		ProjectDetails productDetails=new  ProjectDetails();
		 myprojects.waitForResultsLoadingComplete();
		 ItemGrid itemgrid1=myprojects.getProjectGrid();
			String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
			int index1 = itemgrid1.getColumnIndex("Title");
			itemgrid1.getRows().get(0).getColumn(index1).click();
			productDetails.switchToDetailsDialog();
			String elmValue = productDetails.getValueElement("Art Director").getAttribute("value").trim();
			Assert.assertEquals(elmValue.toUpperCase(), dbValue.toUpperCase());
				myprojects.waitForResultsLoadingComplete();
				myprojects.clickCloseButton();
				TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
				myprojects.waitForResultsLoadingComplete();
	}
	@Then("^\"([^\"]*)\" value displayed on Transmittal Form page matches OTBPM database value for Art Director$")
	public void value_displayed_on_Transmittal_Form_page_matches_OTBPM_database_value_for_Art_Director(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		int index = myprojectsgrid.getColumnIndex("Transmittal Form");
		WaitUtils.waitForDisplayed(myprojectsgrid.getRows().get(0).getColumn(index));
		myprojectsgrid.getRows().get(0).getColumn(index).click();	
		TransmittalForm form=new TransmittalForm();
		form.switchToTransmittalForm();
		String elmValue = form.getValueElement("Art Director:").getAttribute("value").trim();
		Assert.assertEquals(elmValue.toUpperCase(), dbValue.toUpperCase());
		myprojects.waitForResultsLoadingComplete();
		myprojects.clickCloseButton();
		form.clickFormpopUp("No");
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		myprojects.waitForResultsLoadingComplete();
	}
	
	@When("^\"([^\"]*)\" is NOT in OTBPM Art Directors List$")
	public void is_NOT_in_OTBPM_Art_Directors_List(String arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
			ItemGrid itemGrid = myprojects.getProjectGrid();
			WaitUtils.waitForEnabled(myprojects.getProjectGrid().getcheckboxesHeader());
			itemGrid.showAllColumns();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		   Assert.assertTrue(itemgrid.allColumnsText("Art Director").toString().equals("[]"));
	}

	@Then("^Art Director value from OTBPM database is empty$")
	public void art_Director_value_from_OTBPM_database_is_empty() throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		 Assert.assertTrue(dbValue==null);
	}
	
	@When("^User Assigns Art Director as \"([^\"]*)\" in Assign view$")
	public void user_Assigns_Art_Director_as_in_Assign_view(String newArtDirector) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		AssignDialog assign=new AssignDialog();
		assign.switchToAssignDialog();		
		
		assign.getAssignInputs("Art Director").sendKeys("Rajesh Chandrasekar");
		Actions action=new Actions(TestBaseProvider.getTestBase().getDriver());
		action.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.RETURN).build().perform();
		assign.clickAssignSaveBtn();
		
	}

	@When("^User return to search results view$")
	public void user_return_to_search_results_view() throws Throwable {
		 MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    Thread.sleep(20000);
	}
	
	@When("^user selects a project to 'UnAssign'$")
	public void user_selects_a_project_to_UnAssign() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		List<WebElement> checkboxes = itemgrid.findElements(By.id("check_tbmyProjects"));
		if (!(checkboxes.get(1)).isSelected()) {
			checkboxes.get(1).click();
		}
		myprojects.clickElementByJS(myprojects.getAssignButton());
	}

	@When("^User Unassigns Art Director for project in Assign view$")
	public void user_Unassigns_Art_Director_for_project_in_Assign_view() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		AssignDialog assign=new AssignDialog();
		assign.switchToAssignDialog();
		
		assign.clickRemoveADBtn();
		assign.clickAssignSaveBtn();
	}

	@Then("^Art Director value is blank on Project List$")
	public void art_Director_value_is_blank_on_Project_List() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getcheckboxesHeader());
		//itemGrid.showAllColumns();
	   Assert.assertTrue(itemgrid.allColumnsText("Art Director").toString().equals("[]"));
	}

	@Then("^Art Director value is blank on Project Details page$")
	public void art_Director_value_is_blank_on_Project_Details_page() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		ProjectDetails productDetails=new  ProjectDetails();
		 myprojects.waitForResultsLoadingComplete();
		 ItemGrid itemgrid1=myprojects.getProjectGrid();
			int index1 = itemgrid1.getColumnIndex("Title");
			itemgrid1.getRows().get(0).getColumn(index1).click();
			productDetails.switchToDetailsDialog();
			String elmValue1 = productDetails.getValueElement("Art Director").getAttribute("value").trim();
			Assert.assertTrue(elmValue1.trim().equals(""));
			myprojects.waitForResultsLoadingComplete();
			myprojects.clickCloseButton();
			TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
			myprojects.waitForResultsLoadingComplete();
	}

	@Then("^Art Director value is blank on Transmittal Form page$")
	public void art_Director_value_is_blank_on_Transmittal_Form_page() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		int index = myprojectsgrid.getColumnIndex("Transmittal Form");
		WaitUtils.waitForDisplayed(myprojectsgrid.getRows().get(0).getColumn(index));
		myprojectsgrid.getRows().get(0).getColumn(index).click();	
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebDriver driver1=TestBaseProvider.getTestBase().getDriver();
		driver1.switchTo().frame(0);
		String elmValue = driver1.findElement(By.xpath("//label[text()='Art Director:']//following::input[1]")).getAttribute("value").trim();
		Assert.assertTrue(elmValue.trim().equals(""));

	}

	@Then("^Art Director value is empty in OTBPM database$")
	public void art_Director_value_is_empty_in_OTBPM_database() throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		 Assert.assertTrue(dbValue==null);
	}
	
	@Then("^Art Director value from OTBPM database remains as \"([^\"]*)\"$")
	public void art_Director_value_from_OTBPM_database_remains_as(String artDirector) throws Throwable {
		String dbValue=(String) TestBaseProvider.getTestBase().getContext().getProperty("db.artdirector.name");
		String name="";
		if (artDirector.contains(",")) {
			String[] fullname= artDirector.split(",");
			name=fullname[1].replace(",", "").trim()+fullname[0];
		}
		else
		{
			name=artDirector;
		}
		name=name.replaceAll("\\s", "");
		dbValue=dbValue.replaceAll("\\s", "");
		Assert.assertEquals(dbValue.toUpperCase(), name.toUpperCase());
	}
	
	
	@Then("^A table is displayed with the following \"([^\"]*)\"$")
	public void a_table_is_displayed_with_the_following(String columns,DataTable table) throws Throwable {
		
		
		List<List<String>> data= table.raw();
        WebDriver driver = TestBaseProvider.getTestBase().getDriver();
	    AssignDialog assign=new AssignDialog();
	   
	    
	    List<WebElement> Actual=driver.findElements(By.xpath("//*[@id='assignTableDetails']/thead/tr/th"));
	 
		for(int i=0;i<Actual.size();i++)
		{   
			
			//System.out.println("Expected text is  " +data.get(i+1).get(0));
			//System.out.println("Actual text is    " +Actual.get(i).getText());
			
	         
	         Assert.assertEquals(data.get(i+1).get(0), Actual.get(i).getText());
	         
	        /*	AssertUtils.assertTextMatches(Actual.get(i),
				    Matchers.containsString(expected_text[i]));*/
			}
		
		}
	
/*	@Then("^the value for \"([^\"]*)\" is populated for the project$")
	public void the_value_for_is_populated_for_the_project(String arg1) throws Throwable {
		

		
		AssignDialog assign=new AssignDialog();
		ItemGrid itemgrid=assign.getAssignTableGrid();
		ArrayList<Row> rows=assign.getAssignTypeRows();
	     ArrayList<String> colText=new ArrayList<>();
	    
	   
		for(Row row:itemgrid.getRows())
	     {
	    	 colText.add(row.getColumnText("Title"));
	    	 
	    	 colText.add(row.getColumnText("ISBN"));
	    	 colText.add(row.getColumnText("Art Director"));
	    	 colText.add(row.getColumnText("Cover Designer"));
	    	 colText.add(row.getColumnText("Interior Designer"));
	    	 System.out.println("The Table Values are  "+colText);
	    	// colText.clear();
	    	 Assert.assertTrue(colText.contains("BACKSTORIES: SUPERMAN: THE MAN OF TOMORROW  EBA"));
	     }
		
		
	}*/

	@Then("^the \"([^\"]*)\" match information in \"([^\"]*)\"$")
	public void the_match_information_in(String values, String project) throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		 ArrayList<String> Expected_info=new ArrayList<>();
		 
		 String[] Expected_text={values};
		 
		 List<WebElement> results=driver.findElements(By.xpath("//*[@id='assignTableDetails']/tbody/tr[2]/td"));
		 for(int i=0;i<results.size();i++)
		 {
			 Assert.assertEquals(results.get(i).getText(), Expected_text[i]);
				
		 }
		
		
	}
	
	@Then("^the values are populated and matched information with \"([^\"]*)\"\"([^\"]*)\"\"([^\"]*)\"\"([^\"]*)\"\"([^\"]*)\" for \"([^\"]*)\"$")
	public void the_values_are_populated_and_matched_information_with_for(String Title, String ISBN, String Art_Director, String Cover_Designer, String Interior_Designer, String arg6) throws Throwable {
	    
		AssignDialog assign=new AssignDialog();
		ItemGrid itemgrid=assign.getAssignTableGrid();
		ArrayList<Row> rows=assign.getAssignTypeRows();
	     ArrayList<String> colText=new ArrayList<>();
	  //  JOptionPane.showInputDialog(Title);
	     
	   System.out.println(Title);
		for(Row row:itemgrid.getRows())
	     {
	    	 colText.add(row.getColumnText("Title"));
	    	 
	    	 colText.add(row.getColumnText("ISBN"));
	    	 colText.add(row.getColumnText("Art Director"));
	    	 colText.add(row.getColumnText("Cover Designer"));
	    	 colText.add(row.getColumnText("Interior Designer"));
	    	 System.out.println("The Table Values are  "+colText);
	    	
	    	/* if(colText.contains(Title))
	    		 break;*/
	     }
		
		Assert.assertTrue(colText.contains(Title));
		colText.clear();
	}
	

	@Then("^'Assign' Title is displayed$")
	public void assign_Title_is_displayed() throws Throwable {
		AssignDialog assign=new AssignDialog();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		MyProjectsPage myprojects = new MyProjectsPage();
		driver.switchTo().defaultContent();
		myprojects.waitForResultsLoadingComplete();
		//driver.switchTo().frame("display_frame");
		assign.getAssignTitle();
		//WebElement w=driver.findElement(By.xpath("//*[@id='fb_title']"));
		Assert.assertEquals(assign.getAssignTitle().getText(),"Assign");
	
	}
	
	@Then("^The list of projects to assign include \"([^\"]*)\" in that order$")
	public void the_list_of_projects_to_assign_include_in_that_order(String headers) throws Throwable {
	    
		AssignDialog assign=new AssignDialog();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		
		    String[] expected_text={headers};
		//	String[] expected_text={"Title","ISBN","Art Director","Cover Designer","Interior Designer"};
			List<WebElement> Actual=driver.findElements(By.xpath("//*[@id='assignTableDetails']/thead/tr/th/label"));
			//List<WebElement> Actual=assign.getAssignTableHeaders();
			for(int i=0;i<Actual.size();i++)
			{ 
				//System.out.println("Expected text is  " +expected_text[i]);
				System.out.println("Actual text is    " +Actual.get(i).getText());
				AssertUtils.assertTextMatches(Actual.get(i),
					    Matchers.containsString(expected_text[i]));
			
		   }
		}
	
	@When("^user selects a \"([^\"]*)\" and go to  Assign page$")
	public void user_selects_a_and_go_to_Assign_page(String searchText) throws Throwable {
		
		MyProjectsPage myprojects=new MyProjectsPage();
	    AssignDialog assign=new AssignDialog();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.searchFor(searchText);
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    	WaitUtils.waitForEnabled(itemgrid.getcheckboxesHeader());
	    	myprojects.clickElementByJS(itemgrid.getcheckboxesHeader());
	    myprojects.clickAssignButton();
	    assign.switchToAssignDialog();
	   
	}
	
	
	
}
