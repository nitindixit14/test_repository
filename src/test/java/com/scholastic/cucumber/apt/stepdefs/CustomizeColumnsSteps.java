package com.scholastic.cucumber.apt.stepdefs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.ActionChainExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.HeaderSection;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.cucumber.apt.pageobjects.SignOutPage;
import com.scholastic.cucumber.apt.pageobjects.UserHomePage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CustomizeColumnsSteps {

	@Given("^\"([^\"]*)\" is displayed  in the Projects List$")
	public void is_displayed_in_the_Projects_List(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		// WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		//Thread.sleep(1000);
		itemgrid.showAllColumns();
	}

	@When("^user hides  \"([^\"]*)\"$")
	public void user_hides(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		itemgrid.hideColumn(column_name);
		// Thread.sleep(10000);
	}

	@Then("^the column \"([^\"]*)\" is NOT displayed in Project list$")
	public void the_column_is_NOT_displayed_in_Project_list(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		if (column_name.equalsIgnoreCase("Checkboxes")) {
			AssertUtils.assertNotDisplayed(itemgrid.getcheckboxesHeader());
		} else {
			AssertUtils.assertNotDisplayed(itemgrid.getHeader(column_name));
		}
	}

	@When("^user selects \"([^\"]*)\" on the Projects list in the header$")
	public void user_selects_on_the_Projects_list_in_the_header(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		itemgrid.showAllColumns();
		myprojects.waitForResultsLoadingComplete();
		itemgrid = myprojects.getProjectGrid();
		if (!column_name.equalsIgnoreCase("Title")) {
			itemgrid.getHeader(column_name).click();
		}
	}

	@Then("^projects are sorted in ascending by the selected option \"([^\"]*)\"$")
	public void projects_are_sorted_in_ascending_by_the_selected_option(String option) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		ArrayList<String> col_text = itemgrid.allColumnsText(option);
		Assert.assertTrue(itemgrid.isSortedAscend(col_text, option));
	}

	@Then("^'arrow icon' on \"([^\"]*)\" indicates column is sorted in ascending order$")
	public void arrow_icon_on_indicates_column_is_sorted_in_ascending_order(String colName) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex(colName);
		int arrow_index = index + 1;
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebElement img = driver
				.findElement(By.xpath(String.format(".//*[@id='tbmyProjects']/thead/tr/th[" + arrow_index + "]/img")));
		Assert.assertTrue(img.getAttribute("src").contains("arrowcollapse"));
	}

	@Then("^'arrow icon' on \"([^\"]*)\" indicates column is sorted in descending order$")
	public void arrow_icon_on_indicates_column_is_sorted_in_descending_order(String colName) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex(colName);
		int arrow_index = index + 1;
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebElement img = driver
				.findElement(By.xpath(String.format(".//*[@id='tbmyProjects']/thead/tr/th[" + arrow_index + "]/img")));
		Assert.assertTrue(img.getAttribute("src").contains("arrowexpand"));
	}

	@When("^user changes  \"([^\"]*)\" size by moving header's right border	of \"([^\"]*)\"$")
	public void user_changes_size_by_moving_header_s_right_border_of(String header_name, String width_item)
			throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		WebElement element = itemgrid.getHeaderElement(header_name);
		itemgrid.ChangeElementWidthByJS(element, width_item);
	}

	@Then("^the column \"([^\"]*)\" width is displayed with the selected size 	\"([^\"]*)\"$")
	public void the_column_width_is_displayed_with_the_selected_size(String header_name, String expected_width)
			throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WebElement element = itemgrid.getHeaderElement(header_name);
		Assert.assertEquals(element.getCssValue("width"), expected_width);
	}

	@Given("^user customize Project List size of view of columns$")
	public void user_customize_Project_List_size_of_view_of_columns() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		itemgrid.hideColumn("Title");
		itemgrid.hideColumn("ISBN");
		itemgrid.hideColumn("Pub Date");
		WebElement title = itemgrid.getHeaderElement("Title");
		WebElement ISBN = itemgrid.getHeaderElement("ISBN");
		itemgrid.ChangeElementWidthByJS(title, "200px");
		itemgrid.ChangeElementWidthByJS(ISBN, "200px");
		itemgrid.ChangeElementWidthByJS(itemgrid.getHeaderElement("Pub Date"), "200px");

	}

	@When("^user selects to set default Project List$")
	public void user_selects_to_set_default_Project_List() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		itemgrid.showAllColumns();
	}

	@When("^user selects 'Set grid to default' view$")
	public void user_selects_Set_grid_to_default_view() throws Throwable {

		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='__internal__bodyNode']/div[7]/div[2]/div")).click();
		// Thread.sleep(10000);
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		itemgrid.showAllColumns();
	}

	@Then("^the default columns are displayed in default size and order$")
	public void the_default_columns_are_displayed_in_default_size_and_order() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		// Thread.sleep(10000);
		AssertUtils.assertDisplayed(itemgrid.getHeader("Title"));
		AssertUtils.assertDisplayed(itemgrid.getHeader("ISBN"));
		AssertUtils.assertDisplayed(itemgrid.getHeader("Pub Date"));
		Assert.assertFalse(itemgrid.getHeaderElement("Title").getAttribute("width").equalsIgnoreCase("200px"));
		Assert.assertFalse(itemgrid.getHeaderElement("ISBN").getAttribute("width").equalsIgnoreCase("200px"));
		Assert.assertFalse(itemgrid.getHeaderElement("Pub Date").getAttribute("width").equalsIgnoreCase("200px"));
	}

	@Given("^\"([^\"]*)\" is not displayed  in the Project List$")
	public void is_not_displayed_in_the_Project_List(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		// WaitUtils.waitForDisplayed(itemgrid.getWrappedElement());
		itemgrid.showAllColumns();
		itemgrid.hideColumn(column_name);
	}

	@When("^user chose the  \"([^\"]*)\" to show$")
	public void user_chose_the_to_show(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		itemgrid.contextClick();
		itemgrid.columnSelector();
		itemgrid.rightClickElements(column_name);
	}

	@Then("^the column \"([^\"]*)\" is displayed in Project list$")
	public void the_column_is_displayed_in_Project_list(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		if (column_name.equalsIgnoreCase("Checkboxes")) {
			AssertUtils.assertDisplayed(itemgrid.getcheckboxesHeader());
		} else {
			AssertUtils.assertDisplayed(itemgrid.getHeader(column_name));
		}
	}

	@Then("^the \"([^\"]*)\" value is populated for all the projects on the list$")
	public void the_value_is_populated_for_all_the_projects_on_the_list(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		int index = itemgrid.getColumnIndex(column_name);
		if (column_name.equalsIgnoreCase("Checkboxes")) {
			for (Row row : itemgrid.getRows()) {
				List<WebElement> checkboxes = itemgrid.findElements(By.id("check_tbmyProjects"));
				for (int i = 1; i < checkboxes.size(); i++)
					AssertUtils.assertDisplayed(checkboxes.get(i));
			}
		} else {
			for (Row row : itemgrid.getRows()) {
				AssertUtils.assertDisplayed(row.getColumn(index));
			}
		}
	}

	@SuppressWarnings("unchecked")
	@When("^user select the  \"([^\"]*)\"  to show$")
	public void user_select_the_to_show(String col_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		itemgrid.contextClick();
		itemgrid.columnSelector();
		List<String> items = new Gson().fromJson(col_name, List.class);
		for (String item : items) {
			if ((!(itemgrid.getHeaderNames().contains(item))) && (!(item.equalsIgnoreCase("Checkboxes")))) {
				itemgrid.rightClickElements(item);
			} else {
				if (item.equalsIgnoreCase("Checkboxes")) {
					if (itemgrid.getcheckboxesHeader().isDisplayed()) {
						continue;
					} else {
						itemgrid.rightClickElements(item);
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@When("^user unselect the  \"([^\"]*)\"  to hide$")
	public void user_unselect_the_to_hide(String col_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		List<String> items = new Gson().fromJson(col_name, List.class);
		for (String item : items) {
			if ((itemgrid.getHeaderNames().contains(item)) && (!(item.equalsIgnoreCase("Checkboxes")))) {
				itemgrid.rightClickElements(item);
			}
			if (item.equalsIgnoreCase("Checkboxes") && itemgrid.getcheckboxesHeader().isDisplayed()) {
				itemgrid.rightClickElements(item);
			}
		}
	}

	@Then("^the columns \"([^\"]*)\" are NOT displayed in Project list$")
	public void the_columns_are_NOT_displayed_in_Project_list(String col_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		// WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		List<String> items = new Gson().fromJson(col_name, List.class);
		for (String item : items) {
			if (item.equalsIgnoreCase("Checkboxes")) {
				AssertUtils.assertNotDisplayed(itemgrid.getcheckboxesHeader());
			} else {
				AssertUtils.assertNotDisplayed(itemgrid.getHeader(item));
			}
		}
	}

	@Then("^the columns \"([^\"]*)\" are displayed in Project list$")
	public void the_columns_are_displayed_in_Project_list(String col_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
//		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		List<String> items = new Gson().fromJson(col_name, List.class);
		for (String item : items) {
			if (item.equalsIgnoreCase("Checkboxes")) {
				AssertUtils.assertDisplayed(itemgrid.getcheckboxesHeader());
			} else {
				AssertUtils.assertDisplayed(itemgrid.getHeader(item));
			}
		}
	}

	@Then("^the columns \"([^\"]*)\" value is populated for all the projects on the list$")
	public void the_columns_value_is_populated_for_all_the_projects_on_the_list(String col_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
//		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		List<String> items = new Gson().fromJson(col_name, List.class);
		for (String item : items) {
			int index = itemgrid.getColumnIndex(item);
				if (item.equalsIgnoreCase("Checkboxes")) {
					List<WebElement> checkboxes = itemgrid.findElements(By.id("check_tbmyProjects"));
					for (int i = 1; i < checkboxes.size(); i++)
						AssertUtils.assertDisplayed(checkboxes.get(i));
				} else {
					AssertUtils.assertDisplayed(itemgrid.getRows().get(1).getColumn(index));
				}
		}
	}

	@When("^User logs out$")
	public void user_logs_out() throws Throwable {
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		UserHomePage homepage = new UserHomePage();
		HeaderSection headersection = homepage.getHeaderSection();
		headersection.getLogoutButton().click();
	}

	@When("^User logs out of the session$")
	public void user_logs_out_of_the_session() throws Throwable {
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		UserHomePage homepage = new UserHomePage();
		HeaderSection headersection = homepage.getHeaderSection();
		headersection.getLogoutButton().click();
	}

	@When("^logs in for a new session$")
	public void logs_in_for_a_new_session() throws Throwable {
		SignOutPage signout = new SignOutPage();
		signout.getGoToSignInPageButton().click();
		OTBPMSigninPage signin = new OTBPMSigninPage();
		signin.signIn(TestBaseProvider.getTestBase().getContext().getString("userID"),
				TestBaseProvider.getTestBase().getString("password"));
	}

	@SuppressWarnings("unchecked")
	@When("^user changes  \"([^\"]*)\"  by  \"([^\"]*)\"$")
	public void user_changes_by(String header_name, String width) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		List<String> items = new Gson().fromJson(header_name, List.class);
		List<String> width_items = new Gson().fromJson(width, List.class);
		for (int i = 0; i < items.size(); i++) {
			String item = items.get(i);
			String width_item = width_items.get(i);
			WebElement element = itemgrid.getHeaderElement(item);
			itemgrid.ChangeElementWidthByJS(element, width_item);
		}
	}

	@SuppressWarnings("unchecked")
	@Then("^the \"([^\"]*)\" columns are displayed in the selected size	\"([^\"]*)\"$")
	public void the_columns_are_displayed_in_the_selected_size(String header_name, String expected_width)
			throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		List<String> items = new Gson().fromJson(header_name, List.class);
		List<String> width_items = new Gson().fromJson(expected_width, List.class);
		for (int i = 0; i < items.size(); i++) {
			String item = items.get(i);
			String width_item = width_items.get(i);
			WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
			WebElement element = itemgrid.getHeaderElement(item);
			Assert.assertEquals(element.getAttribute("width"), width_item);
		}
	}

	@When("^user changes  \"([^\"]*)\"  position by dragging and dropping the column header$")
	public void user_changes_position_by_dragging_and_dropping_the_column_header(String header_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WebElement source_element = itemgrid.getHeaderElement(header_name);
		WebElement target_element = itemgrid.getHeaderElement("File Name");
		TestBaseProvider.getTestBase().getContext().setProperty("target.index", itemgrid.getColumnIndex("File Name"));
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		Actions actions = new Actions(driver);
		// Thread.sleep(1000);
		actions.dragAndDrop(source_element, target_element).perform();
		// actions.moveToElement(source_element).clickAndHold(source_element).release(target_element).perform();
		// Thread.sleep(10000);
	}

	@Then("^the column \"([^\"]*)\" is displayed inserted in the selected position$")
	public void the_column_is_displayed_inserted_in_the_selected_position(String column_name) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex(column_name);
		int actual_index = (int) TestBaseProvider.getTestBase().getContext().getProperty("target.index");
		// Assert.assertEquals(index,actual_index );
	}
}
