package com.scholastic.cucumber.apt.stepdefs;

import static org.hamcrest.MatcherAssert.assertThat;

import java.rmi.dgc.DGC;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.apache.commons.configuration.Configuration;
import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.EpicHomePage;
import com.scholastic.cucumber.apt.pageobjects.EpicKeyInfoPage;
import com.scholastic.cucumber.apt.pageobjects.EpicSearchResults;
import com.scholastic.cucumber.apt.pageobjects.EpicSignin;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.cucumber.apt.pageobjects.SignOutPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;

public class EpicIntegrationSteps {

	@Given("^user login to epic$")
	public void user_login_to_epic() throws Throwable {
		EpicSignin signin = new EpicSignin();
		signin.launchPage();
		signin.signIn(TestBaseProvider.getTestBase().getString("epic.userID"),
				TestBaseProvider.getTestBase().getString("epic.password"));
	}

	public String getXmlString(String xmlPath) {
		return TestBaseProvider.getTestBase().getContext().getString(xmlPath);
	}

	@When("^user searches for title$")
	public void user_searches_for_title() throws Throwable {
		EpicHomePage homepage = new EpicHomePage();
		EpicSearchResults searchresults = new EpicSearchResults();
		WaitUtils.waitForDisplayed(homepage.getsearchInput());
		homepage.getsearchInput().sendKeys(getXmlString("epic.searchText.title"));
		WaitUtils.waitForDisplayed(homepage.getsearchButton());
		homepage.getsearchButton().click();
//		Thread.sleep(10000);
		WaitUtils.waitForDisplayed(searchresults.getResult());
		searchresults.getResult().click();
//		Thread.sleep(10000);
	}

	@When("^user selects the owner$")
	public void user_selects_the_owner() throws Throwable {
		EpicKeyInfoPage keyInfo = new EpicKeyInfoPage();
		Select select = new Select(keyInfo.getOwnerDropdown());
		select.selectByVisibleText(getXmlString("epic.selectOwner.name"));
		keyInfo.getCloneButton().click();
	}

	@When("^user fills the mandatory fields$")
	public void user_fills_the_mandatory_fields() throws Throwable {
		EpicIntegrationSteps epicIntegration = new EpicIntegrationSteps();
		EpicKeyInfoPage keyInfo = new EpicKeyInfoPage();
		clickBothButton();
		String inputOptions = getXmlString("form.inputFields.requiredFieldsKeyInfo");
		inputFields(inputOptions,"form");
//		Thread.sleep(10000);
		String selectOptions = getXmlString("form.selectFields.requiredSelectFields");
		selectFields(selectOptions,"form");
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		if (getXmlString("epic.selectOwner.name").contains("TRADE")) {
			WebElement pubDate = keyInfo.getpubDate();
			pubDate.clear();
			pubDate.sendKeys("01/01/2016");
			WebElement tradeInStoreDate = keyInfo.gettradeInStoreDate();
			tradeInStoreDate.clear();
			tradeInStoreDate.sendKeys("02/02/2016");
			WebElement tradeGroup = keyInfo.getTradeGroup();
			Select selectTradeGroup = new Select(tradeGroup);
			selectTradeGroup.selectByVisibleText("NOVELTY/BOARD BOOK");
			WebElement budgetCategory = keyInfo.getBudgetCategory();
			Select selectBudgetCategory = new Select(budgetCategory);
			selectBudgetCategory.selectByVisibleText("BOARD");
			String tradeInputOptions = getXmlString("form.inputFields.tradeRequiredInputs");
			selectFields(tradeInputOptions,"form");
			String tradeSelectOptions = getXmlString("form.selectFields.tradeSelectedOptions");
			selectFields(tradeSelectOptions,"form");
		}

		WebElement properties = keyInfo.getpropertiesOption();
		Select selectProperty = new Select(properties);
		selectProperty.selectByVisibleText("39 Clues");
		keyInfo.getpropertiesButton().click();

		WebElement editorialAge = keyInfo.geteditorialAge1();
		Select select2 = new Select(editorialAge);
		select2.selectByIndex(3);
		WebElement editorialAge2 = keyInfo.geteditorialAge2();
		Select select3 = new Select(editorialAge2);
		select3.selectByIndex(5);
		WebElement majorCategory = keyInfo.getmajorCategory();
		majorCategory.click();

		WebElement primaryCategories = keyInfo.getprimaryCategory();
		primaryCategories.click();
		String curWindow4 = driver.getWindowHandle();
		Set<String> windows4 = driver.getWindowHandles();
		if (windows4.size() > 1) {
			for (String window : windows4) {
				System.err.println("window " + window);
				if (!window.equalsIgnoreCase(curWindow4))
					;
				driver = driver.switchTo().window(window);
			}
			WebElement primaryCategoryOneCheckBox = keyInfo.getprimaryCategoryCheckbox();
			primaryCategoryOneCheckBox.click();
			WebElement primaryCategoryOkButton = keyInfo.getprimaryCategoryOkButton();
			primaryCategoryOkButton.click();
			driver.switchTo().window(curWindow4);

			
		}
	}

	public void clickBothButton() throws InterruptedException {
		EpicKeyInfoPage keyInfo = new EpicKeyInfoPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String curWindow = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		if (windows.size() > 1) {
			for (String window : windows) {
				System.err.println("window " + window);
				if (!window.equalsIgnoreCase(curWindow))
					;
				driver = driver.switchTo().window(window);
			}
			keyInfo.getBothRadioButton().click();
			keyInfo.getIncludeRadioButton().click();
			driver.switchTo().window(curWindow);
		}
	}

	@SuppressWarnings("unchecked")
	public void inputFields(String options,String formType) {
		String inputField = "//td//span[starts-with(text(),'%s')]/following::input[1]";
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		List<String> items = new Gson().fromJson(options, List.class);
		for (String item : items) {
			String editFieldXpath = String.format(inputField, item);

			try {
				WebElement editField = driver.findElement(By.xpath(editFieldXpath));
				editField.clear();
				item = item.replaceAll("\\s", "");
				if (item.contains("Char")) {
					item = item.substring(2);
				}
				editField.sendKeys(getXmlString(formType+".input." + item + ".value"));
			} catch (Exception e) {
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void selectFields(String options,String formType) {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String selectField = "//td//span[starts-with(text(),'%s')]/following::select[1]";
		List<String> items = new Gson().fromJson(options, List.class);
		for (String item : items) {

			try {
				String selectFieldXpath = String.format(selectField, item);
				WebElement selectFieldElement = driver.findElement(By.xpath(selectFieldXpath));
				Select select = new Select(selectFieldElement);
				select.selectByVisibleText(getXmlString(formType+".select." + item.replaceAll("\\s", "") + ".value"));
			} catch (Exception e) {
				System.out.println("select not found");
			}
		}
	}

	public void continueButton() throws InterruptedException {
		EpicKeyInfoPage keyInfo = new EpicKeyInfoPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String continueButtonWindow = driver.getWindowHandle();
		Set<String> continueButtonwindows11 = driver.getWindowHandles();
		try {
			if (continueButtonwindows11.size() > 1) {
				for (String window : continueButtonwindows11) {
					System.err.println("window " + window);
					if (!window.equalsIgnoreCase(continueButtonWindow)) {
						driver = driver.switchTo().window(window);
						break;
					}
				}
				WebElement continueButton = keyInfo.getcontinueButton();
				continueButton.click();
				Thread.sleep(2000);
				driver.switchTo().window(continueButtonWindow);
			}
		} catch (Exception e) {
			System.err.println("no continue button present");
			driver.switchTo().window(continueButtonWindow);
		}
		Thread.sleep(1000);
	}

	@When("^user clicks save button$")
	public void user_clicks_save_button() throws Throwable {
		EpicKeyInfoPage keyInfo = new EpicKeyInfoPage();
		WebElement saveButton = keyInfo.getsaveButton();
		saveButton.click();
		continueButton();
		continueButton();
		continueButton();
		Thread.sleep(2000);
	
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebElement contractsSaveButton = keyInfo.getcontractSaveButton();
		WaitUtils.waitForDisplayed(contractsSaveButton);
		contractsSaveButton.click();
		try {
			String continuecurWindow = driver.getWindowHandle();
			Set<String> continuewindows11 = driver.getWindowHandles();
			if (continuewindows11.size() > 1) {
				for (String window : continuewindows11) {
					System.err.println("window " + window);
					if (!window.equalsIgnoreCase(continuecurWindow)) {
						driver = driver.switchTo().window(window);
						break;
					}
				}
				Thread.sleep(2000);
				contractsSaveButton.click();
			}
		} catch (Exception e) {
			System.err.println("save button is not visible");
		}
	}

	@When("^user clicks on recenttabs$")
	public void user_clicks_on_recenttabs() throws Throwable {
		EpicKeyInfoPage keyInfo = new EpicKeyInfoPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebElement recentItemsTab = keyInfo.getrecentItemsTab();
		recentItemsTab.click();
		WebElement FirstrecentItem = keyInfo.getfirstRecentItem();
		WaitUtils.waitForDisplayed(FirstrecentItem);
		FirstrecentItem.click();

	}

	@When("^user request for isbn$")
	public void user_request_for_isbn() throws Throwable {
		EpicKeyInfoPage keyInfo = new EpicKeyInfoPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebElement requestISBN = keyInfo.getrequestIsbn();
		WaitUtils.waitForDisplayed(requestISBN);
		requestISBN.click();

		String curWindow12 = driver.getWindowHandle();
		Set<String> windows12 = driver.getWindowHandles();

		if (windows12.size() > 1) {
			for (String window : windows12) {
				System.err.println("window " + window);
				if (!window.equalsIgnoreCase(curWindow12))
					;
				driver = driver.switchTo().window(window);
			}
			WebElement requestScholasticISBN = keyInfo.getrequestScholasticIsbn();
			requestScholasticISBN.click();
			Thread.sleep(1000);
			WebElement submitBut = keyInfo.getrequestScholasticIsbnSubmitButton();
			submitBut.click();

			driver.switchTo().window(curWindow12);
		}
	}

	/*
	 * @When("^user finalize$") public void user_finalize() throws Throwable {
	 * EpicKeyInfoPage keyInfo=new EpicKeyInfoPage(); WebDriver
	 * driver=TestBaseProvider.getTestBase().getDriver(); WebElement
	 * finalizeButton=keyInfo.getfinalizeButton(); finalizeButton.click();
	 * Thread.sleep(1000); String curWindowFinalize = driver.getWindowHandle();
	 * Set<String> windowsFinalize = driver.getWindowHandles();
	 * if(windowsFinalize.size()>1) { for (String window : windowsFinalize) {
	 * System.err.println("window " + window); if
	 * (!window.equalsIgnoreCase(curWindowFinalize)) ;
	 * driver=driver.switchTo().window(window); } WebElement
	 * paidISBN=keyInfo.getpaidIsbn(); paidISBN.click(); Thread.sleep(500);
	 * WebElement budget=keyInfo.getbudget(); budget.sendKeys("1011");
	 * Thread.sleep(500); WebElement desc=keyInfo.getdesc();
	 * desc.sendKeys("test"); Thread.sleep(500); WebElement
	 * contractExpected=keyInfo.getcontractExpected(); Select selectcontract=new
	 * Select(contractExpected); selectcontract.selectByValue("CNR");
	 * Thread.sleep(500); WebElement territory=keyInfo.getterritory(); Select
	 * selectTerritory=new Select(territory); selectTerritory.selectByIndex(2);
	 * Thread.sleep(500); WebElement distribution=keyInfo.getdistribution();
	 * Select selectDistribution=new Select(distribution);
	 * selectDistribution.selectByIndex(2); WebElement
	 * apply=keyInfo.getapplyButton(); apply.click(); Thread.sleep(10000);
	 * driver.switchTo().window(curWindowFinalize); } }
	 */

	@Then("^epic is created$")
	public void epic_is_created() throws Throwable {

	}

	@Given("^user login into the epic$")
	public void user_login_into_the_epic() throws Throwable {
		EpicSignin signin = new EpicSignin();
		EpicHomePage homepage = new EpicHomePage();
		EpicSearchResults searchresults = new EpicSearchResults();
		EpicKeyInfoPage keyInfo = new EpicKeyInfoPage();
		signin.launchPage();
		signin.signIn(TestBaseProvider.getTestBase().getString("epic.userID"),
				TestBaseProvider.getTestBase().getString("epic.password"));
		Thread.sleep(1000);
	}

	@When("^user searches for \"([^\"]*)\" title$")
	public void user_searches_for_title(String c) throws Throwable {
		EpicHomePage homepage = new EpicHomePage();
		EpicSearchResults searchresults = new EpicSearchResults();
		String isbn13 = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn13.number");
		homepage.getsearchIsbn13().sendKeys(isbn13.trim());
		homepage.getsearchButton().click();
		searchresults.getResult().click();
	}

	@Given("^User searches for New record by ISBN (\\d+)$")
	public void user_searches_for_New_record_by_ISBN(int arg1) throws Throwable {
		EpicHomePage homepage = new EpicHomePage();
		EpicSearchResults searchresults = new EpicSearchResults();
		String isbn13 = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		homepage.getsearchIsbn13().sendKeys(isbn13.trim());
		homepage.getsearchButton().click();
		// searchresults.getResult().click();
	}

	@SuppressWarnings("unchecked")
	@Given("^User Query product information for \"([^\"]*)\" from OTBPM database$")
	public void user_Query_product_information_for_from_OTBPM_database(String fields) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		DBConnection db = new DBConnection();
		Thread.sleep(20000);
		String isbn13 = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn13.number");
		String upn = (String) TestBaseProvider.getTestBase().getContext().getProperty("upn.number");
		for (int i = upn.length(); i < 11; i++) {
			upn = "0" + upn;
		}
		Thread.sleep(10000);
		ArrayList<String> databaseResults = new ArrayList<String>();
		ResultSet rs = db.readDataFromDB(upn.trim());
		List<String> items = new Gson().fromJson(fields, List.class);
		while (rs.next()) {
			for (String item : items) {

				if (item.contains("DATE")) {
					String date = rs.getString(item);
					databaseResults.add(myprojects.changeDateFormat(date));
				}
				else {
					databaseResults.add(rs.getString(item));
				}
			}
		}
		TestBaseProvider.getTestBase().getContext().setProperty("db.results", databaseResults);
	}

	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" values from OTBPM database matches displayed \"([^\"]*)\" in EPIC$")
	public void values_from_OTBPM_database_matches_displayed_in_EPIC(String arg1, String arg2) throws Throwable {
		EpicHomePage homepage=new EpicHomePage();
		ArrayList<String> dbResults = (ArrayList<String>) TestBaseProvider.getTestBase().getContext()
				.getProperty("db.results");
		ArrayList<String> epicResults = (ArrayList<String>) TestBaseProvider.getTestBase().getContext()
				.getProperty("epic.results");
		homepage.assertResults(dbResults, epicResults);
	}


	@When("^User searches for New record by ISBN (\\d+) in the OTBPM$")
	public void user_searches_for_New_record_by_ISBN_in_the_OTBPM(int arg1) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		String isbn13 = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn13.number");
		Thread.sleep(40000);
		System.out.println("waiting for the record to be updated in the database");
		Thread.sleep(60000);
		myprojects.searchFor(isbn13.trim());

		myprojects.waitForResultsLoadingComplete();
	}

	@Then("^New EPIC record is displayed in Search results$")
	public void new_EPIC_record_is_displayed_in_Search_results() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemGrid = myprojects.getProjectGrid();
		String expected_isbn13 = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn13.number");
		for (Row row : itemGrid.getRows()) {
			assertThat(row.getText().replaceAll("-", ""),
					Matchers.containsString(expected_isbn13.trim().replaceAll("-", "")));
		}
		Assert.assertTrue(itemGrid.getRows().size() > 0);

	}

	@When("^user searches for any of the record by title$")
	public void user_searches_for_any_of_the_record_by_title() throws Throwable {
		EpicHomePage homepage = new EpicHomePage();
		EpicSearchResults searchresults = new EpicSearchResults();
		WaitUtils.waitForDisplayed(homepage.getsearchInput());
		homepage.getsearchInput().sendKeys("BATMAN BEGINS");
		homepage.getsearchButton().click();
		WaitUtils.waitForDisplayed(searchresults.getResult());
		searchresults.getResult().click();
	}

	// label[(text()='%s')]/following::div[1] product details otbpm xpath
	@SuppressWarnings("unchecked")
	@Then("^INFO_IN _MY_PROJECTS values for the record matches OTBPM DATABASE_MP_FIELDS$")
	public void info_in__MY_PROJECTS_values_for_the_record_matches_OTBPM_DATABASE_MP_FIELDS() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		EpicHomePage homepage=new EpicHomePage();
		String isbn13 = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		ItemGrid itemgrid = myprojects.getProjectGrid();
		String options = getXmlString("otbpm.myProjectsInfo");
		// String isbn13=(String)
		// TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		ArrayList<String> projectsList = new ArrayList<String>();
		ArrayList<String> databaseResults = new ArrayList<String>();
		List<String> items = new Gson().fromJson(options, List.class);
		for (String item : items) {
			projectsList.addAll(itemgrid.allColumnsText(item));
		}
		String sqlQ = "select * FROM dbo.PRODUCT_CUSTOM_INFO,dbo.PRODUCT_V3_INFO"
				+ " where PRODUCT_CUSTOM_INFO.ISBN=PRODUCT_V3_INFO.ISBN and PRODUCT_V3_INFO.ISBN_13='" + isbn13 + "'";
		ResultSet rs = DBConnection.dbConnection(sqlQ);
		String fields = getXmlString("otbpm.database");
		List<String> dbitems = new Gson().fromJson(fields, List.class);
		while (rs.next()) {
			for (String item : dbitems) {

				if (item.contains("DATE")) {
					String date = rs.getString(item);
					databaseResults.add(myprojects.changeDateFormat(date));
				} else {
					databaseResults.add(rs.getString(item));
				}
			}
		}
		homepage.assertResults(databaseResults, projectsList);

	}

	@SuppressWarnings({ "static-access", "unchecked" })
	@Then("^<INFO_IN _PRODUCT_DETAILS> values for the record matches OTBPM <DATABASE_PD_FIELDS>$")
	public void info_in__PRODUCT_DETAILS_values_for_the_record_matches_OTBPM_DATABASE_PD_FIELDS() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		EpicHomePage homepage=new EpicHomePage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String isbn13 = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		ItemGrid itemgrid = myprojects.getProjectGrid();
		// itemgrid.getColumnIndex("Title");
		itemgrid.getRows().get(0).getColumn("Title").click();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		// WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().frame(0);
		String options = getXmlString("otbpm.productDetails");
		// String isbn13=(String)
		// TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		ArrayList<String> projectsList = new ArrayList<String>();
		ArrayList<String> databaseResults = new ArrayList<String>();
		List<String> items = new Gson().fromJson(options, List.class);
		String xpathExpression = "//label[text()='%s']//following::input[1]";
		for (String item : items) {
			String formatXpath = xpathExpression.format(xpathExpression, item);
			WebElement elmValue = driver.findElement(By.xpath(formatXpath));
			projectsList.add(elmValue.getAttribute("value"));
		}
		String sqlQ = "SELECT * FROM " + "dbo.PRODUCT_CUSTOM_INFO,dbo.PRODUCT_V3_INFO,dbo.PLANNING_INFO "
				+ "WHERE PRODUCT_CUSTOM_INFO.CORP_ID_VALUE=PRODUCT_V3_INFO.CORP_ID_VALUE "
				+ "AND PRODUCT_V3_INFO.CORP_ID_VALUE=PLANNING_INFO.CORP_ID_VALUE AND " + "PRODUCT_V3_INFO.ISBN_13='"
				+ isbn13 + "'";
		ResultSet rs = DBConnection.dbConnection(sqlQ);
		String fields = getXmlString("otbpm.productDatabase");
		List<String> dbitems = new Gson().fromJson(fields, List.class);
		while (rs.next()) {
			for (String item : dbitems) {

				if (item.contains("DATE")) {
					String date = rs.getString(item);
					databaseResults.add(myprojects.changeDateFormat(date));
				} else {
					databaseResults.add(rs.getString(item));
				}
			}
		}
		myprojects.waitForResultsLoadingComplete();
		myprojects.getcloseButton().click();
		driver.switchTo().defaultContent();
		homepage.assertResults(databaseResults, projectsList);
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	@Then("^<INFO_IN_TRANSMITTAL_FORM> values for the record matches OTBPM <DATABASE_TF_FIELDS>$")
	public void info_in_transmittal_form_values_for_the_record_matches_OTBPM_DATABASE_TF_FIELDS() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		EpicHomePage homepage=new EpicHomePage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String isbn13 = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		ItemGrid itemgrid = myprojects.getProjectGrid();
		// itemgrid.getColumnIndex("Title");
		itemgrid.getRows().get(0).getColumn("Transmittal Form").click();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		// WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().frame(0);
		String options = getXmlString("otbpm.transmittalForm");
		// String isbn13=(String)
		// TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		ArrayList<String> projectsList = new ArrayList<String>();
		ArrayList<String> databaseResults = new ArrayList<String>();
		List<String> items = new Gson().fromJson(options, List.class);
		String xpathExpression = "//label[text()='%s']//following::input[1]";
		for (String item : items) {
			String formatXpath = xpathExpression.format(xpathExpression, item);
			WebElement elmValue = driver.findElement(By.xpath(formatXpath));
			projectsList.add(elmValue.getAttribute("value"));
		}
		String sqlQ = "SELECT * FROM " + "dbo.PRODUCT_CUSTOM_INFO,dbo.PRODUCT_V3_INFO,dbo.PLANNING_INFO "
				+ "WHERE PRODUCT_CUSTOM_INFO.CORP_ID_VALUE=PRODUCT_V3_INFO.CORP_ID_VALUE "
				+ "AND PRODUCT_V3_INFO.CORP_ID_VALUE=PLANNING_INFO.CORP_ID_VALUE AND " + "PRODUCT_V3_INFO.ISBN_13='"
				+ isbn13 + "'";
		ResultSet rs = DBConnection.dbConnection(sqlQ);
		String fields = getXmlString("otbpm.transmittaldatabase");
		List<String> dbitems = new Gson().fromJson(fields, List.class);
		while (rs.next()) {
			for (String item : dbitems) {

				if (item.contains("DATE")) {
					String date = rs.getString(item);
					databaseResults.add(myprojects.changeDateFormat(date));
				} else {
					databaseResults.add(rs.getString(item));
				}
			}
		}
		myprojects.waitForResultsLoadingComplete();
		myprojects.getcloseButton().click();
		driver.switchTo().defaultContent();
//		myprojects.getcloseButton().click();
		homepage.assertResults(databaseResults, projectsList);

	}

	@SuppressWarnings("unchecked")
	@When("^read the product details values$")
	public void read_the_product_details_values() throws Throwable {
		readProductDetailsValues("epic.readFields.bookClubs.keyInfoPage", "epic.readFields.bookClubs.displayPage");
		Thread.sleep(5000);
		System.out.println("read the fields from epic");		
	}

	@SuppressWarnings("unchecked")
	public void readProductDetailsValues(String createRecordReading, String displayRecordReading)
			throws InterruptedException {
		EpicKeyInfoPage keyinfo = new EpicKeyInfoPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		ArrayList<String> epicArr = new ArrayList<String>();
		try {
			String options = getXmlString(createRecordReading);
			String xpathExpression = "//form//td[@class='editCell']//span[starts-with(text(),'%s')]/following::td";
			List<String> items = new Gson().fromJson(options, List.class);
			for (String item : items) {
				if (item.contains("Editor")) {
					String[] c = keyinfo.readDataFromEpic(item, xpathExpression).split(",");
					String fName = c[1].replace(",", "");
					String fullName = fName.trim() + c[0].trim();
					epicArr.add(fullName);
				} else if (item.equals("UPN:")) {
					String r = keyinfo.readDataFromEpic(item, xpathExpression);
					TestBaseProvider.getTestBase().getContext().setProperty("upn.number", r.trim());
				} else if (item.equals("ISBN 13")) {
					String value = keyinfo.readDataFromEpic(item, xpathExpression);
					epicArr.add(value);
					TestBaseProvider.getTestBase().getContext().setProperty("isbn.number", value.trim());
				}

				else {
					String value = keyinfo.readDataFromEpic(item, xpathExpression);
					epicArr.add(value);
				}	
			}
			driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[1]/a[1]/img")).click();
			String Displayoptions = getXmlString(displayRecordReading);
			String DisplayxpathExpression = "//td[@class='editCell']//span[starts-with(text(),'%s')]/following::td";
			List<String> displayItems = new Gson().fromJson(Displayoptions, List.class);
			for (String displayItem : displayItems) {

				if (displayItem.equals("Cover Colors") || displayItem.equals("Text Colors")) {
					String c = keyinfo.readDataFromEpic(displayItem, DisplayxpathExpression).trim();
					if (c.length() > 1) {
						String[] colorsSplit = c.split(" ");
						epicArr.add(colorsSplit[0].trim());
						epicArr.add(colorsSplit[1].trim());
					} else if (c.length() <= 1) {
						epicArr.add(c);
						epicArr.add(" ");
					}
				} else if (displayItem.equals("Jacket Colors")) {
					String c = keyinfo.readDataFromEpic(displayItem, DisplayxpathExpression).trim();
					if (c.length() > 1) {
						String colorsSplit = c.substring(1);
						epicArr.add(colorsSplit.trim());
					} else if (c.length() <= 1) {
						epicArr.add(" ");
					}
				} else {
					String Displayvalue = keyinfo.readDataFromEpic(displayItem, DisplayxpathExpression);
					System.out.println(displayItem + "" + Displayvalue);
					epicArr.add(Displayvalue);
				}
			}
			TestBaseProvider.getTestBase().getContext().setProperty("epic.results", epicArr);
		} catch (Exception e) {
			TestBaseProvider.getTestBase().getContext().setProperty("epic.results", epicArr);
		}
	}

	@When("^user logout of epic$")
	public void user_logout_of_epic() throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.findElement(By.xpath("//img[contains(@src,'logout')]")).click();
	}

	@When("^user edits the record information$")
	public void user_edits_the_record_information() throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.findElement(By.xpath("//img[contains(@src,'edit')]")).click();
		String editInputOptions = getXmlString("edit.inputFields.requiredFieldsKeyInfo");
		String editSelectOptions = getXmlString("edit.selectFields.requiredSelectFields");
		String editTradeInputOptions = getXmlString("edit.inputFields.tradeRequiredInputs");
		String editTradeSelectOptions = getXmlString("edit.selectFields.tradeSelectedOptions");
		inputFields(editInputOptions,"edit");
		selectFields(editSelectOptions,"edit");
		try {
			inputFields(editTradeInputOptions,"edit");
			selectFields(editTradeSelectOptions,"edit");
		} catch (Exception e) {

		}
		continueButton();
		EpicKeyInfoPage keyinfo=new EpicKeyInfoPage();
		keyinfo.getsaveButton().click();
		continueButton();
		continueButton();
		Thread.sleep(60000);
		System.out.println("waiting for the save to complete");
		Thread.sleep(60000);
		System.out.println("waiting for the page to navigate");
	}

	@When("^User searches in My Projects view using isbn$")
	public void user_searches_in_My_Projects_view_using_isbn() throws Throwable {
		String searchText = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.searchFor(searchText);
	}

	@Then("^Search results matches for the project field that displays text$")
	public void search_results_matches_for_the_project_field_that_displays_text() throws Throwable {
		String textToMatch = (String) TestBaseProvider.getTestBase().getContext().getProperty("isbn.number");
		MyProjectsPage projectsPage = new MyProjectsPage();
		boolean navNextPage = false;
		do {
			if (navNextPage) {
				projectsPage.getNextButton().click();
				projectsPage.waitForResultsLoadingComplete();
			}
			ItemGrid results = projectsPage.getProjectGrid();

			for (Row row : results.getRows()) {
				String rowText = row.getText();

				assertThat(rowText.toUpperCase(), Matchers.containsString(textToMatch.toUpperCase()));
			}
			navNextPage = true;
		} while (projectsPage.getNextButton().isEnabled());

	}
	
	@When("^User updates the New EPIC record with \"([^\"]*)\" value$")
	public void user_updates_the_New_EPIC_record_with_value(String artDirector) throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.findElement(By.xpath("//img[contains(@src,'edit')]")).click();
		String editInputOptions = getXmlString("edit.inputFields.requiredFieldsKeyInfo");
		String editSelectOptions = getXmlString("edit.selectFields.requiredSelectFields");
		String editTradeInputOptions = getXmlString("edit.inputFields.tradeRequiredInputs");
		String editTradeSelectOptions = getXmlString("edit.selectFields.tradeSelectedOptions");
		inputFields(editInputOptions,"edit");
		selectFields(editSelectOptions,"edit");
		try {
			inputFields(editTradeInputOptions,"edit");
			selectFields(editTradeSelectOptions,"edit");
		} catch (Exception e) {

		}
		WebElement selectFieldElement = driver.findElement(By.xpath("//td//span[starts-with(text(),'Art Director:')]/following::select[1]"));
		Select select = new Select(selectFieldElement);
		select.selectByVisibleText(artDirector);
		continueButton();
		EpicKeyInfoPage keyinfo=new EpicKeyInfoPage();
		keyinfo.getsaveButton().click();
		continueButton();
		continueButton();
		Thread.sleep(5000);
		System.out.println("waiting for the save to complete");
	}
	
	@When("^user selects the \"([^\"]*)\"$")
	public void user_selects_the(String artDirector) throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebElement selectFieldElement = driver.findElement(By.xpath("//td//span[starts-with(text(),'Art Director:')]/following::select[1]"));
		Select select = new Select(selectFieldElement);
		select.selectByVisibleText(artDirector);
	}
	
	@Given("^user creates a epic record with art director \"([^\"]*)\"$")
	public void user_creates_a_epic_record_with_art_director(String artDirector) throws Throwable {
		user_login_to_epic();
		user_searches_for_title();
		user_selects_the_owner();
		user_fills_the_mandatory_fields();
		user_selects_the(artDirector);
		user_clicks_save_button();
		user_clicks_on_recenttabs();
		user_request_for_isbn();
		read_the_product_details_values();
		user_logout_of_epic();
	}
}