package com.scholastic.cucumber.apt.stepdefs;

import static com.scholastic.torque.common.StringMatchers.containsIgnoreCase;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

import java.util.List;
import java.util.Set;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.AssignDialog;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.ManageMyTeamPage;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MyProjectsListSteps {
	@Given("^user is logged in OTBPM$")
	public void user_is_logged_in_OTBPM() throws Throwable {
		OTBPMSigninPage signin = new OTBPMSigninPage();
		signin.launchPage();
		signin.signIn(TestBaseProvider.getTestBase().getContext().getString("userID"),
				TestBaseProvider.getTestBase().getString("password"));
	}

	@When("^user is in 'My Projects' view$")
	public void user_is_in_My_Projects_view() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		Thread.sleep(1000);
		ItemGrid itemGrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(myprojects.getProjectGrid().getcheckboxesHeader());
		WebDriverWait wait1 = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
		wait1.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loadHolder")));
		itemGrid.showAllColumns();
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loadHolder")));
		// WaitUtils.waitForEnabled(itemGrid.getWrappedElement());
	}

	@Then("^'Advanced Search' option is displayed$")
	public void advanced_Search_option_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		AssertUtils.assertDisplayed(myprojects.getAdvancedSearchLink());
		Assert.assertEquals(myprojects.getAdvancedSearchLink().getText(), "Advanced Search");
	}

	@Given("^user is logged in OTBPM as \"([^\"]*)\"$")
	public void user_is_logged_in_OTBPM_as(String user) throws Throwable {
		OTBPMSigninPage signin = new OTBPMSigninPage();
		signin.launchPage();
		String username = TestBaseProvider.getTestBase().getString("signin.signinuser." + user + ".name");
		String password = TestBaseProvider.getTestBase().getString("signin.signinuser." + user + ".password");
		signin.signIn(username, password);
	}

	@Then("^'Assign' option is displayed only for \"([^\"]*)\"$")
	public void assign_option_is_displayed_only_for(String role) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		WaitUtils.waitForDisplayed(myprojects.getAssignButton());
		AssertUtils.assertDisplayed(myprojects.getAssignButton());
		Assert.assertEquals(myprojects.getAssignButton().getText(), "Assign");
	}

	@Then("^'Assign' option is not displayed$")
	public void assign_option_is_not_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		AssertUtils.assertDisabled(myprojects.getAssignButton());
		Assert.assertEquals(myprojects.getAssignButton().getText(), "Assign");
	}

	@When("^Project List is populated$")
	public void project_List_is_populated() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		itemgrid.showAllColumns();
		myprojects.waitForResultsLoadingComplete();
		myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		// Thread.sleep(10000);
		String searchkey = TestBaseProvider.getTestBase().getString("signin.search");
		if (itemgrid.getRows().size() == 0) {
			myprojects.searchFor(searchkey);
		}
	}

	@When("^user selects a project to 'Assign'$")
	public void user_selects_a_project_to_Assign() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		List<WebElement> checkboxes = itemgrid.findElements(By.id("check_tbmyProjects"));
		if (!(checkboxes.get(1)).isSelected()) {
			myprojects.clickElementByJS(checkboxes.get(1));
		}
		myprojects.clickElementByJS(myprojects.getAssignButton());
	}

	@Then("^'Assign' window is displayed$")
	public void assign_window_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		WebElement assignwindow = myprojects.getDialog().getDialogTitleElement();
		WaitUtils.waitForDisplayed(assignwindow);
		AssertUtils.assertDisplayed(assignwindow);
		Assert.assertEquals(assignwindow.getText(), "Assign");
	}

	@When("^user selects more than one project 'to Assign'$")
	public void user_selects_more_than_one_project_to_Assign() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WebElement checkboxes = itemgrid.findElement(By.id("showAll_tbmyProjects"));
		if (itemgrid.getRows().size() < 2) {
			myprojects.searchFor("harry");
			myprojects.waitForResultsLoadingComplete();
		}
		checkboxes.click();
		myprojects.getAssignButton().click();
	}

	@Then("^'barcode small icon' is displayed for the listed projects$")
	public void barcode_small_icon_is_displayed_for_the_listed_projects() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		// Thread.sleep(1000);
		int index = itemgrid.getColumnIndex("Barcode");
		for (Row row : itemgrid.getRows()) {
			WaitUtils.waitForDisplayed(row.getColumn(index));
			AssertUtils.assertDisplayed(row.getColumn(index));
		}
	}

	@When("^user selects a project by 'Title'$")
	public void user_selects_a_project_by_Title() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		// Thread.sleep(1000);
		String searchkey = TestBaseProvider.getTestBase().getString("signin.search");
		if (itemgrid.getRows().size() == 0) {
			myprojects.searchFor(searchkey);
		}
		List<WebElement> titles = itemgrid.findElements(By.id("title"));
		titles.get(2).click();
	}

	@Then("^'Product Detail' window is displayed$")
	public void product_Detail_window_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		WebElement productDetailWindow = myprojects.getDialog().getDialogTitleElement();
		AssertUtils.assertDisplayed(productDetailWindow);
		Assert.assertTrue((productDetailWindow.getText().contains("Product Details")));
	}

	@When("^user selects a project from by 'Cover Image'$")
	public void user_selects_a_project_from_by_Cover_Image() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
		// Thread.sleep(1000);
		String searchkey = TestBaseProvider.getTestBase().getString("signin.search");
		if (itemgrid.getRows().size() == 0) {
			myprojects.searchFor(searchkey);
		}
		List<WebElement> covers = itemgrid.findElements(By.id("coverimage"));
		WaitUtils.waitForDisplayed(covers.get(1));
		covers.get(1).click();
		// Thread.sleep(1000);
	}

	@Then("^'Transmittal History' is displayed on the Project List$")
	public void transmittal_History_is_displayed_on_the_Project_List() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		WebElement transmittalHistory = myprojectsgrid.getHeader("Transmittal History");
		AssertUtils.assertDisplayed(transmittalHistory);
		List<String> headers = myprojectsgrid.getHeaderNames();
		Assert.assertTrue(headers.contains("Transmittal History"));
	}

	@Then("^'Transmittal History icon' is displayed for the listed projects$")
	public void transmittal_History_icon_is_displayed_for_the_listed_projects() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex("Transmittal History");
		for (Row row : itemgrid.getRows()) {
			AssertUtils.assertDisplayed(row.getColumn(index));
		}
	}

	@When("^user views the 'Transmittal History' for the project$")
	public void user_views_the_Transmittal_History_for_the_project() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
	}

	@Then("^'Transmittal History' window is displayed$")
	public void transmittal_History_window_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex("Transmittal History");
		boolean navNextPage = true;
		boolean firstpage = false;
		do {
			if (firstpage) {
				if (myprojects.getNextButton().isEnabled()) {
					myprojects.clickNextButon();
				}
			}
			for (Row row : itemgrid.getRows()) {
				row.getColumn(index).click();
				// Thread.sleep(1000);
				WebElement transmittalHistoryWindow = myprojects.getDialog().getDialogTitleElement();
				WaitUtils.waitForDisplayed(transmittalHistoryWindow);
				if (transmittalHistoryWindow.getText().equalsIgnoreCase("Transmittal History")) {
					navNextPage = false;
					TransmittalHistoryDialog transmittaldialog = myprojects.getTransmittalHistoryDialog();
					Assert.assertEquals(transmittalHistoryWindow.getText(), "Transmittal History");
					transmittaldialog.switchToDialog();
					ItemGrid items = transmittaldialog.getHistoryGrid();
					Assert.assertTrue(items.getRows().size() > 0, "number of rows is zero");
					myprojects.waitForResultsLoadingComplete();
					break;
				} else {
					myprojects.waitForResultsLoadingComplete();
					myprojects.clickCloseButton();
					WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
					// Thread.sleep(1000);
					firstpage = true;
				}
			}

		} while (navNextPage);
	}

	@Then("^'Online Transmittal Form' window is displayed$")
	public void online_Transmittal_Form_window_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex("Transmittal History");
		boolean navNextPage = true;
		boolean firstpage = false;
		do {
			if (firstpage) {
				if (myprojects.getNextButton().isEnabled()) {
					myprojects.clickNextButon();
				}
			}
			for (Row row : itemgrid.getRows()) {
				row.getColumn(index).click();
				WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
				WebElement transmittalFormWindow = myprojects.getDialog().getDialogTitleElement();
				WaitUtils.waitForDisplayed(transmittalFormWindow);
				// Thread.sleep(1000);
				if (!(transmittalFormWindow.getText().equalsIgnoreCase("Transmittal History"))) {
					navNextPage = false;
					myprojects.waitForResultsLoadingComplete();
					Assert.assertTrue(transmittalFormWindow.getText().contains("Production/Manufacturing Transmittal Form for Cover"));
					break;
				} else {
					myprojects.waitForResultsLoadingComplete();
					myprojects.clickCloseButton();
					WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
					// Thread.sleep(1000);
					firstpage = true;
				}
			}
		} while (navNextPage);
	}

	@Then("^\"([^\"]*)\" is displayed on the Project List$")
	public void is_displayed_on_the_Project_List(String routing) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		myprojectsgrid.showAllColumns();
		WebElement routingType = myprojectsgrid.getHeaderElement(routing);
		WaitUtils.waitForDisplayed(routingType);
		AssertUtils.assertDisplayed(routingType);
		List<String> headers = myprojectsgrid.getHeaderNames();
		Assert.assertTrue(headers.contains(routing));
	}

	@Then("^\"([^\"]*)\" is displayed for the listed project$")
	public void is_displayed_for_the_listed_project(String routing) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex(routing);
		for (Row row : itemgrid.getRows()) {
			WaitUtils.waitForDisplayed(row.getColumn(index));
			AssertUtils.assertDisplayed(row.getColumn(index));
		}
	}

	@When("^user views the \"([^\"]*)\" for the project$")
	public void user_views_the_for_the_project(String routing) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex(routing);
		itemgrid.getRows().get(1).getColumn(index).click();
	}

	@Then("^\"([^\"]*)\" is displayed as pdf$")
	public void is_displayed_as_pdf(String routing_form) throws Throwable {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		String curWindow = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		for (String window : windows) {
			System.err.println("window " + window);
			if (!window.equalsIgnoreCase(curWindow))
				;
			driver = driver.switchTo().window(window);
		}

		assertThat(driver.getTitle(), containsIgnoreCase("pdf"));
	}

	@Then("^'Transmittal Form' is displayed on the Project List$")
	public void transmittal_Form_is_displayed_on_the_Project_List() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		WebElement transmittalForm = myprojectsgrid.getHeader("Transmittal Form");
		AssertUtils.assertDisplayed(transmittalForm);
		List<String> headers = myprojectsgrid.getHeaderNames();
		Assert.assertTrue(headers.contains("Transmittal Form"));

	}

	@Then("^'Transmittal Form' is NOT included on the Project List$")
	public void transmittal_Form_is_NOT_included_on_the_Project_List() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		int index = myprojectsgrid.getColumnIndex("Barcode");
		for (Row row : myprojectsgrid.getRows()) {
			WaitUtils.waitForDisplayed(row.getColumn(index));
			AssertUtils.assertNotDisplayed(row.getColumn(index));
		}
	}

	@Then("^'Transmittal Form icon' is displayed for the listed projects$")
	public void transmittal_Form_icon_is_displayed_for_the_listed_projects() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		int index = myprojectsgrid.getColumnIndex("Transmittal Form");
		for (Row row : myprojectsgrid.getRows()) {
			AssertUtils.assertDisplayed(row.getColumn(index));
		}
	}

	@When("^user views the 'Transmittal Form' for the project$")
	public void user_views_the_Transmittal_Form_for_the_project() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myprojects.getProjectGrid();
		int index = myprojectsgrid.getColumnIndex("Transmittal Form");
		myprojectsgrid.getRows().get(0).getColumn(index).click();
	}

	@Then("^'Transmittal Form' window is displayed$")
	public void transmittal_Form_window_is_displayed() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		WebElement transmittalFormWindow = myprojects.getDialog().getDialogTitleElement();
		Assert.assertTrue(transmittalFormWindow.getText().contains("Production/Manufacturing Transmittal Form for Cover"));
	}

	@When("^user searches for \"([^\"]*)\"$")
	public void user_searches_for(String searchText) throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.searchFor(searchText);
	}

	@Then("^Project List is updated	with \"([^\"]*)\"$")
	public void project_List_is_updated_with(String searchText) throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		// Thread.sleep(1000);
		ItemGrid results = projectspage.getProjectGrid();
		WaitUtils.waitForEnabled(results.getWrappedElement());
		int index = results.getColumnIndex("Title");
		for (Row row : results.getRows()) {
			if (results.getRows().size() == 0) {
				Assert.assertTrue(results.getRows().contains(null));
			} else {
				if (row.getText().replaceAll("-", "").contains(searchText.replaceAll("-", ""))) {
					assertThat(row.getText().replaceAll("-", ""),
							Matchers.containsString(searchText.replaceAll("-", "")));
				} else {
					row.getColumn(index).click();
					// Thread.sleep(1000);

					projectspage.getDialog();
					WaitUtils.waitForDisplayed(projectspage.getDialog().getDialogTitleElement());
					WebDriver driver = TestBaseProvider.getTestBase().getDriver();
					driver.switchTo().frame(0);
					// Thread.sleep(1000);
					WebElement isbn = driver.findElement(By.id("input9"));
					WaitUtils.waitForDisplayed(isbn);
					String isbn_ten = isbn.getAttribute("value");
					assertThat(isbn_ten.replaceAll("-", ""), Matchers.containsString(searchText.replaceAll("-", "")));
					projectspage.waitForResultsLoadingComplete();
				}
			}
		}
	}

	@Then("^'Show My Teams Project' option is displayed$")
	public void show_My_Teams_Project_option_is_displayed() throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		WebElement showProjectsLink = projectspage.getshowTeamProjLink();
		// WaitUtils.waitForDisplayed(showProjectsLink);
		AssertUtils.assertDisplayed(showProjectsLink);
		Assert.assertEquals(showProjectsLink.getText(), "Show My Team's Projects");
	}

	@Then("^'Show My Teams Project' link is not displayed$")
	public void show_My_Teams_Project_link_is_not_displayed() throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		WebElement showProjectsLink = projectspage.getshowTeamProjLink();
		AssertUtils.assertNotDisplayed(showProjectsLink);
	}

	@When("^user selects 'Show My Team Project'$")
	public void user_selects_Show_My_Team_Project() throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		WaitUtils.waitForDisplayed(projectspage.getshowTeamProjLink());
		Thread.sleep(5000);
		projectspage.clickElementByJS(projectspage.getshowTeamProjLink());
		Thread.sleep(4000);
	}

	@Then("^'Show My Team Project' link is not displayed$")
	public void show_My_Team_Project_link_is_not_displayed() throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		String showprojects = projectspage.getshowTeamProjLink().getText();
		Assert.assertFalse(showprojects.equals("Show My Team's Projects"));
	}

	@Then("^'Show My Projects' link is displayed$")
	public void show_My_Projects_link_is_displayed() throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		// WaitUtils.waitForDisplayed(projectspage.getshowTeamProjLink());
		String showprojects = projectspage.getshowTeamProjLink().getText();
		Assert.assertTrue(showprojects.equals("Show Only My Projects"));
	}

	@When("^user selects 'Show My Projects'$")
	public void user_selects_Show_My_Projects() throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		WaitUtils.waitForDisplayed(projectspage.getshowTeamProjLink());
		Thread.sleep(8000);
		projectspage.clickShowTeamProj();
		// WaitUtils.waitForEnabled(projectspage.getshowTeamProjLink());

	}

	@Then("^'Show My Projects' link is not displayed$")
	public void show_My_Projects_link_is_not_displayed() throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		// WaitUtils.waitForDisplayed(projectspage.getshowTeamProjLink());
		// Thread.sleep(10000);
		String showprojects = projectspage.getshowTeamProjLink().getText();
		Assert.assertFalse(showprojects.equals("Show Only My Projects"));
	}

	@Then("^'Show My Teams Project' link is displayed$")
	public void show_My_Teams_Project_link_is_displayed() throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		// WaitUtils.waitForDisplayed(projectspage.getshowTeamProjLink());
		String showprojects = projectspage.getshowTeamProjLink().getText();
		Assert.assertTrue(showprojects.equals("Show My Team's Projects"));
	}

	@Then("^project 'ISBN (\\d+)' is displayed as ISBN$")
	public void project_ISBN_is_displayed_as_ISBN(int arg1) throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		ItemGrid projectsgrid = projectspage.getProjectGrid();
		int index = projectsgrid.getColumnIndex("ISBN");
		String isbn = TestBaseProvider.getTestBase().getString("isbn.thirteen");
		for (Row row : projectsgrid.getRows()) {
			Assert.assertEquals(row.getColumnText(index).replaceAll("-", "").length(), 13);
			break;
		}
		List<String> headers = projectsgrid.getHeaderNames();
		Assert.assertTrue(headers.contains("ISBN"));
		AssertUtils.assertDisplayed(projectsgrid.getHeader("ISBN"));
	}

	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" is hidden in the Project List$")
	public void is_hidden_in_the_Project_List(String col_name) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myProjects.getProjectGrid();
		myprojectsgrid.showAllColumns();
		List<String> items = new Gson().fromJson(col_name, List.class);
		for (String item : items) {
			Assert.assertTrue(myprojectsgrid.getHeaderNames().contains(item));
		}
	}

	@When("^My Projects grid view is set to default$")
	public void my_Projects_grid_view_is_set_to_default() throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myProjects.getProjectGrid();
		myprojectsgrid.showAllColumns();
	}

	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" is displayed in the Project List$")
	public void is_displayed_in_the_Project_List(String col_name) throws Throwable {
		MyProjectsPage myProjects = new MyProjectsPage();
		myProjects.waitForResultsLoadingComplete();
		ItemGrid myprojectsgrid = myProjects.getProjectGrid();
		myprojectsgrid.showAllColumns();
		List<String> items = new Gson().fromJson(col_name, List.class);
		for (String item : items) {
			AssertUtils.assertDisplayed(myprojectsgrid.getHeaderElement(item));
		}
	}

	@Then("^\"([^\"]*)\" target is displayed on the Project List$")
	public void target_is_displayed_on_the_Project_List(String target) throws Throwable {
		MyProjectsPage projectspage = new MyProjectsPage();
		projectspage.waitForResultsLoadingComplete();
		ItemGrid itemgrid = projectspage.getProjectGrid();
		itemgrid.showAllColumns();
		AssertUtils.assertDisplayed(itemgrid.getHeaderElement(target));
	}



	@Given("^user selects a \"([^\"]*)\" from My Projects and go to Assign page$")
	public void user_selects_a_from_My_Projects_and_go_to_Assign_page(String isbn) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.searchFor(isbn);
		myprojects.waitForResultsLoadingComplete();
		AssignDialog assign=new AssignDialog();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		WaitUtils.waitForEnabled(itemgrid.getCheckbox().get(1));
		myprojects.clickElementByJS(itemgrid.getCheckbox().get(1));
		myprojects.clickAssignButton();
		assign.switchToAssignDialog();

	}

	@Then("^user should seee \"([^\"]*)\" is not listed in My Projects page$")
	public void user_should_seee_is_not_listed_in_My_Projects_page(String PROJECT) throws Throwable {

		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		ManageMyTeamPage myteam=new ManageMyTeamPage();
		myteam.clickAptMyProjects();

		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		List<String> items=
				itemgrid.allColumnsText("ISBN");
		if(items.contains(PROJECT))	
		{
			Assert.fail(" Still project is present in list");

		}

	}


	@Then("^User can Search for \"([^\"]*)\"$")
	public void user_can_Search_for_Project(String PROJECT) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.searchFor(PROJECT);
		myprojects.waitForResultsLoadingComplete();

	}


	@Then("^\"([^\"]*)\" Art Director value is empty$")
	public void art_Director_value_is_empty(String arg1) throws Throwable {

		MyProjectsPage myprojects = new MyProjectsPage();
		ItemGrid myprojectsgrid=myprojects.getProjectGrid();
		int index = myprojectsgrid.getColumnIndex("Art Director");
		String ArtDirectorText=myprojectsgrid.getRows().get(0).getColumn(index).getText();
		Assert.assertEquals(ArtDirectorText,"","FAIL-Artdirector not Empty");

	}


}