package com.scholastic.cucumber.apt.stepdefs;

import java.util.ArrayList;

import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.ProjectDetails;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class FileNameSteps {

	@Then("^the file Name on the myprojects and projects details page are the same$")
	public void the_file_Name_on_the_myprojects_and_projects_details_page_are_the_same() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    ProjectDetails details=new ProjectDetails();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
    	String fileNamecolValue= itemgrid.getRows().get(0).getColumnText("File Name");   	
	    myprojects.openProjectDetails(0);
	    String fileNameInDetails=details.getValueElement("File Name").getAttribute("value");
	    Assert.assertEquals(fileNameInDetails, fileNamecolValue);
	 }

	@Then("^the file name should have the prefix of the Record Number followed by an underscore$")
	public void the_file_name_should_have_the_prefix_of_the_Record_Number_followed_by_an_underscore() throws Throwable {
	    ProjectDetails details=new ProjectDetails();
	    String recordNum=details.getValueElement("Record #").getAttribute("value").trim();
	    String fileName=details.getValueElement("File Name").getAttribute("value");
	    recordNum=recordNum+"_";
	    Assert.assertTrue(fileName.toUpperCase().startsWith(recordNum.toUpperCase()));
	}

	@Then("^the file name should have the first (\\d+) alphanumeric values of the title followed by the text \"([^\"]*)\"$")
	public void the_file_name_should_have_the_first_alphanumeric_values_of_the_title_followed_by_the_text(int arg1, String suffix) throws Throwable {
	    	ProjectDetails details=new ProjectDetails();
	    	String fileName=details.getValueElement("File Name").getAttribute("value");
		    String title=details.getValueElement("Title").getAttribute("value");
		    title=title.replaceAll("[^a-zA-Z0-9]", "");
		    title=title.substring(0, 12).replaceAll("\\s", "");
		    Assert.assertTrue(fileName.toUpperCase().contains(title.toUpperCase()));   
	    	Assert.assertTrue(fileName.endsWith(suffix));

	}
	@Then("^the file name should have the prefix of the unformatted ISBN_(\\d+) followed by an underscore$")
	public void the_file_name_should_have_the_prefix_of_the_unformatted_ISBN__followed_by_an_underscore(int arg1) throws Throwable {
		ProjectDetails details=new ProjectDetails();
		String ISBN=details.getValueElement("ISBN 13").getAttribute("value").replaceAll("[\\s\\-()]", "").trim();
		String fileName=details.getValueElement("File Name").getAttribute("value");
		ISBN=ISBN+"_";
		Assert.assertTrue(fileName.startsWith(ISBN));
		
		
		}
	
}
