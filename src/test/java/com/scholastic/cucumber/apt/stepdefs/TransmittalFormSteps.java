package com.scholastic.cucumber.apt.stepdefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.cucumber.apt.pageobjects.TransmittalForm;
import com.scholastic.cucumber.apt.pageobjects.TransmittalHistoryDialog;
import com.scholastic.cucumber.apt.pageobjects.TransmittalPdf;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransmittalFormSteps {

	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" 'Cover/Jacket/Interior' radiobuttons are displayed$")
	public void cover_Jacket_Interior_radiobuttons_are_displayed(String transmittalTypes) throws Throwable {
	 
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		 List<String> types = new Gson().fromJson(transmittalTypes, List.class);
			for (String transmittal : types) { 	  
		 WebElement transmittalType=transmittals.getTransmittalFormButtons(transmittal);
		 AssertUtils.assertDisplayed(transmittalType);
			}
	}
	
	@Given("^\"([^\"]*)\" is logged in OTBPM$")
	public void is_logged_in_OTBPM(String user) throws Throwable {
		OTBPMSigninPage signin = new OTBPMSigninPage();
		signin.launchPage();
		signin.signIn(TestBaseProvider.getTestBase().getContext().getString("transmittalFormUsers."+user+".name"),
				TestBaseProvider.getTestBase().getString("transmittalFormUsers."+user+".password"));
	}

	@Then("^\"([^\"]*)\" radiobutton is selected$")
	public void radiobutton_is_selected(String transmittal) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		myprojects.switchToFrame();
		 WebElement transmittalType=transmittals.getTransmittalFormButtons(transmittal);
		 AssertUtils.assertSelected(transmittalType);
	}
	
	@When("^the user selects the Transmittal Form$")
	public void the_user_selects_the_Transmittal_Form() throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		int index = itemgrid.getColumnIndex("Transmittal Form");		
	}

	@Then("^Transmittal Form \"([^\"]*)\" is updated for \"([^\"]*)\"$")
	public void transmittal_Form_is_updated_for(String title, String transmittal) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		myprojects.getDialog();
		myprojects.switchToFrame();
		Assert.assertTrue(myprojects.getDialog().getDialogTitleElement().getText().trim().contains(title));
	}

	@Then("^\"([^\"]*)\" label is updated for \"([^\"]*)\"$")
	public void label_is_updated_for(String typeOfColors, String transmittalType) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.switchToFrame();
		TransmittalForm transmittalForm=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittalForm.getLabel(typeOfColors));
		Assert.assertTrue(transmittalForm.getLabel(typeOfColors).getText().contains(transmittalType));
	}
	
	@Then("^Transmittal Form for \"([^\"]*)\" is displayed as a pdf that opens in a new tab$")
	public void transmittal_Form_for_is_displayed_as_a_pdf_that_opens_in_a_new_tab(String transmitalType) throws Throwable {
		 WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		 String transmittalFormWindow = driver.getWindowHandle();
		 TestBaseProvider.getTestBase().getContext().setProperty("transmittal.window", transmittalFormWindow);
			Set<String> windows = driver.getWindowHandles();
			Assert.assertTrue(windows.size()>1);
			for (String window : windows) {
				System.err.println("window " + window);
				if (!window.equalsIgnoreCase(transmittalFormWindow))
					;
				driver.switchTo().window(window);
			}
			String pdfValue=transmittalpdf.getLabelElement(transmitalType).getText();
			Assert.assertTrue(pdfValue.contains(transmitalType));
		   Assert.assertTrue(driver.getTitle().contains("pdf"));
		   
	}
	
//	@When("^user selects 'Mech' option \"([^\"]*)\"$")
//	public void user_selects_Mech_option(String other) throws Throwable {
//		MyProjectsPage myprojects=new MyProjectsPage();
//		TransmittalForm transmittals=new TransmittalForm();
//		myprojects.getDialog();
//		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
//		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
//		driver.switchTo().frame(0);
//		transmittals.getTransmittalFormButtons(other).click();
//	}
//
//	@Then("^a text field is displayed for user to type$")
//	public void a_text_field_is_displayed_for_user_to_type() throws Throwable {
//	    TransmittalForm transmittalform=new TransmittalForm();
//	    AssertUtils.assertDisplayed(transmittalform.getFormMechOther());
//	}
	
	@When("^user selects 'Mech/ProofsTypes' option \"([^\"]*)\"$")
	public void user_selects_Mech_ProofsTypes_option(String other) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		myprojects.getDialog();
		WaitUtils.waitForDisplayed(myprojects.getDialog().getDialogTitleElement());
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().frame(0);
		transmittals.getTransmittalFormButtons(other).click();
		transmittals.getProofTypeOtherCheckbox().click();
		transmittals.getHerewithOtherCheckbox().click();
	}

	@Then("^a text field is displayed for user to type for 'Mech/ProofsTypes'$")
	public void a_text_field_is_displayed_for_user_to_type_for_Mech_ProofsTypes() throws Throwable {
		  TransmittalForm transmittalform=new TransmittalForm();
		    AssertUtils.assertDisplayed(transmittalform.getFormMechOther());
		    AssertUtils.assertDisplayed(transmittalform.getProofTypeOtherInput());
		    AssertUtils.assertDisplayed(transmittalform.getHerewithOtherInput());
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The selected \"([^\"]*)\" option in \"([^\"]*)\" is displayed in the Transmittal Form$")
	public void the_selected_option_in_is_displayed_in_the_Transmittal_Form(String types, String selectedOptions) throws Throwable {
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		 List<String> itemTypes = new Gson().fromJson(types, List.class);
		 List<String> options = new Gson().fromJson(selectedOptions, List.class);
			for(int i=0;i<itemTypes.size();i++)
			{
				Assert.assertEquals(transmittalpdf.getTransmittalPdfValue(itemTypes.get(i)), options.get(i));
			}
	}
	
	@Then("^'ARC'checkbox is unchecked$")
	public void arc_checkbox_is_unchecked() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.switchToTransmittalForm();
		WebElement transmittalType=transmittals.getTransmittalFormButtons("Advanced Reader Copy");
		Assert.assertFalse(transmittalType.isSelected());
	}
	
	@When("^user selects option \"([^\"]*)\"$")
	public void user_selects_option(String option) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.getTransmittalFormButtons(option).click();
	}

	@When("^user select an \"([^\"]*)\" from the list$")
	public void user_select_an_from_the_list(String option) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		transmittals.getAdditionalDropdown(option);
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		transmittals.switchToTransmittalForm();
	}

	@Then("^The additional \"([^\"]*)\" is displayed in the Transmittalpdf Form$")
	public void the_additional_is_displayed_in_the_Transmittalpdf_Form(String option) throws Throwable {
	    TransmittalPdf transmittalpdf=new TransmittalPdf();
	   String pdfValue= transmittalpdf.getValueElement("Proofs:").getText().trim();
	   Assert.assertEquals(pdfValue,option);
	}
	
	

	@When("^user selects \"([^\"]*)\" custom \"([^\"]*)\"$")
	public void user_selects_custom(String option, String value) throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		transmittals.switchToTransmittalForm();
		 if(option.equals("OtherHereWith"))
		   {
			   transmittals.getHerewithOtherCheckbox().click();
			   WaitUtils.waitForPresent(transmittals.getHerewithOtherInput());
			   transmittals.getHerewithOtherInput().clear();
			   transmittals.getHerewithOtherInput().sendKeys(value);
		   }
		   
		   else  if(option.equals("OtherProofType"))
		   {
			   transmittals.getProofTypeOtherCheckbox().click();
			   WaitUtils.waitForDisplayed(transmittals.getProofTypeOtherInput());
			   transmittals.getProofTypeOtherInput().clear();
			   transmittals.getProofTypeOtherInput().sendKeys(value);
		   }
		   else
		   {
				transmittals.getTransmittalFormButtons(option).click();
				transmittals.getFormMechOtherInput().clear();
				transmittals.getFormMechOtherInput().sendKeys(value);
		   }
	}

	@SuppressWarnings("unchecked")
	@Then("^The selected 'Proofs Types' \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" options are is displayed in the Transmittal Form$")
	public void the_selected_Proofs_Types_options_are_is_displayed_in_the_Transmittal_Form(String checkboxes, String option, String value) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		List<String> types = new Gson().fromJson(checkboxes, List.class);
		String selectedValues="";
		for (String proofType : types) { 	  
			if(selectedValues.isEmpty())
			{
				selectedValues=proofType;
			}
			else{
			selectedValues=selectedValues+", "+proofType;
			}
		}
		selectedValues=selectedValues+", "+option+", "+value;
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String pdfValues= transmittalpdf.getTransmittalPdfValue("Proofs:");
		ArrayList<String> pdf=transmittalform.splitAndSortString(pdfValues);
		ArrayList<String> form=transmittalform.splitAndSortString(selectedValues);
		Assert.assertEquals(form, pdf);
	}
	
	@Then("^The custom value \"([^\"]*)\" is displayed in the Transmittal Form pdf \"([^\"]*)\"$")
	public void the_custom_value_is_displayed_in_the_Transmittal_Form_pdf(String value, String label) throws Throwable {
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String pdfValue=transmittalpdf.getTransmittalPdfValue(label);
		Assert.assertEquals(pdfValue,value);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The selected 'Herewith' \"([^\"]*)\" options are is displayed in the Transmittal Form$")
	public void the_selected_Herewith_options_are_is_displayed_in_the_Transmittal_Form(String options) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		List<String> types = new Gson().fromJson(options, List.class);
		String selectedValues="";
		for (String herewith : types) { 	  
			if(selectedValues.isEmpty())
			{
				selectedValues=herewith;
			}
			else{
			selectedValues=selectedValues+", "+herewith;
			}
		}
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String pdfValues= transmittalpdf.getTransmittalPdfValue("Herewith:");
		ArrayList<String> pdf=transmittalform.splitAndSortString(pdfValues);
		ArrayList<String> form=transmittalform.splitAndSortString(selectedValues);
		Assert.assertEquals(form, pdf);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^The selected 'Herewith' \"([^\"]*)\",\"([^\"]*)\" options are is displayed in the Transmittal Form$")
	public void the_selected_Herewith_options_are_is_displayed_in_the_Transmittal_Form(String checkboxes, String value) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		List<String> types = new Gson().fromJson(checkboxes, List.class);
		String selectedValues="";
		for (String herewith : types) { 
			if(selectedValues.isEmpty())
			{
				selectedValues=herewith;
			}
			else{
			selectedValues=selectedValues+", "+herewith;
			}
		}
		selectedValues=selectedValues+", "+value;
		TransmittalPdf transmittalpdf=new TransmittalPdf();
		String pdfValues= transmittalpdf.getTransmittalPdfValue("Herewith:");
		ArrayList<String> pdf=transmittalform.splitAndSortString(pdfValues);
		ArrayList<String> form=transmittalform.splitAndSortString(selectedValues);
		Assert.assertEquals(form, pdf);
	}
	
	@When("^user Cancels the Transmittal Form$")
	public void user_Cancels_the_Transmittal_Form() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.getcloseButton().click();
		TransmittalForm form=new TransmittalForm();
		form.clickFormpopUp("No");
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		myprojects.waitForResultsLoadingComplete();
	}

	@Then("^'Transmittal Form' window closes$")
	public void transmittal_Form_window_closes() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	   AssertUtils.assertNotDisplayed(myprojects.getDialog().getDialogTitleElement());
	}

	@SuppressWarnings("unchecked")
	@Then("^the 'Transmittal Form' of \"([^\"]*)\" and \"([^\"]*)\" is not listed in the transmittal history$")
	public void the_Transmittal_Form_of_and_is_not_listed_in_the_transmittal_history(String type, String fields) throws Throwable {
	   	MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalHistoryDialog historyDialog = myprojects.getTransmittalHistoryDialog();
		historyDialog.openTransmittalHistory(0);
		historyDialog.waitForPresent();
		historyDialog.switchToDialog();
		Row row=historyDialog.getFirstHistoryTypeRow(type);
		String mechs=row.getColumnText("Mechs");
		String proofs=row.getColumnText("Proofs");
		List<String> types = new Gson().fromJson(fields, List.class);
		String selectedValues="";
		for (String values : types) { 
			if(selectedValues.isEmpty())
			{
				selectedValues=values;
			}
			else{
			selectedValues=selectedValues+", "+values;
			}
		}
		Assert.assertNotEquals(selectedValues, mechs+", "+proofs);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^the 'Transmittal Form' of \"([^\"]*)\" and \"([^\"]*)\" are listed in the transmittal history$")
	public void the_Transmittal_Form_of_and_are_listed_in_the_transmittal_history(String type, String fields) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String window=(String) TestBaseProvider.getTestBase().getContext().getProperty("transmittal.window");
		driver.switchTo().window(window);
		TransmittalHistoryDialog historyDialog = myprojects.getTransmittalHistoryDialog();
		myprojects.waitForResultsLoadingComplete();
		historyDialog.switchToHistoryDialog();
		Row row=historyDialog.getFirstHistoryTypeRow(type);
		String mechs=row.getColumnText("Mechs");
		String proofs=row.getColumnText("Proofs");
		List<String> types = new Gson().fromJson(fields, List.class);
		String selectedValues="";
		for (String values : types) { 
			if(selectedValues.isEmpty())
			{
				selectedValues=values;
			}
			else{
			selectedValues=selectedValues+", "+values;
			}
		}
		selectedValues=selectedValues.replaceAll("\\s+","");
		String options=mechs+","+proofs;
		options=options.replaceAll("\\s+","");
		Assert.assertEquals(selectedValues,options );
	}
	
	@SuppressWarnings("unchecked")
	@Then("^grouped \"([^\"]*)\" are displayed inside a container border$")
	public void grouped_are_displayed_inside_a_container_border(String fields) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		transmittalform.switchToTransmittalForm();
		List<String> types = new Gson().fromJson(fields, List.class);
		for (String values : types) { 
			AssertUtils.assertDisplayed(transmittalform.getInformationbox());
			Assert.assertTrue(transmittalform.getInformationbox().getText().contains(values));
		}
	}
	
	@SuppressWarnings("unchecked")
	@Then("^\"([^\"]*)\" is displayed$")
	public void is_displayed(String fields) throws Throwable {
		TransmittalForm transmittalform=new TransmittalForm();
		MyProjectsPage myprojects=new MyProjectsPage();
		transmittalform.switchToTransmittalForm();
		List<String> types = new Gson().fromJson(fields, List.class);
		for (String values : types) { 
			AssertUtils.assertDisplayed(transmittalform.getLabel(values));
		}
	}
	
	@Then("^The selected 'Mech' option in \"([^\"]*)\" is displayed in the Transmittal Form$")
	public void the_selected_Mech_option_in_is_displayed_in_the_Transmittal_Form(String option) throws Throwable {
		TransmittalPdf transmittalpdf=new TransmittalPdf();
			String pdfValues= transmittalpdf.getTransmittalPdfValue("Mech:");
			pdfValues=pdfValues.replaceAll("\\s+","");
			option=option.replaceAll("\\s+","");
			Assert.assertEquals(option, pdfValues);
		}
	
	@Then("^a text field is displayed for user to type for 'Mech'$")
	public void a_text_field_is_displayed_for_user_to_type_for_Mech() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittals.getFormMechOtherInput());
	}
	
	@Then("^a text field is displayed for user to type$")
	public void a_text_field_is_displayed_for_user_to_type() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittals.getProofTypeOtherInput());
	}
	
	@Then("^a 'Herewith' text field is displayed for user to type$")
	public void a_Herewith_text_field_is_displayed_for_user_to_type() throws Throwable {
		TransmittalForm transmittals=new TransmittalForm();
		AssertUtils.assertDisplayed(transmittals.getHerewithOtherInput());
	}
	@Then("^'New Transmittal Form' view is displayed$")
	public void new_Transmittal_Form_view_is_displayed() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		TransmittalForm transmittals=new TransmittalForm();
		
		AssertUtils.assertDisplayed(myprojects.getDialog().getDialogTitleElement());
		
	}
	
	
	
	@When("^User generates a \"([^\"]*)\" Transmittal Form for a project, using any \"([^\"]*)\" and \"([^\"]*)\" value$")
	public void user_generates_a_Transmittal_Form_for_a_project_using_any_and_value(String type, String Mechs, String proofs) throws Throwable {
	   
		TransmittalForm tf=new TransmittalForm();
		
		   tf.switchToTransmittalForm();
		   WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		   tf.getTransmittalFormButtons(Mechs).click();
		   tf.getTransmittalFormButtons(proofs).click();
		   //driver.findElement(By.xpath("//*[@id='radio1_1']")).click();
		   Thread.sleep(2000);
		   //driver.findElement(By.xpath("//*[@id='chEpson']")).click();
		   tf.getSubmitButton().click();
		   Thread.sleep(4000);
		
	}

	@Then("^A header is displayed as \"([^\"]*)\" followed by the selected \"([^\"]*)\" value$")
	public void a_header_is_displayed_as_followed_by_the_selected_value(String name, String Mechs) throws Throwable {
	    
		 TransmittalPdf transmittalpdf=new TransmittalPdf();
		 transmittalpdf.getValueElement(name);
		 AssertUtils.assertDisplayed(transmittalpdf.getValueElement(name));
		// AssertUtils.assertDisplayed(transmittalpdf.getValueElement(Mech:));
	}
	
}
