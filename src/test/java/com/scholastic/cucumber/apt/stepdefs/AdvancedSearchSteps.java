package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.AdvancedSearchPage;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AdvancedSearchSteps {

	@Given("^the user is on the Advanced Search page$")
	public void the_user_is_on_the_Advanced_Search_page() throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.clickAdvancedSearchLink();
	    AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
	    advancedSearch.switchToAdvSearchDialog();
	    
	}

	@When("^the user conducts a \"([^\"]*)\" advanced search of \"([^\"]*)\"$")
	public void the_user_conducts_a_advanced_search_of(String label, String searchText) throws Throwable {
		 AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		 advancedSearch.search(label, searchText);
		 advancedSearch.clickAdvSearchBtn();
	}

	@Then("^the returned results should contain all of the search values \"([^\"]*)\" in the \"([^\"]*)\" column$")
	public void the_returned_results_should_contain_all_of_the_search_values_in_the_column(String searchText, String column) throws Throwable {
		  MyProjectsPage myprojects=new MyProjectsPage();
		  AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    if(column.equals("Designer"))
		    {
		    	ArrayList<String> designers=itemgrid.getdesignerValues("Cover Designer");
	    		ArrayList<String> interiorDesigner=itemgrid.getdesignerValues("Interior Designer");
	    		designers.addAll(interiorDesigner);
		    	for(String designer:designers)
		    	{
		    		Assert.assertTrue(designer.toUpperCase().contains(searchText.toUpperCase().trim()));
		    	}
		    }
		    
		    else if(column.equals("Editor"))
		    {
		    	for(String text:advancedSearch.getEditorValue("Editor"))
		    	{
		    		Assert.assertTrue(text.toUpperCase().contains(searchText.toUpperCase().trim()));
		    	}
		    }
		    
		    else
		    { 
		    ArrayList<String> completeColumnText=itemgrid.allColumnsText(column);
		    String[] searchkeys=searchText.split(",");
		  		    for(String rowText:completeColumnText)
		    {
		    	for (String searchValue:searchkeys) {
					Assert.assertTrue(rowText.toUpperCase().contains(searchValue.toUpperCase().trim()));
		    	}
		  	}
		    }
	}

	@When("^the user conducts a \"([^\"]*)\" advanced search for fifty isbns \"([^\"]*)\"$")
	public void the_user_conducts_a_advanced_search_for_fifty_isbns(String label, String searchText) throws Throwable {
		 AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		 String searchValues=TestBaseProvider.getTestBase().getString("advancedSearch.isbns."+searchText);
		 advancedSearch.search(label, searchValues);
		 advancedSearch.clickAdvSearchBtn();
		 TestBaseProvider.getTestBase().getContext().setProperty("search.50isbns", searchValues);
	}

	@Then("^all the returned titles should all an ISBN from \"([^\"]*)\"$")
	public void all_the_returned_titles_should_all_an_ISBN_from(String isbn13Values) throws Throwable {
		String isbns=TestBaseProvider.getTestBase().getString("advancedSearch.isbns."+isbn13Values);
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid=myprojects.getProjectGrid();
		ArrayList<String> projectUIisbns=itemgrid.allColumnsText("ISBN");
		ArrayList<String> isbn13=new ArrayList<>();
		String[] isbn=isbns.split(" ");
		for (String string : isbn) {
			isbn13.add(string);
		}
		Collections.sort(projectUIisbns);
		Collections.sort(isbn13);
		Assert.assertEquals(projectUIisbns, isbn13);
	}
	
	@Then("^the application should present an error message$")
	public void the_application_should_present_an_error_message() throws Throwable {
		 AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		 Assert.assertTrue(advancedSearch.getErrorMessages().size()>0);
	}
	
	@Then("^results displayed on the Search Results for \"([^\"]*)\" should match the results from the query results of \"([^\"]*)\" with \"([^\"]*)\"$")
	public void results_displayed_on_the_Search_Results_for_should_match_the_results_from_the_query_results_of_with(String column, String query, String dbName) throws Throwable {
	    	 MyProjectsPage myprojects=new MyProjectsPage();
		  AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    DBConnection dbcon=new DBConnection();
		    ResultSet rs= dbcon.dbConnection(query);
		    ArrayList<String> dbvalues=new ArrayList<>();
		    ArrayList<String> completeColumnText=new ArrayList<>();
		    if(dbName.equals("DESIGNER"))
		    {
		    	ArrayList<String> designers=itemgrid.getdesignerValues("Cover Designer");
	    		ArrayList<String> interiorDesigner=itemgrid.getdesignerValues("Interior Designer");
	    		designers.addAll(interiorDesigner);
	    		while(rs.next())
	    		{
	    			dbvalues.add(rs.getString("COVER_DESIGNER_NAME"));
	    			dbvalues.add(rs.getString("INTERIOR_DESIGNER_NAME"));
	    		}
	    		dbvalues.removeAll(Collections.singleton(null));
	    		designers.removeAll(Collections.singleton(""));
	    		Assert.assertEquals(designers, dbvalues);
		    }
		    else
		    {
		    	if(dbName.equals("EDITOR"))
		    	{
		    		completeColumnText=advancedSearch.getEditorValue(column);
		    	}
		    	else
		    	{
		    	completeColumnText=itemgrid.allColumnsText(column);
		    	}
		    	while(rs.next())
	    		{
		    		dbvalues.add(rs.getString(dbName));
	    		}
		    	Assert.assertEquals(completeColumnText, dbvalues);
		    }  
	}
	
	@SuppressWarnings("static-access")
	@Then("^'Search Results' in \"([^\"]*)\" view displays matching results for any of the following database fields \"([^\"]*)\"$")
	public void search_Results_in_view_displays_matching_results_for_any_of_the_following_database_fields(String tableLabels, String databaseFields) throws Throwable {
		MyProjectsPage myprojects = new MyProjectsPage();
		  AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		myprojects.waitForResultsLoadingComplete();
		ItemGrid itemgrid = myprojects.getProjectGrid();
		String fromDate = (String) TestBaseProvider.getTestBase().getContext().getProperty("from.date");
		String toDate = (String) TestBaseProvider.getTestBase().getContext().getProperty("to.date");
		ArrayList<String> labelsText=new ArrayList<>();
		ArrayList<String> databaseResults=new ArrayList<>();
		List<String> items = new Gson().fromJson(tableLabels, List.class);
		List<String> dbitems = new Gson().fromJson(databaseFields, List.class);
		for (String item : items) {
			if(item.equals("In Store"))
			{
				ArrayList<String>dates=advancedSearch.getEditorValue("In Store");
				for(String date:dates)
				{
					labelsText.add(myprojects.setFormatFromYY(date));
				}
			}
			else{
				labelsText.addAll(itemgrid.allColumnsText(item));
			}
		}
		DBConnection dbcon=new DBConnection();
		String query="";
		if(tableLabels.contains("Target"))
		{
			query="select * from APT_PROJECTS_VIEW " + "where "
					+ "(COVER_TARGET_DUE_MFG >=  '" + fromDate
					+ "' and COVER_TARGET_DUE_MFG < ='" + toDate + "' ) " + 
					"OR (JACKET_TARGET_DUE_MFG >=  '" + fromDate + "' and"
							+ " JACKET_TARGET_DUE_MFG < = '" + toDate
					+ "' )" + "OR (INTERIOR_TARGET_DUE_MFG >=  '" + fromDate + "' and INTERIOR_TARGET_DUE_MFG < = '" + toDate + "' )";
		}
		else
		{
			for (String dbItem : dbitems) {
				query="select * from APT_PROJECTS_VIEW " + "where "+ dbItem +" >='" + fromDate+"' and "+dbItem+" <= '" + toDate + "'";
			}
		}
		ResultSet rs=dbcon.dbConnection(query);
		
		while (rs.next()) {
			for (String dbItem : dbitems) {
					String date = rs.getString(dbItem);
					databaseResults.add(myprojects.changeDateFormat(date));
				} 
			}
		
		Collections.sort(labelsText);
		Collections.sort(databaseResults);
		System.out.println("advanced searchUI by firstusedate:"+labelsText);
		System.out.println("advanced searchDB by firstusedate:"+databaseResults);
		Assert.assertEquals(labelsText, databaseResults);
		
		}
	@Then("^the returned results should contain at least one of the search values in the \"([^\"]*)\" column$")
	public void the_returned_results_should_contain_at_least_one_of_the_search_values_in_the_column(String arg1) throws Throwable {
	    
		  MyProjectsPage myprojects=new MyProjectsPage();
		  //AdvancedSearchPage advancedSearch=new AdvancedSearchPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    itemgrid.showAllColumns();
		    Thread.sleep(6000);
		    
		    WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		    driver.switchTo().defaultContent();
		    driver.switchTo().frame("display_frame");
		    
		//String[] Expected={"John Williams",""};
		String  S=  driver.findElement(By.xpath("//*[@id='tbmyProjects']/tbody/tr[2]/td[19]/div/input")).getAttribute("value");
		Assert.assertTrue(S.contains("John Williams"),"FAIL-Test didn't match");
		
		
		   /* List<WebElement> l=driver.findElements(By.xpath("//*[@id='opArtDirector']"));
		    for(int i=0;i<=l.size();i++)
			  {
				  System.out.println("@@@@@@@@@@@@@@"  +l.get(i).getAttribute("value"));
			  }*/
		    
		  /*  ArrayList<String> completeColumnText=itemgrid.allColumnsText("Art Director");
		  for(int i=0;i<=completeColumnText.size();i++)
		  {
			  System.out.println("@@@@@@@@@@@@@@"  +completeColumnText.get(i));
		  }*/
	}

	
}
