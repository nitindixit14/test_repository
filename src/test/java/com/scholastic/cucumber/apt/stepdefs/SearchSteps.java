/**
 * 
 */
package com.scholastic.cucumber.apt.stepdefs;

import static com.scholastic.torque.common.StringMatchers.containsIgnoreCase;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebElement;

import com.scholastic.cucumber.apt.pageobjects.AdvanceSearchDialog;
import com.scholastic.cucumber.apt.pageobjects.AdvanceSearchDialog.SearchBy;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.cucumber.apt.pageobjects.OTBPMSigninPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author chirag.jayswal
 *
 */
public class SearchSteps {
	@Given("^User is logged into OTBPM$")
	public void user_is_on_OTMM_sing_in_page() {
		OTBPMSigninPage signinPage = new OTBPMSigninPage();
		signinPage.launchPage();
		signinPage.signIn(TestBaseProvider.getTestBase().getString("userID"),
				TestBaseProvider.getTestBase().getString("password"));
	}

	// this is a test.
	@When("^User searches for \"([^\"]*)\"$")
	public void user_searches_for(String searchText) throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.searchFor(searchText);
	}

	@When("^User searches for ISBN$")
	public void user_searches_for_ISBN() throws Throwable {
		String ISBN = getContext().getString("search.isbnsearch.isbn");
		user_searches_for(ISBN);
	}

	@Then("^There should be only one result with ISBN in ISBN column$")
	public void there_should_be_only_one_result_with_ISBN_in_ISBN_column() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		ItemGrid results = projectsPage.getProjectGrid();

		// Check for # of result is 1
		List<Row> rows = results.getRows();
		assertEquals(rows.size(), 1, "ISBN search result");
		Row row = results.getRows().get(0);

		// ensure result is for the given ISBN
		String ISBN = getContext().getString("search.isbnsearch.isbn");

		assertThat("Project with ISBN", row.getColumnText("ISBN"), containsIgnoreCase(ISBN));

	}

	@When("^User searches using \"([^\"]*)\"$")
	public void user_searches_using(String searchTextKey) throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		String searchText = getContext().getString("search.opensearch." + searchTextKey.toLowerCase().trim());
		projectsPage.searchFor(searchText);
	}

	@Then("^each of the results returned should contain the same \"([^\"]*)\" in any of the column$")
	public void each_of_the_results_returned_should_contain_the_same_in_any_of_the_column(String textToMatchKey)
			throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		String textToMatch = getContext().getString("search.opensearch." + textToMatchKey.toLowerCase().trim());
		boolean navNextPage = false;
		do {
			if (navNextPage) {
				projectsPage.getNextButton().click();
				projectsPage.waitForResultsLoadingComplete();
			}
			
		 ItemGrid results = projectsPage.getProjectGrid();

			// ensure each result row contains search text
			for (Row row : results.getRows()) {
				String rowText = row.getText();
				// match case insensitively row content
				assertThat(rowText.toUpperCase(), Matchers.containsString(textToMatch.toUpperCase()));
			}
			navNextPage = true;
		} while (projectsPage.getNextButton().isEnabled());

	}

	@Given("^User presented with serach results$")
	public void user_presented_with_serach_results() throws Throwable {
		List<String> result = getSearchResults();
		getContext().setProperty("search.results", result);
	}

	@When("^User searches for any text$")
	public void user_searches_for_any_text() throws Throwable {
		String searchText = getContext().getString("search.opensearch.bookname");
		user_searches_for(searchText);
	}

	@When("^User searches for same text again$")
	public void user_searches_for_again() throws Throwable {

		String searchText = getContext().getString("search.opensearch.bookname");
		user_searches_for(searchText);
	}

	@SuppressWarnings("unchecked")
	@Then("^User should presented with the same serach results$")
	public void user_should_presented_the_same_serach_results() throws Throwable {
		// get the new search result
		List<String> actual = getSearchResults();
		// get actual result for first time serach
		List<String> expected = (List<String>) getContext().getProperty("search.results");

		// make sure same number of results
		assertThat("There should be same number of results ", actual.size(), equalTo(expected.size()));
		// make sure both are same
		assertEquals(actual, expected);
	}

	@When("^user click the \"([^\"]*)\" column on the search results$")
	public void user_click_the_Title_column_on_the_search_results(String colName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}

	@Then("^The Results of all the pages combined for the \"([^\"]*)\" column should be sorted in ascending order$")
	public void the_Results_of_all_the_pages_combined_for_the_tab_column_should_be_sorted_in_ascending_order(
			String colName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		MyProjectsPage projectsPage = new MyProjectsPage();
		String cellID = projectsPage.getProjectGrid().getHeader(colName).getAttribute("__parent");
		List<WebElement> columnValues = projectsPage.getProjectGrid().getcolValues(cellID);
		assertTrue(projectsPage.getProjectGrid().isSorted(columnValues),
				"column" + colName + "is not sorted in ascending order");
	}

	@When("^User searches without serach text$")
	public void user_searches_with_empty_filter() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.searchUsing("");
	}

	@Then("^There should be warning message to provide serach filter$")
	public void there_should_be_warning_message_to_provide_serach_filter() throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		List<WebElement> errors = projectsPage.getErrorMessages();
		Thread.sleep(1000);
		assertThat(errors.size(), greaterThan(0));
		System.err.println("Messages: " + errors.get(0).getText());
	}

	@When("^User sort by \"([^\"]*)\"$")
	public void user_sort_by(String column) throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.getProjectGrid().getHeader(column).click();
	}

	private Configuration getContext() {
		return TestBaseProvider.getTestBase().getContext();
	}

	private List<String> getSearchResults() {
		MyProjectsPage projectsPage = new MyProjectsPage();
		List<String> list = new ArrayList<String>();

		boolean navNextPage = false;
		do {
			if (navNextPage) {
				projectsPage.getNextButton().click();
				projectsPage.waitForResultsLoadingComplete();
			}
			ItemGrid results = projectsPage.getProjectGrid();
			for (Row row : results.getRows()) {
				String rowText = row.getText();
				if (isNotBlank(rowText))
					list.add(rowText);
			}
			navNextPage = true;
		} while (projectsPage.getNextButton().isEnabled());

		return list;
	}

	@When("^User searches by \"([^\"]*)\" with all words in Advanced Search$")
	public void user_searches_for_designer_with_word_using_Advanced_Search(String searchByName) throws Throwable {
		MyProjectsPage projectsPage = new MyProjectsPage();
		projectsPage.waitForResultsLoadingComplete();
		AdvanceSearchDialog advanceSearchDialog = projectsPage.getAdvanceSearchDialog();
		projectsPage.waitForResultsLoadingComplete();
		WaitUtils.waitForDisplayed(projectsPage.getAdvancedSearchLink());
		projectsPage.clickElementByJS(projectsPage.getAdvancedSearchLink());
		advanceSearchDialog.waitForPresent();
		WaitUtils.waitForDisplayed(advanceSearchDialog.getWrappedElement());
		projectsPage.waitForResultsLoadingComplete();

		Thread.sleep(2000);
		advanceSearchDialog.switchToDialog();
		WaitUtils.waitForDisplayed(advanceSearchDialog.getSearchAdvancedAllthesewordsInput());
		String searchDataKey = "advancesearch." + searchByName.toLowerCase().replaceAll(" ", "") + ".searchtext";
		String textToSerach = getContext().getString(searchDataKey);
		System.err.println("textToSearch: " + textToSerach + " key: " + searchDataKey);
		advanceSearchDialog.getSearchAdvancedAllthesewordsInput().sendKeys(textToSerach);
		advanceSearchDialog.setChecked(SearchBy.All, false);
		advanceSearchDialog.setChecked(SearchBy.title(searchByName), true);
		advanceSearchDialog.getSearchAdvancedSearchButton().click();
		//Thread.sleep(5000);
		projectsPage.waitForResultsLoadingComplete();
//		Thread.sleep(5000);
	}

	@Then("^The result should have all projects having \"([^\"]*)\" in \"([^\"]*)\" column$")
	public void the_result_should_have_all_projects_having_in_column(String searchByName, String colHeader)
			throws Throwable {
		String colContent = getContext()
				.getString("advancesearch." + searchByName.toLowerCase().replaceAll(" ", "") + ".searchtext");

		MyProjectsPage projectsPage = new MyProjectsPage();
		boolean navNextPage = false;
		do {
			if (navNextPage) {
				projectsPage.getNextButton().click();
				projectsPage.waitForResultsLoadingComplete();
			}
			ItemGrid results = projectsPage.getProjectGrid();
			int column = results.getColumnIndex(colHeader);
			List<Row> rows = results.getRows();

			assertThat("Search results", rows.size(), Matchers.greaterThan(0));
			for (Row row : rows) {
				// ensure result has searched text in given column
				assertThat(row.getColumnText(column), containsIgnoreCase(colContent));
			}
			navNextPage = true;
		} while (projectsPage.getNextButton().isEnabled());
	}

}
