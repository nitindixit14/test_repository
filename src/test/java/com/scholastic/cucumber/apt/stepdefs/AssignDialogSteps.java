package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.openqa.selenium.support.ui.Select;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.AssignDialog;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AssignDialogSteps {

	@When("^user selects a \"([^\"]*)\" and go to Assign page$")
	public void user_selects_a_and_go_to_Assign_page(String searchText) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    AssignDialog assign=new AssignDialog();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.searchFor(searchText);
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    	WaitUtils.waitForEnabled(itemgrid.getCheckbox().get(1));
	    	myprojects.clickElementByJS(itemgrid.getCheckbox().get(1));
	    myprojects.clickAssignButton();
	    assign.switchToAssignDialog();
	}

	@Then("^\"([^\"]*)\" label and input field are displayed$")
	public void label_and_input_field_are_displayed(String labels) throws Throwable {
	    AssignDialog assign=new AssignDialog();
	    List<String> labelsList = new Gson().fromJson(labels, List.class);
	    for(String label:labelsList)
	    {
	    AssertUtils.assertDisplayed(assign.getAssignLabels(label));
	    AssertUtils.assertDisplayed(assign.getAssignInputs(label));
	    }
	}
	
	@When("^User selects a \"([^\"]*)\" for \"([^\"]*)\" from the Autosuggested list$")
	public void user_selects_a_for_from_the_Autosuggested_list(String value, String field) throws Throwable {
		 AssignDialog assign=new AssignDialog();
		assign.selectAssignAutoSuggest(field, value);
		assign.clickAssignSaveBtn();
	}

	@Then("^the selected value \"([^\"]*)\" is displayed in the \"([^\"]*)\" column for all the \"([^\"]*)\" projects in the table$")
	public void the_selected_value_is_displayed_in_the_column_for_all_the_projects_in_the_table(String value, String field, String projects) throws Throwable {
	   	MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.getProjectGrid().showAllColumns();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    for(int i=0;i<projectsNo;i++)
	    {
	    	String coltext=itemgrid.getRows().get(i).getColumnText(field);
	    	Assert.assertTrue(coltext.contains(value));
	    }
	}
	
	@When("^user can view the \"([^\"]*)\" and \"([^\"]*)\" in the table for \"([^\"]*)\" project$")
	public void user_can_view_the_and_in_the_table_for_project(String selectedFields, String remainingFields, String projectsNo) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
	    ArrayList<String> selectedFieldsvalues=itemgrid.getColumnTextForList(selectedFields,projectsNo);
	    ArrayList<String> remainingFieldsvalues= itemgrid.getColumnTextForList(remainingFields,projectsNo);
	    getContext().setProperty("selected.fields", selectedFieldsvalues);
	    getContext().setProperty("remaining.fields",remainingFieldsvalues);
	}
	
	private Configuration getContext() {
		return TestBaseProvider.getTestBase().getContext();
	}

	@When("^User selects \"([^\"]*)\" for \"([^\"]*)\" from the Autosuggested list$")
	public void user_selects_for_from_the_Autosuggested_list(String values, String selectedValuesList) throws Throwable {
		AssignDialog assign=new AssignDialog(); 
		List<String> selected = new Gson().fromJson(selectedValuesList, List.class);
		List<String> inputs = new Gson().fromJson(values, List.class);
		    for(int i=0;i<selected.size();i++)
		    {
				assign.selectAssignAutoSuggest(selected.get(i), inputs.get(i));
		    }
	}

	@When("^User Saves Assign selection$")
	public void user_Saves_Assign_selection() throws Throwable {
		 AssignDialog assign=new AssignDialog();
		 assign.clickAssignSaveBtn();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Then("^Assigned values \"([^\"]*)\" for \"([^\"]*)\" are stored in database for \"([^\"]*)\" project$")
	public void assigned_values_for_are_stored_in_database_for_project(String values, String dbselected, String projects) throws Throwable {
		   DBConnection dbcon=new DBConnection();
		   MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ItemGrid itemgrid=myprojects.getProjectGrid();
		    List<String> dboptions = new Gson().fromJson(dbselected, List.class);
		    List<String> selectedValue = new Gson().fromJson(values, List.class);
		    int projectsNo=Integer.parseInt(projects);
		    String dbvalue="";
		 for(int i=0;i<projectsNo;i++)
		    {
			 String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
		    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
		    	while(rs.next())
		    	{
		    	for(int j=0;j<dboptions.size();j++)
		    	{
		    		dbvalue=rs.getString(dboptions.get(j));
		    		if(null==dbvalue)
		    		{
		    			dbvalue="";
		    		}
		    		Assert.assertEquals(dbvalue, selectedValue.get(j));
		    	}
		    	}
		    }
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Then("^Initial project values remains unchanged in database \"([^\"]*)\" for \"([^\"]*)\" projects$")
	public void initial_project_values_remains_unchanged_in_database_for_projects(String dbremaining, String projects) throws Throwable {
	   ArrayList<String> remainingValues= (ArrayList<String>) getContext().getProperty("remaining.fields");
	    List<String> dboptions = new Gson().fromJson(dbremaining, List.class);
	    DBConnection dbcon=new DBConnection();
		   MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    ArrayList<String> dbvalues=new ArrayList<>();
	    for(int i=0;i<projectsNo;i++)
	    {
		 String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
	    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
	    	while(rs.next())
	    	{
	    	for(int j=0;j<dboptions.size();j++)
	    	{
	    		String dbvalue=rs.getString(dboptions.get(j));
	    		if(null==dbvalue)
	    		{
	    			dbvalue="";
	    		}
	    		dbvalues.add(dbvalue.trim());
	    	}
	    	}
	    }
	    Assert.assertEquals(dbvalues, remainingValues);
	}

	@SuppressWarnings("static-access")
	@Then("^Values displayed in Project List for \"([^\"]*)\" matches database values$")
	public void values_displayed_in_Project_List_for_matches_database_values(String projects) throws Throwable {
		DBConnection dbcon=new DBConnection();
		   MyProjectsPage myprojects=new MyProjectsPage();
		   myprojects.waitForResultsLoadingComplete();
		   ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    ArrayList<String> dbvalues=new ArrayList<>();
	    ArrayList<String> col_text=new ArrayList<>();
	    for(int i=0;i<projectsNo;i++)
	    {
		 String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
	    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
	    	while(rs.next())
	    	{
	    		dbvalues.add(rs.getString("ART_DIRECTOR_NAME").trim());
	    		dbvalues.add(rs.getString("COVER_DESIGNER_NAME").trim());
	    		dbvalues.add(rs.getString("INTERIOR_DESIGNER_NAME").trim());

	    	}
	    	Row row=itemgrid.getRows().get(i);
	    	col_text.add(row.getColumnText("Art Director").trim());
	    	col_text.add(row.getColumnText("Cover Designer").trim());
	    	col_text.add(row.getColumnText("Interior Designer").trim());
	    }
	    Collections.replaceAll(dbvalues, null, "");	    
	    Assert.assertEquals(dbvalues, col_text);
	}
	@When("^User unassigns himself as 'Art Director' and saves unassign selection$")
	public void user_unassigns_himself_as_Art_Director_and_saves_unassign_selection() throws Throwable {
		
		MyProjectsPage myprojects=new MyProjectsPage();
		AssignDialog assign=new AssignDialog();
		//assign.switchToAssignDialog();
		Thread.sleep(2000);
		assign.clickRemoveADBtn();
		assign.clickAssignSaveBtn();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		
	}
	@When("^User is in 'Assign' view$")
	public void user_is_in_Assign_view() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		myprojects.waitForResultsLoadingComplete();
		//driver.switchTo().frame("display_frame");
		WebElement W=driver.findElement(By.xpath("//*[@id='fb_title']"));
		AssertUtils.assertDisplayed(W);
		Assert.assertEquals(W.getText(),"Assign");
		
	}

	@Then("^Select fields for 'Art Director','Cover Designer' and'Interior Designer' are aligned$")
	public void select_fields_for_Art_Director_Cover_Designer_and_Interior_Designer_are_aligned() throws Throwable {
		AssignDialog assign=new AssignDialog();
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		
		Thread.sleep(4000);
		int AD_loc=assign.getAssignInputs("Art Director").getLocation().x;
		int CD_loc=assign.getAssignInputs("Cover Designer").getLocation().x;
		int ID_loc=assign.getAssignInputs("Interior Designer").getLocation().x;
		
	   Assert.assertEquals(AD_loc,CD_loc);
	    Assert.assertEquals(CD_loc,ID_loc);
	    
	}
	

	
	@Then("^A list of 'Art Directors' containing the typed \"([^\"]*)\" is displayed$")
	public void a_list_of_Art_Directors_containing_the_typed_is_displayed(String CHARACTER_SEQUENCE) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		//List<WebElement> l=assign.getassignAutoSuggestElements();
		
		List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		
       for(WebElement names:l)
		{
			System.out.println(names.getText());
			String Artdirectors=names.getText();
			Assert.assertTrue(Artdirectors.toLowerCase().trim().contains(CHARACTER_SEQUENCE), "FAIL-required suggestion doesn't contains");
		}
		
	 }
	
  @Then("^The \"([^\"]*)\" list is updated each time user types an  additional character$")
	public void the_list_is_updated_each_time_user_types_an_additional_character(String input,DataTable table) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		
		System.out.println(table);
		List<List<String>> data= table.raw();
		System.out.println(data.get(1).get(0));
		
		System.out.println(data.get(3).get(0));
		
		
	
		AssignDialog assign=new AssignDialog();
		assign.getAssignInputs(input).clear();
		assign.getAssignInputs(input).sendKeys(data.get(1).get(0));
		
		Thread.sleep(5000);
		List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		//List<WebElement> l=	assign.getassignAutoSuggestElements();
		Thread.sleep(5000);
		int x=l.size();
		
		
		assign.getAssignInputs(input).clear();
		assign.getAssignInputs(input).sendKeys(data.get(3).get(0));
		
		Thread.sleep(5000);
		List<WebElement> l2=driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		//List<WebElement> l2=assign.getassignAutoSuggestElements();
		Thread.sleep(5000);
		int y=l2.size();
		
		if(x==y)
		{
			
			Assert.fail();
		}
	}
	
  	@When("^user types \"([^\"]*)\" in \"([^\"]*)\"$")
	public void user_types_in(String CHARACTER_SEQUENCE, String SELECT_FIELD) throws Throwable {
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		assign.getAssignInputs(SELECT_FIELD).sendKeys(CHARACTER_SEQUENCE);
		Thread.sleep(3000);
		
		
	}

	@Then("^No autosuggest list is displayed$")
	public void no_autosuggest_list_is_displayed() throws Throwable {
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		Assert.assertFalse(assign.getAssignAutoSuggest().isDisplayed(),"FAIL-");
		
		
		//Assert.assertFalse(driver.findElement(By.id("__AutoSuggest_suggestionNode")).isDisplayed(), "FAIL-");
	   
	}

	
	
	@Then("^A list of 'Designers' containing the typed \"([^\"]*)\" is displayed$")
	public void a_list_of_Designers_containing_the_typed_is_displayed(String CHARACTER_SEQUENCE) throws Throwable {
		
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		AssignDialog assign=new AssignDialog();
		//List<WebElement> l=	assign.getassignAutoSuggestElements();
		List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div"));
		for(WebElement names:l)
		{
			System.out.println(names.getText());
			
			String designers=names.getText();
			Assert.assertTrue(designers.toLowerCase().trim().contains(CHARACTER_SEQUENCE), "FAIL-required suggestion doesn't contains");
		}
		
	 }
		

	

@When("^user types \"([^\"]*)\" in \"([^\"]*)\" field$")
public void user_types_in_field(String CHARACTER_SEQUENCE, String input) throws Throwable {
	WebDriver driver=TestBaseProvider.getTestBase().getDriver();
	AssignDialog assign=new AssignDialog();
	assign.getAssignInputs(input).clear();
	
	assign.getAssignInputs(input).sendKeys(CHARACTER_SEQUENCE);
	
	/*driver.findElement(By.xpath("//*[@id='inpInteriorDesigner']")).clear();
	driver.findElement(By.xpath("//*[@id='inpInteriorDesigner']")).sendKeys(CHARACTER_SEQUENCE);*/
	Thread.sleep(6000);
	
}

	@Then("^A list of 'InteriorDesigners' containing the typed \"([^\"]*)\" is displayed$")
	public void a_list_of_InteriorDesigners_containing_the_typed_is_displayed(String CHARACTER_SEQUENCE) throws Throwable {
	 
       WebDriver driver=TestBaseProvider.getTestBase().getDriver();
       AssignDialog assign=new AssignDialog();
       List<WebElement> l=	driver.findElements(By.xpath("//*[@id='__AutoSuggest_suggestionNode']/div")); 
	
		for(WebElement names:l)
		{
			System.out.println(names.getText());
			String designers=names.getText();
			Assert.assertTrue(designers.toLowerCase().trim().contains(CHARACTER_SEQUENCE), "FAIL-required suggestion doesn't contains");
		}
		
	}
	
	
}

	
	
	

