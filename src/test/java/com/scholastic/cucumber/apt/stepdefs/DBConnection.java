package com.scholastic.cucumber.apt.stepdefs;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;

import com.scholastic.cucumber.apt.pageobjects.EpicKeyInfoPage;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.torque.common.TestBaseProvider;

public class DBConnection {

	public static ResultSet dbConnection(String sqlQ) throws SQLException, ClassNotFoundException {
		Class.forName("net.sourceforge.jtds.jdbc.Driver");
		System.out.println("class name is correct");
//		 String connectionUrl=
//		 "jdbc:jtds:sqlserver://10.49.71.125:1433;DatabaseName=OTBPM_APP_DB;user=QAAutomation_Readonly;password=Et%4RDQ?Qh";
		String connectionUrl = "jdbc:jtds:sqlserver://10.49.72.116:53500;DatabaseName=OTBPM_APP_DB;user=QAAutomation_Readonly;password=Y8SuAnZV9h";
		//OTMMSTGSQLLSN:53500
		Connection conn = DriverManager.getConnection(connectionUrl);
		System.out.println("connected");
		Statement sta = conn.createStatement();
		ResultSet rs = sta.executeQuery(sqlQ);
		return rs;
	}

	public ResultSet readDataFromDB(String corpIdValue) throws ClassNotFoundException, SQLException, ParseException {
		ArrayList<String> arr = new ArrayList<String>();
		Class.forName("net.sourceforge.jtds.jdbc.Driver");
		String connectionUrl = "jdbc:jtds:sqlserver://10.49.72.116:53500;DatabaseName=OTBPM_APP_DB;user=QAAutomation_Readonly;password=Y8SuAnZV9h";
		// String connectionUrl=
		// "jdbc:jtds:sqlserver://ECMSDEVDBLSN:1433;DatabaseName=OTBPM_APP_DB;user=QAAutomation_Readonly;password=Et%4RDQ?Qh";
		Connection conn = DriverManager.getConnection(connectionUrl);
		System.out.println("connected");
		Statement sta = conn.createStatement();
		System.out.println("query executed");
		System.out.println(corpIdValue);
		String sqlQuery3 = "SELECT * FROM " + "dbo.PRODUCT_CUSTOM_INFO,dbo.PRODUCT_V3_INFO,dbo.PLANNING_INFO "
				+ "WHERE PRODUCT_CUSTOM_INFO.CORP_ID_VALUE=PRODUCT_V3_INFO.CORP_ID_VALUE "
				+ "AND PRODUCT_V3_INFO.CORP_ID_VALUE=PLANNING_INFO.CORP_ID_VALUE AND "
				+ "PRODUCT_V3_INFO.CORP_ID_VALUE='" + corpIdValue + "'";
		ResultSet rs1 = sta.executeQuery(sqlQuery3);
       return rs1;
	}
}

// while (rs1.next()) {
// arr.add(rs1.getString("TITLE_NAME"));
// arr.add(rs1.getString("AUTHOR"));
// arr.add(rs1.getString("ILLUSTRATOR"));
// arr.add(rs1.getString("ISBN"));
// arr.add(rs1.getString("ISBN_13"));
// arr.add(rs1.getString("UPC"));
// arr.add(rs1.getString("PRODUCT_TYPE"));
// arr.add(myprrojects.changeDateFormat(rs1.getString("PUBLICATION_DATE")));
// arr.add(rs1.getString("TRIM_SIZE"));
// arr.add(rs1.getString("EDITOR"));
//// arr.add(rs1.getString("MANAGING_EDITOR"));
// arr.add(rs1.getString("NO_OF_PAGES"));
// arr.add(myprrojects.changeDateFormat(rs1.getString("FIRST_USE_DATE")));
// arr.add(rs1.getString("RECORD_NO"));
// arr.add(myprrojects.changeDateFormat(rs1.getString("TRADE_IN_STORE_DATE")));
// arr.add(rs1.getString("TRADE_PRICE"));
// arr.add(rs1.getString("CANADAR_PRICE"));
// arr.add(rs1.getString("BF_PRICE"));
// arr.add(rs1.getString("COVER_COMMENTS"));
// arr.add(rs1.getString("JACKET_COMMENTS"));
// arr.add(rs1.getString("BOOK_INTERIOR_TEXT_COMMENTS"));
// arr.add(rs1.getString("INSERT_COMMENTS"));
// arr.add(rs1.getString("BINDING"));
// arr.add(rs1.getString("COVER_COLOR_COUNT"));
// arr.add(rs1.getString("COVER_COLOR"));
// arr.add(rs1.getString("JACKET_COLOR"));
// arr.add(rs1.getString("BOOK_INTERIOR_COLOR_COUNT"));
// arr.add(rs1.getString("BOOK_INTERIOR_COLOR"));
// arr.add(rs1.getString("PRODUCTION_SCHEDULE_CODE"));
// }
// System.out.println(arr);
