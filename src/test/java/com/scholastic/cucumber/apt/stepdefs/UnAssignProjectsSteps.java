package com.scholastic.cucumber.apt.stepdefs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.pageobjects.AssignDialog;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid;
import com.scholastic.cucumber.apt.pageobjects.MyProjectsPage;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UnAssignProjectsSteps {

	@When("^user selects \"([^\"]*)\" total projects and goto assign page$")
	public void user_selects_total_projects_and_goto_assign_page(String projects) throws Throwable {
	    MyProjectsPage myprojects=new MyProjectsPage();
	    AssignDialog assign=new AssignDialog();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    for(int i=1;i<=projectsNo;i++)
	    {
//	    	Thread.sleep(1000);
	    	WaitUtils.waitForEnabled(itemgrid.getCheckbox().get(i));
	    	myprojects.clickElementByJS(itemgrid.getCheckbox().get(i));
	    }
	    myprojects.clickAssignButton();
	    assign.switchToAssignDialog();
	}

	@SuppressWarnings("unchecked")
	@When("^selected projects has assigned Art Director, Cover Designer and Interior Designer \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void selected_projects_has_assigned_Art_Director_Cover_Designer_and_Interior_Designer_with_value(String roles, String values) throws Throwable {
	    AssignDialog assign=new AssignDialog();
	    List<String> role = new Gson().fromJson(roles, List.class);
	    List<String> inputs = new Gson().fromJson(values, List.class);
	    for(int i=0;i<role.size();i++)
	    {
	    assign.getAssignInputs(role.get(i)).sendKeys(inputs.get(i));
	    }
	    assign.getAssignSaveButton().click();
	}

	@SuppressWarnings("unchecked")
	@When("^User selects remove icon for \"([^\"]*)\"$")
	public void user_selects_remove_icon_for(String roles) throws Throwable {	
	    AssignDialog assign=new AssignDialog();
	    List<String> role = new Gson().fromJson(roles, List.class);
	    for(String item:role)
	    {
	    assign.getAssignRemoveButtons(item).click();
	    }
	    assign.getAssignSaveButton().click();
	}

	@Then("^The \"([^\"]*)\" value for \"([^\"]*)\" is displayed empty for all the projects on the list$")
	public void the_value_for_is_displayed_empty_for_all_the_projects_on_the_list(String roles, String projects) throws Throwable {
			MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		  myprojects.getProjectGrid().showAllColumns();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    List<String> items = new Gson().fromJson(roles, List.class);
		    int projectsNo=Integer.parseInt(projects);
		    for(int i=0;i<projectsNo;i++)
		    {
		    	for(String role:items)
		    	{
		    	Assert.assertTrue(itemgrid.getRows().get(i).getColumnText(role).isEmpty());
		    	}
		    }
	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Then("^Then Unassigned empty values \"([^\"]*)\" for projects \"([^\"]*)\" are stored in database for project$")
	public void then_Unassigned_empty_values_for_projects_are_stored_in_database_for_project(String dboptions, String projects) throws Throwable {
	   	MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    Thread.sleep(3000);
	    DBConnection dbcon=new DBConnection();
	    List<String> items = new Gson().fromJson(dboptions, List.class);
	    for(int i=0;i<projectsNo;i++)
	    {
	    	String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
	    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
	    	while(rs.next())
	    	{
	    		for(String dboption:items)
	    		{
	    		Assert.assertTrue(null==rs.getString(dboption)|| ""==rs.getString(dboption));
	    		}
	    	}
	    }
	}
	
	@SuppressWarnings("unchecked")
	@When("^User selects remove icon for \"([^\"]*)\" Art Director, Cover Designer, and Interior Designer$")
	public void user_selects_remove_icon_for_Art_Director_Cover_Designer_and_Interior_Designer(String roles) throws Throwable {
		AssignDialog assign=new AssignDialog();
	    List<String> role = new Gson().fromJson(roles, List.class);
	    for(String item:role)
	    {
	    assign.getAssignRemoveButtons(item).click();
	    }
	}

	@When("^User closes window without Saving$")
	public void user_closes_window_without_Saving() throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		myprojects.getcloseButton().click();
		TestBaseProvider.getTestBase().getDriver().switchTo().defaultContent();
		myprojects.waitForResultsLoadingComplete();
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Then("^Initial project values \"([^\"]*)\" for \"([^\"]*)\" for \"([^\"]*)\" total projects remains unchanged in database$")
	public void initial_project_values_for_for_total_projects_remains_unchanged_in_database(String values, String dboptions, String projects) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    DBConnection dbcon=new DBConnection();
	    List<String> dboption = new Gson().fromJson(dboptions, List.class);
	    List<String> inputs = new Gson().fromJson(values, List.class);
	    for(int i=0;i<projectsNo;i++)
	    {
	    	String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
	    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
	    	while(rs.next())
	    	{
	    		for(int j=0;j<dboption.size();j++)
	    		{
	    		Assert.assertTrue(rs.getString(dboption.get(j)).equals(inputs.get(j)));
	    		}
	    	}
	    }
	}

	@SuppressWarnings("unchecked")
	@Then("^Initial project values \"([^\"]*)\" for \"([^\"]*)\" for \"([^\"]*)\" are displayed unchanged in Project List$")
	public void initial_project_values_for_for_are_displayed_unchanged_in_Project_List(String values, String roles, String projects) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    myprojects.getProjectGrid().showAllColumns();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    List<String> options = new Gson().fromJson(roles, List.class);
	    List<String> inputs = new Gson().fromJson(values, List.class);
	    int projectsNo=Integer.parseInt(projects);
	    for(int i=0;i<projectsNo;i++)
	    {
	    	for(int j=0;j<options.size();j++)
	    	{
	    	Assert.assertEquals(itemgrid.getRows().get(i).getColumnText(options.get(j)),inputs.get(j));
	    	}
	    }
	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Then("^Unassigned values for \"([^\"]*)\" for \"([^\"]*)\" are stored in database for project$")
	public void unassigned_values_for_for_are_stored_in_database_for_project(String selectedvalues, String projects) throws Throwable {
		MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    DBConnection dbcon=new DBConnection();
	    List<String> items = new Gson().fromJson(selectedvalues, List.class);
	    String dbvalue=null;
	    ArrayList<String> dbselectedValues=new ArrayList<>();
	    for(int i=0;i<projectsNo;i++)
	    {
	    	String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
	    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
	    	while(rs.next())
	    	{
	    		for(String dboption:items)
	    		{
	    			dbvalue=rs.getString(dboption);
	    			dbselectedValues.add(dbvalue);
	    		Assert.assertTrue(null==dbvalue);
	    		}
	    	}
	    }
	}

	@Then("^Initial project values for \"([^\"]*)\" for \"([^\"]*)\" remains unchanged in databse$")
	public void initial_project_values_for_for_remains_unchanged_in_databse(String remainingValues, String projects) throws Throwable {
	   	MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    int projectsNo=Integer.parseInt(projects);
	    DBConnection dbcon=new DBConnection();
	    List<String> dboption = new Gson().fromJson(remainingValues, List.class);
	    String dbvalue=null;
	    ArrayList<String> dbremainingValues=new ArrayList<>();
	    for(int i=0;i<projectsNo;i++)
	    {
	    	String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
	    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
	    	while(rs.next())
	    	{
	    		for(int j=0;j<dboption.size();j++)
	    		{
	    			dbvalue=rs.getString(dboption.get(j));
	    			String previousValue=TestBaseProvider.getTestBase().getString("Assign.database."+dboption.get(j));
	    			dbremainingValues.add(dbvalue);
	    		Assert.assertEquals(dbvalue, previousValue);
	    		}
	    	}
	    }
	}

	@SuppressWarnings("unchecked")
	@Then("^Values displayed in Project List for \"([^\"]*)\" for \"([^\"]*)\" total projects matches database values$")
	public void values_displayed_in_Project_List_for_for_total_projects_matches_database_values(String roles, String projects) throws Throwable {
	   	String dbfields=TestBaseProvider.getTestBase().getString("Assign.database.dbfields");
	    List<String> dboptions = new Gson().fromJson(dbfields, List.class);
		 MyProjectsPage myprojects=new MyProjectsPage();
		    myprojects.waitForResultsLoadingComplete();
		    ItemGrid itemgrid=myprojects.getProjectGrid();
		    int projectsNo=Integer.parseInt(projects);
		    String dbvalue="";
		    String roleValue="";
		    DBConnection dbcon=new DBConnection();
		    List<String> options = new Gson().fromJson(roles, List.class);
		 for(int i=0;i<projectsNo;i++)
		    {
			 String isbn=itemgrid.getRows().get(i).getColumnText("ISBN");
		    	ResultSet rs= dbcon.dbConnection("SELECT * FROM APT_PROJECTS_VIEW WHERE APT_PROJECTS_VIEW.ISBN_13='"+isbn+"'");
		    	while(rs.next())
		    	{
		    	for(int j=0;j<options.size();j++)
		    	{
		    		dbvalue=rs.getString(dboptions.get(j));
		    		roleValue=itemgrid.getRows().get(i).getColumnText(options.get(j));
		    		if(null==dbvalue)
		    		{
		    			dbvalue="";
		    		}
		   		 Assert.assertEquals(dbvalue, roleValue);
		    	}
		    }
		    }
	}
}
