/**
 * 
 */
package com.scholastic.cucumber.apt.data.model;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.HierarchicalConfiguration;

import com.scholastic.torque.common.TestBaseProvider;

/**
 * @author chirag.jayswal
 *
 */
public class UserHelper {

	public static List<User> getAllUsers() {
		try {
			return fetchFromXMLData("users");
		} catch (Exception e) {
			throw new RuntimeException("Unable to get user data: " + e);
		}
	}

	public static User getUserWithRoles(List<String> roles) {
		for (User user : getAllUsers()) {
			if (null != user.getRoles() && user.getRoles().size() == roles.size()
					&& user.getRoles().containsAll(roles)) {
				return user;
			}
		}
		return null;
	}

	public static User getUserWithRole(String role) {
		return getUserWithRoles(Arrays.asList(role.split(";")));
	}

	@SuppressWarnings("unchecked")
	private static List<User> fetchFromXMLData(String key) throws IllegalAccessException, InvocationTargetException {
		HierarchicalConfiguration config = (HierarchicalConfiguration) TestBaseProvider.getTestBase()
				.getTestDataFromXml().subset(key);
		List<User> users = new ArrayList<User>();
		for (int i = 0; i < config.getRoot().getChildrenCount(); i++) {
			Configuration configNode = config.configurationAt("user(" + i + ")");
			User user = new User();

			user.setName(configNode.getString("name"));
			user.setPassword(configNode.getString("password"));
			user.setFirstName(configNode.getString("firstname"));
			user.setLastName(configNode.getString("lastname"));
			user.setRoles(configNode.getList("roles"));

			users.add(user);
		}
		return users;
	}

	public static void main(String[] args) {
		List<User> users = getAllUsers();
		System.out.println(users.size());
		User u = getUserWithRole("APT Art Director");
		System.out.println(u.getName());
		u = getUserWithRole("APT Designer;SCCG Manufacturing");
		System.out.println(u.getName());

		ArrayList<String> roles = new ArrayList<String>();
		roles.add("APT Designer");
		roles.add("SCCG Manufacturing");
		u = getUserWithRoles(roles);

		System.out.println(u.getName());

		u = getUserWithRole("SCCG Manufacturing;APT Designer");
		System.out.println(u.getName());
	}

}
