package com.scholastic.cucumber.apt.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.AdvancedSearchPageLocators;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

public class AdvancedSearchPage extends BaseTestPage<TestPage> implements AdvancedSearchPageLocators{

	@FindBy(locator = ADVSEARCH_BTN)
	private WebElement searchBtn;

	@FindBy(locator = ERROR_MESSAGES_LIST)
	private List<WebElement> errorMessages;
	
	
	
	
	public List<WebElement> getErrorMessages() {
		return errorMessages;
	}
	
	
	public WebElement getAdvSearchBtn() {
		return searchBtn;
	}
	
	public void clickAdvSearchBtn()
	{
		WaitUtils.waitForDisplayed(getAdvSearchBtn());
		getAdvSearchBtn().click();
	}
	
	public Dialog getDialog() {
		return new Dialog(APP_COMMON_DIALOG);
	}
	
	public void switchToAdvSearchDialog()
	{
		getDialog();
		WaitUtils.waitForDisplayed(getDialog().getDialogTitleElement());
		getDriver().switchTo().frame(0);
	}
	
	
	public WebElement getSearchInputFromDate(String label)
	{
		String productDetailsXpath=String.format(SEARCH_BY_FROMDATE_INPUTS,label);	
		return	getDriver().findElement(LocatorUtils.getBy(productDetailsXpath));
	}
	
	public WebElement getSearchInputToDate(String label)
	{
		String productDetailsXpath=String.format(SEARCH_BY_TODATE_INPUTS,label);	
		return	getDriver().findElement(LocatorUtils.getBy(productDetailsXpath));
	}
	
	public void searchByDate(String label,String fromDate,String toDate)
	{
		getSearchInputFromDate(label).sendKeys(fromDate);
		getSearchInputToDate(label).sendKeys(toDate);
	}
	
	public WebElement getSearchInputElement(String label)
	{
		String productDetailsXpath=String.format(ADVANCED_SEARCH_LABEL_INPUTS,label);	
		return	getDriver().findElement(LocatorUtils.getBy(productDetailsXpath));
	}
	
	public void search(String label,String searchText)
	{
		WaitUtils.waitForDisplayed(getSearchInputElement(label));
		getSearchInputElement(label).sendKeys(searchText);
	}
	
	public ArrayList<String> getEditorValue(String colName)
	{
		MyProjectsPage myprojects=new MyProjectsPage();
	    myprojects.waitForResultsLoadingComplete();
	    ItemGrid itemgrid=myprojects.getProjectGrid();
	    TransmittalForm tsf=new TransmittalForm(); 
	    ArrayList<String> editors=new ArrayList<>();
	    boolean navNextPage = false;
		do {
			if (navNextPage) {                              
				myprojects.clickElementByJS(myprojects.getNextButton());
				myprojects.waitForResultsLoadingComplete();
				WaitUtils.waitForEnabled(itemgrid.getWrappedElement());
			}
	    for(Row row:itemgrid.getRows())
    	{
	    	try{
	    		WaitUtils.waitForDisplayed(row.getColumn("Cover"));
	    		row.getColumn("Cover").click();
	    	}
	    	catch(Exception e)
	    	{
	    		row.getColumn("Title").click();
	    	}
	    	
	    	 tsf.switchToTransmittalForm();
    		editors.add(tsf.getValueElement(colName).getAttribute("value"));
    		myprojects.waitForResultsLoadingComplete();
    		myprojects.clickCloseButton();
    		myprojects.waitForResultsLoadingComplete();
    	}
	    navNextPage = true;
		} while (myprojects.getNextButton().isEnabled());
		myprojects.clickElementByJS(myprojects.getFirstButton());
	    return editors;
	}
	
	
	
	@Override
	protected void openPage() {
		// TODO Auto-generated method stub
		
	}
}
