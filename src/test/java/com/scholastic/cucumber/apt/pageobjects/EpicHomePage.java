package com.scholastic.cucumber.apt.pageobjects;

import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.scholastic.cucumber.apt.Keys.EPICHomePageLocators;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;

public class EpicHomePage extends BaseTestPage<TestPage> implements EPICHomePageLocators {

	@FindBy(locator = SEARCH_FIELD)
	private WebElement searchInput;

	@FindBy(locator = SEARCH_BUTTON)
	private WebElement searchButton;

	@FindBy(locator = SEARCH_ISBN13)
	private WebElement searchIsbn13;

	public WebElement getsearchIsbn13() {
		return searchIsbn13;
	}

	public WebElement getsearchInput() {
		return searchInput;
	}

	public WebElement getsearchButton() {
		return searchButton;
	}

	public void assertResults(ArrayList<String> dbResults, ArrayList<String> epicResults) {
		ArrayList<String> epicResults1 = new ArrayList<>();
		ArrayList<String> dbResults1 = new ArrayList<>();
		dbResults.removeAll(Collections.singleton(null));
		epicResults.removeAll(Collections.singleton("  "));
		epicResults.removeAll(Collections.singleton(""));
		epicResults.removeAll(Collections.singleton(" "));
		dbResults.removeAll(Collections.singleton("  "));
		dbResults.removeAll(Collections.singleton(""));
		dbResults.removeAll(Collections.singleton(" "));
		for (String string : epicResults) {

			string = string.replaceAll("\\s", "");
			if (string == "" || string == " ") {

			} else {
				epicResults1.add(string);
			}
		}

		for (String string : dbResults) {
			if (string == null) {
				string = "";
			}
			string = string.replaceAll("\\s", "");
			if (string == "" || string == " ") {

			} else {
				dbResults1.add(string);
			}
		}
		ArrayList<String> finalepicResults = new ArrayList<>();
		ArrayList<String> finalDatabaseResults = new ArrayList<>();
		for (int i = 0; i < epicResults1.size(); i++) {
			String epicString = epicResults1.get(i).trim();
			if (epicString.contains("/")) {
				epicString = epicString.replaceFirst("^0+(?!$)", "");
				epicString = epicString.replaceAll("/0", "/");
				finalepicResults.add(epicString.trim());
			} else {
				epicString = epicString.replaceFirst("^0+(?!$)", "");
				finalepicResults.add(epicString.toString());
			}
		}
		for (int i = 0; i < dbResults1.size(); i++) {

			String dbString = dbResults1.get(i).trim();
			if (dbString.contains("/")) {
				dbString = dbString.replaceFirst("^0+(?!$)", "");
				dbString = dbString.replaceAll("/0", "/");
				finalDatabaseResults.add(dbString.trim());
			} else {
				dbString = dbString.replaceFirst("^0+(?!$)", "");
				finalDatabaseResults.add(dbString.toString());
			}
		}
		Collections.sort(finalDatabaseResults);
		Collections.sort(finalepicResults);
		Assert.assertTrue(finalepicResults.equals(finalDatabaseResults));
	}
	
	@Override
	protected void openPage() {
		// TODO Auto-generated method stub

	}
}
