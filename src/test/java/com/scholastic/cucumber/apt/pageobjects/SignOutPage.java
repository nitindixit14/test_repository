/**
 * 
 */
package com.scholastic.cucumber.apt.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.SignOutPageLocators;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;

/**
 * @author chirag.jayswal
 *
 */
public class SignOutPage extends BaseTestPage<TestPage> implements SignOutPageLocators {

	@FindBy(locator = GO_TO_SIGNIN_PAGE_BUTTON)
	private WebElement goToSignInPageButton;
	@FindBy(locator = SIGN_OUT_MESSAGE)
	private WebElement signOutMessage;

	public WebElement getSignOutMessage() {
		return signOutMessage;
	}

	public WebElement getGoToSignInPageButton() {
		return goToSignInPageButton;
	}

	@Override
	protected void openPage() {
		// TODO Auto-generated method stub

	}
}
