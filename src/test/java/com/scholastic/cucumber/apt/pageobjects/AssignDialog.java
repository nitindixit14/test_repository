package com.scholastic.cucumber.apt.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.scholastic.cucumber.apt.Keys.AssignDialogLocators;
import com.scholastic.cucumber.apt.Keys.MyProjectsPageLocators;
import com.scholastic.cucumber.apt.pageobjects.ItemGrid.Row;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;
import com.scholastic.torque.webdriver.ExtendedElement;

public class AssignDialog extends BaseTestPage<TestPage> implements AssignDialogLocators {

	
	@FindBy(locator = ASSIGN_DIALOG_SAVE_BUTTON)
	private WebElement assignSaveButton;
	
	@FindBy(locator = ASSIGN_DIALOG_REMOVE_ARTDIRECTOR)
	private WebElement removeArtDirectorButton;
	
	@FindBy(locator = ASSIGN_AUTOSUGGEST)
	private ExtendedElement assignAutoSuggest;
	
	@FindBy(locator = ASSIGN_AUTOSUGGEST_ELEMENTS)
	private List<WebElement> assignAutoSuggestElements;
	
	
	@FindBy(locator =ASSIGN_TABLE_HEADERS)
	private ExtendedElement assignTableHeaders;
	
	@FindBy(locator =ASSIGN_TITLE)
	private ExtendedElement assignTitle;
	
	public WebElement getAssignTitle()
	{
		return assignTitle;
	}
	
	public List<WebElement> getAssignTableHeaders()
	{
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		return driver.findElements(By.xpath(ASSIGN_TABLE_HEADERS));
	}
	
	/*public List<WebElement> getassignAutoSuggestElements()
	{
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		return driver.findElements(By.xpath(ASSIGN_AUTOSUGGEST_ELEMENTS));
	}
	*/
	
	public WebElement getAssignAutoSuggest()
	{
		return assignAutoSuggest;
	}
	
	public WebElement getRemoveArtDirectorButton()
	{
		return removeArtDirectorButton;
	}
	
	public void clickRemoveADBtn()
	{
		WaitUtils.waitForDisplayed(getRemoveArtDirectorButton());
		getRemoveArtDirectorButton().click();
	}
	
	public WebElement getAssignSaveButton()
	{
		return assignSaveButton;
	}
	
	public void clickAssignSaveBtn()
	{
		WaitUtils.waitForDisplayed(getAssignSaveButton());
		getAssignSaveButton().click();
	}
	
	public void selectAssignAutoSuggest(String field,String name)
	{
		getAssignInputs(field).sendKeys(name);
		WaitUtils.waitForPresent(assignAutoSuggest);
		String xpath=String.format(ASSIGN_DROPDOWN, name);
//		WaitUtils.waitForPresent(By.xpath(xpath));
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
//		WaitUtils.waitforpres
		WebElement elm=assignAutoSuggest.findElement(By.xpath(xpath));
		elm.click();
	}
	
	public WebElement getAssignRemoveButtons(String label)
	{
		
		String xpath=String.format(ASSIGN_REMOVE_BUTTONS,label);	
	return	getDriver().findElement(LocatorUtils.getBy(xpath));
	}
	
	public void clickRemoveButton(String label)
	{
		WebElement element=getAssignRemoveButtons(label);
		WaitUtils.waitForDisplayed(element);
		element.click();
	}
	
	public WebElement getAssignInputs(String label)
	{
		String xpath=String.format(ASSIGN_DIALOG_INPUTS,label);	
	return	getDriver().findElement(LocatorUtils.getBy(xpath));
	}
	
	public void sendAssignInputs(String label,String value)
	{
		WebElement element=getAssignInputs(label);
		WaitUtils.waitForDisplayed(element);
		element.sendKeys(value);
	}
	
	public WebElement getAssignLabels(String label)
	{
		
		String xpath=String.format(ASSIGN_DIALOG_LABELS,label);	
	return	getDriver().findElement(LocatorUtils.getBy(xpath));
	}
	
	
	
	public Dialog getDialog() {
		return new Dialog(APP_COMMON_DIALOG);
	}
	
	public void switchToAssignDialog()
	{
		getDialog();
		WaitUtils.waitForDisplayed(getDialog().getDialogTitleElement());
		getDriver().switchTo().frame(0);
	}
	
	
	@Override
	protected void openPage() {
		// TODO Auto-generated method stub
		
	}
	
	public ItemGrid getAssignTableGrid() {
		return new ItemGrid(ASSIGN_TABLE);
	}
	

	public ArrayList<Row> getAssignTypeRows()
	{
		Row firstRow=null;
		ArrayList<Row> rows=new ArrayList<>();
		for (Row row : getAssignTableGrid().getRows()) {
			//String projectRows = row.getColumnText("Type");
			//WebElement input = row.findElement(By.tagName("input"));
			
		      rows.add(row);
			
		}
		return rows;
	}
	
}
