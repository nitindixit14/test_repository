package com.scholastic.cucumber.apt.pageobjects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.scholastic.cucumber.apt.Keys.MyProjectsPageLocators;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;
import com.thoughtworks.selenium.Wait;

public class MyProjectsPage extends BaseTestPage<TestPage> implements MyProjectsPageLocators {

	@FindBy(locator = SEARCH_BOX_INPUT)
	private WebElement searchText;

	@FindBy(locator = LOADING_MESSAGE)
	private WebElement loadingMessage;

	@FindBy(locator = SEARCH_BUTTON)
	private WebElement searchButton;

	@FindBy(locator = ASSIGN_BUTTON)
	private WebElement assignButton;

	// will get text ends with current page label for e.g: "Scholastic > My
	// Projects", "Scholastic > Manage Studio List"
	@FindBy(locator = SHOW_TEAM_PROJECT_LINK)
	private WebElement showTeamProjLink;

	@FindBy(locator = ADVANCE_SEARCH_LINK)
	private WebElement advancedSearchLink;

	@FindBy(locator = NEXT_BUTTON)
	private WebElement nextButton;

	@FindBy(locator = ERROR_MESSAGES_LIST)
	private List<WebElement> errorMessages;

	// these locators are also added in the keys.java file
	@FindBy(locator = PREVIOUS_BUTTON)
	private WebElement previousButton;

	@FindBy(locator = FIRST_BUTTON)
	private WebElement firstButton;

	@FindBy(locator = LAST_BUTTON)
	private WebElement lastButton;

	@FindBy(locator = TRANSMITTAL_HISTORY_IMG)
	private WebElement transmittalHistoryImg;

	@FindBy(locator = CLOSE_BUTTON)
	private WebElement closeButton;

	@FindBy(locator = PAGING_DISPLAY)
	private WebElement displayPerPage;

	@FindBy(locator = PAGING_FIVE)
	private WebElement displayFiveperPage;

	@FindBy(locator = PAGING_TEN)
	private WebElement displayTenperPage;

	@FindBy(locator = PAGING_FIFTEEN)
	private WebElement displayFifteenperPage;

	@FindBy(locator = TOTAL_PROJECTS)
	private WebElement displayTotalProjects;

	@FindBy(locator = SELECTED_PAGING)
	private WebElement selectedPagingOption;

	@FindBy(locator = PAGE_NAVIGATOR)
	private WebElement pageNavigator;

	@FindBy(locator = TOTAL_PAGES)
	private WebElement totalPages;

	@FindBy(locator = MY_PROJECTS_TITLE)
	private WebElement myProjectsTitle;

	@Override
	protected void openPage() {

	}

	public WebElement getproductDetailsFields(String label)
	{
		
		String productDetailsXpath=String.format(PRODUCT_DETAILS_FILEDS,label);	
	return	getDriver().findElement(LocatorUtils.getBy(productDetailsXpath));
	}
	
	public WebElement getSearchTypeCheckbox(String searchType)
	{
		return getDriver().findElement(LocatorUtils.getBy(String.format(SEARCH_TYPE_CHECKBOXES,searchType)));
	}
	
	public void clickSearchType(String searchType)
	{
		WaitUtils.waitForDisplayed(getSearchTypeCheckbox(searchType));
		getSearchTypeCheckbox(searchType).click();
	}
	
	public WebElement getMyProjectsTitle() {
		return myProjectsTitle;
	}

	public WebElement getAdvancedSearchLink() {
		return advancedSearchLink;
	}
	
	public void clickAdvancedSearchLink()
	{
		WaitUtils.waitForDisplayed(getAdvancedSearchLink());
		getAdvancedSearchLink().click();
	}

	public WebElement getLoadingMessage() {
		return loadingMessage;
	}

	public WebElement getTotalPages() {
		return totalPages;
	}

	public WebElement getPageNavigator() {
		return pageNavigator;
	}

	public WebElement getSelectedPagingOption() {
		return selectedPagingOption;
	}

	public WebElement getDisplayTotalProjects() {
		return displayTotalProjects;
	}

	public WebElement getDisplaypageOption() {
		return displayPerPage;
	}

	public void clickPagingOption()
	{
		WaitUtils.waitForDisplayed(getDisplaypageOption());
		getDisplaypageOption().click();
	}
	
	public WebElement getPagingOp1() {
		return displayFiveperPage;
	}
	
	public void clickPagingOp1()
	{
		WaitUtils.waitForDisplayed(getPagingOp1());
		getPagingOp1().click();
	}

	public WebElement getPagingOp2() {
		return displayTenperPage;
	}
	
	public void clickPagingOp2()
	{
		WaitUtils.waitForDisplayed(getPagingOp2());
		getPagingOp2().click();
	}

	public WebElement getPagingOp3() {
		return displayFifteenperPage;
	}
	
	public void clickPagingOp3()
	{
		WaitUtils.waitForDisplayed(getPagingOp3());
		getPagingOp3().click();
	}

	public WebElement getshowTeamProjLink() {
		return showTeamProjLink;
	}
	
	public void clickShowTeamProj()
	{
		WaitUtils.waitForDisplayed(getshowTeamProjLink());
		clickElementByJS(getshowTeamProjLink());
	}

	public WebElement getAssignButton() {
		return assignButton;
	}
	
	public void clickAssignButton()
	{
		WaitUtils.waitForDisplayed(getAssignButton());
		getAssignButton().click();
	}
	

	public WebElement getcloseButton() {
		return closeButton;
	}
	
	public void clickCloseButton()
	{
		WaitUtils.waitForDisplayed(getcloseButton());
		getcloseButton().click();
	}

	public WebElement getTransmittalHistoryImg() {
		return transmittalHistoryImg;
	}

	public void searchFor(String text) throws InterruptedException {
		// WaitUtils.waitForEnabled(getProjectGrid().getWrappedElement());
		Thread.sleep(4000);
		switchToResults();
		waitForResultsLoadingComplete();
		WaitUtils.waitForEnabled(searchText);
		waitForResultsLoadingComplete();
		searchText.clear();
		searchText.sendKeys(text);
		WaitUtils.waitForEnabled(searchButton);
		searchButton.click();
		waitForResultsLoadingComplete();
		WaitUtils.waitForEnabled(getProjectGrid().getWrappedElement());
		Thread.sleep(2000);
	}

	public void searchUsing(String text) throws InterruptedException {
		// WaitUtils.waitForEnabled(getProjectGrid().getWrappedElement());
		Thread.sleep(4000);
		switchToResults();
		waitForResultsLoadingComplete();
		WaitUtils.waitForEnabled(searchText);
		searchText.clear();
		searchText.sendKeys(text);
		WaitUtils.waitForEnabled(searchButton);
		searchButton.click();
		WaitUtils.waitForDisplayed(getErrorMessages().get(0));
	}

	public void switchToResults() {
		
		try {
			getDriver().switchTo().frame(RESULT_FRAME);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public ItemGrid getProjectGrid() {
		return new ItemGrid(MY_PROJECTS_TABLE);
	}

	public TransmittalHistoryDialog getTransmittalHistoryDialog() {
		return new TransmittalHistoryDialog(APP_COMMON_DIALOG);
	}

	public AdvanceSearchDialog getAdvanceSearchDialog() {
		return new AdvanceSearchDialog(MY_PROJECTS_DIALOG_IFRAME);
	}

	public Dialog getDialog() {
		return new Dialog(APP_COMMON_DIALOG);
	}

	public List<WebElement> getRowEles() {
		return getDriver().findElements(By.cssSelector(TABLE_ROWS_LIST));
	}

	public void waitForResultsLoadingComplete() {
		getDriver().switchTo().defaultContent();
		WaitUtils.waitForNotDisplayed(loadingMessage);
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loadHolder")));
		WebDriverWait wait1 = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 50);
		wait1.until(ExpectedConditions.invisibilityOfElementLocated(By.className("loadHolder")));
		// switch back to results frame
		switchToResults();
	}

	public void clickElementByJS(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click();", element);
	}

	public WebElement getNextButton() {
		return nextButton;
	}
	
	public void clickNextButon()
	{
		WaitUtils.waitForDisplayed(getNextButton());
		getNextButton().click();
	}

	public WebElement getPreviousButton() {
		return previousButton;
	}
	
	public void clickPreviousButton()
	{
		WaitUtils.waitForDisplayed(getPreviousButton());
		getPreviousButton().click();
	}

	public WebElement getFirstButton() {
		return firstButton;
	}
	
	public void clickFirstButton()
	{
		WaitUtils.waitForDisplayed(getFirstButton());
		getFirstButton().click();
	}

	public WebElement getLastButton() {
		return lastButton;
	}
	
	public void clickLastButton()
	{
		WaitUtils.waitForDisplayed(getLastButton());
		getLastButton().click();
	}

	public List<WebElement> getErrorMessages() {
		return errorMessages;
	}

	public void getPagingOption(String pagingOption) throws InterruptedException {
		waitForResultsLoadingComplete();
		// WaitUtils.waitForEnabled(getProjectGrid().getWrappedElement());
		//Thread.sleep(5000);
		
		WaitUtils.waitForDisplayed(getDisplaypageOption());
		WaitUtils.waitForEnabled(getDisplaypageOption());
		getDisplaypageOption().click();
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		driver.switchTo().defaultContent();
		WaitUtils.waitForDisplayed(getPagingOp1());
		if (pagingOption.equals("50")) {
			clickPagingOp1();
		}
		if (pagingOption.equals("100")) {
			clickPagingOp2();
		}
		if (pagingOption.equals("150")) {
			clickPagingOp3();
		}
		WebDriverWait wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loadHolder")));
	}

	public String getLastPageNum() {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		String[] pages = myprojects.getTotalPages().getText().split("of ");
		String lastPageNum = pages[1];
		return lastPageNum;
	}

	public String getCurrentPageNum() {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		String[] pageDetails = myprojects.getTotalPages().getText().split("Page ");
		String[] PageNumber = pageDetails[1].split(" ");
		String currentPageNumber = PageNumber[0];
		return currentPageNumber;
	}

	public String getLastProjectNum() {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		String[] projects = myprojects.getDisplayTotalProjects().getText().split("of ");
		String totalProjects = projects[1];
		return totalProjects;
	}

	public String getCurrentProjectNum() {
		MyProjectsPage myprojects = new MyProjectsPage();
		myprojects.waitForResultsLoadingComplete();
		String[] projects = myprojects.getDisplayTotalProjects().getText().split("Projects ");
		String[] totalProjects = projects[1].split(" of");
		String currentProjects = totalProjects[0];
		return currentProjects;
	}

	public void setAttribute(WebElement element, String attributeName, String value) {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("arguments[0].setAttribute(arguments[1],arguments[2])", element, attributeName, value);
	}

	public String changeDateFormat(String stringDate) throws ParseException {
		String date = new String();
		if (stringDate != null && stringDate != "") {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
			date = new SimpleDateFormat("mm/dd/yyyy").format(df.parse(stringDate));
		}
		return date;
	}

	public String setDateFormat(String stringDate) throws ParseException {
		String date = new String();
		if (stringDate != null && stringDate != "") {
			SimpleDateFormat df = new SimpleDateFormat("mm/dd/yyyy");
			date = new SimpleDateFormat("mm/dd/yyyy").format(df.parse(stringDate));
		}
		return date;
	}
	
	public String setFormatFromYY(String stringDate)
	{
		SimpleDateFormat yy = new SimpleDateFormat( "MM/dd/yy" );
        SimpleDateFormat yyyy = new SimpleDateFormat( "MM/dd/yyyy" );
        Date actualDate=null;
        String pubDateInForm=stringDate;
        try {
            actualDate = yy.parse( pubDateInForm );
        }
        catch ( ParseException pe ) {
            System.out.println( pe.toString() );
        }
        String date= yyyy.format( actualDate ).trim();
        return date;	
	}
	
	public void switchToWindow()
	{
		WebDriver driver=TestBaseProvider.getTestBase().getDriver();
		String curWindow = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		for (String window : windows) {
			System.err.println("window " + window);
			if (!window.equalsIgnoreCase(curWindow))
				;
			driver.switchTo().window(window);
		}
	}
	
	public void switchToDefault()
	{
		getDriver().switchTo().defaultContent();
	}
	
	public void switchToFrame()
	{
		getDriver().switchTo().frame(0);
	}
	
	public void openProjectDetails(int rowNum)
	{
		ProjectDetails details=new ProjectDetails();
		waitForResultsLoadingComplete();
		ItemGrid itemgrid=getProjectGrid();
		itemgrid.getRows().get(rowNum).getColumn("Title").click();
		details.switchToDetailsDialog();
	}
}
