/**
 * 
 */
package com.scholastic.cucumber.apt.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.scholastic.cucumber.apt.Keys.OTBPMSigninPageLocators;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

/**
 * @author chirag.jayswal
 *
 */
public class OTBPMSigninPage extends BaseTestPage<TestPage> implements OTBPMSigninPageLocators {

	@FindBy(locator = USER_NAME_INPUT)
	private WebElement userNameInputBox;

	@FindBy(locator = PASSWORD_INPUT)
	private WebElement passwordInputBox;

	@FindBy(locator = SIGN_IN_BTN)
	private WebElement signInButton;

	@FindBy(locator = ERROR_MESSSAGE_AREA)
	private WebElement errorMessage;

	@FindBy(locator = LOADING_MESSAGE)
	private WebElement loadingMessage;

	public UserHomePage signIn(String userID, String password) {
		userNameInputBox.sendKeys(userID);
		passwordInputBox.sendKeys(password);

		signInButton.click();
		//WaitUtils.waitForNotDisplayed(loadingMessage);
		return new UserHomePage();
	}

	public WebElement getSignInButton() {
		return signInButton;
	}

	@Override
	protected void openPage() {
		getDriver().get(getContext().getString(PAGE_URL));
	}

}
