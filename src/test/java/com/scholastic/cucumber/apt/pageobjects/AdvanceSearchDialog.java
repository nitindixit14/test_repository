/**
 * 
 */
package com.scholastic.cucumber.apt.pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.scholastic.cucumber.apt.Keys.AdvanceSearchDialogLocators;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.Section;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;
import com.torque.automation.core.TestDataUtils;

/**
 * @author chirag.jayswal
 *
 */
public class AdvanceSearchDialog extends Section implements AdvanceSearchDialogLocators {

	public class Container extends Section {
		@FindBy(locator = SEARCH_BTN)
		private WebElement searchAdvancedSearchButton;

		@FindBy(locator = ALL_THE_WORDS_INPUT)
		private WebElement searchAdvancedAllthesewordsInput;

		@FindBy(locator = EXACT_WORD_INPUT)
		private WebElement searchAdvancedExactwordInput;

		@FindBy(locator = ANY_OF_WORDS_INPUT)
		private WebElement searchAdvancedAnyofthesewordsInput;

		@FindBy(locator = NONE_OF_THESE_WORDS_INPUT)
		private WebElement searchAdvancedNoneofthesewordsInput;

		@FindBy(locator = TARGET_DUE_TO_MFG)
		private WebElement targetDueToMfg;

		@FindBy(id = "groupbox3")
		private WebElement groupBox;

		@FindBy(id = "inputrtm_from")
		private WebElement targetFrom;

		@FindBy(id = "inputrtm_to")
		private WebElement targetTo;

		public Container() {
			super("css=.maincontainer", TestDataUtils.getDriver());
		}

	}

	private Container container = new Container();

	public enum SearchBy {
		All, ISBN("ISBN 10/13"), Title, Author, Illustrator, Designer, ArtDirector("Art Director"), Editor;

		String title;

		private SearchBy(String title) {
			this.title = title;
		}

		private SearchBy() {
			this.title = name();
		}

		public String getTitle() {
			return title;
		}

		public static SearchBy title(String s) {
			for (SearchBy searchBy : SearchBy.values()) {
				if (searchBy.getTitle().equalsIgnoreCase(s) || searchBy.name().equalsIgnoreCase(s))
					return searchBy;
			}

			return null;
		}

	}

	public AdvanceSearchDialog(String loc) {
		super(loc);
	}

	public WebElement getSearchAdvancedSearchButton() {
		return container.searchAdvancedSearchButton;
	}

	public WebElement getSearchAdvancedAllthesewordsInput() {
		return container.searchAdvancedAllthesewordsInput;
	}

	public WebElement getSearchAdvancedExactwordInput() {
		return container.searchAdvancedExactwordInput;
	}

	public WebElement getSearchAdvancedAnyofthesewordsInput() {
		return container.searchAdvancedAnyofthesewordsInput;
	}

	public WebElement getSearchAdvancedNoneofthesewordsInput() {
		return container.searchAdvancedNoneofthesewordsInput;
	}

	public WebElement getTargetDueToMfg() {
		return container.targetDueToMfg;
	}

	public WebElement getTargetFrom() {
		return container.targetFrom;
	}

	public WebElement getTargetTo() {
		return container.targetTo;
	}

	public WebElement getGroupBox() {
		return container.groupBox;
	}

	public void setChecked(SearchBy searchBy, boolean checked) {
		WebElement searchByInput = container.findElement(
				LocatorUtils.getBy(String.format(getProps().getString(SEARCH_BY_OPT_FORMAT), searchBy.getTitle())));

		if (searchByInput.isSelected() != checked)
			clickElementByJS(searchByInput);
	}

	private TestBase getProps() {
		return TestBaseProvider.getTestBase();
	}

	public void WaitForAjax() {
		(new WebDriverWait(TestDataUtils.getDriver(), 10)).until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver d) {
				JavascriptExecutor js = (JavascriptExecutor) TestDataUtils.getDriver();
				return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");

			}
		});
	}

	public void clickElementByJS(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) TestDataUtils.getDriver();
		executor.executeScript("arguments[0].click();", element);
	}

	public Dialog getDialog() {
		return new Dialog(APP_COMMON_DIALOG);
	}
	
	public void switchToDialog() {
		try {
			// TestDataUtils.getDriver().switchTo().defaultContent();
			// TestDataUtils.getDriver().switchTo().frame("ufo_dialog_My
			// Projects");
			TestDataUtils.getDriver().switchTo()
					.frame(TestDataUtils.getDriver().findElement(LocatorUtils.getBy(DIALOG_IFRAME)));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

}
