/**
 * 
 */
package com.scholastic.cucumber.otmm.pageobjects;

import org.openqa.selenium.By;

import com.scholastic.torque.common.Section;

/**
 * @author chirag.jayswal
 *
 */
public class OTMMHeader extends Section {

	public OTMMHeader() {
		this(By.cssSelector(".ot-web-header-bar"));
	}
	public OTMMHeader(By by) {
		super(by);
	}
	

}
