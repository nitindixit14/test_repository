/**
 * 
 */
package com.scholastic.cucumber.otmm.stepdefs;

import static com.scholastic.torque.common.TestBaseProvider.getTestBase;

import org.testng.Assert;

import com.scholastic.cucumber.otmm.pageobjects.OTMMSignInPage;
import com.scholastic.cucumber.otmm.pageobjects.OTMMTemplate;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author chirag.jayswal
 *
 */
public class LoginSteps {
	@Given("^User is on OTMM sing in page$")
	public void user_is_on_OTMM_sing_in_page() throws Throwable {
	    OTMMSignInPage otmmSignInPage = new OTMMSignInPage();
	    otmmSignInPage.launchPage();
	}

	@When("^user signing in to OTMM using tester AD credential$")
	public void user_signing_in_to_OTMM_using_tester_AD_credential() throws Throwable {
	    OTMMSignInPage otmmSignInPage = new OTMMSignInPage();
	    otmmSignInPage.signIn(getTestBase().getString("test.username"), getTestBase().getString("test.password"));
	}

	@Then("^Login Succeeds$")
	public void login_Succeeds() throws Throwable {
	    OTMMTemplate template = new OTMMTemplate();
	    Assert.assertTrue(template.isPageActive(), "User logged in");
	}

}
