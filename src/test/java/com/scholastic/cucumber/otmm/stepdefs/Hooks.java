package com.scholastic.cucumber.otmm.stepdefs;

import java.io.IOException;

import com.scholastic.torque.cucumber.ScenarioHook;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
/**
 * 
 * @author chirag.jayswal
 *
 */
public class Hooks extends ScenarioHook {

	@Before
	public void beforeScenario(Scenario scenario) throws IOException {
		super.beforeScenario(scenario);
	}

	@After
	public void afterScenario(Scenario scenario) {
		super.afterScenario(scenario);
	}

}