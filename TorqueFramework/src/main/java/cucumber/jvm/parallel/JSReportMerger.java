package cucumber.jvm.parallel;

import java.io.File;
import java.util.Collection;
import java.util.UUID;

import org.apache.commons.io.FileUtils;

/**
*
* @author Tristan McCarthy
* @see - http://www.opencredo.com/2013/07/02/running-cucumber-jvm-tests-in-parallel/
* @see - https://github.com/tristanmccarthy/Cucumber-JVM-Parallel
*/
public class JSReportMerger {
	private static String reportFileName = "report.js";
	private static String reportImageExtension = "png";

	public static void main(String[] args) throws Throwable {
		File reportDirectory = new File(args[0]);
		if (reportDirectory.exists()) {
			JSReportMerger munger = new JSReportMerger();
			munger.mergeReports(reportDirectory);
		}
	}

	public void mergeReports(File reportDirectory) throws Throwable {
		Collection<File> existingReports = FileUtils.listFiles(reportDirectory,
				new String[] { "js" }, true);

		File mergedReport = null;

		for (File report : existingReports) {
			if (report.getName().equals(reportFileName)) {
				renameEmbededImages(report);

				if (mergedReport == null) {
					FileUtils.copyDirectory(report.getParentFile(),
							reportDirectory);
					mergedReport = new File(reportDirectory, reportFileName);
				} else {
					mergeFiles(mergedReport, report);
				}
			}
		}
	}

	public void mergeFiles(File target, File source) throws Throwable {
		Collection<File> embeddedImages = FileUtils.listFiles(
				source.getParentFile(), new String[] { reportImageExtension },
				true);
		for (File image : embeddedImages) {
			FileUtils.copyFileToDirectory(image, target.getParentFile());
		}

		String targetReport = FileUtils.readFileToString(target);
		String sourceReport = FileUtils.readFileToString(source);

		FileUtils.writeStringToFile(target, targetReport + sourceReport);
	}

	public void renameEmbededImages(File reportFile) throws Throwable {
		File reportDirectory = reportFile.getParentFile();
		Collection<File> embeddedImages = FileUtils.listFiles(reportDirectory,
				new String[] { reportImageExtension }, true);

		String fileAsString = FileUtils.readFileToString(reportFile);

		for (File image : embeddedImages) {
			String curImageName = image.getName();
			String uniqueImageName = UUID.randomUUID().toString() + "."
					+ reportImageExtension;

			image.renameTo(new File(reportDirectory, uniqueImageName));
			fileAsString = fileAsString.replace(curImageName, uniqueImageName);
		}

		FileUtils.writeStringToFile(reportFile, fileAsString);
	}
}