package cucumber.jvm.parallel;

import java.io.File;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * A Cucumber JSON report merger, based on Tristan McCarthy's Cucumber JS Report Merger
 * @author Janusz Kowalczyk
 * @see - http://www.opencredo.com/2013/07/02/running-cucumber-jvm-tests-in-parallel/
 * @see - https://github.com/tristanmccarthy/Cucumber-JVM-Parallel
 */
public class JSONReportMerger {
	private static String reportFileName = "cucumber.json";
	private static String reportImageExtension = "png";
	static Logger log;

	static {
		log = Logger.getLogger(JSONReportMerger.class);
	}

	public static void main(String[] args) throws Throwable {
		File reportDirectory = new File(args[0]);
		if (reportDirectory.exists()) {
			JSONReportMerger munger = new JSONReportMerger();
			munger.mergeReports(reportDirectory);
		}
	}

	public void mergeReports(File reportDirectory) throws Throwable {
		Path targetReportPath = Paths.get(reportDirectory.toString()
				+ File.separator + reportFileName);
		if (Files.exists(targetReportPath, LinkOption.NOFOLLOW_LINKS)) {
			FileUtils.forceDelete(targetReportPath.toFile());
		}

		File mergedReport = null;
		Collection<File> existingReports = FileUtils.listFiles(reportDirectory,
				new String[] { "json" }, true);

		for (File report : existingReports) {
			if (report.getName().equals(reportFileName)) {
				renameEmbededImages(report);
				renameFeatureIDsAndNames(report);

				if (mergedReport == null) {
					FileUtils.copyFileToDirectory(report, reportDirectory);
					mergedReport = new File(reportDirectory, reportFileName);
				} else {
					mergeFiles(mergedReport, report);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void mergeFiles(File target, File source) throws Throwable {

		String targetReport = FileUtils.readFileToString(target);
		String sourceReport = FileUtils.readFileToString(source);

		JSONParser jp = new JSONParser();

		try {
			JSONArray parsedTargetJSON = (JSONArray) jp.parse(targetReport);
			JSONArray parsedSourceJSON = (JSONArray) jp.parse(sourceReport);

			parsedTargetJSON.addAll(parsedSourceJSON);

			Writer writer = new JSONWriter();

			parsedTargetJSON.writeJSONString(writer);

			FileUtils.writeStringToFile(target, writer.toString());
		} catch (ParseException pe) {
			pe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void renameFeatureIDsAndNames(File reportFile) throws Throwable {
		String reportDirName = reportFile.getParentFile().getName();
		String fileAsString = FileUtils.readFileToString(reportFile);
		JSONParser jp = new JSONParser();

		try {
			JSONArray parsedJSON = (JSONArray) jp.parse(fileAsString);

			for (Object o : parsedJSON) {
				JSONObject jo = (JSONObject) o;
				String curFeatureID = jo.get("id").toString();
				String curFeatureName = jo.get("name").toString();
				String curUriName = jo.get("uri").toString();
				String newFeatureID = String.format("%s - %s", reportDirName,
						curFeatureID);
				String newFeatureName = String.format("%s - %s", reportDirName,
						curFeatureName);
				String newUriName = String.format("%s - %s", reportDirName,
						curUriName);

				log.info("Changing feature ID and Name from: " + curFeatureID
						+ " / " + curFeatureName + " to: " + newFeatureID
						+ " / " + newFeatureName);

				jo.put("id", newFeatureID);
				jo.put("name", newFeatureName);
				jo.put("uri", newUriName);
			}
			Writer writer = new JSONWriter();
			parsedJSON.writeJSONString(writer);
			FileUtils.writeStringToFile(reportFile, writer.toString());
		} catch (ParseException pe) {
			pe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void renameEmbededImages(File reportFile) throws Throwable {
		File reportDirectory = reportFile.getParentFile();
		Collection<File> embeddedImages = FileUtils.listFiles(reportDirectory,
				new String[] { reportImageExtension }, true);

		String fileAsString = FileUtils.readFileToString(reportFile);

		for (File image : embeddedImages) {
			String curImageName = image.getName();
			String uniqueImageName = reportDirectory.getName() + "-"
					+ UUID.randomUUID().toString() + "." + reportImageExtension;

			image.renameTo(new File(reportDirectory, uniqueImageName));
			fileAsString = fileAsString.replace(curImageName, uniqueImageName);
		}

		FileUtils.writeStringToFile(reportFile, fileAsString);
	}
}