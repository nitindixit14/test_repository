/**
 * 
 */
package com.scholastic.torque.common;

/**
 * @author chirag.jayswal
 *
 */
public interface TestPage {

	boolean isPageActive();
	public void launchPage();

}
