/**
 * 
 */
package com.scholastic.torque.common;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

/**
 * @author chirag.jayswal
 *
 */
public class FileUtils {
	public static InputStream getResourceAsStream(String resource) {
		return Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
	}

	public static File getFile(String f) {
		URL uri = Thread.currentThread().getContextClassLoader().getResource(f);
		if(null!=uri)
		return new File(uri.getFile());
		
		return new File(f);
	}

}
