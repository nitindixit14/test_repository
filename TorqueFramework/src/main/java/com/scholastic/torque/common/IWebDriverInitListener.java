/**
 * 
 */
package com.scholastic.torque.common;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

/**
 * Implement this interface to perform some tasks before/after drive initialize
 * or before driver going to quite. To register implemented listener set
 * property "webdriver.init.listener" with fully qualified class name which implements this interface.
 * 
 * @author chirag.jayswal
 *
 */
public interface IWebDriverInitListener {
	public static final String KEY = "webdriver.init.listener";

	public Capabilities beforeInit(Capabilities capabilities);

	public WebDriver afterInit(WebDriver driver);

	public void beforeTearDown(WebDriver driver);

}
