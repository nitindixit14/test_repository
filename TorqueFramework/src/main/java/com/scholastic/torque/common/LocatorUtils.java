/**
 * 
 */
package com.scholastic.torque.common;

import org.openqa.selenium.By;

/**
 * @author chirag.jayswal
 *
 */
public class LocatorUtils {

	/**
	 * This method is useful to convert string locator to {@link By}. String
	 * locator must be in format <locator strategy>=<locator value> For example:
	 * xpath=.//a or id=signBtn or css=#signBtn
	 * 
	 * it supports following locator strategies:
	 * <ul>
	 * <li>
	 * xpath
	 * </li>
	 * <li>
	 * name
	 * </li>
	 * <li>
	 * link
	 * </li>
	 * <li>
	 * partiallink
	 * </li>
	 * <li>
	 * id
	 * </li>
	 * <li>
	 * css
	 * </li>
	 * <li>
	 * className
	 * </li>
	 * <li>
	 * tagname
	 * </li>
	 * </ul>
	 * 
	 * @param locator
	 *            string locator or property key holding string locator
	 * @return By object
	 */
	public static By getBy(String locator) {
		locator = TestBaseProvider.getTestBase().getString(locator, locator);
		String[] parts = locator.split("=", 2);
		if (parts.length < 2) {
			throw new RuntimeException(
					"invalid locator:" + locator + " Required '<locator stretgy>=<value>', for example: 'id=userName'");
		}
		String stretegy = parts[0].trim().toLowerCase();
		String loc = parts[1].trim();
		By by = null;
		switch (stretegy) {
		case "xpath":
			by = By.xpath(loc);
			break;
		case "name":
			by = By.name(loc);
			break;
		case "link":
			by = By.linkText(loc);
			break;
		case "partiallink":
			by = By.partialLinkText(loc);
			break;
		case "id":
			by = By.id(loc);
			break;
		case "css":
			by = By.cssSelector(loc);
			break;
		case "className":
			by = By.className(loc);
			break;
		case "tagname":
			by = By.tagName(loc);
			break;
		default:
			throw new RuntimeException("Invalid (or not supported) locator stretegy : " + stretegy);
		}
		return by;
	}

}
