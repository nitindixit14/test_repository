/**
 * 
 */
package com.scholastic.torque.common;

import org.apache.commons.configuration.Configuration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;

import com.scholastic.torque.webdriver.ExtendedElementDecorator;

/**
 * @author chirag.jayswal
 *
 */
public abstract class BaseTestPage<T extends TestPage> implements TestPage {
	protected T parent = null;

	public BaseTestPage() {
		FieldDecorator decorator = new ExtendedElementDecorator(new DefaultElementLocatorFactory(getDriver()));
		PageFactory.initElements(decorator, this);
	}

	protected WebDriver getDriver() {
		return TestBaseProvider.getTestBase().getDriver();
	}

	protected TestBase getTestBase() {
		return TestBaseProvider.getTestBase();
	}

	protected Configuration getContext() {
		return TestBaseProvider.getTestBase().getContext();
	}

	protected abstract void openPage();

	@Override
	public void launchPage() {

		if (isPageActive()) {
			return;
		}
		this.parent = getParent();
		if (parent != null) {
			parent.launchPage();
		}

		openPage();
	}

	public final T getParent() {
		if (this.parent == null) {
			initParent();
		}
		return this.parent;
	}

	@SuppressWarnings("unchecked")
	protected void initParent() {
		try {
			Class<?> class1 = (Class<?>) ((java.lang.reflect.ParameterizedType) super.getClass().getGenericSuperclass())
					.getActualTypeArguments()[0];
			if (!(class1.isInterface()))
				this.parent = (T) ((TestPage) class1.newInstance());
		} catch (Exception e) {
			System.out.println("Unable to init parent class");
		}
	}

	@Override
	public boolean isPageActive() {
		return false;
	}

}
