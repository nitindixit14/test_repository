/**
 * 
 */
package com.scholastic.torque.common;

import static com.scholastic.torque.common.WaitUtils.waitForAttributeMatches;
import static com.scholastic.torque.common.WaitUtils.waitForAttributeNotMatches;
import static com.scholastic.torque.common.WaitUtils.waitForDisabled;
import static com.scholastic.torque.common.WaitUtils.waitForDisplayed;
import static com.scholastic.torque.common.WaitUtils.waitForEnabled;
import static com.scholastic.torque.common.WaitUtils.waitForNotDisplayed;
import static com.scholastic.torque.common.WaitUtils.waitForNotPresent;
import static com.scholastic.torque.common.WaitUtils.waitForNotSelected;
import static com.scholastic.torque.common.WaitUtils.waitForPresent;
import static com.scholastic.torque.common.WaitUtils.waitForSelected;
import static com.scholastic.torque.common.WaitUtils.waitForTextMatches;
import static com.scholastic.torque.common.WaitUtils.waitForTextNotMatches;
import static com.scholastic.torque.common.WaitUtils.waitForValueMatches;
import static com.scholastic.torque.common.WaitUtils.waitForValueNotMatches;

import org.hamcrest.Matcher;
import org.openqa.selenium.WebElement;

import com.scholastic.torque.webdriver.ExtendedElement;

/**
 * This is utility class for assertion. It provides different assertion methods
 * on element. While using this assertion methods you don't need to put
 * additional waits.
 * 
 * @author chirag.jayswal
 *
 */
public class AssertUtils {

	public static void assertNotPresent(ExtendedElement element) {
		try {
			waitForNotPresent(element);
		} catch (Exception e) {

		}
		if (element.isPresent())
			throw new AssertionError("Expected element " + element + " should not present : but found present");

	}

	public static void assertDisplayed(WebElement element) {
		try {
			waitForDisplayed(element);
		} catch (Exception e) {

		}
		if (!element.isDisplayed()) {
			throw new AssertionError("Expected element " + element + " should be visible : but found not visible");
		}
	}

	public static void assertNotDisplayed(WebElement element) {
		try {
			waitForNotDisplayed(element);
			;
		} catch (Exception e) {

		}
		if (element.isDisplayed()) {
			throw new AssertionError("Expected element " + element + " should not be visible : but found visible");
		}
	}

	public static void assertEnabled(WebElement element) {
		try {
			waitForEnabled(element);
			;
		} catch (Exception e) {

		}
		if (!element.isEnabled()) {
			throw new AssertionError("Expected element " + element + " should be Enabled : but found Disabled");
		}
	}

	public static void assertDisabled(WebElement element) {
		try {
			waitForDisabled(element);
			;
		} catch (Exception e) {

		}
		if (element.isEnabled()) {
			throw new AssertionError("Expected element " + element + " should be Disabled : but found Enabled");
		}
	}

	public static void assertSelected(WebElement element) {
		try {
			waitForSelected(element);
			;
		} catch (Exception e) {

		}
		if (!element.isSelected()) {
			throw new AssertionError("Expected element " + element + " should not be Selected : but found Selected");
		}
	}

	public static void assertNotSelected(WebElement element) {
		try {
			waitForNotSelected(element);
			;
		} catch (Exception e) {

		}
		if (element.isSelected()) {
			throw new AssertionError("Expected element " + element + " should be Selected : but found not Selected");
		}
	}

	public static void assertAttributeMatches(WebElement element, String attrName, Matcher<String> val) {
		try {
			waitForAttributeMatches(element, attrName, val);
			;
		} catch (Exception e) {

		}
		String actualVal = element.getAttribute(attrName);
		if (!val.matches(actualVal)) {
			String msg = String.format("Expected %s attribute %s value should be [%s] : but found [%s] ", element,
					attrName, val, actualVal);
			throw new AssertionError(msg);
		}
	}

	public static void assertAttributeNotMatches(WebElement element, String attrName, Matcher<String> val) {
		try {
			waitForAttributeNotMatches(element, attrName, val);
			;
		} catch (Exception e) {

		}
		String actualVal = element.getAttribute(attrName);
		if (val.matches(actualVal)) {
			String msg = String.format("Expected %s attribute %s value should not be [%s] : but found [%s] ", element,
					attrName, val, actualVal);
			throw new AssertionError(msg);
		}
	}

	/**
	 * Examples:<br>
	 * <code>
	 * 		assertTextMatches(userHomePage.getPageInfoLabels(), containsString("My Projects"));
			assertTextMatches(userHomePage.getUserName(), equalToIgnoringCase(firstName+" "+lastName));
			assertTextMatches(userHomePage.getPageInfoLabels(), containsIgnoreCase("My Projects"));

	 * </code>
	 * 
	 * @param element
	 * @param val
	 *            one of the hamcrest matcher
	 */
	public static void assertTextMatches(WebElement element, Matcher<String> val) {
		try {
			waitForTextMatches(element, val);
		} catch (Exception e) {

		}
		String actualVal = element.getText();
		if (!val.matches(actualVal)) {
			String msg = String.format("Expected %s text should be [%s] : but found [%s] ", element, val, actualVal);
			throw new AssertionError(msg);
		}
	}

	public static void assertTextNotMatches(WebElement element, Matcher<String> val) {
		try {
			waitForTextNotMatches(element, val);
		} catch (Exception e) {

		}
		String actualVal = element.getText();
		if (val.matches(actualVal)) {
			String msg = String.format("Expected %s text should not be [%s] : but found [%s] ", element, val,
					actualVal);
			throw new AssertionError(msg);
		}
	}

	public static void assertValueMatches(WebElement element, Matcher<String> val) {
		try {
			waitForValueMatches(element, val);
		} catch (Exception e) {

		}
		String actualVal = element.getText();
		if (!val.matches(actualVal)) {
			String msg = String.format("Expected %s text should be [%s] : but found [%s] ", element, val, actualVal);
			throw new AssertionError(msg);
		}
	}

	public static void assertValueNotMatches(WebElement element, Matcher<String> val) {
		try {
			waitForValueNotMatches(element, val);
		} catch (Exception e) {

		}
		String actualVal = element.getText();
		if (val.matches(actualVal)) {
			String msg = String.format("Expected %s text should not be [%s] : but found [%s] ", element, val,
					actualVal);
			throw new AssertionError(msg);
		}
	}

	public static void assertPresent(ExtendedElement element) {
		try {
			waitForPresent(element);
		} catch (Exception e) {

		}
		if (!element.isPresent())
			throw new AssertionError("Expected element " + element + " should present : but found not present");

	}
}
