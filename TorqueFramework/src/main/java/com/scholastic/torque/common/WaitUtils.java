/**
 * 
 */
package com.scholastic.torque.common;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Matcher;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;
import com.scholastic.torque.webdriver.ExtendedElement;

/**
 * This utility class provides wait methods for different conditions. The
 * default time out for wait is 30 seconds and can be set through
 * "wait.timeout.sec" property.
 * 
 * @author chirag.jayswal
 *
 */
public class WaitUtils {
	public static final long TIMEOUT_SEC = Long.valueOf(TestBaseProvider.getTestBase().getString("wait.timeout.sec", "30"));

	public static void waitForPresent(final ExtendedElement element) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					return ((ExtendedElement) element).isPresent();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s to be present", element);
			}
		});
	}

	public static void waitForNotPresent(final ExtendedElement element) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					return !((ExtendedElement) element).isPresent();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s to be present", element);
			}
		});
	}

	public static void waitForDisplayed(final WebElement element) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					return element.isDisplayed();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s to be visible", element);
			}
		});
	}

	public static void waitForNotDisplayed(final WebElement element) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					return !element.isDisplayed();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s not to be visible", element);
			}
		});

	}

	public static void waitForSelected(final WebElement element) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					return element.isSelected();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s to be Selected", element);
			}
		});

	}

	public static void waitForNotSelected(final WebElement element) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					return !element.isSelected();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s not to be Selected", element);
			}
		});

	}

	public static void waitForEnabled(final WebElement element) {

		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					return element.isEnabled();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s to be Enabled", element);
			}
		});
	}

	public static void waitForDisabled(final WebElement element) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					return !element.isEnabled();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s to be disabled", element);
			}
		});

	}

	public static void waitForTextMatches(final WebElement element, final Matcher<String> matcher) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					String elementText = element.getText();
					return matcher.matches(elementText);
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("element ('%s') text %s", element, matcher.toString());
			}
		});
	}

	public static void waitForTextNotMatches(final WebElement element, final Matcher<String> matcher) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					String elementText = element.getText();
					return !matcher.matches(elementText);
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("element ('%s') text not %s", element, matcher.toString());
			}
		});
	}

	public static void waitForValueMatches(final WebElement element, final Matcher<String> matcher) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					String elementText = element.getAttribute("value");
					return matcher.matches(elementText);
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("element ('%s') text %s", element, matcher.toString());
			}
		});
	}

	public static void waitForValueNotMatches(final WebElement element, final Matcher<String> matcher) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					String elementText = element.getAttribute("value");
					return !matcher.matches(elementText);
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("element ('%s') text not %s", element, matcher.toString());
			}
		});
	}

	public static void waitForAttributeMatches(final WebElement element, final String attribute,
			final Matcher<String> matcher) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					String attributeValue = element.getAttribute(attribute);
					return matcher.matches(attributeValue);
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("element ('%s') attribute %s value %s", element, attribute, matcher.toString());
			}
		});
	}

	public static void waitForAttributeNotMatches(final WebElement element, final String attribute,
			final Matcher<String> matcher) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebElement element) {
				try {
					String attributeValue = element.getAttribute(attribute);
					return !matcher.matches(attributeValue);
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("element ('%s') attribute %s value not %s", element, attribute,
						matcher.toString());
			}
		});
	}

	public static void waitForPresent(final By by) {
		waitForPresent(TestBaseProvider.getTestBase().getDriver(), by);
	}

	public static void waitForPresent(String locator) {
		waitForPresent(LocatorUtils.getBy(locator));
	}

	public static void waitForPresent(SearchContext context, String locator) {
		waitForPresent(context, LocatorUtils.getBy(locator));
	}

	public static void waitForPresent(SearchContext context, final By by) {
		FluentWait<SearchContext> wait = new FluentWait<SearchContext>(context)
				.withTimeout(TIMEOUT_SEC, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS);
		wait.until(new PresentCondition<Boolean>() {
			@Override
			public Boolean apply(SearchContext context) {
				try {

					return context.findElements(by).size() > 0;
				} catch (Exception e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("element ('%s') to be present", by);
			}
		});
	}

	protected WebElement waitForPresent(final String locator, long timeout) {
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), timeout);

		WebElement ele = wait.ignoring(StaleElementReferenceException.class)
				.until(ExpectedConditions.presenceOfElementLocated(LocatorUtils.getBy(locator)));

		return ele;
	}

	private interface ElementExpectedCondition<T> extends Function<WebElement, T> {
	}

	private interface PresentCondition<T> extends Function<SearchContext, T> {
	}

	private static Wait<? extends WebElement> wait(WebElement element) {
		return new FluentWait<WebElement>(element).withTimeout(TIMEOUT_SEC, TimeUnit.SECONDS).pollingEvery(1,
				TimeUnit.SECONDS);
	}

}
