package com.scholastic.torque.common;

import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class StringMatchers {

	public static ContainsIngnoringCase containsIgnoreCase(String str) {
		return new ContainsIngnoringCase(str);
	}

	/**
	 * 
	 * @param words :words separated by space or array of words/string
	 * @return
	 */
	public static ContainsAnyIngnoringCase containsAnyIgnoreCase(String... words) {
		return new ContainsAnyIngnoringCase(words);
	}

	public static class ContainsIngnoringCase extends BaseMatcher<String> {
		private final String mathchWith;

		public ContainsIngnoringCase(String regex) {
			this.mathchWith = regex;
		}

		public boolean matches(Object o) {

			return StringUtils.containsIgnoreCase((String) o, mathchWith);

		}

		public void describeTo(Description description) {
			description.appendText("Contains Ingnoring Case " + mathchWith);
		}

	}
	
	public static class ContainsAnyIngnoringCase extends BaseMatcher<String[]> {
		private String[] mathchWith;

		public ContainsAnyIngnoringCase(String... regex) {
			this.mathchWith = regex;
		}

		public boolean matches(Object o) {
			if(mathchWith.length==1){
				mathchWith = mathchWith[0].split(" ");
			}
			for(String s : mathchWith){
				if(!StringUtils.containsIgnoreCase((String) o, s))
					return false;
			}
			return true;

		}

		public void describeTo(Description description) {
			description.appendText("Contains  Ingnoring Case Any of " + mathchWith);
		}

	}
}
