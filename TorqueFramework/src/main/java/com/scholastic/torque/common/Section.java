/**
 * 
 */
package com.scholastic.torque.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.WrapsElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;

import com.scholastic.torque.webdriver.ExtendedElementDecorator;

/**
 * This is base class for creating custom components. It allows
 * {@link WebElement} annotated with {@link FindBy}, where such element
 * automatically becomes child element.
 * 
 * @author chirag.jayswal
 *
 */
public class Section implements SearchContext, WrapsElement {
	private By by;

	private SearchContext searchContext;

	private SearchContext element;

	public Section(By by) {
		this(by, TestBaseProvider.getTestBase().getDriver());
	}

	public Section(By by, SearchContext parent) {
		this.by = by;
		FieldDecorator decorator = new ExtendedElementDecorator(new DefaultElementLocatorFactory(this));
		PageFactory.initElements(decorator, this);
		this.searchContext = parent;
	}

	public Section(String loc) {
		this(LocatorUtils.getBy(loc));
	}

	public Section(WebElement element) {
		this.element = element;
		FieldDecorator decorator = new ExtendedElementDecorator(new DefaultElementLocatorFactory(this));
		PageFactory.initElements(decorator, this);
	}

	public Section(String loc, SearchContext parent) {
		this.by = LocatorUtils.getBy(loc);
		FieldDecorator decorator = new ExtendedElementDecorator(new DefaultElementLocatorFactory(this));
		PageFactory.initElements(decorator, this);
		this.searchContext = parent;
	}

	@Override
	public List<WebElement> findElements(By by) {
		waitForPresent();
		return getWrappedElement().findElements(by);
	}

	@Override
	public WebElement findElement(By by) {
		waitForPresent();
		WebElement ele = getWrappedElement();
		return ele.findElement(by);
	}

	public WebElement getWrappedElement() {
		return element != null ? (WebElement) element : searchContext.findElement(by);
	}

	public boolean isPresent() {
		return element != null || findElements(by).size() > 0;
	}

	public void waitForPresent() {
		if (element == null)
			WaitUtils.waitForPresent(searchContext, by);
	}

}
