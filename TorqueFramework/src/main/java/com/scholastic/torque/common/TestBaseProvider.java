/**
 * 
 */
package com.scholastic.torque.common;

import java.util.Iterator;
import java.util.Vector;

/**
 * @author chirag.jayswal
 *
 */
public class TestBaseProvider extends ThreadLocal<TestBase> {

	private final Vector<TestBase> lst = new Vector<TestBase>();
	private static final TestBaseProvider INSTANCE = new TestBaseProvider();

	private TestBaseProvider() {
	}

	@Override
	protected TestBase initialValue() {
		TestBase stb = new TestBase();
		lst.add(stb);
		return stb;
	}

	@Override
	public void remove() {
		get().tearDown();
		super.remove();
	}

	@Override
	public void set(TestBase value) {
		if (null == value) {
			remove();
		} else {
			super.set(value);
		}
	}

	public static TestBase getTestBase() {
		return INSTANCE.get();
	}
	
	static {

		Runtime.getRuntime().addShutdownHook(new Thread() {
			
			@Override
			public void run() {
				Iterator<TestBase> iter = INSTANCE.lst.iterator();
				while (iter.hasNext()) {
					iter.next().tearDown();
					iter.remove();
				}	
			}

		});
	}
}
