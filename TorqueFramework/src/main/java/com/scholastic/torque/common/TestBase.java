/**
 * 
 */
package com.scholastic.torque.common;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationMap;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.torque.automation.core.TestDataUtils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * This is a base class that holds driver and other information that required
 * during test execution. In order to get thread-safe test base object use
 * {@link TestBaseProvider#getTestBase()}
 * 
 * @author chirag.jayswal
 *
 */
public class TestBase {
	public static final String REMOTE_DRIVER_URL = "remote.driver.url";
	public static final String WEBDRIVER_CAPABILITIES = "driver.capabilities";
	private static final String WEBDRIVER_NAME = "driver.name";
	private static final String RESOURCES_KEY = "resources";
	private static final String TESTDATA_KEY = "testdata";
	private static final String LOCATOR_REPO_KEY = "locator.repo";
	private static final String CHROME_PATH = "webdriver.chrome.driver";
	// private final Map<String, Object> context = new TreeMap<String,
	// Object>(String.CASE_INSENSITIVE_ORDER);
	
	private List<IWebDriverInitListener> driverListeners;
	private WebDriver driver;
	private PropertiesConfiguration context;
	private XMLConfiguration testData;
	private Configuration specificTestData;

	TestBase() {
		loadProperties();
	}

	public WebDriver getDriver() {
		if (null == driver)
			initDriver();
		return driver;
	}

	public void tearDown() {
		System.out.println("Tearing down thread " + Thread.currentThread().getId());

		if (null != driver) {
			beforeTearDown(driverListeners, driver);
			driver.quit();
			driver = null;
		}
	}

	public Configuration getContext() {
		return context;
	}

	public String getString(String key) {
		return context.getString(key);
	}

	public String getString(String key, String defVal) {

		return context.getString(key, defVal);
	}

	public void addAll(Map<String, String> props) {
		try {
			props.entrySet().removeAll(System.getProperties().entrySet());
		} catch (Exception e) {
		}
		Iterator<String> iterator = props.keySet().iterator();

		while (iterator.hasNext()) {
			String key = iterator.next();
			String val = props.get(key);
			this.context.clearProperty(key);
			this.context.setProperty(key, val);
			if ((key.equalsIgnoreCase(RESOURCES_KEY) || key.equalsIgnoreCase(LOCATOR_REPO_KEY))
					&& StringUtils.isNotBlank(val)) {
				for (String resouces : val.split(",")) {
					loadProperties(resouces);
				}
			}
		}
		overrideSystemProperites();
	}

	public Configuration getTestDataFromXml() {
		if (null == testData) {
			testData = new XMLConfiguration();
			testData.setExpressionEngine(new XPathExpressionEngine());
			loadTestData();
		}
		return testData;
	}

	private void initDriver() {
		try {
			driverListeners = getWebDriverInitListeneres();
			String chromePath = context.getString(CHROME_PATH, "servers/chromedriver.exe");
			System.setProperty(CHROME_PATH, chromePath);

			WEBDRIVER webdriver = WEBDRIVER.valueOf(getString(WEBDRIVER_NAME, "Firefox"));
			Capabilities desiredCapabilities = getDesiredCapabilities(webdriver.getCapabilites(), webdriver.name());
			desiredCapabilities = beforeInitDriver(desiredCapabilities, driverListeners);

			boolean remote = context.containsKey(REMOTE_DRIVER_URL);
			if (remote) {

				String URL = context.getString(REMOTE_DRIVER_URL);
				driver = webdriver.getDriver(new java.net.URL(URL), desiredCapabilities);

			} else {

				driver = webdriver.getDriver(desiredCapabilities);

			}

			driver = afterInitDriver(driverListeners, driver);

		} catch (Exception e) {
			throw new RuntimeException("Unable to create local driver", e);
		}
	}

	@SuppressWarnings("unchecked")
	private DesiredCapabilities getDesiredCapabilities(DesiredCapabilities capabilities, String driver) {
		if (!context.containsKey(WEBDRIVER_CAPABILITIES))
			return capabilities;
		Map<String, Object> desiredCapabilities = new HashMap<String, Object>(capabilities.asMap());
		Gson gson = new GsonBuilder().create();
		Map<String, Object> extraCapabilities = gson.fromJson(context.getString(WEBDRIVER_CAPABILITIES, "{}"),
				Map.class);
		ConfigurationMap driveCap = new ConfigurationMap(context.subset(driver));

		desiredCapabilities.putAll(extraCapabilities);
		desiredCapabilities.putAll(driveCap);
		return new DesiredCapabilities(desiredCapabilities);
	}

	private void loadProperties(String filename) {
		File file = FileUtils.getFile(filename);
		loadProperties(file);
	}

	private void loadProperties(File file) {
		if (file.exists() && file.isFile()) {
			System.out.println("Loading: " + file.getAbsolutePath());
			try {
				if (file.getName().endsWith(".properties")) {
					PropertiesConfiguration properties = new PropertiesConfiguration();
					properties.load(file);
					context.copy(properties);
					properties.clear();
				} else {
					XMLConfiguration xmlProps = new XMLConfiguration(file);
					context.copy(xmlProps);
					xmlProps.clear();
				}
			} catch (ConfigurationException e) {
				e.printStackTrace();
			}
		} else if (file.isDirectory()) {
			System.out.println("Loading resources from: " + file.getAbsolutePath());

			String[] files = file.list(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					boolean isDir = new File(dir, name).isDirectory();
					return isDir || name.endsWith(".properties") || name.endsWith(".xml");
				}
			});
			for (String f : files) {
				loadProperties(new File(file, f));
			}

		}
	}

	private void loadProperties() {
		AbstractConfiguration.setDefaultListDelimiter(';');
		context = new PropertiesConfiguration();
		String propFile = System.getProperty("properties-file", "application.properties");
		loadProperties(propFile);
		overrideSystemProperites();
		loadResources(RESOURCES_KEY);
		loadResources(LOCATOR_REPO_KEY);
		overrideSystemProperites();
	}

	private void loadResources(String key) {
		String[] envResoucesFile = context.getStringArray(key);

		if (null!=envResoucesFile) {
			for (String resouces : envResoucesFile) {
				loadProperties(resouces);
			}
		}
	}

	private void loadTestDataFromFile(File file) {
		System.out.println("loading test data from: " + file.getAbsolutePath());
		if (file.exists() && file.isFile()) {
			try {
				if (file.getName().endsWith(".xml")) {
					((XMLConfiguration) testData).load(file);
				}
			} catch (ConfigurationException e) {
				e.printStackTrace();
			}
		} else if (file.isDirectory()) {
			String[] files = file.list(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					boolean isDir = new File(dir, name).isDirectory();
					return isDir || name.endsWith(".xml");
				}
			});
			for (String f : files) {
				loadTestDataFromFile(new File(file, f));
			}
		}
	}

	private void loadTestDataFromFile(String filename) {
		loadTestDataFromFile(FileUtils.getFile(filename));
	}

	private void overrideSystemProperites() {
		Iterator<Object> iterator1 = System.getProperties().keySet().iterator();

		while (iterator1.hasNext()) {
			String key = (String) iterator1.next();
			String val = System.getProperties().getProperty(String.valueOf(key));
			this.context.clearProperty(key);
			this.context.setProperty(key, val);
			if ((key.equalsIgnoreCase(RESOURCES_KEY) || key.equalsIgnoreCase(TESTDATA_KEY)
					|| key.equalsIgnoreCase(LOCATOR_REPO_KEY)) && StringUtils.isNotBlank(val)) {
				for (String resouces : val.split(";")) {
					loadProperties(resouces);
				}
			}
		}

	}

	private void loadTestData() {
		String[] envResoucesFile = context.getStringArray(TESTDATA_KEY);
		if(null== envResoucesFile || envResoucesFile.length==0){
			envResoucesFile = context.getStringArray(RESOURCES_KEY);
		}
		if (null!=envResoucesFile) {
			for (String resouces : envResoucesFile) {
				loadTestDataFromFile(resouces);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private List<IWebDriverInitListener> getWebDriverInitListeneres() {
		List<IWebDriverInitListener> listeners = new ArrayList<IWebDriverInitListener>();
		String[] listenersStrs = context.getStringArray(IWebDriverInitListener.KEY);
		for (String listener : listenersStrs) {
			try {
				Class<IWebDriverInitListener> cl = (Class<IWebDriverInitListener>) Class.forName(listener);
				listeners.add(cl.newInstance());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return listeners;
	}

	private Capabilities beforeInitDriver(Capabilities capabilities, List<IWebDriverInitListener> listeners) {
		for (IWebDriverInitListener listener : listeners) {
			capabilities = listener.beforeInit(capabilities);
		}
		return capabilities;
	}

	private WebDriver afterInitDriver(List<IWebDriverInitListener> listeners, WebDriver driver) {
		for (IWebDriverInitListener listener : listeners) {
			driver = listener.afterInit(driver);
		}
		return driver;
	}

	private void beforeTearDown(List<IWebDriverInitListener> listeners, WebDriver driver) {
		for (IWebDriverInitListener listener : listeners) {
			listener.beforeTearDown(driver);
		}
	}

	private enum WEBDRIVER {
		Firefox(FirefoxDriver.class, DesiredCapabilities.firefox()), IE(InternetExplorerDriver.class,
				DesiredCapabilities.internetExplorer()),Edge(EdgeDriver.class, DesiredCapabilities.edge()), Chrome(ChromeDriver.class,
						DesiredCapabilities.chrome()), Safari(SafariDriver.class,
								DesiredCapabilities.safari()), Android(null, DesiredCapabilities.android(),
										AndroidDriver.class), IOs(null, DesiredCapabilities.iphone(),
												IOSDriver.class), Remote(null, new DesiredCapabilities(),
														RemoteWebDriver.class);

		private Class<? extends WebDriver> driverClass, remoteDriverClass;
		private DesiredCapabilities capabilites;

		private WEBDRIVER(Class<? extends WebDriver> driverClass, DesiredCapabilities cap,
				Class<? extends WebDriver> remotedriverClass) {
			this.driverClass = driverClass;
			this.capabilites = cap;
			this.remoteDriverClass = remotedriverClass;
		}

		private WEBDRIVER(Class<? extends WebDriver> driverClass, DesiredCapabilities cap) {
			this(driverClass, cap, RemoteWebDriver.class);
		}

		public DesiredCapabilities getCapabilites() {
			return capabilites;
		}

		public WebDriver getDriver(Capabilities desiredCapabilities) {
			try {
				Constructor<? extends WebDriver> con = driverClass.getConstructor(Capabilities.class);
				con.setAccessible(true);
				return con.newInstance(desiredCapabilities);
			} catch (Exception e) {
				System.err.println("desired capabilities will be ignored.");
			}
			try {
				return driverClass.newInstance();
			} catch (Exception e) {
				throw new WebDriverException("Unalbe to create webdriver", e);
			}
		}

		public WebDriver getDriver(URL url, Capabilities desiredCapabilities) {
			try {
				Constructor<? extends WebDriver> con = remoteDriverClass.getConstructor(URL.class, Capabilities.class);
				con.setAccessible(true);
				return con.newInstance(url, desiredCapabilities);
			} catch (Exception e) {
				throw new WebDriverException("Unalbe to create remote webdriver", e);
			}

		}
	}

	public void setTestDataFromXml(String testCaseId) {
		specificTestData = TestDataUtils.getTestDataFromXml(testCaseId);
	}

	public void setTestDataFromTDM(String jsonString) {
		specificTestData = new PropertiesConfiguration();
		JSONObject jsonObj = new JSONObject(jsonString);
		Iterator<String> keys = jsonObj.keys();
		while (keys.hasNext()) {
			String key = keys.next();

			specificTestData.addProperty(key, jsonObj.get(key));
		}
	}

	public Configuration getTestData() {
		return specificTestData;
	}
}
