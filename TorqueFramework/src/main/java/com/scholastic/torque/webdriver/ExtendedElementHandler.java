/**
 * 
 */
package com.scholastic.torque.webdriver;

import static com.scholastic.torque.common.WaitUtils.waitForPresent;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;
/**
 * @author chirag.jayswal
 *
 */
public class ExtendedElementHandler implements InvocationHandler {
	private final ElementLocator locator;

	public ExtendedElementHandler(ElementLocator locator) {
		this.locator = locator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object,
	 * java.lang.reflect.Method, java.lang.Object[])
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] objects) throws Throwable {
		WebElement element;
		try {
			element = locator.findElement();

		} catch (NoSuchElementException e) {
			if ("isPresent".equalsIgnoreCase(method.getName())) {
				return false;
			}

			if ("toString".equals(method.getName())) {
				return locator.toString();
			} 
				waitForPresent((ExtendedElement) proxy);
				element = locator.findElement();
		}

		if ("isPresent".equalsIgnoreCase(method.getName())) {
			return true;
		}
		if ("getWrappedElement".equals(method.getName())) {
			return element;
		}

		try {
			return method.invoke(element, objects);
		} catch (InvocationTargetException e) {
			throw e.getCause();
		}
	}
}
