/**
 * 
 */
package com.scholastic.torque.webdriver;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.internal.WrapsElement;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementListHandler;

/**
 * @author chirag.jayswal
 *
 */
public class ExtendedElementDecorator extends DefaultFieldDecorator {

	public ExtendedElementDecorator(ElementLocatorFactory factory) {
		super(factory);
	}

	public WebElement proxyForLocator(ClassLoader loader, ElementLocator locator) {
		InvocationHandler handler = new ExtendedElementHandler(locator);

		WebElement proxy;
		proxy = (WebElement) Proxy.newProxyInstance(loader,
				new Class[] { ExtendedElement.class, WrapsElement.class, Locatable.class }, handler);
		return proxy;
	}

	@SuppressWarnings("unchecked")
	protected List<WebElement> proxyForListLocator(ClassLoader loader, ElementLocator locator) {
		InvocationHandler handler = new LocatingElementListHandler(locator);

		List<WebElement> proxy;
		proxy = (List<WebElement>) Proxy.newProxyInstance(loader, new Class[] { List.class }, handler);
		return proxy;
	}

}
