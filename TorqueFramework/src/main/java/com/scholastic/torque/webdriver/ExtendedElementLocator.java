/**
 * 
 */
package com.scholastic.torque.webdriver;

import java.lang.reflect.Field;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.support.pagefactory.DefaultElementLocator;

/**
 * @author chirag.jayswal
 *
 */
public class ExtendedElementLocator extends DefaultElementLocator {

	public ExtendedElementLocator(SearchContext searchContext, Field field) {
		super(searchContext, field);
	}

	
}
