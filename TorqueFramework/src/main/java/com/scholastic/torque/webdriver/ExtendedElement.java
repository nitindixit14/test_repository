/**
 * 
 */
package com.scholastic.torque.webdriver;

import org.openqa.selenium.WebElement;

/**
 * @author chirag.jayswal
 *
 */
public interface ExtendedElement extends WebElement {
	public boolean isPresent();
}
