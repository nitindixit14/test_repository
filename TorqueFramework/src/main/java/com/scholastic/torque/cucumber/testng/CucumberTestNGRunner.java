package com.scholastic.torque.cucumber.testng;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.testng.ITestContext;

import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.cucumber.ScenarioHook;

import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.FeatureResultListener;
import cucumber.api.testng.TestNgReporter;
import cucumber.runtime.ClassFinder;
import cucumber.runtime.CucumberException;
import cucumber.runtime.Runtime;
import cucumber.runtime.RuntimeOptions;
import cucumber.runtime.RuntimeOptionsFactory;
import cucumber.runtime.formatter.PluginFactory;
import cucumber.runtime.io.MultiLoader;
import cucumber.runtime.io.ResourceLoader;
import cucumber.runtime.io.ResourceLoaderClassFinder;
import cucumber.runtime.model.CucumberFeature;
import gherkin.formatter.Formatter;

public class CucumberTestNGRunner {

	private Runtime runtime;
	private RuntimeOptions runtimeOptions;
	private ResourceLoader resourceLoader;
	private FeatureResultListener resultListener;
	private ClassLoader classLoader;

	/**
	 * Bootstrap the cucumber runtime
	 *
	 * @param clazz
	 *            Which has the cucumber.api.CucumberOptions and
	 *            org.testng.annotations.Test annotations
	 */

	public CucumberTestNGRunner(ITestContext context, Class<?> clazz) {
		TestBaseProvider.getTestBase().addAll(context.getCurrentXmlTest().getAllParameters());
		Configuration tcontext = TestBaseProvider.getTestBase().getContext();

		String testName = context.getCurrentXmlTest().getName();
		String outputDir = System.getProperty("outputdir") + "/" + testName;
		File resultDir = new File(outputDir);
		resultDir.mkdirs();

		this.classLoader = clazz.getClassLoader();
		resourceLoader = new MultiLoader(classLoader);

		RuntimeOptionsFactory runtimeOptionsFactory = new RuntimeOptionsFactory(clazz);
		runtimeOptions = runtimeOptionsFactory.create();

		final PluginFactory pluginFactory = new PluginFactory();
		String plugins[] = { "pretty", "html:" + outputDir, "json:" + outputDir + "/cucumber.json",
				"junit:" + outputDir + "/cucumber.xml" };
		for (String plugin : plugins) {
			Object pluginObj = pluginFactory.create(plugin.trim());
			runtimeOptions.addPlugin(pluginObj);
		}

		TestNgReporter reporter = new TestNgReporter(System.out);
		runtimeOptions.addPlugin(reporter);

		if (tcontext.containsKey("features")) {
			System.out.println(tcontext.getStringArray("features"));
			String[] featurPaths = (tcontext.getStringArray("features"));
			runtimeOptions.getFeaturePaths().addAll(Arrays.asList(featurPaths));
		}

		if (tcontext.containsKey("glue")) {
			String[] glues = (tcontext.getStringArray("glue"));
			runtimeOptions.getGlue().addAll(Arrays.asList(glues));
		}

		runtimeOptions.getGlue().add("classpath:"+ScenarioHook.class.getCanonicalName());
		if (tcontext.containsKey("tags")) {
			String[] tags = (tcontext.getStringArray("tags"));
			runtimeOptions.getFilters().addAll(Arrays.asList(tags));
		}

		ClassFinder classFinder = new ResourceLoaderClassFinder(resourceLoader, classLoader);
		resultListener = new FeatureResultListener(runtimeOptions.reporter(classLoader), runtimeOptions.isStrict());
		runtime = new Runtime(resourceLoader, classFinder, classLoader, runtimeOptions);
	}

	/**
	 * Run the Cucumber features
	 */
	public void runCukes() {
		for (CucumberFeature cucumberFeature : getFeatures()) {
			cucumberFeature.run(runtimeOptions.formatter(classLoader), resultListener, runtime);
		}
		finish();
		if (!resultListener.isPassed()) {
			throw new CucumberException(resultListener.getFirstError());
		}
	}

	public void runCucumber(CucumberFeature cucumberFeature) {
		resultListener.startFeature();
		cucumberFeature.run(runtimeOptions.formatter(classLoader), resultListener, runtime);

		if (!resultListener.isPassed()) {
			throw new CucumberException(resultListener.getFirstError());
		}
	}

	public void finish() {
		Formatter formatter = runtimeOptions.formatter(classLoader);

		formatter.done();
		formatter.close();
		runtime.printSummary();
	}

	/**
	 * @return List of detected cucumber features
	 */
	public List<CucumberFeature> getFeatures() {
		return runtimeOptions.cucumberFeatures(resourceLoader);
	}

	/**
	 * @return returns the cucumber features as a two dimensional array of
	 *         {@link CucumberFeatureWrapper} objects.
	 */
	public Object[][] provideFeatures() {
		List<CucumberFeature> features = getFeatures();
		List<Object[]> featuresList = new ArrayList<Object[]>(features.size());
		for (CucumberFeature feature : features) {
			featuresList.add(new Object[] { new CucumberFeatureWrapper(feature) });
		}
		return featuresList.toArray(new Object[][] {});
	}
}
