package com.scholastic.torque.cucumber;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.gargoylesoftware.htmlunit.OnbeforeunloadHandler;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * This class can be extended for hook. Override method and call super method in
 * it. Place @ {@link Before} on {@link #beforeScenario(Scenario)} and @
 * {@link After} {@link #afterScenario(Scenario)}
 * 
 * @author chirag.jayswal
 *
 */
public class ScenarioHook {

	/**
	 * Delete all cookies at the start of each scenario to avoid shared state
	 * between tests
	 */
	public void beforeScenario(Scenario scenario) throws IOException {

		TestBase testBase = TestBaseProvider.getTestBase();
		testBase.getContext().setProperty("current.scenario", scenario);
		testBase.getDriver().manage().deleteAllCookies();
		testBase.getDriver().manage().window().maximize();
	}

	/**
	 * Embed a screenshot in test report if test is marked as failed or
	 * configured to always capture
	 */
	public void afterScenario(Scenario scenario) {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		boolean alwaysCaptureScreenshot = TestBaseProvider.getTestBase().getContext()
				.getBoolean("always.capture.screenshot", true);
		if (alwaysCaptureScreenshot || scenario.isFailed()) {
			try {
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (WebDriverException e) {
				System.err.println(e.getMessage());
			}
		}
		TestBaseProvider.getTestBase().tearDown();
	}
}