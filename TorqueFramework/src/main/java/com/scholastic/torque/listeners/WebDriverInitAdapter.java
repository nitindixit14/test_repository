/**
 * 
 */
package com.scholastic.torque.listeners;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

import com.scholastic.torque.common.IWebDriverInitListener;

/**
 * Adapter class for {@link IWebDriverInitListener}. Extend this class if you
 * don't want to implement all of the methods from
 * {@link IWebDriverInitListener}
 * 
 * @author chirag.jayswal
 *
 */
public class WebDriverInitAdapter implements IWebDriverInitListener {

	@Override
	public Capabilities beforeInit(Capabilities capabilities) {
		return capabilities;
	}

	@Override
	public WebDriver afterInit(WebDriver driver) {
		return driver;
	}

	@Override
	public void beforeTearDown(WebDriver driver) {

	}

}
