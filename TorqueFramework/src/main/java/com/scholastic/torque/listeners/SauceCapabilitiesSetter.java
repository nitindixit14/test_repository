package com.scholastic.torque.listeners;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.gson.Gson;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.Scenario;

/**
 * This listener assumes {@link Scenario} object is available in context with
 * key "current.scenario". Using "current.scenario" property it will set sauce
 * specific capabilities "name", "tags". If "BUILD_ID" property provided it will
 * set "build" as well.
 * 
 * @author chirag.jayswal
 *
 */
public class SauceCapabilitiesSetter extends WebDriverInitAdapter {

	@Override
	public Capabilities beforeInit(Capabilities capabilities) {
		Configuration config = TestBaseProvider.getTestBase().getContext();
		Object scenario = config.getProperty("current.scenario");
		if (null != scenario) {
			Map<String, String> capMap = new HashMap<String, String>();
			capMap.put("name", ((Scenario) scenario).getName());
			String tags = new Gson().toJson(((Scenario) scenario).getSourceTagNames());
			capMap.put("tags", tags);
			capMap.put("build", config.getString("BUILD_ID", System.getenv("BUILD_ID")));
			if(config.containsKey("tunnelIdentifier")){
				capMap.put("tunnelIdentifier", config.getString("tunnelIdentifier"));
			}
			DesiredCapabilities newCapabilities = new DesiredCapabilities(capMap);
			newCapabilities.merge(capabilities);
			return newCapabilities;
		}
		return capabilities;
	}

}
