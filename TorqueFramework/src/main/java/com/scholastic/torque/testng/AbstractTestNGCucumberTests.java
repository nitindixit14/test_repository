package com.scholastic.torque.testng;



import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.testng.CucumberFeatureWrapper;

/**
 * 
 * @author Infostretch
 */
public abstract class AbstractTestNGCucumberTests {
	private TestNGCucumberRunner testNGCucumberRunner;

	@BeforeClass(alwaysRun = true)
	public void setUpClass(ITestContext context) throws Exception {
		TestBaseProvider.getTestBase().addAll(context.getCurrentXmlTest().getAllParameters());
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
	}

	@Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
	public void feature(CucumberFeatureWrapper cucumberFeature) {
		testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
	}

	@DataProvider
	public Object[][] features() {
		return testNGCucumberRunner.provideFeatures();
	}

	@AfterClass
	public void tearDownClass() throws Exception {
		testNGCucumberRunner.finish();
	}
}
