package com.scholastic.torque.testng;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.FeatureResultListener;
import cucumber.runtime.ClassFinder;
import cucumber.runtime.CucumberException;
import cucumber.runtime.Runtime;
import cucumber.runtime.RuntimeOptions;
import cucumber.runtime.RuntimeOptionsFactory;
import cucumber.runtime.formatter.PluginFactory;
import cucumber.runtime.io.MultiLoader;
import cucumber.runtime.io.ResourceLoader;
import cucumber.runtime.io.ResourceLoaderClassFinder;
import cucumber.runtime.model.CucumberFeature;
import gherkin.formatter.Formatter;

/**
 * 
 * @author Infostretch
 */
public class TestNGCucumberRunner {
	private Runtime runtime;
	private RuntimeOptions runtimeOptions;
	private ResourceLoader resourceLoader;
	private FeatureResultListener resultListener;
	private ClassLoader classLoader;

	@SuppressWarnings("rawtypes")
	public TestNGCucumberRunner(Class clazz) {
		classLoader = clazz.getClassLoader();
		resourceLoader = new MultiLoader(classLoader);
		TestBase threadDriver = TestBaseProvider.getTestBase();
		RuntimeOptionsFactory runtimeOptionsFactory = new RuntimeOptionsFactory(clazz);
		runtimeOptions = runtimeOptionsFactory.create();

		if (threadDriver.getContext().containsKey("glue")) {
			runtimeOptions.getGlue().add(threadDriver.getContext().getString("glue"));
		}
		if (threadDriver.getContext().containsKey("tags")) {
			String tags[] = threadDriver.getContext().getString("tags").split(" ");
			for (String tag : tags) {
				runtimeOptions.getFilters().add(tag);
			}
		}
		//String timestamp = System.getProperty("buildid");
		String RESULTS = System.getProperty("buildid");//"results/" + timestamp;
		File resultDir = new File(RESULTS);
		resultDir.mkdirs();

		final PluginFactory pluginFactory = new PluginFactory();
		String plugins[] = { "pretty", "html:" + RESULTS + "/" + threadDriver.getContext().getString("driver.name"),
				"json:" + RESULTS + "/" + threadDriver.getContext().getString("driver.name") + "/cucumber.json",
				"junit:" + RESULTS + "/" + threadDriver.getContext().getString("driver.name") + "/cucumber.xml" };
		for (String plugin : plugins) {
			Object pluginObj = pluginFactory.create(plugin.trim());
			runtimeOptions.addPlugin(pluginObj);
		}

		if (threadDriver.getContext().containsKey("features")) {
			runtimeOptions.getFeaturePaths().add(threadDriver.getContext().getString("features"));
		}

		ClassFinder classFinder = new ResourceLoaderClassFinder(resourceLoader, classLoader);
		resultListener = new FeatureResultListener(runtimeOptions.reporter(classLoader), runtimeOptions.isStrict());
		runtime = new Runtime(resourceLoader, classFinder, classLoader, runtimeOptions);
	}

	public void runCukes() {
		for (CucumberFeature cucumberFeature : getFeatures()) {
			cucumberFeature.run(runtimeOptions.formatter(classLoader), resultListener, runtime);
		}
		finish();
		if (!resultListener.isPassed()) {
			throw new CucumberException(resultListener.getFirstError());
		}
	}

	public void runCucumber(CucumberFeature cucumberFeature) {
		resultListener.startFeature();
		cucumberFeature.run(runtimeOptions.formatter(classLoader), resultListener, runtime);

		if (!resultListener.isPassed()) {
			throw new CucumberException(resultListener.getFirstError());
		}
	}

	public void finish() {
		Formatter formatter = runtimeOptions.formatter(classLoader);

		formatter.done();
		formatter.close();
		runtime.printSummary();
	}

	public List<CucumberFeature> getFeatures() {
		return runtimeOptions.cucumberFeatures(resourceLoader);
	}

	public Object[][] provideFeatures() {
		List<CucumberFeature> features = getFeatures();
		List<Object[]> featuresList = new ArrayList<Object[]>(features.size());
		for (CucumberFeature feature : features) {
			featuresList.add(new Object[] { new CucumberFeatureWrapper(feature) });
		}
		return featuresList.toArray(new Object[][] {});
	}
}
