package com.torque.automation.core;

import static com.scholastic.torque.common.TestBaseProvider.getTestBase;

import java.util.Iterator;

import org.apache.commons.configuration.Configuration;
import org.openqa.selenium.WebDriver;

/**
 * 
 * @author Infostretch
 */
public class TestDataUtils {
	protected static final String XML_DATA_PROVIDER = "xml-data-provider";

	public static WebDriver getDriver() {
		return getTestBase().getDriver();
	}

	public static String getString(String key) {
		return getTestBase().getContext().getString(key);
	}

	public Configuration getSubset(String prefix) {
		return getTestBase().getContext().subset(prefix);
	}

	public static Configuration getContext() {
		return getTestBase().getContext();
	}

	public Configuration getTestData() {
		return getTestBase().getTestData();
	}

	public static Configuration getTestDataFromXml(String tcId) {

		Configuration temp = getTestBase().getTestDataFromXml().subset(
				"testcase[@id='" + tcId + "']");
		Iterator<?> keys = temp.getKeys();
		while (keys.hasNext()) {

			String k = (String) keys.next();
			String v = temp.getString(k);
			String r = getV(v);
			temp.setProperty(k, r);

		}
		return temp;
	}

	public static String getV(String v) {

		if (!v.contains("$")) {
			return v;
		} else {
			v = getContext().getString(v.substring(2, v.length() - 1));
			getV(v);
		}
		return v;
	}

}
