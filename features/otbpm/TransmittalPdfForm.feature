@OTBPM-Transmittal @OTBPM-Transmittal(ECIMT-503-514-576)
Feature: OTBPM - Transmittal (ECIMT-503-514-576)

 
#  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P3 
#  Scenario Outline: 1- The Transmittal Forms backgrounds are displayed with expected background images
#
#    Given User is logged in OTBPM
#    When User generates a <type> Transmittal Form for any project
#    Then <type> Transmittal Form is displayed as a pdf
#    Then <type> Transmittal Form background images color is <color>
#    Examples:
#      | type     | color  |
#      | Cover    | purple |
#      | Jacket   | green  |
#      | Interior | orange |
#
#
#  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P3
#  Scenario Outline: 2- The printed Transmittal Forms fits in a single page
#
#    Given User is logged in OTBPM
#    And User generates a <type> Transmittal Form for a project
#    When User prints the <type> Transmittal Form
#    Then The printed <type> Transmittal Form fits in a single 8.5x11 paper
#    Examples:
#      | type     |
#      | Cover    |
#      | Jacket   |
#      | Interior |
#
#
#  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P3
#  Scenario Outline: 3- The Transmittal Forms are displayed using the expected layout
#
#    Given User is logged in OTBPM
#    And User generates a <type> Transmittal Form for a project
#    When User prints the <type> Transmittal Form
#    Then The elements are located and aligned as the printed layout
#    Examples:
#      | type     |
#      | Cover    |
#      | Jacket   |
#      | Interior |
#
#
#  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P3
#  Scenario Outline: 4- A header is displayed to identify the Transmittal Forms
#
#    Given User is logged in OTBPM
#    When User generates a <type> Transmittal Form for a project, using any 'Mechs' value
#    Then A header is displayed as <name> followed by the selected 'Mechs' value
#    Examples:
#      | type     | name                                                   |
#      | Cover    | Production/Manufacturing Transmittal Form for Cover    |
#      | Jacket   | Production/Manufacturing Transmittal Form for Jacket   |
#      | Interior | Production/Manufacturing Transmittal Form for Interior |


  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2 @CurrentDateInPdf
  Scenario Outline: 5- The date of creation is displayed on the Transmittal Forms

    Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then The current system date is displayed in the top right corner
    Examples:
      | type     |fields|
      | Cover    |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Jacket   |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Interior |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|


  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P1 @PdfFieldsAgainstDatabase
  Scenario Outline: 6- Transmittal Forms Information display product information from Database

    Given user is logged in OTBPM as "APT_Art_Director"
     And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then information "<pdfFields>" is displayed in "<type>" Transmittal Form
    And information "<type>" value matches database value for a project
    Examples:
      | type     | pdfFields                                                                                                                                                                                                                                                                								 |fields|
      | Cover    | ['Title','ISBN13','ISBN','Record #','Art Director','Cover Designer','Interior Designer','Production Editor','Editor','Mfg. Manager','Pub Date','Trade Price','Can. Price','BF Price','Trim Size','Page Count','Cover # of Colors','Cover Colors','List Colors','Product Type','Binding','Files Posted to']|['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg','Files']|
#      | Jacket   |  ['Title','ISBN13','ISBN','Record #','Art Director','Cover Designer','Interior Designer','Production Editor','Editor','Mfg. Manager','Pub Date','Trade Price','Can. Price','BF Price','Trim Size','Page Count','Product Type','Binding','Files Posted to']  |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg','Files']|
#      | Interior | ['Title','ISBN13','ISBN','Record #','Art Director','Cover Designer','Interior Designer','Production Editor','Editor','Mfg. Manager','Pub Date','Trade Price','Can. Price','BF Price','Trim Size','Page Count','Interior # of Colors','Interior Colors','List Colors','Product Type','Binding','Files Posted to']|['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg','Files Posted to']|
		
		
  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2 @ArcNotDisplayedInPdf 
  Scenario Outline: 7- ARC is not displayed when it is not selected for Transmittal Forms

    Given user is logged in OTBPM
      And the user selects the Transmittal Form of a project
    When the user selects "<type>"
     And user does not select 'Advanced Reader Copy'
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then 'ARC' is not displayed in the "<type>" Transmittal Form pdf
    Examples:
      | type     |fields|
      | Cover    |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Jacket   |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Interior |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|


  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2 @ArcDisplayedInPdf
  Scenario Outline: 8- ARC:Yes is displayed when checked for Transmittal Forms

    Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    When user checks 'Advanced Reader Copy'
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then 'ARC:YES' is displayed in the "<type>" Transmittal Form pdf
    Examples:
      | type     |fields|
#      | Cover    |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
      | Jacket   |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
      | Interior |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|


  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2 @InputFieldsInPdf
  Scenario Outline: 9- Transmittal Form displays user provided information for Online Transmittal Forms

    Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And User enters data into the "<information>" field
     And selects the required fields "<fields>"
      And user submits the form
     And generates the transmittal form
    Then the data entered into the "<information>" feild matches the user's input
    And is displayed with the label as "<information>"
    Examples:
      | type     | information          |fields|
      | Cover    | ['List Colors:','List Matched Inks:','Files Posted to:','Special Instructions']|['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
      | Jacket   | ['List Colors:','List Matched Inks:','Files Posted to:','Special Instructions']|['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
      | Interior | ['List Colors:','List Matched Inks:','Files Posted to:','Special Instructions']|['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|

#  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2  @duplicate
#  Scenario Outline: 10- Transmittal Form displays all user provided information for Online Transmittal Forms
#
#    Given user is logged in OTBPM
#    And User opens a New Transmittal Form for a project
#    And User enters data into the List Colors, List Matched Ink, Files Posted to and Special Instruction fields
#    When User Submits <type> Transmittal Form
#    Then the data entered into these feilds should matches the user's inputs
#    Examples:
#      | type     |
#      | Cover    |
#      | Jacket   |
#      | Interior |


  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2 @SingleValueValidationInPdf
  Scenario Outline: 11- Mech, Proof Type and Herewith selection for single value is displayed in the Transmittal forms pdf

    Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<eachFieldSingleOption>"
     And user submits the form
 	 And generates the transmittal form
    Then The selected option "<eachFieldSingleOption>" is displayed in the "<type>" Transmittal Form
    Examples:
      | type     |eachFieldSingleOption|
      | Cover    |['Art released to Mfg','Edox','CDs']|
      | Jacket   |['Art released to Mfg','Edox','CDs']|
      | Interior |['Art released to Mfg','Edox','CDs']|


#  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2   
#  Scenario Outline: 12- Proof Type selection for single value is displayed in the Transmittal forms pdf
##covered in @SingleValueValidationInPdf
#    Given user is logged in OTBPM
#    And New <type> Transmittal Form page is displayed for a project
#    And user selects Proof Type option 5
#    When user Submits the Transmittal Form
#    Then The selected option is displayed in the <type> Transmittal Form
#    Examples:
#      | type     |
#      | Cover    |
#      | Jacket   |
#      | Interior |
#
#
#  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2
#  Scenario Outline: 13- Herewith selection for single value is displayed in the Transmittal forms pdf
##covered in @SingleValueValidationInPdf
#    Given user is logged in OTBPM
#    And New <type> Transmittal Form page is displayed for a project
#    And user selects 'Herewith' option 1
#    When user Submits the Transmittal Form
#    Then The selected option is displayed in the <type> Transmittal Form
#    Examples:
#      | type     |
#      | Cover    |
#      | Jacket   |
#      | Interior |
#
#
#  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2 @duplicate
#  Scenario Outline: 14- Herewith selection of multiple values is displayed in the Transmittal forms pdf
#
#    Given user is logged in OTBPM
#    And New <type> Transmittal Form page is displayed for a project
#    And user selects all 'Herewith' options
#    And user enters a custom value for the 'Herewith' option 'Other'
#    When user Submits the Transmittal Form
#    Then The selected options are displayed in the <type> Transmittal Form separated by comma, followed by a space
#    Examples:
#      | type     |
#      | Cover    |
#      | Jacket   |
#      | Interior |


  @ORPHAN @ECIMT503 @ECIMT514 @ECIMT576 @P2 @SpineNotPresentInPdf
  Scenario Outline: 15- Spine is removed from the Transmittal Forms

    Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then 'Spine' information is not displayed on the printed Form
    Examples:
      | type     |fields|
      | Cover    |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Jacket   |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|
#      | Interior |['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']|


