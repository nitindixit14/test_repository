Feature: OTBPM - ManageMyTeam kartheek3

  @TestCase6
  Scenario Outline: 1- Selected Project information is displayed in Assign view
    Given user is logged in OTBPM
    When user selects a "<PROJECT>" and go to Assign page
    Then A table is displayed with the following "<COLUMNS>"
    And the value for "<COLUMNS>" is populated for the project
    And the "<VALUES>" match information in "<PROJECT>"
    Examples: 
      | PROJECT           | COLUMNS                                                    | VALUES  |
      | 978-0-545-91372-0 | [Title,ISBN,Art Director,Cover Designer,Interior Designer] | [BACKSTORIES: SUPERMAN: THE MAN OF TOMORROW  EBA,978-0-545-91372-0,,Chirag Jayswal,Nandhakumar Shanmugam]|
     # | [ 978-0-545-91372-0 978-1-443-15633-2]              | [Title, ISBN, Art Director, Cover Designer, Interior Designer] |
    #  | [ 978-0-545-91372-0 978-1-443-15633-2 SC-00405424] | [Title, ISBN, Art Director, Cover Designer, Interior Designer]|
