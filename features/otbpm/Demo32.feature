Feature: OTBPM - ManageMyTeam kartheek3

  @TestCase5  
  Scenario Outline: 4- A header is displayed to identify the Transmittal Forms
    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    When User generates a "<type>" Transmittal Form for a project, using any "<Mechs>" and "<proofs>" value
    Then A header is displayed as "<name>" followed by the selected "<Mechs>" value

    Examples: 
      | type  | name                                                | Mechs               | proofs |
      | Cover | Production/Manufacturing Transmittal Form for Cover | Art released to Mfg | Epson  |
 # | Jacket   | Production/Manufacturing Transmittal Form for Jacket   |Art released to Mfg|  Epson |
 # | Interior | Production/Manufacturing Transmittal Form for Interior |Art released to Mfg|  Epson |
