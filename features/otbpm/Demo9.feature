Feature: OTBPM - ManageMyTeam kartheek3

  @TestCase5



 @ORPHAN @ECIMT510 @P2 @SpecialEffectsLabelInPdf
  Scenario Outline: 8- Display Special Effects label with room for designers to write in notes in the Cover/Jacket Routing Print Form

     Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then Special Effects label with room to write in notes is displayed on the printable PDF form
Examples:
|routingType|
|Cover Routing|
|Interior Routing|