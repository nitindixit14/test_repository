@OTBPM-Search @OTBPM-Search(Search)
Feature: OTBPM - Search (Search)

 
  @ORPHAN @ECIMT596  @SimpleSearchOneValue
  Scenario Outline: 1- User performs a simple Search by entering a value in My Projects view

    Given user is logged in OTBPM
    When User searches for a "<value>" in My Projects view
    Then Search results matches "<value>" for at least one project field that displays text or numeric value
Examples:
|value|
|Boys' book|
#|Keirsten Geise|
#|Nelson Gomez|

 @ECIMT596 @SimpleSearchPositiveTestCases
Scenario Outline: 2- Search by positive cases knowing Results expected
#the known fields example column is just for reference.  It is actually not used in the scenario.
Given user is logged in OTBPM
When User searches for a "<value>" in My Projects view
Then the count of search results should be greaeter or equal to "<sql_query>" 

Examples:
|value |known fields|sql_query|
|SUPERMAN |Title|SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where TITLE_NAME LIKE '%SUPERMAN%'|
#|SHELLEY |Art Director|SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where ART_DIRECTOR LIKE '%Rick DeMonico%'|
#|ALEX |['Title', 'Interior Designer']|SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where INTERIOR_DESIGNER_NAME LIKE '%Phil%' OR TITLE_NAME like '%phil%'|
#|978-0-439-22514-4  |ISBN |SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where ISBN_13 LIKE '%978-0-545-86816-7%'|
#|9780439225144 |ISBN|SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where ISBN_13_UNFORMATTED LIKE '%9780545868167%'|
#|978 |ISBN|SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where ISBN_13 LIKE '%978%'|
#|0-439-22514-0 |ISBN|SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where ISBN LIKE '%0-545-86816-5%'|
#|0439225140 |ISBN|SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where ISBN_UNFORMATTED LIKE '%0545868165%'|
#|10 |['ISBN', 'Title']|SELECT count(*) AS TOTAL_COUNT FROM APT_PROJECTS_VIEW where TITLE_NAME LIKE '%10%' OR ISBN_13 like '%10%' |


  @ORPHAN @ECIMT596 @SimpleSearchNegativeTestCases
  Scenario Outline: 3- Search by negative cases returns NO Results

    Given user is logged in OTBPM
    When User searches for a "<value>" in My Projects view
    Then No projects are displayed for fake Search Results
    Examples:
      | value                                                   |
      | FSGDJFLKF: DLDJSHSHS                                     |
#      | 10/02/2015                                               |
#      | value for a name not listed for title or a person        |
#      | value for a fake ISBN not listed or not well constructed |


  @ORPHAN @ECIMT596 @SearchResultsTitle
  Scenario Outline: 4- Page Title displays 'Search Results' in Search Results view

    Given user is logged in OTBPM
    When User searches for a "<value>" in My Projects view
    Then Page Title is displayed as 'Search Results'
Examples:
|value|
|batman|

  @ORPHAN @ECIMT596 @SearchInSearchResults
  Scenario Outline: 5- Search functionality remains in Search Results view

    Given user is logged in OTBPM
    When User searches for a "<value>" in Search Results view
    Then Search results matches "<value>" for at least one project field that displays text or numeric value
Examples:
|value|
|Boys' book|

  @ORPHAN @ECIMT596 @SearchResultsUIElementDisplayed
  Scenario Outline: 6- User interface elements not displayed in Search Results view

    Given user is logged in OTBPM
    When User searches for a "<value>" in My Projects view
    Then 'Show Only My Projects' link is NOT displayed
    And 'Show My Teams Projects' link is NOT displayed
Examples:
|value|
|Boys' book|

  @ORPHAN @ECIMT596 @SearchResultsBreadcrumbs
  Scenario Outline: 7- Breadcrumbs are displayed in Search Results view

    Given user is logged in OTBPM
    When User searches for a "<value>" in My Projects view
    Then Breadcrumbs is displayed as 'Scholastic> My Projects'
Examples:
|value|
|batman|

  @ORPHAN @ECIMT596 @SearchResultsBreadCrumbClick
  Scenario Outline: 8- User can return to My Projects selecting the breadcrumbs

    Given user is logged in OTBPM
    And User searches for a "<value>" in My Projects view
    When User selects 'My Projects' breadcrumb
    Then My Projects view is displayed
Examples:
|value|
|batman|

 
@ORPHAN @ECIMT858 @AdvancedSearchMyProjectsView
  Scenario Outline: 1- User can perform an Advanced Search from My Projects view

    Given user is logged in OTBPM
    And User searches for a "<value>" in My Projects view
    When the user navigates to the My Projects page
	And User selects 'Advanced Search'
    Then 'Advanced Search' view is open in a new window
    And User searches by "<searchBy>" with all words in Advanced Search
    Examples:
    |value		|searchBy|
	|Boys' book |title	 |


  @ORPHAN @ECIMT858 @AdvancedSearchSearchView
  Scenario Outline: 2- Advanced Search functionality remains in Search Results view

    Given user is logged in OTBPM
    And User searches for a "<value>" in My Projects view
    When User selects 'Advanced Search'
    Then 'Advanced Search' view is open in a new window
    And User searches by "<searchBy>" with all words in Advanced Search
     Examples:
    |value		|searchBy|
	|Boys' book |title	 |


  @ORPHAN @ECIMT858 @AdvanceSearchViewPageTitle @SameAsAboveRemove
  Scenario: 3- Page Title displays 'Advanced Search' in Advanced Search view

    Given user is logged in OTBPM
    When User is in 'Advanced Search' view
    Then Page Title is displayed as 'Advanced Search'


  @ORPHAN @ECIMT858 @AdvancedSearchByDate
  Scenario: 4- 'Target due to manufacturing' is added as 'By Date' criteria

    Given user is logged in OTBPM
    When User is in 'Advanced Search' view
    Then "Target Due to Mfg" search option is displayed in the date area

 
  @ORPHAN @ECIMT1168 @DateFormat
  Scenario: 1- Pub Date and First Use Date are displayed in format(MM/DD/YYYY) in My Project List

    Given user is logged in OTBPM
    When user is in 'My Projects' view
    Then project 'Pub Date' is displayed as (MM/DD/YYYY)
    And project 'First Use Date' is displayed as (MM/DD/YYYY)
	

  @ORPHAN @ECIMT1168 @SimpleSearchViewDateFormat
  Scenario Outline: 2- Pub Date and First Use Date are displayed in format(MM/DD/YYYY) in for Search results

    Given user is logged in OTBPM
    When User searches for a "<value>" in My Projects view
    Then project 'Pub Date' is displayed as (MM/DD/YYYY)
    And project 'First Use Date' is displayed as (MM/DD/YYYY)
Examples:
|value|
|Boys' book|

  @ORPHAN @ECIMT1168  @AdvancedSearchDateFormat
  Scenario Outline: 3- Pub Date and First Use Date are displayed in format(MM/DD/YYYY) in for Advanced Search results
#User executes and advanced search by title for <'project'>
    Given user is logged in OTBPM
    When User searches by "<searchBy>" with all words in Advanced Search 
    Then project 'Pub Date' is displayed as (MM/DD/YYYY)
    And project 'First Use Date' is displayed as (MM/DD/YYYY)
Examples:
		| searchBy	| 
		| title		| 
