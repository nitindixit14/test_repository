Feature: OTBPM - ManageMyTeam 
  Scenario Outline: 11 -- Project Details - Managing Editor is displayed as Production Editor
  
    Given user is logged in OTBPM
    When user searches for "<project>"	
    When User opens Project Details for "<project>"
    And user is in 'Project Details' page for "<project>"
    Then 'Production Editor' label is displayed
    And 'Managing Editor' label is not displayed
Examples:
      | project           |
      | 978-0-545-44868-0 |

