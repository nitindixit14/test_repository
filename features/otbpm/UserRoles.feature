Feature: OTBPM access control for user with different role(s)
#Validate User Roles and permission applicable to APT  
#Description:  Configure User Roles and permissions 

#Associated Function: Active Directory
#Stories covered: ECIMT-024  (It crosses functionality with ECIMT-760 which includes menu/submenu behavior)

# Iterated for 'Default' as: APT Production Editor|APT Editor/Mgmt/Guest|APT Manufacturing|APT Vendor
# Designer as: APT Designer
# Director roles: Iterated for 'Art Director' as: Art Director|Art Director & Production Editor|Art Director & Designer Role.
# Iterated for 'SCCG' as: SCCG Reprint Editor|SCCG Manufacturing Role. 
# Mixed role - Designer/SCCG

# NOTE: SCCG menu access is covered in user-story 760
 
@ECIMT024  @Smoke24 @Smoke
Scenario Outline: Art Production Tracking access for different roles
	Given user with "<ROLE(s)>" role	
	When user with "<ROLE(s)>" role signing in
	When user is in 'My Projects' view
	And user can view the menu
	Then Art Production Tracking Menu should be avialable
	And user should have access to "<APT_SUB_MENU>"
	
	Examples:
		#User may have one or more role, multiple role separated with space
	 
		| ROLE(s)										| APT_SUB_MENU											|
		| ['APT Art Director']							| ['My Projects','Manage My Team']	|
		| ['APT Art Director','APT Production Editor']	| ['My Projects','Manage My Team']	|
		| ['APT Art Director', 'APT Designer']			| ['My Projects','Manage My Team']	|
		| ['APT Production Editor']						| ['My Projects']										|
		| ['APT Manufacturing']							| ['My Projects']										|
		| ['APT Vendor']								| ['My Projects']										|
		| ['APT Designer']								| ['My Projects']					|		  
		| ['APT Designer','SCCG Manufacturing']			| ['My Projects']					|
#		| ['SCCGReprintEditor']							| ['My Projects']										|
##		| ['Guest']										| ['My Projects']										|
#		| ['SCCGManufacturing']							| ['My Projects']										|

@ECIMT024 @UserName
Scenario: User name should be populated from AD
	Given A valid user 
	When user signing in 
	Then user name should displayed in header 
	

