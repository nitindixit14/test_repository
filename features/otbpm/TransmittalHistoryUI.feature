@Pending @ECIMT628 @TH @en_US
Feature: Update Transmittal History with additional data elements & UI Changes
# Review the modified Transmittal History Mock Up from attached to this user story 
# Make the Cover Image Smaller (See Mockup) 
# Remove the Bar Code download from this view 
# Reduce the size and amount of Production Information at the top of the screen (See Mock Up) 


# Apply these rules to the Type Column 
#-If Type = TRANSMITTAL_INTERIOR_FORM display "Interior" and make the text Yellowish Brown for the row 
#-If Type = TRANSMITTAL_JACKET_FORM display " Jacket" and make the text Green for the row 
#-If Type = TRANSMITTAL_COVER_FORM display "Cover" and make the text Purple for that row

Scenario: identify text color of row
	Given A valid user 
	When user signing in 
	Then user should be taken to dashboard
	When user open transmittal history 
	Then verify the text color of differen row

@InteriorRouting
Scenario: POC for PDF content validation
	Given A valid user 
	When user signing in 
	Then user should be taken to dashboard
	When user open Interior Routing 
	Then verify generated PDF document