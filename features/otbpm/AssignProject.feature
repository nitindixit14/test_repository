@OTBPM-Assign
Feature: OTBPM - Assign

  OTBPM - Assign (Project Assignment)
  Jira QA Backlog tickets ECIMT-1545 & ECIMT-2074
  
  
  Stories Covered:  ECIMT-565, ECIMT-597, ECIMT-1183, ECIMT-1443
  
  ECIMT-565::  APT: Unassign a Project
  As an Art Director, I want the system to apply business rules to assign projects to me automatically, such that I do not 	need to do this task manually for all projects.
  
  ECIMT-597::  APT: Update Assign Project Screen with additional data elements and multiple project assignment capability (Manual Assignment)
  As an Art Director or Designer, I want to be able to assign an owner (Art Director, Cover Designer, Interior Designer) to one or multiple projects in a single action, such that I can complete this task as quickly as possible and everyone has system access to their assignments to plan and do their work
  
  ECIMT-1183::  APT: DEFECT: Assign Projects Functionality
  As a Product Owner, I want all identified defects resolved, such that the user experience is consistent and accurate in all interactions with the APT system.
  
  ECIMT-1443:: APT: Unassign a Project
  As an Art Director, I need the ability to unassign a project or projects, such that I can remove projects from individuals lists that are no longer applicable to them.

 
  @ORPHAN @ECIMT565 @P1 @CombinedInEditArtDirector
  Scenario Outline: 1 - Valid Art Director value for a New EPIC record is stored in OTBPM database

        Given user creates a epic record with art director "<ART_DIRECTOR>"
	Given user is logged in OTBPM
	When user is in 'My Projects' view
	When User searches in My Projects view using isbn
	And "<ART_DIRECTOR>" is in OTBPM Art Directors List
    When New EPIC record exists in OTBPM
    And User Query "<ART_DIRECTOR>" for New record from OTBPM database
    Then Art Director value from OTBPM database matches displayed "<ART_DIRECTOR>" in EPIC
    And "<ART_DIRECTOR>" value displayed on Project List matches OTBPM database value for Art Director
    And "<ART_DIRECTOR>" value displayed on Project Details page matches OTBPM database value for Art Director
    And "<ART_DIRECTOR>" value displayed on Transmittal Form page matches OTBPM database value for Art Director
    Examples:
      | ART_DIRECTOR      |NEW_ART_DIRECTOR	  |
      | Parisi, Elizabeth |Rajesh Chandrasekar|
#      | DEMONICO, RICK   |Rajesh Chandrasekar|


  @ORPHAN @ECIMT565 @P3 @InvalidArtDirector
  Scenario Outline: 2- Invalid Art Director value for a New EPIC record is NOT stored in OTBPM database

	Given user creates a epic record with art director "<INVALID_ART_DIRECTOR>"
	Given user is logged in OTBPM
	When user is in 'My Projects' view
	When User searches in My Projects view using isbn
	 And "<INVALID_ART_DIRECTOR>" is NOT in OTBPM Art Directors List
    When New EPIC record exists in OTBPM
    And User Query "<INVALID_ART_DIRECTOR>" for New record from OTBPM database
    Then Art Director value from OTBPM database is empty
    Examples:
      | INVALID_ART_DIRECTOR  |
#      | DARCY, PAMELA |
      | BANKS, PAUL	  |


  @ORPHAN @ECIMT565 @P1 @CombinedInEditArtDirector
  Scenario Outline: 3- Validate Art Director from OTBPM database in displayed in OTBPM UI

    #use art director value from EPIC NOT listed in OTBPM
    #create at least 2 records, and use the ISBN_13 for following scenarios: ISBN03, ISBN04
    Given New EPIC record with <RECORD_ISBN_13> exist in OTBPM
    When User logs in OTBPM
    And User searches for New record by <RECORD_ISBN_13>
    Then New EPIC record is displayed in Search results
    And Art Director value displayed on Project List matches OTBPM database value for Art Director
    And Art Director value displayed on Project Details page matches OTBPM database value for Art Director
    And Art Director value displayed on Transmittal Form page matches OTBPM database value for Art Director
    Examples:
      | RECORD_ISBN_13                   |
      | use ISBN01 of new record created |
      | use ISBN02 of new record created |
      | use ISBN03 of new record created |
      | use ISBN04 of new record created |


  @ORPHAN @ECIMT565 @P3 @EditArtDirector
  Scenario Outline: 4- Assigned Art Director value can be edited in OTBPM
	

    #Art Director edited in OTBPM (ISBN01)
    #Art Director added in OTBPM (ISBN03)
    Given user creates a epic record with art director "<ART_DIRECTOR>"
	Given user is logged in OTBPM
	When user is in 'My Projects' view
	When User searches in My Projects view using isbn
	And "<ART_DIRECTOR>" is in OTBPM Art Directors List
    When New EPIC record exists in OTBPM
    And User Query "<ART_DIRECTOR>" for New record from OTBPM database
    Then Art Director value from OTBPM database matches displayed "<ART_DIRECTOR>" in EPIC
    And "<ART_DIRECTOR>" value displayed on Project List matches OTBPM database value for Art Director
    And "<ART_DIRECTOR>" value displayed on Project Details page matches OTBPM database value for Art Director
    And "<ART_DIRECTOR>" value displayed on Transmittal Form page matches OTBPM database value for Art Director
     And user selects a project to 'Assign'
    When User Assigns Art Director as "<NEW_ART_DIRECTOR>" in Assign view
    And User return to search results view
	And User Query "<NEW_ART_DIRECTOR>" for New record from OTBPM database
    Then Art Director value from OTBPM database matches displayed "<NEW_ART_DIRECTOR>" in EPIC
    And "<NEW_ART_DIRECTOR>" value displayed on Project List matches OTBPM database value for Art Director
    And "<NEW_ART_DIRECTOR>" value displayed on Project Details page matches OTBPM database value for Art Director
    And "<NEW_ART_DIRECTOR>" value displayed on Transmittal Form page matches OTBPM database value for Art Director
	Given user login to epic
	When User searches for New record by ISBN 13 
    And User updates the New EPIC record with "<EPIC_ART_DIRECTOR>" value
    Given A valid user
    When User searches in My Projects view using isbn
    And User Query "<ART_DIRECTOR>" for New record from OTBPM database
    Then Art Director value from OTBPM database remains as "<OTBPM_ART_DIRECTOR>"
    Examples:
       | ART_DIRECTOR      |NEW_ART_DIRECTOR  |EPIC_ART_DIRECTOR|OTBPM_ART_DIRECTOR |
      | Parisi, Elizabeth |Rajesh Chandrasekar|Harris, Patti Ann|Rajesh Chandrasekar|
#      | DEMONICO, RICK   |Rajesh Chandrasekar|Harris, Patti Ann|Rajesh Chandrasekar|


  @ORPHAN @ECIMT565 @P2 @UnassignArtDirector
  Scenario Outline: 5- Art Director value can be unassigned in OTBPM

	    Given user creates a epic record with art director "<ART_DIRECTOR>"
	Given user is logged in OTBPM
	When user is in 'My Projects' view
	When User searches in My Projects view using isbn
	And user selects a project to 'UnAssign'
    When User Unassigns Art Director for project in Assign view
    And User return to search results view
    Then Art Director value is blank on Project List
    And Art Director value is blank on Project Details page
    And Art Director value is blank on Transmittal Form page
    And User Query "<ART_DIRECTOR>" for New record from OTBPM database
    And Art Director value is empty in OTBPM database
    Examples:
      | ART_DIRECTOR      |
      | Parisi, Elizabeth |
      | DEMONICO, RICK    |


  @ORPHAN @ECIMT565 @P2 @PersistArtDirector @CombinedInEditArtDirector
  Scenario Outline: 6- Assigned Art Director value in OTBPM persist over updated EPIC value
  
	Given New EPIC record with <RECORD_ISBN_13> and <OTBPM_ART_DIRECTOR> exist in OTBPM
    And User updates the New EPIC record with <‘EPIC_ART_DIRECTOR’> value
    When New EPIC record is updated in OTBPM
    And User Query Art Director for New record from OTBPM database
    Then Art Director value from OTBPM database remains as <OTBPM_ART_DIRECTOR>
    Examples:
      | RECORD_ISBN_13                   | OTBPM_ART_DIRECTOR  | EPIC_ART_DIRECTOR |
      | use ISBN01 of new record created | Rajesh Chandrasekar | Patti Ann Harris  |
#      | use ISBN02 of new record created |                     | Patti Ann Harris  |
#      | use ISBN03 of new record created | Rajesh Chandrasekar | Patti Ann Harris  |
#      | use ISBN04 of new record created |                     | Patti Ann Harris  |


  @ORPHAN @ECIMT597 @P2
  Scenario Outline: 1- Selected Project information is displayed in Assign view

    Given user is logged in OTBPM
    When user selects a <PROJECT> and go to Assign page
    Then A table is displayed with the following <COLUMNS>
    And table is displayed with the following <COLUMNS>
    And the value for <COLUMNS> is populated for the project
    And the values match information in Project List
    Examples:
      | PROJECT                                              | COLUMNS                                                        |
      | 978-0-545-91372-0                                    | [Title, ISBN, Art Director, Cover Designer, Interior Designer] |
      | [ 978-0-545-91372-0, 978-1-443-15633-2]              | [Title, ISBN, Art Director, Cover Designer, Interior Designer] |
      | [ 978-0-545-91372-0, 978-1-443-15633-2, SC-00405424] | [Title, ISBN, Art Director, Cover Designer, Interior Designer] |

@ORPHAN @ECIMT597
  Scenario Outline: 2- UI Cleanup -Removed Cover Image

    #Scenario is only for manual test
    Given user is logged in OTBPM
    When user selects a <PROJECT> and go to Assign page
    Then 'Cover Image' is not displayed
    Examples:
      | PROJECT           |
      | 978-0-545-91372-0 |


  @ORPHAN @ECIMT597 @P3
  Scenario Outline: 3- Title of window is displayed as Assign

    Given user is logged in OTBPM
    When user selects a "<PROJECT>" and go to Assign page
    Then 'Assign' Title is displayed
    Examples:
      | PROJECT           |
      | 978-0-545-91372-0 |


  @ORPHAN @ECIMT597 @P1 @AssignFieldsDisplayed
  Scenario Outline: 4- Select Art Director, Cover Designer and Interior Designer fields are displayed

    Given user is logged in OTBPM
    When user selects a "<PROJECT>" and go to Assign page
    Then "<LABEL>" label and input field are displayed
    Examples:
      | PROJECT           |LABEL|
      | 978-0-545-91372-0 |['Art Director','Cover Designer','Interior Designer']|



  @ORPHAN @ECIMT597 @P3
  Scenario Outline: 5- Autosuggest Art Director is enabled when user types on the field

    Given user is logged in OTBPM
    And user selects a <PROJECT> and go to Assign page
    When user types <CHARACTER_SEQUENCE> in Art Director field
    Then A list of 'Art Directors' containing the typed character sequence is displayed
    And The list is updated each time user types an additional character
    Examples:
      | PROJECT           | CHARACTER_SEQUENCE |
      | 978-0-545-91372-0 | j                  |
      | 978-0-545-91372-0 | ja                 |
      | 978-0-545-91372-0 | jay                |
      | 978-0-545-91372-0 | jayswal            |
      | 978-0-545-91372-0 | john               |


  @ORPHAN @ECIMT597 @P3
  Scenario Outline: 6- Autosuggest Cover Designer is enabled when user types on the field

    Given user is logged in OTBPM
    And user selects a <PROJECT> and go to Assign page
    When user types <CHARACTER_SEQUENCE> in Cover Designer field
    Then A list of 'Designers' containing the typed character sequence is displayed
    And The list is updated each time user types an additional character
    Examples:
      | PROJECT           | CHARACTER_SEQUENCE |
      | 978-0-545-91372-0 | j                  |
      | 978-0-545-91372-0 | ja                 |
      | 978-0-545-91372-0 | jay                |
      | 978-0-545-91372-0 | jayswal            |
      | 978-0-545-91372-0 | Ann                |


  @ORPHAN @ECIMT597 @P3
  Scenario Outline: 7- Autosuggest Interior Designer is enabled when user types on the field

    Given user is logged in OTBPM
    And user selects a <PROJECT> and go to Assign page
    When user types <CHARACTER_SEQUENCE> in Interior Designer field
    Then A list of 'Designers' containing the typed character sequence is displayed
    And The list is updated each time user types an additional character
    Examples:
      | PROJECT           | CHARACTER_SEQUENCE |
      | 978-0-545-91372-0 | j                  |
      | 978-0-545-91372-0 | ja                 |
      | 978-0-545-91372-0 | jay                |
      | 978-0-545-91372-0 | jayswal            |
      | 978-0-545-91372-0 | Ann                |


  @ORPHAN @ECIMT597 @P3
  Scenario Outline: 8- Autosuggest is NOT enabled when user types invalid characters

    Given user is logged in OTBPM
    And user selects a <PROJECT> and go to Assign page
    When user types <CHARACTER_SEQUENCE> in "<SELECT_FIELD>"
    Then No autosuggest list is displayed
    Examples:
      | PROJECT           | CHARACTER_SEQUENCE | SELECT_FIELD      |
      | 978-0-545-91372-0 | #$%                | Art Director      |
      | 978-0-545-91372-0 | 12345              | Art Director      |
      | 978-0-545-91372-0 | United States      | Art Director      |
      | 978-0-545-91372-0 | #$%                | Cover Designer    |
      | 978-0-545-91372-0 | 12345              | Cover Designer    |
      | 978-0-545-91372-0 | United States      | Cover Designer    |
      | 978-0-545-91372-0 | #$%                | Interior Designer |
      | 978-0-545-91372-0 | 12345              | Interior Designer |
      | 978-0-545-91372-0 | United States      | Interior Designer |



  @ORPHAN @ECIMT597 @P1 @AssignWithAutoSuggest
  Scenario Outline: 9- Selected value for Art Director, Cover Designer, Interior Designer are updated in the table for the selected projects

    Given user is logged in OTBPM
    When user is in 'My Projects' view
	When User searches for a "ASSIGN FEATURE EXAMPLE" in My Projects view
    And user selects "<PROJECT>" total projects and goto assign page
    When User selects a "<VALUE>" for "<FIELD>" from the Autosuggested list
    Then the selected value "<VALUE>" is displayed in the "<FIELD>" column for all the "<PROJECT>" projects in the table
    Examples:
      | PROJECT        | FIELD             |VALUE  |
#      | 1              | Art Director      |Sudheer|
#      | 1              | Cover Designer    |Hiral  |
#      | 1              | Interior Designer |Sun	   |
      | 2              | Art Director      |Sudheer|
#      | 2              | Cover Designer    |Hiral  |
#      | 2              | Interior Designer |Sun    |
#      | 3 			   | Art Director      |Sudheer|
#      | 3			   | Cover Designer    |Hiral  |
#      | 3 			   | Interior Designer |Sun    |


  @ORPHAN @ECIMT597 @P2
  Scenario Outline: 10- Save updates all of the selected projects with the changed information

    #Note for automation: use different values for Art Direct, Cover Designer and Interior Designer in each iteration
    Given user is logged in OTBPM
    And user selects a <PROJECT> and go to Assign page
    When User selects a Art Director, Cover Designer and Interior Designer form the Autosuggested list
    And User Saves Assign selection
    Then Assigned values are stored in database for project
    And Assigned values are displayed in Project List
    Examples:
      | PROJECT                                              |
      | 978-0-545-91372-0                                    |
      | [ 978-0-545-91372-0, 978-1-443-15633-2]              |
      | [ 978-0-545-91372-0, 978-1-443-15633-2, SC-00405424] |


  @ORPHAN @ECIMT597 @P3
  Scenario Outline: 11- Close Assign window returns user to My Project view without committing updates

    Given user is logged in OTBPM
    And user selects a <PROJECT> and go to Assign page
    When User selects a Art Director, Cover Designer and Interior Designer from the Autosuggested list
    And User closes window without Saving
    Then Initial project values remains unchanged in database
    And Initial project values are displayed unchanged in Project List
    Examples:
      | PROJECT                                              |
      | 978-0-545-91372-0                                    |
      | [ 978-0-545-91372-0, 978-1-443-15633-2]              |
      | [ 978-0-545-91372-0, 978-1-443-15633-2, SC-00405424] |


  @ORPHAN @ECIMT597 @P1 @AssignSaveUpdates
  Scenario Outline: 12- Save updates of the selected projects without overwritten unchanged fields

    # Click Save updates all of the selected projects with the changed information --
    #Could be one or multiple ñ Testing needs to cover both scenarios.
    # If a user accesses the View and Assigns only a single task (e.g. Cover Designer) --
    #the Save action should not overwrite the other previously saved information for the other
    #tasks (e.g. Interior Designer, Art Director). Need to test all the variations of the
    #updates ñ assigning Cover Designer only, assigning Interior Designer only, assigning Art Directory
    #only, Assigning Cover Designer and Interior Designer, Assigning Art Director and Cover Designer,
    #Assigning Art Director and Interior Designer, Assigning all three of them at one.
    Given user is logged in OTBPM
    When user is in 'My Projects' view		
    When User searches for a "ASSIGN FEATURE EXAMPLE" in My Projects view
    And user can view the "<SELECTED_VALUES>" and "<REMAINING_VALUES>" in the table for "<PROJECT>" project
    And user selects "<PROJECT>" total projects and goto assign page
    When User selects "<value>" for "<SELECTED_VALUES>" from the Autosuggested list
    And User Saves Assign selection
    Then Assigned values "<value>" for "<DBOPTION_SELECTEDVALUES>" are stored in database for "<PROJECT>" project
    Then Initial project values remains unchanged in database "<DBOPTION_REMAININGVALUES>" for "<PROJECT>" projects
    And Values displayed in Project List for "<PROJECT>" matches database values
    Examples:
 | PROJECT|value	  				     |DBOPTION_SELECTEDVALUES		|DBOPTION_REMAININGVALUES    				   	 | SELECTED_VALUES   | REMAINING_VALUES                     |
 | 2 	  |['Sudheer Mogi']				 |['ART_DIRECTOR_NAME']  	|['COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME'] | ['Art Director'] | ['Cover Designer', 'Interior Designer'] |
 | 2 	  |['Hiral Desai']				 |['COVER_DESIGNER_NAME']	|['ART_DIRECTOR_NAME','INTERIOR_DESIGNER_NAME']   |['Cover Designer']| ['Art Director', 'Interior Designer']   |
 | 2 	  |['Angela Jun']					 |['INTERIOR_DESIGNER_NAME']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME']	 |['Interior Designer']|['Art Director', 'Cover Designer']  |
 | 2 	  |['Sudheer Mogi','Hiral Desai']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME']	  |['INTERIOR_DESIGNER_NAME']	 |['Art Director', 'Cover Designer']      | ['Interior Designer']  |
 | 2 	  |['Sudheer Mogi','Angela Jun']	 |['ART_DIRECTOR_NAME','INTERIOR_DESIGNER_NAME']  |['COVER_DESIGNER_NAME']|['Art Director', 'Interior Designer']	  |['Cover Designer']| 	
 | 2 	  |['Hiral Desai','Angela Jun']	 |['COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME']|['ART_DIRECTOR_NAME']|['Cover Designer', 'Interior Designer']    | ['Art Director'] |


  @ORPHAN @ECIMT597 @P3
  Scenario Outline: 13- Navigates to Assign view from Project Details

    Given user is logged in OTBPM
    When user is in Project Details view for a "<PROJECT>"
    And User clicks on 'Assign' button
    Then Assign view is displayed
    And Selected Project is displayed in Assign List
    And the value for <COLUMNS> is populated for the project
    And the values match information in Project Details view
    Examples:
      | PROJECT           | COLUMNS                                                        |
      | 978-0-545-91372-0 | [Title, ISBN, Art Director, Cover Designer, Interior Designer] |
      | 978-1-443-15633-2 | [Title, ISBN, Art Director, Cover Designer, Interior Designer] |
      | SC-00405424       | [Title, ISBN, Art Director, Cover Designer, Interior Designer] |


  @ORPHAN @ECIMT1183 @P3
  Scenario Outline: 1- Columns in Assign list are displayed in expected order

    Given User is logged in OTBPM
    When User is in 'Assign' view
    Then The list of projects to assign include '<INFORMATION_HEADERS>' in that order
    Examples:
      | INFORMATION_HEADERS                                                  |
      | ['Title','ISBN','Art Director','Cover Designer','Interior Designer'] |


  @ORPHAN @ECIMT1183 @P3
  Scenario: 2- Fields for Assign Art Director, Cover and Interior Designer are aligned

    Given User is logged in OTBPM
    When User is in 'Assign' view
    Then Select fields for 'Art Director','Cover Designer' and'Interior Designer' are aligned


  @ORPHAN @ECIMT1183 @P3
  Scenario Outline: 3- Options to select Art Director, Cover and Interior Designer are displayed in Assign view

    Given User is logged in OTBPM
    When User is in 'Assign' view
    Then '<SELECTABLE>' field is displayed labeled as '<LABEL>'
    And '<SELECTABLE>' field is populated with all the Active users with '<ROLE>'
    Examples:
      | SELECTABLE        | LABEL             | ROLE                        |
      | Art Director      | Art Director      | ['Art Director']            |
      | Cover Designer    | Cover Designer    | ['Art Director','Designer'] |
      | Interior Designer | Interior Designer | ['Art Director','Designer'] |


  @ORPHAN @ECIMT1443 @P2 @UnAssignDirectorDesigner
  Scenario Outline: 1- Unassign Art Director, Cover Designer, Interior Designer deletes value in the table for the selected projects

   Given user is logged in OTBPM
   When user is in 'My Projects' view
    When User searches for a "<TITLE>" in My Projects view
    And user selects "<PROJECT>" total projects and goto assign page
    And selected projects has assigned Art Director, Cover Designer and Interior Designer "<OPTION>" with value "<value>"
    And user selects "<PROJECT>" total projects and goto assign page
    When User selects remove icon for "<OPTION>"
    Then The "<OPTION>" value for "<PROJECT>" is displayed empty for all the projects on the list
    Then Then Unassigned empty values "<DBOPTION>" for projects "<PROJECT>" are stored in database for project
    Examples:
    | TITLE					 | PROJECT        | OPTION            |value	   |DBOPTION|
    |UNASSIGN FEATURE EXAMPLE  | 1              | ['Art Director']      |['Sudheer Mogi']|['ART_DIRECTOR_NAME']|
    |UNASSIGN FEATURE EXAMPLE  | 1              | ['Cover Designer']    |['Hiral Desai'] |['COVER_DESIGNER_NAME']|
    |UNASSIGN FEATURE EXAMPLE  | 1              | ['Interior Designer'] |['Sun Lee']	   |['INTERIOR_DESIGNER_NAME']|
    |UNASSIGN FEATURE EXAMPLE | 2              | ['Art Director']      |['Sudheer Mogi']|['ART_DIRECTOR_NAME']|
    |UNASSIGN FEATURE EXAMPLE | 2              | ['Cover Designer']    |['Hiral Desai'] |['COVER_DESIGNER_NAME']|
    |UNASSIGN FEATURE EXAMPLE  | 2              | ['Interior Designer'] |['Sun Lee']	   |['INTERIOR_DESIGNER_NAME']|
    |UNASSIGN FEATURE EXAMPLE| 3 			  | ['Art Director']      |['Sudheer Mogi']|['ART_DIRECTOR_NAME']|
    |UNASSIGN FEATURE EXAMPLE  | 3 			  | ['Cover Designer']    |['Hiral Desai'] |['COVER_DESIGNER_NAME']|
    |UNASSIGN FEATURE EXAMPLE | 3 			  | ['Interior Designer'] |['Sun Lee']	   |['INTERIOR_DESIGNER_NAME']|
	 |UNASSIGN FEATURE EXAMPLE  | 3 			  | ['Art Director','Cover Designer'] |['Sudheer Mogi','Hiral Desai']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME']|
	 |UNASSIGN FEATURE EXAMPLE  | 3 			  | ['Art Director','Interior Designer'] |['Sudheer Mogi','Sun Lee']|['ART_DIRECTOR_NAME','INTERIOR_DESIGNER_NAME']|
	 |UNASSIGN FEATURE EXAMPLE | 3 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME']|

  @ORPHAN @ECIMT1443 @P2 @CombinedInScenario1
  Scenario Outline: 2- Unassigns Art Director, Cover Designer, and Interior Designer changes persist when Saved

    Given User is logged in OTBPM
    And user selects a <PROJECT> and go to Assign page
    And selected projects has assigned Art Director, Cover Designer and Interior Designer
    When User selects remove icon for Art Director, Cover Designer, and Interior Designer
    And User Saves unassign changes
    Then Unassigned empty values are stored in database for project
    And Unassigned empty values are displayed in Project List
    Examples:
      | PROJECT                                  |
      | New ISBN 01                              |
      | [ New ISBN 01, New ISBN 02]              |
      | [ New ISBN 01, New ISBN 02, New ISBN 03] |


  @ORPHAN @ECIMT1443 @P2 @CancelUnAssignChanges
  Scenario Outline: 3- User cancels unassign changes in Assigning Art Director, Cover and Interior Designer

   Given user is logged in OTBPM
   When user is in 'My Projects' view
    When User searches for a "<TITLE>" in My Projects view
    And user selects "<PROJECT>" total projects and goto assign page
    And selected projects has assigned Art Director, Cover Designer and Interior Designer "<OPTION>" with value "<value>"
    And user selects "<PROJECT>" total projects and goto assign page
    When User selects remove icon for "<OPTION>" Art Director, Cover Designer, and Interior Designer 
    And User closes window without Saving
    Then Initial project values "<value>" for "<DBOPTION>" for "<PROJECT>" total projects remains unchanged in database
    And Initial project values "<value>" for "<OPTION>" for "<PROJECT>" are displayed unchanged in Project List
    Examples:
     | TITLE				  | PROJECT       | OPTION            |value	   |DBOPTION|
     |UNASSIGN FEATURE EXAMPLE  | 3 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME']|
     |UNASSIGN FEATURE EXAMPLE  | 1 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME']|
     |UNASSIGN FEATURE EXAMPLE  | 2 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME']|
    


  @ORPHAN @ECIMT1443 @P2 @UnAssignPersistUnChanged
  Scenario Outline: 4- Save unassign changes of the selected projects without ovewritten unchanged fields

    #differentiate when user is art director and unassigne himself and return to My projects, then project is not in my project list
    Given user is logged in OTBPM
    When user is in 'My Projects' view
    When User searches for a "<TITLE>" in My Projects view
    And user selects "<PROJECT>" total projects and goto assign page
    And selected projects has assigned Art Director, Cover Designer and Interior Designer "<OPTION>" with value "<value>"
    And user selects "<PROJECT>" total projects and goto assign page
    When User selects remove icon for "<SELECTED_VALUES>"
    Then Unassigned values for "<DBOPTION_SELECTEDVALUES>" for "<PROJECT>" are stored in database for project
    Then Initial project values for "<DBOPTION_REMAININGVALUES>" for "<PROJECT>" remains unchanged in databse
    Then Values displayed in Project List for "<OPTION>" for "<PROJECT>" total projects matches database values
    Examples:
         | TITLE				  | PROJECT       | OPTION           									 |value	  							   |DBOPTION_SELECTEDVALUES|DBOPTION_REMAININGVALUES    				   	 | SELECTED_VALUES   | REMAINING_VALUES                     |
     |UNASSIGN FEATURE EXAMPLE  | 2 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['ART_DIRECTOR_NAME']  |['COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME'] | ['Art Director'] | ['Cover Designer', 'Interior Designer'] |
      |UNASSIGN FEATURE EXAMPLE  | 2 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['COVER_DESIGNER_NAME']|['ART_DIRECTOR_NAME','INTERIOR_DESIGNER_NAME']   |['Cover Designer']| ['Art Director', 'Interior Designer']   |
     |UNASSIGN FEATURE EXAMPLE  | 2 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['INTERIOR_DESIGNER_NAME']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME']	 |['Interior Designer']|['Art Director', 'Cover Designer']  |
	  |UNASSIGN FEATURE EXAMPLE | 2 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['ART_DIRECTOR_NAME','COVER_DESIGNER_NAME']|['INTERIOR_DESIGNER_NAME']	 |['Art Director', 'Cover Designer']      | ['Interior Designer']                   |
      |UNASSIGN FEATURE EXAMPLE  | 2 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['ART_DIRECTOR_NAME','INTERIOR_DESIGNER_NAME']|['COVER_DESIGNER_NAME']|['Art Director', 'Interior Designer']	  |['Cover Designer']| 	
	  |UNASSIGN FEATURE EXAMPLE  | 2 			  | ['Art Director','Cover Designer','Interior Designer'] |['Sudheer Mogi','Hiral Desai','Sun Lee']|['COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME']|['ART_DIRECTOR_NAME']|['Cover Designer', 'Interior Designer']    | ['Art Director'] |


  @ORPHAN @ECIMT1443 @P2
  Scenario Outline: 5- User unassigns project to himself so is not displayed in My Projects List

    Given 'Art Director' user logs in OTBPM
    And user selects a <PROJECT> from My Projects and go to Assign page
    When User unassigns himself as 'Art Director'
    And User Saves unassign selection
    Then Project is not listed in My  Projects page
    And User can Search for Project
    And Project Art Director value is empty
    Examples:
      | PROJECT     |
      | New ISBN 01 |


  @ORPHAN @ECIMT597
  Scenario: Error display when no product selection made in My Projects or Search results table and Assign button is clicked on

    Given user is logged in OTBPM								
    And user is in My Projects view or Search Results view								
    When user clicks on Assign button without selecting a product								
    Then an error message is displayed with the text: Select at least one product to Assign.								


  @ORPHAN @ECIMT597
  Scenario: Error display in Assign view when no change made to the assignments

    Given user is logged in OTBPM									
    And user is in the Assign view 									
    When user clicks save without making any change to assignments									
    Then an error message is displayed with the text: Select or remove an Art Director or Designer.									

