 Feature: OTBPM 
  @ORPHAN @ECIMT598 @P3
  Scenario Outline: 23- Specific comments can be located in the EPIC Comments section

    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    And user expands 'EPIC comments'
    Then "<specific_comments>"are displayed when available
    And label for "<specific_comments>" is displayed as "<label>" when comments are available
    Examples:
      | project           | specific_comments | label             |
      | 978-0-545-03517-0 | textarea_cover    | Cover Comments    |
    #  | 978-0-545-03517-0 | Jacket            | Jacket Comments   |
    #  | 978-0-545-03517-0 | Interior          | Interior Comments |
    #  | 978-0-545-44868-0 | Cover             | Cover Comments    |
    #  | 978-0-545-44868-0 | Interior          | Interior Comments |
    #  | 978-0-545-44868-0 | Jacket            | Jacket Comments   |


