@OTBPM-SearchAdvanced
Feature: OTBPM - Search Advanced (new)

  EAPT-1313 ::  APT: MVP--Redesign of Advanced Search
  
  
  
  As an APT user, when I conduct an Advanced Search, I want to be able to search by ISBN, Title, Art Director, Designer, Author, Illustrator, Editor, and the "By Date" section (pick up existing implementation). Each category should be treated as an "AND", so if I search for title = harry potter and art director = robert mahabir, I ONLY want to see results where both of those criteria are upheld.
  
  • Each category should represent a column in the DB. Note: ISBN might be queried on more than one column in the DB.
  • When searching on the categories for Art Director and/or Designer, the query needs to run on the USER_MGNT table in the DB.

 
  @ORPHAN @EAPT1313 @P3
  Scenario Outline: 1- Verify new Advanced Search UI

    Given the user is logged into OTBPM
    When the user clicks the Advanced Search link from the “<page>” page
    Then the Advanced search window should display
    And the ‘Find Product By’ section should have search fields for each the “<find product>” types
    And the ‘By Date’ section should have a from and to date dropdown for the “<by date>” types
    Examples:
      | page           | find product                                                     | by date                                                       |
      | my projects    | [ISBN,Title,Art Director, Designer, Author, Illustrator, Editor] | [In store Date, Pub, Date, First Use Date, Target Due to Mfg] |
      | search results | [ISBN,Title,Art Director, Designer, Author, Illustrator, Editor] | [In store Date, Pub, Date, First Use Date, Target Due to Mfg] |

 @ORPHAN @EAPT1313 @P1 @AdvancedSearchError
  Scenario: 2- Advanced Search with 51 ISBNs should display error

    Given user is logged in OTBPM
     And user is in 'My Projects' view
    And the user is on the Advanced Search page
    When the user conducts a "ISBN" advanced search for fifty isbns "isbn13_51"
    Then  the application should present an error message 
 
  @ORPHAN @EAPT1313 @P1 @AdvancedSearch50Isbns
  Scenario Outline: 3- Advanced Search with ISBN variations

    Given user is logged in OTBPM
     And user is in 'My Projects' view
    And the user is on the Advanced Search page
    When the user conducts a "ISBN" advanced search for fifty isbns "<value>"
    Then all the returned titles should all an ISBN from "<isbn_13_value>"
    Examples:
      | value                    | isbn_13_value     |
      | isbn10_50               |     isbn13_50     |
      |    isbn13_50            |         isbn13_50 |
      | isbn10U_50 			 |      isbn13_50    |
      | isbn13U_50				 |       isbn13_50   |
#      | isbnmix_50   			 |       isbn13_50   |


 @ORPHAN @EAPT1313 @P1 @AdvancedSearchISBNS
  Scenario Outline: 4- Advanced Search Title with isbns

    Given user is logged in OTBPM
     And user is in 'My Projects' view
    And the user is on the Advanced Search page
    When the user conducts a "ISBN" advanced search of "<searchKey>"
    Then the returned results should contain all of the search values "<value>" in the "ISBN" column
    Examples:
       | searchKey 					| value                  |
       | 1-443-10579-1          | 978-1-443-10579-8 |
      | 978-1-443-10579-8       | 978-1-443-10579-8 |
      | 1443105791              | 978-1-443-10579-8 |
      | 9781443105798           | 978-1-443-10579-8 |



  @ORPHAN @EAPT1313 @P1 @AdvancedSearchTitle
  Scenario Outline: 4- Advanced Search Title with multiple terms comma separated

    Given user is logged in OTBPM
     And user is in 'My Projects' view
    When the user selects "<type>" search option
	And conducts a search for "<value>"
    And the user is on the Advanced Search page
    When the user conducts a "<type>" advanced search of "<value>"
    Then the returned results should contain all of the search values "<value>" in the "<type>" column
    Examples:
      | type  | value                  |
#      | Title | Batman                 |
#      | Title | Batman, robin          |
      | Title | Batman, robin, riddler |
      | Title | Batman, Clifford       |


  @ORPHAN @EAPT1313 @P1 @AdvancedSearchFullName
  Scenario Outline: 5- Advanced Search full name

    Given user is logged in OTBPM
    And user is in 'My Projects' view
    And the user is on the Advanced Search page
    When the user conducts a "<type>" advanced search of "<value>"
    Then the returned results should contain all of the search values "<value>" in the "<type>" column
    Examples:
      | type         | value               |
      | Art Director | rick demonico       |
      | Designer     | patti ann harris    |
      | Author       | Timothy V. Rasinski |
      | Illustrator  | Tom LaPadula        |
      | Editor       |Emma Miller|


  @ORPHAN @EAPT1313 @P1 @AdvancedSearchPartial
  Scenario Outline: 6- Advanced Search single and partial name

    Given user is logged in OTBPM
    And user is in 'My Projects' view
    And the user is on the Advanced Search page
    When the user conducts a "<type>" advanced search of "<value>"
    Then the returned results should contain all of the search values "<value>" in the "<type>" column
    Examples:
      | type         | value    |
      | Art Director | john     |
#      | Art Director | williams |
#      | Art Director | willi    |
#      | Designer     | angela   |
#      | Designer     | jun      |
#      | Designer     | angel    |
#      | Author       | Timothy  |
      | Author       | Rasinski |
#      | Author       | Tim      |
#      | Illustrator  | Tom      |
      | Illustrator  | LaPadula |
#      | Illustrator  | Padula   |
      | Editor       | BEZDECHECK   |
#      | Editor       |TOBENGAUZ  |
#      | Editor       | lester    |

 @ORPHAN @ECIMT858 @AdvancedSearchByDateAllFields
  Scenario Outline: 8- User perform an Advanced Search by date(all the date types)

    Given user is logged in OTBPM as "APT_Art_Director"
     And user is in 'My Projects' view
    And the user is on the Advanced Search page
    When User selects a "<dateRangeFrom>" and "<dateRangeTo>" for "<label>"
    Then 'Advanced Search' view is dismissed
    Then 'Search Results' in "<tableLables>" view displays matching results for any of the following database fields "<databaseFields>" 
Examples:
|dateRangeFrom|dateRangeTo|databaseFields|		label|			tableLables|
|02/10/16|02/21/16|	['FIRST_USE_DATE']	|First Use Date:|['First Use Date']|
|01/01/15|01/01/16|['COVER_TARGET_DUE_MFG','JACKET_TARGET_DUE_MFG','INTERIOR_TARGET_DUE_MFG']|Target Due to Mfg|['Cover Target Due to Mfg.','Jacket Target Due to Mfg.','Interior Target Due to Mfg.']|
|10/01/14|11/10/16|['COVER_TARGET_DUE_MFG','JACKET_TARGET_DUE_MFG','INTERIOR_TARGET_DUE_MFG']|Target Due to Mfg|['Cover Target Due to Mfg.','Jacket Target Due to Mfg.','Interior Target Due to Mfg.']|
|02/10/16|02/21/16|['PUBLICATION_DATE']|	Pub Date|['Pub Date']|
|02/10/16|02/21/16|['TRADE_IN_STORE_DATE']|		In Store|['In Store']|


 @ORPHAN @EAPT1313 @P1 @AdvancedSearchDb
  Scenario Outline: 10- Advanced Search DB verification

     Given user is logged in OTBPM
    And user is in 'My Projects' view
    And the user is on the Advanced Search page
    When the user conducts a "<type>" advanced search of "<value>"
    Then results displayed on the Search Results for "<type>" should match the results from the query results of "<query>" with "<dbName>"
    Examples:
      | type        | value              | query                                                                                                                                                                  |dbName|
	  | Designer 	|Christopher Stengel				|select COVER_DESIGNER_NAME,INTERIOR_DESIGNER_NAME,* from APT_PROJECTS_VIEW where COVER_DESIGNER_NAME like '%Christopher Stengel%' or INTERIOR_DESIGNER_NAME like '%Christopher Stengel%' order by APT_PROJECTS_VIEW.TITLE_NAME |DESIGNER|
      | Author      | Debbie Dadey       | select AUTHOR,* from APT_PROJECTS_VIEW where AUTHOR like '%Debbie Dadey%' order by APT_PROJECTS_VIEW.TITLE_NAME|AUTHOR|
      | Illustrator | Steven Petruccio   | select ILLUSTRATOR,* from APT_PROJECTS_VIEW where ILLUSTRATOR like '%Steven Petruccio%' order by APT_PROJECTS_VIEW.TITLE_NAME|ILLUSTRATOR|
      | Editor      | Emma Miller     | select EDITOR,* from APT_PROJECTS_VIEW where EDITOR like '%Emma Miller%' order by APT_PROJECTS_VIEW.TITLE_NAME|EDITOR|
      | Title     	| captain underpants | select TITLE_NAME,* from APT_PROJECTS_VIEW where TITLE_NAME like '%captain underpants%'order by APT_PROJECTS_VIEW.TITLE_NAME											| TITLE_NAME|
 	  | Art Director| John Williams | select ART_DIRECTOR_NAME,* from APT_PROJECTS_VIEW where ART_DIRECTOR_NAME like '%John Williams%' order by APT_PROJECTS_VIEW.TITLE_NAME							|ART_DIRECTOR_NAME|
     

  @ORPHAN @EAPT1313 @P2
  Scenario Outline: 7- Advanced Search Names with multiple terms comma separated

    Given user is logged in OTBPM
    And the user is on the Advanced Search page
    When the user conducts a "<type>" advanced search of "<value>"
    Then the returned results should contain at least one of the search values in the "<type>" column
    Examples:
      | type         | value             |
      | Art Director | williams,john    |
      | Art Director | John, rick        |
      | Art Director | john, will        |
      | Designer     | angela, jun       |
      | Designer     | angela,rick       |
      | Designer     | ang, jun          |
      | Author       | Timothy, Rasinski |
      | Author       | rasinski, david   |
      | Author       | tim, rasinski     |
      | Illustrator  |
      | Illustrator  |
      | Illustrator  |
      | Editor       |
      | Editor       |
      | Editor       |


  @ORPHAN @EAPT1313 @P2
  Scenario Outline: 8- Advanced Search Multi fields

    Given 978-0-439-02548-5 has title like Barbie, Art Director of Rick, Designer as Angela, author of Judy and Pub date of 8-1-2007
    And the user is logged into OTBPM
    When the user navigates to the Advanced Search page
    And conducts an “<type>” search of “<value>”
    Then the application should return results, that matches all the criterias
    Examples:
      | type                             | value                                         |
      | AD/Designer                      | rick/ angela                                  |
      | Ad/designer                      | rick demonico/angela jun                      |
      | Title/AD/Designer                | batman/rick/angela                            |
      | Title/AD/Designer/author         | barbie/rick/angela/judy                       |
      | Title/AD/Designer/author         | barbie/rick demonico/angela jun/judy katschke |
      | Title/AD/Designer/author/pubdate | barbie/rick/angela/judy/(07-01/07~08/31/07)   |


  @ORPHAN @EAPT1313 @P2
  Scenario Outline: 9- Advanced Search Non matching terms

    Given 978-0-439-02548-5 has title like Barbie, Art Director of Rick, Designer as Angela, author of Judy and Pub date of 8-1-2007
    And 978-0-439-02548-5 has title like Barbie, , Art Director of Rick, Designer as Angela, author of Shannon and Pub date of 8-1-2007
    And the user is logged into OTBPM
    When the user navigates to the Advanced Search page
    And conducts an ISBN/Title/AD/Designer/author/pubdate search of “<value>”
    Then the application should not return any results
    Examples:
      | value                                                              |
      | [978-0-545-08120-7,barbie,rick,angela,judy,(07-01/07~08/31/07) ]   |
      | [978-0-439-02548-5,batman,rick,angela,judy,(07-01/07~08/31/07)]    |
      | [978-0-439-02548-5,barbie,john,angela,judy,(07-01/07~08/31/07) ]   |
      | [978-0-439-02548-5,barbie,rick,jessica,judy,(07-01/07~08/31/07) ]  |
      | [978-0-439-02548-5,barbie,rick,angela,robert,(07-01/07~08/31/07) ] |
      | [978-0-439-02548-5,barbie,rick,angela,judy,(05-01/16~09/01/16)]    |


  