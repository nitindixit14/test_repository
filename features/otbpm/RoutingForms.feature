@OTBPM-Routing @OTBPM-Routing(ECIMT510)
Feature: OTBPM - Routing (ECIMT-510)

   #ECIMT-510: Implement Cover/Jacket Routing Print Form

 
#  @ORPHAN @ECIMT510 @P1 @RoutingFormPrintableFormat
#  Scenario Outline: 1- Verify the Cover/Jacket Routing Print Form is displayed in a printable format with the light blue background
#
#    Given user is logged in OTBPM
#    When user is in 'My Projects' view	
#    And user selects "<routingType>" Routing for a project
#    Then pdf form is displayed in a printable format with the light blue background in the new tab.
#    And can be printed on 8.5x11 page for all the supported browsers.
#	Examples:
#	|routingType|
#	|Cover Routing|
#	|Interior Routing|

  @ORPHAN @ECIMT510 @P1 @printRoutingDisplay
  Scenario Outline: 2- Printable form display immediately upon clicking the "Cover Routing/Interior Routing" button on the My Projects List

    Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then Printable form is displayed in the new tab.
    And no cover online forms are displayed             
Examples:
|routingType|
|Cover Routing|
|Interior Routing|

  @ORPHAN @ECIMT510 @P1 @HeaderDisplay
  Scenario: 3- Header should display Internal Routing Form Cover/Jacket/Case

    Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "Cover Routing" Routing for a project
    Then PDF file header displays Internal Routing Form Cover/Jacket/Case

@ORPHAN @ECIMT770 @P1 @InteriorHeader
  Scenario: 3- Header should display Internal Routing Form Interior

   Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "Interior Routing" Routing for a project
    Then internal Routing Form Interior is displayed on header

  @ORPHAN @ECIMT510 @P2 @dateLabelInRoutingForm
  Scenario Outline: 4- Display date label without date in Cover/Jacket Routing Print Form

     Given user is logged in OTBPM
    When user is in 'My Projects' view
      And user selects "<routingType>" Routing for a project
    Then date label is displayed without populating date on the printable PDF form
Examples:
|routingType|
|Cover Routing|
|Interior Routing|

  @ORPHAN @ECIMT510 @P1 @VerifyRoutingInDatabase @BlockedByDefect
  Scenario Outline: 5- Display the datafields Title, ISBN13, ISBN, Record #, Art Director, Cover Designer, Interior Designer, Production Editor, Editor, Mfg. Manager, Pub Date, Trade Price, Can. Price, BF Price, Trim Size, Page Count, Cover # of colors, Jacket # of colors, Product Type, Binding, Target Due to Mfg pulled from the OTBPM Database"

   Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then "<data_fields>" are displayed in the Cover/Jacket Routing Print Form
    And verify values on the PDF field should match with the OTBPM database "<database_fields>".
    Examples:
    |routingType  | data_fields        |database_fields|
    |Cover Routing  | ['ISBN13','Title','ISBN:','Record #','Art Director','Cover Designer','Interior Designer','Production Editor','Editor','Mfg. Manager','Pub Date','Trade Price','Can. Price','BF Price','Trim Size','Page Count','Cover # of Colors','Product Type','Binding','Target Due to Mfg']|['TITLE_NAME','ISBN','ISBN_13','RECORD_NO','ART_DIRECTOR_NAME','COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME','PROD_EDITOR','EDITOR','MFG_MANAGER','PUBLICATION_DATE','TRADE_PRICE','CANADAR_PRICE','BF_PRICE','TRIM_SIZE','NO_OF_PAGES','COVER_COLOR_COUNT','PRODUCT_TYPE','BINDING','COVER_TARGET_DUE_MFG']|
	|Interior Routing|['ISBN13','Title','ISBN:','Record #','Art Director','Cover Designer','Interior Designer','Production Editor','Editor','Mfg. Manager','Pub Date','Trade Price','Can. Price','BF Price','Trim Size','Page Count','Interior # of Colors','Product Type','Binding','Target Due to Mfg']|['TITLE_NAME','ISBN','ISBN_13','RECORD_NO','ART_DIRECTOR_NAME','COVER_DESIGNER_NAME','INTERIOR_DESIGNER_NAME','PROD_EDITOR','EDITOR','MFG_MANAGER','PUBLICATION_DATE','TRADE_PRICE','CANADAR_PRICE','BF_PRICE','TRIM_SIZE','NO_OF_PAGES','BOOK_INTERIOR_COLOR_COUNT','PRODUCT_TYPE','BINDING','INTERIOR_TARGET_DUE_MFG']|

  @ORPHAN @ECIMT510 @P2 @pdfCheckBoxesMech
  Scenario Outline: 6- Display Mechs Sketches, 1st pass mechs, 2nd pass mechs, 3rd pass mechs, 4th pass mechs,5th pass mechs, Other as a list of checkboxes in the Cover/Jacket Routing Print Form"

   Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then "<mech_checkbox>" is displayed in the Cover/Jacket Routing Print Form
    Examples:
    |routingType    | mech_checkbox  |
    |Cover Routing  | ['Sketches','1st pass mechs','2nd pass mechs','3rd pass mechs','4th pass mechs','5th pass mechs','Other']|
    |Interior Routing| ['Sketches','1st pass mechs','2nd pass mechs','3rd pass mechs','4th pass mechs','5th pass mechs','Other']|
      #When other is selected, it is A write line for users to write


  @ORPHAN @ECIMT510 @P2 @pdfCheckBoxesProofs
  Scenario Outline: 7- Display Proofs 1st separator proofs, Revised separator proofs, Press proof, Epson, Case Samples, Other as a list of checkboxes in the Cover/Jacket Routing Print Form"

    Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then "<proofs_checkbox>" is displayed in the Cover/Jacket Routing Print Form
    Examples:
     |routingType  | proofs_checkbox          |
     |Cover Routing | ['1st separator proofs','Revised separator proofs','Press Proof','Epson','Case Samples','Other']|
     |Interior Routing|['1st separator proofs','Revised separator proofs','Epson','F & Gs','Blues','Other']|
       #When other is selected, it is A write line for users to write


  @ORPHAN @ECIMT510 @P2 @SpecialEffectsLabelInPdf
  Scenario Outline: 8- Display Special Effects label with room for designers to write in notes in the Cover/Jacket Routing Print Form

     Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then Special Effects label with room to write in notes is displayed on the printable PDF form
Examples:
|routingType|
|Cover Routing|
|Interior Routing|


  @ORPHAN @ECIMT510 @P2 @DisplayCircleHeadersInPdf
  Scenario Outline: 9- Display header for the five circles in the Cover/Jacket Routing Print Form

    Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user selects "<routingType>" Routing for a project
    Then header is displayed for the five circles on printable PDF form
Examples:
|routingType|
|Cover Routing|
|Interior Routing|

#  @ORPHAN @ECIMT510 @P3
#  Scenario Outline: 10- Display five circles with labels Art/Design, Production, Editorial, Ed. Director/Publisher, Studio/Outside OK respectively.
#
#    Given user is on the home page
#    When user selects the Cover Routing for a project
#    Then <five_circles> with labels are displayed
#    Examples:
#      | five_circles           |
#      | Art/Design             |
#      | Production             |
#      | Editorial              |
#      | Ed. Director/Publisher |
#      | Studio/Outside OK      |
#
#
#  @ORPHAN @ECIMT510 @P3
#  Scenario: 11- Display signature lines at bottom of the page for "After routing return to" and "by" in the Cover/Jacket Routing Print Form
#
#    Given user is on the home page
#    When user selects the Cover Routing for a project
#    Then "After routing return to" and "by" are displayed at the bottom of the page
#
#
#  @ORPHAN @ECIMT510 @P3
#  Scenario: 12- Remove the Spine field from the Cover/Jacket Routing Print Form
#
#    Given user is on the home page
#    When user selects the Cover Routing for a project
#    Then Spine field is not displayed in the Cover/Jacket Routing Print Form
#
