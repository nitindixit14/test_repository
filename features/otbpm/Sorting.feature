Feature: Implement selected Sorting approach for MyProjects
As a APT users, 
I want to be able to sort my entire MyProject List (or set of Search Results) even if there are multiple pages, 
Such that I can find the content I am looking for.

@SortingAscendingOrder  @ECIMT761 @Smoke761 @Smoke @updated
Scenario Outline: - My Project list retains sorting criteria and order through multiple pages  (ascending/ 160-200 records )									
      Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM									
      And user is in 'My Projects' view									
      And My Project List is sorted descending order by "<OPTION>"									
      And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"	 									
      When User navigates to page "<PAGE_NUMBER>"			
      And user selects "<OPTION>" on the Projects list							
      Then projected listed are sorted in ascending order by "<OPTION>"	
      And first project on the page continues previous page sorting with "<OPTION>"									
Examples:									
|ASSIGNED_PROJECTS|OPTION| PROJECTS_PER_PAGE| PAGE_NUMBER|
|RECORDS_160_200|Trade Price	|150	|1|
|RECORDS_160_200|Trim	  		|50	|1|	
|RECORDS_160_200|Pub Date	|50	|2|		
|RECORDS_160_200|Interior Designer	|100	|1|
|RECORDS_160_200|Illustrator	|150	|1|	
#|RECORDS_160_200|Cover Designer	|150	|1|		
#|RECORDS_160_200|# of Pages	|50	|3|							
#|RECORDS_160_200|Title	|50	|1|							
#|RECORDS_160_200|Title	|50	|2|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|100	|1|	
#|RECORDS_160_200|Title	|50	|3|							
#|RECORDS_160_200|Title	|100	|1|													
#|RECORDS_160_200|ISBN	|50	|1|							
#|RECORDS_160_200|ISBN	|50	|2|							
#|ISBN	|50	|3|							
#|ISBN	|100	|1|							
#|ISBN	|150	|1|														
#|RECORDS_160_200|Trim			|50	|2|							
#|RECORDS_160_200|Trim			|50	|3|							
#|RECORDS_160_200|Trim			|100	|1|							
#|RECORDS_160_200|Trim			|150	|1|							
#|RECORDS_160_200|Trade Price	|50	|1|							
#|RECORDS_160_200|Trade Price	|50	|2|							
#|RECORDS_160_200|Trade Price	|50	|3|							
#|RECORDS_160_200|Trade Price	|100	|1|																				
#|RECORDS_160_200|Interior Designer	|50	|1|							
#|RECORDS_160_200|Interior Designer	|50	|2|							
#|RECORDS_160_200|Interior Designer	|50	|3|														
#|RECORDS_160_200|Interior Designer	|150	|1|							
#|RECORDS_160_200|Art Director	|50	|1|							
#|RECORDS_160_200|Art Director	|50	|2|							
#|RECORDS_160_200|Art Director	|50	|3|							
#|RECORDS_160_200|Art Director	|100	|1|													
#|RECORDS_160_200|Illustrator	|50	|1|							
#|RECORDS_160_200|Illustrator	|50	|2|							
#|RECORDS_160_200|Illustrator	|50	|3|							
#|RECORDS_160_200|Illustrator	|100	|1|													
#|RECORDS_160_200|Cover Designer	|50	|1|							
#|RECORDS_160_200|Cover Designer	|50	|2|							
#|RECORDS_160_200|Cover Designer	|50	|3|							
#|RECORDS_160_200|Cover Designer	|100	|1|							
#|RECORDS_160_200|Pub Date	|150	|1|							
#|RECORDS_160_200|First Use Date	|150	|1|							
#|RECORDS_160_200|Pub Date	|100	|1|							
#|RECORDS_160_200|Pub Date	|150	|1|							
#|RECORDS_160_200|# of Pages	|50	|1|							
#|RECORDS_160_200|# of Pages	|50	|2|							
#|RECORDS_160_200|# of Pages	|100	|1|							
#|RECORDS_160_200|# of Pages	|150	|1|	
#|RECORDS_160_200|File Name		|50	|1|	
#|RECORDS_160_200|Art Director	|150	|1|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|5	|1|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|50	|2|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|50	|3|													
#|RECORDS_160_200|Cover Target Due to Mfg.	|150	|1|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|50	|1|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|50	|2|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|50	|3|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|100	|1|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|150	|1|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|50	|1|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|50	|2|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|50	|3|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|100	|1|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|150	|1|
#|RECORDS_160_200|Title	|15	|1|								
						
@SortingDescendingOrder	@ECIMT761		@updated					
Scenario Outline: - My Project list retains sorting criteria and order through multiple pages  (descending/ 160-200 records )									
      Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM								
      And user is in 'My Projects' view									
      And My Project List is sorted ascending order by "<OPTION>"									
      And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>" 									
      When User navigates to page "<PAGE_NUMBER>"	
       And user selects "<OPTION>" on the Projects list								
      Then projected listed are sorted in descending order by "<OPTION>"									
      And first project on the page continues previous page sorting with "<OPTION>"		
Examples:									
|ASSIGNED_PROJECTS|OPTION| PROJECTS_PER_PAGE|PAGE_NUMBER|									
#|RECORDS_160_200|Trim			|50	|1|							
#|RECORDS_160_200|Trim			|50	|2|							
|RECORDS_160_200|Trim			|50	|3|							
#|RECORDS_160_200|Trim			|100	|1|							
#|RECORDS_160_200|Trim			|150	|1|							
#|RECORDS_160_200|Trade Price	|50	|1|							
#|RECORDS_160_200|Trade Price	|50	|2|							
#|RECORDS_160_200|Trade Price	|50	|3|							
|RECORDS_160_200|Trade Price	|100	|1|							
#|RECORDS_160_200|Trade Price	|150	|1|							
#|RECORDS_160_200|File Name		|50	|1|							
#|RECORDS_160_200|File Name		|50	|2|							
#|RECORDS_160_200|File Name		|50	|3|							
#|RECORDS_160_200|File Name		|100	|1|							
#|RECORDS_160_200|File Name		|150	|1|							
|RECORDS_160_200|Interior Designer	|50	|1|							
#|RECORDS_160_200|Interior Designer	|50	|2|							
#|RECORDS_160_200|Interior Designer	|50	|3|							
#|RECORDS_160_200|Interior Designer	|100	|1|							
#|RECORDS_160_200|Interior Designer	|150	|1|							
#|RECORDS_160_200|Art Director	|50	|1|							
#|RECORDS_160_200|Art Director	|50	|2|							
#|RECORDS_160_200|Art Director	|50	|3|							
#|RECORDS_160_200|Art Director	|100	|1|							
#|RECORDS_160_200|Art Director	|150	|1|							
#|RECORDS_160_200|Illustrator	|50	|1|							
|RECORDS_160_200|Illustrator	|50	|2|							
#|RECORDS_160_200|Illustrator	|50	|3|							
#|RECORDS_160_200|Illustrator	|100	|1|							
#|RECORDS_160_200|Illustrator	|150	|1|							
#|RECORDS_160_200|Cover Designer	|50	|1|							
#|RECORDS_160_200|Cover Designer	|50	|2|							
#|RECORDS_160_200|Cover Designer	|50	|3|							
|RECORDS_160_200|Cover Designer	|100	|1|							
#|RECORDS_160_200|Cover Designer	|150	|1|							
#|RECORDS_160_200|Pub Date	|50	|1|							
#|RECORDS_160_200|Pub Date	|50	|2|							
#|RECORDS_160_200|Pub Date	|50	|3|							
|RECORDS_160_200|Pub Date	|100	|1|							
#|RECORDS_160_200|Pub Date	|150	|1|							
#|RECORDS_160_200|# of Pages	|50	|1|							
#|RECORDS_160_200|# of Pages	|50	|2|							
#|RECORDS_160_200|# of Pages	|50	|3|							
#|RECORDS_160_200|# of Pages	|100	|1|							
#|RECORDS_160_200|# of Pages	|150	|1|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|50	|1|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|50	|2|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|50	|3|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|100	|1|							
#|RECORDS_160_200|Cover Target Due to Mfg.	|150	|1|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|50	|1|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|50	|2|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|50	|3|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|100	|1|							
#|RECORDS_160_200|Jacket Target Due to Mfg.	|150	|1|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|50	|1|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|50	|2|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|50	|3|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|10	|1|							
#|RECORDS_160_200|Interior Target Due to Mfg.	|150	|1|	
#|RECORDS_160_200|Title	|50	|1|							
#|RECORDS_160_200|Title	|50	|2|							
#|RECORDS_160_200|Title	|50	|3|							
#|RECORDS_160_200|Title	|100	|1|							
#|RECORDS_160_200|Title	|150	|1|							
#|RECORDS_160_200|ISBN	|50	|1|							
#|RECORDS_160_200|ISBN	|50	|2|							
#|RECORDS_160_200|ISBN	|50	|3|							
#|RECORDS_160_200|ISBN	|100	|1|							
#|RECORDS_160_200|ISBN	|150	|1|							
									
@SortingSearchResultsDescending 		@ECIMT761		@LongRunScenario @updated
Scenario Outline: 1- Search Results retains sorting criteria and order through multiple pages  (descending/ 106-120 records)									
      Given user is logged in OTBPM														
     And user searches for small search results	
      And My Project List is sorted ascending order by "<OPTION>"									
      And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>" 									
      When User navigates to page "<PAGE_NUMBER>"	
       And user selects "<OPTION>" on the Projects list									
      Then projected listed are sorted in descending order by "<OPTION>"									
      And first project on the page continues previous page sorting with "<OPTION>"							
Examples:									
|OPTION| PROJECTS_PER_PAGE|PAGE_NUMBER|															
|Trim			|50	|1|							
#|Trim			|50	|2|							
#|Trim			|50	|3|							
|Trade Price	|100	|1|					
|File Name		|100	|1|						
#|File Name		|100	|2|							
|Interior Designer	|150	|1|						
|Art Director	|150	|1|						
#|Art Director	|150	|2|							
|Illustrator	|150	|1|							
#|Illustrator	|50	|2|							
#|Illustrator	|50	|3|							
#|Illustrator	|50	|4|							
#|Illustrator	|50	|5|							
#|Illustrator	|50	|6|							
#|Illustrator	|50	|7|							
#|Illustrator	|50	|8|							
#|Illustrator	|50	|9|							
#|Cover Designer	|150	|1|						
#|Cover Designer	|150	|2|							
#|Cover Designer	|150	|3|							
|Cover Designer	|150	|4|							
#|Pub Date		|100	|1|							
#|Pub Date		|100	|2|							
#|Pub Date		|100	|3|							
#|Pub Date		|100	|4|							
#|Pub Date		|100	|5|							
#|# of Pages		|150	|1|					
#|# of Pages		|150	|2|							
#|# of Pages		|150	|3|							
#|# of Pages		|150	|4|							
#|# of Pages		|150	|5|							
#|Cover Target Due to Mfg.	|50	|1|						
#|Cover Target Due to Mfg.	|50	|2|							
#|Cover Target Due to Mfg.	|50	|3|							
#|Jacket Target Due to Mfg.	|100	|1|							
#|Jacket Target Due to Mfg.	|100	|2|							
#|Interior Target Due to Mfg.	|150	|1|							
#|Interior Target Due to Mfg.	|150	|2|													
#|ISBN	|150	|1|						
#|ISBN	|150	|2|							
#|ISBN	|150	|3|							
#|ISBN	|150	|4|							
#|ISBN	|150	|5|							
#|ISBN	|150	|6|							
#|ISBN	|150	|7|							
									
@SortingSearchResultsAscending		@LongRunScenario			@updated				
Scenario Outline: 2- Search Results retains sorting criteria and order through multiple pages  (ascending/ ~3000 records)									
      Given user is logged in OTBPM									
      And user searches for big result set with searchkey									
      And My Project List is sorted descending order by "<OPTION>"								
      And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"  									
      When User navigates to page "<PAGE_NUMBER>"	
       And user selects "<OPTION>" on the Projects list									
      Then projected listed are sorted in ascending order by "<OPTION>"								
      And first project on the page continues previous page sorting with "<OPTION>"										
Examples:									
|OPTION| PROJECTS_PER_PAGE| PAGE_NUMBER|									
#|Title	  				|50	|15|						
#|Title					|50	|39|						
#|Title					|50	|30|							
#|Title					|50	|31|							
#|Title					|50	|32|							
#|ISBN					|100	|25|							
#|ISBN					|100	|26|							
#|ISBN					|100	|27|							
#|ISBN					|100	|28|							
#|ISBN					|100	|29|							
|Author              	|150	|10|							
#|Author              	|150	|10|							
#|Author              	|150	|11|							
#|Author              	|150	|12|							
#|Author              	|150	|13|							
#|Pub Date            	|50	|37|							
#|Pub Date            	|50	|38|							
#|Pub Date            	|50	|39|							
#|Pub Date            	|50	|30|							
|Pub Date            	|50	|30|							
#|File Name           	|100	|11|							
#|File Name           	|100	|12|							
#|File Name           	|100	|13|							
|File Name           	|100	|14|							
#|Trim                	|150	|1|							
#|Trim                	|150	|2|							
#|Trim                	|150	|3|							
#|Trim                	|150	|4|							
#|Trim                	|150	|5|							
#|# of Pages          	|150	|19|							
#|# of Pages          	|50	|10|							
#|# of Pages          	|50	|11|							
#|# of Pages          	|50	|12|							
#|# of Pages          	|50	|13|							
#|Trade Price         	|100	|7|							
#|Trade Price         	|100	|8|							
#|Trade Price         	|100	|9|							
#|Trade Price         	|100	|10|							
#|Trade Price         	|100	|11|							
#|Art Director        	|150	|11|							
#|Art Director        	|150	|12|							
#|Art Director        	|150	|13|							
#|Art Director        	|150	|14|							
#|Art Director        	|150	|15|							
