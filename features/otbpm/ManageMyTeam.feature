Feature: OTBPM - ManageMyTeam

  OTBPM - ManageMyTeam
  Jira QA Backlog Tickets ECIMT-1550 & ECIMT-2082
  
  
  Stories Covered: ECIMT-730
  
  ECIMT-730::  APT: Implement ability to view Project list for My Designers include UI for Art Director/Designer relationship set up
  As an Art Director, I want to be able to view the Project Lists of the My Designers along with my own, such that I can manage the team's work

 
  @ORPHAN @ECIMT730 @P2 @LinkNameChanges
  Scenario: 1- Show only my Projects link to Show My Team’s Projects

   Given user is logged in OTBPM as "APT_Art_Director"
    And  user is in 'My Projects' view	
    And  the user selected the Show My Team’s Project link
    When the user selects the ‘Show only my Projects’ link
    Then the ‘Show only my Projects’ link should display as ‘Show My Team’s Projects’


  @ORPHAN @ECIMT730 @P2 @ValidateProjectContent
  Scenario Outline: 2 - Validate Project content Show only my Projects

   Given user with "<ROLE(s)>" role	
	When user with "<ROLE(s)>" role signing in
   	And  the user selected the Show My Team’s Project link
    And the user selects the ‘Show only my Projects’ link
    Then the ‘Show only my Projects’ link should display as ‘Show My Team’s Projects’
    Then And all the projects in the project list should be assigned to the user
    Examples:
      | ROLE(s)                                         | ASSIGNED AS                 |
      | ['APT Art Director']                         | [‘Art Director’]            |
      | ['APT Art Director','APT Production Editor'] | [‘Art Director’]            |
      | ['APT Art Director', 'APT Designer']         | [‘Art Director’,’Designer’] |


  @ORPHAN @ECIMT730 @P1 @DefaultShowMyTeams
  Scenario: 3 - Show my Team’s Projects default view

    Given user is logged in OTBPM as "APT_Art_Director"
    And the user selected the Show My Team’s Project link
    And the link reads ‘Show only my Projects’
  	When User logs out 								
	And logs in for a new session
    Then the link should by default read ‘Show My Teams Projects’


  @ORPHAN @ECIMT730 @P2 @ManageTeamView
  Scenario: 4 - Manage My Team view and list of designers

    Given user is logged in OTBPM as "APT_Art_Director"
    When the user selects Manage my Team
    Then the Manage my Team view should display
    And the list on the left should contain a list of all the active designers not already assign to Art Directors team
    And the list on the right should contain a list of all the active designers assigned to the Art Directors Team


  @ORPHAN @ECIMT730 @P1 @AssignTeam
  Scenario: 5 - Assign Designer to team

    #The number of projects changed will not be all the projects assigned to the ‘Designer’
    #It will be the total number assigned to the ‘Designer’ less what has the same Art Director Assigned,
    #As the projects with the Art Director assigned would already be included
    Given user is logged in OTBPM as "APT_Designer"
    And a ‘Designer’ has active project assignments where user is not the art director
    And User logs outs and login as "APT_Art_Director" user
    And the user selects Manage my Team
    When that ‘Designer’ is assigned to the team
    And the user navigates to the ‘Show my Teams Project’ page
    Then the projects of the ’Designer’ should also be included in the ‘Show my Teams Projects’ page
	When the user is on the Manage my Team page
	When that ‘Designer’ is removed from the team
    And the user navigates to the ‘Show my Teams Project’ page
    Then the projects of the ’Designers’ does not display in the ‘Show my Teams Projects’ page
	
#  @ORPHAN @ECIMT730 @P1 @CombinedInAssignTeam
#  Scenario: 6 - Remove Designer from team
#
#    #This is assuming that these projects do not have the art director assigned
#    #Projects that have the removed Designer and the Art Director assigned would still display as it is considered the Art Directors Project
#    Given the user is logged into OTBPM as an Art Director
#    And a ‘Designer’ assigned to the Art Directors team has project assignments that show up on the ‘Show my Teams Projects’ page
#    And the user is on the Manage my Team page
#    When that ‘Designer’ is removed from the team
#    And the user navigates to the ‘Show my Teams Project’ page
#    Then the projects of the ’Designers’ does not display in the ‘Show my Teams Projects’ page


  @ORPHAN @ECIMT730 @P3 
  Scenario Outline: 7 - Designer selection is persistent
    Given user is logged in OTBPM as "APT_Art_Director"
    And the user is on the Manage my Team page
    And the user assigns a Designer "<designer>" to the Team
    When User logs outs and login as "APT_Art_Director" user
    And the user returns to the Manage my Team page
    Then the same Designers "<designer>" still remain in the Team

    Examples: 
      | designer         |
      | ['Albey QaTest'] |

  @ORPHAN @ECIMT730 @P3 @TeamAssignDatabase
  Scenario Outline: 8 - Designer Team assignment is immediately updated to the database

    #team assignments is stored in the ROLE_MAPPING table in the OTBPM DB.
    Given user is logged in OTBPM as "APT_Art_Director"
    And the user is on the Manage my Team page
    When a ‘Designer’ "<designer>" is "<action>" to the Team
    Then the update "<action>" on "<designer>" should reflect immediately in the database
    Examples:
      | action                                   |designer	  |
      | assigned                                 |Albey QaTest|
      | removed                                  |Albey QaTest|
      | reassigned from a different Art Director |Albey QaTest|


  @ORPHAN @ECIMT730 
  
  Scenario Outline:9 -Error display when Assign Designer button clicked without selecting at least one designer from the "All Designers" list
  
    Given user is logged in OTBPM as "<value>"
    When the user is on the Manage my Team page
    Then an error message with the title "Manage My Team" displays with the text:Select an available Designer to add to your team

    Examples: 
      | value            |
      | APT_Art_Director |

  @ORPHAN @ECIMT730
Scenario Outline: 10 - Error display when Remove Designer button clicked without selecting at least one designer from the "Designers who report to me" list
    Given user is logged in OTBPM as "<value>"
    When the user is on the Manage my Team page
    When the user clicks the Remove Designer button without selecting at least one designer from the "Designers who report to me" list
    Then an error message with the title "Manage My Team" displays with the text: Select a Designer to remove from your team.

    Examples: 
      | value            |
      | APT_Art_Director |
#
#
#  @ORPHAN @ECIMT730
#  Scenario: 11 - Error display when user with Art Director role selects Show My Team's Projects link with no designers added to that Art Director's team
#
#    Given the user is logged into OTBPM as an Art Director	
#    And the user is on the Manage my Team page	
#    When the user clicks the Remove Designer button without selecting at least one designer from the "Designers who report to me" list	
#    Then an error message with the title "Manage My Team" displays with the text: Select a Designer to remove from your team.	
#
