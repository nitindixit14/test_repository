Feature: OTBPM - ManageMyTeam kartheek3

  @TestCase5
  Scenario Outline: 3- Edited Transmittal Form displays multiple Proofs separated by comma
    Given user is logged in OTBPM
    And User views Transmittal History for  "<PROJECT>"
    #And User creates a New "<TYPE_OF_TRANSMITTAL_FORM>"
    When User submits  New "<TYPE_OF_TRANSMITTAL_FORM>"  with "<MECHS>" and "<PROOFS>"
    When User edits most recent "<TYPE_OF_TRANSMITTAL_FORM>" with multiple proofs
   # When User 'Edit' the most recent "<TYPE_OF_TRANSMITTAL_FORM>" with "<OTHER_MECHS>" and "<OTHER_PROOFS>"
    Then "<TYPE_OF_TRANSMITTAL_FORM>" on the list displays 'Proofs' separated by comma

    #And User views Transmittal History for  "<PROJECT>"
    Examples: 
      | PROJECT           | TYPE_OF_TRANSMITTAL_FORM | MECHS               | PROOFS | 
      | 978-0-545-90883-2 | Cover                    | Art released to Mfg | Edox   |
