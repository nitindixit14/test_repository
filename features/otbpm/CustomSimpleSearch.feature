Feature: New Search Test Cases

@ValidateCheckbox @CustomizeSearch
Scenario Outline: Validate new checkbox on Application UI
Given user is logged in OTBPM
When the user view the "<page>" 
Then user should be presented with "<options>" search options
Examples:
|page			|options	|
|My Projects	|['ISBN','Title']	|
|Search Results	|['ISBN','Title']	|

@SimpleSearchOptions @CustomizeSearch
Scenario Outline: Validate simple search with options
Given user is logged in OTBPM
When the user selects "<option>" search option
And conducts a search for "<term>"
Then each of the results should contain the "<term>" in the "<option>" column 
And the count of results should match the results returned from the "<query>" query
Examples:
|term				|option	|query	|
|batman				|Title	|select count(*) AS TOTAL_COUNT from APT_PROJECTS_VIEW where TITLE_NAME like '%batman%'|
|978-0-545-80523-0	|ISBN	|select count(*) AS TOTAL_COUNT from APT_PROJECTS_VIEW where ISBN = '978-0-545-80523-0' OR ISBN_13='978-0-545-80523-0' OR ISBN_UNFORMATTED = '9780545805230' OR ISBN_13_UNFORMATTED = '9780545805230'	|

@ResetSearchOptions @CustomizeSearch
Scenario Outline: Validate simple search options resets when user leaves to myProjects page
Given user is logged in OTBPM
When the user selects "<option>" search option
And conducts a search for "<term>"
And the user navigates to the My Projects page
Then the search options should reset
Examples:
|term				|option	|
|batman				|Title	|
|978-0-545-80523-0	|ISBN	|

@SearchOptionInSearchView @CustomizeSearch
Scenario Outline: Validate that the simple search option remain following a completed search
Given user is logged in OTBPM
When the user selects "<option>" search option
And conducts a search for "<term>"
Then the selected search option "<option>" remains checked
Examples:
|term				|option	|
|batman				|Title	|
|978-0-545-80523-0	|ISBN	|

@SearchWithNoOptions @CustomizeSearch
Scenario Outline: Validate simple search with no options 
#require data generation (manual DB entry) to include in examples before execution
#ISBN in title and title in isbn
Given user is logged in OTBPM
When conducts a search for "<term>"
Then each of the results should contain the "<term>" in one of the "<option>" column 
Examples:
|term				|option				|
|superman			|['Title','ISBN']	|
|978-0-545-80523-0	|['Title','ISBN']	|

@SearchOptionErrorMessage @CustomizeSearch
Scenario Outline: Validate search option error message when both search options are selected
Given user is logged in OTBPM
When the user selects "<option>" search option
And the user selects "<second option>" search option
Then the user should be presented with a error message
And the error message should read "<error message>"
Examples:
|option	|second option	|error message									|
|ISBN	|Title			|Select either ISBN or Title to perform the Search.	|
|Title	|ISBN			|Select either ISBN or Title to perform the Search.	|
		
			
