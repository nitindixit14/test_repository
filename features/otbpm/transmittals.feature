@ECIMT-Transmittal @ECIMT-Transmittal(Transmittal)
Feature: OTBPM - Transmittal (Transmittal)

 
  @ORPHAN @ECIMT1064 @P3 @ActualnameOTBPM
  Scenario: 1- Verify the users actual name is displayed in OTBPM

    Given A valid user 
	When user signing in 
	Then user name should displayed in header 


  @ORPHAN @ECIMT1064 @P3 @UserNameAllAssignments
  Scenario: 2- Verify actual name on My Project Page

    Given user with all assignments is logged in OTBPM
    And user is in 'My Projects' view
    When at least one of these projects that has all assignments
#    And the the user views the project in their My Project page
    Then the names in the Cover Designer, Interior Designer and Art Director is the actual name of the user


  @ORPHAN @ECIMT1064 @P3 @UserNameSearchResults
  Scenario Outline: 3- Verify actual name on Search Results Page

    Given user with all assignments is logged in OTBPM
    When User searches for a "<value>" in My Projects view
    And at least one of these projects that has all assignments
#    And the the user views that project from the Search Results page
    Then the names in the Cover Designer, Interior Designer and Art Director is the actual name of the user
Examples:
|value|
|i wonder|

  @ORPHAN @ECIMT1064 @P3 @UserNameOtherLocation
  Scenario Outline: 4- Verify actual name on other locations of OTBPM

    Given user with all assignments is logged in OTBPM
    When User searches for a "<value>" in My Projects view
    And at least one of these projects that has all assignments
    And the user navigates to the "<location>" of that project and views names of "<roles>"
    Then the names of the Cover Designer, Interior Designer and Art Director is the actual name of the user
    Examples:
    |value  | location |roles|
    |i wonder |Project Details|['Cover Designer','Interior Designer','Art Director']|
     	|i wonder|Transmittal History|['Cover Designer','Interior Designer','Art Director']|
      	|i wonder|Online Transmittal Form|['Cover Designer','Interior Designer','Art Director']|
#     	|i wonder|Cover Routing Form|['Cover Designer','Interior Designer','Art Director']|
#      	|i wonder|Interior Routing Form|['Cover Designer','Interior Designer','Art Director']|

  @ORPHAN @ECIMT958 @CommaSeperatedHereWithsInPdf
  Scenario Outline: 1- Verify multiple Proof types selected are separated by a comma followed by space

    Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then multiple proof types should be separated by a comma followed by space are displayed in the PDF in the proof type section
    Examples:
      | type   | fields                                                                                |
      | Jacket | ['Edox','Epson','Art released to Mfg']                                                                               |
      | Jacket | ['Edox','Epson','Soft Proof','Art released to Mfg']                                                              |
      | Jacket | ['Edox','Epson','Soft Proof','Wet Proof','Art released to Mfg']                                                    |
      | Jacket | ['Edox','Epson','Soft Proof','Wet Proof','Random','Art released to Mfg']                                             |
#      | Jacket | ['Edox','Epson','Soft Proof','Wet Proof','Random','Printer Proofs','Art released to Mfg']                              |
#      | Jacket | ['Edox','Epson','Soft Proof','Wet Proof','Random','Printer Proofs',Additional Proof Types','Art released to Mfg']       |
#      | Jacket | ['Edox','Epson','Soft Proof','Wet Proof','Random','Printer Proofs',Additional Proof Types','Other','Art released to Mfg'] |


  @ORPHAN @ECIMT958 @P2 @VerifyCommaSeperatedHereWiths
  Scenario Outline: 2- Verify multiple herewiths selected are separated by a comma followed by space

    Given user is logged in OTBPM
    And the user selects the Transmittal Form of a project
    When the user selects "<type>"
    And selects the required fields "<fields>"
     And user submits the form
    And generates the transmittal form
    Then multiple herewiths should be separated by a comma followed by space are displayed in the PDF in the herewith section
    Examples:
      | type   | fields                                                  |
      | Jacket | ['CDs','Transparencies','Edox','Mechanicals released to Mfg']                                       |
      | Jacket | ['CDs','Transparencies','Slides','Edox','Mechanicals released to Mfg']                                |
      | Jacket | ['CDs','Transparencies','Slides','4/c Flat Art','Edox','Mechanicals released to Mfg']       |
      | Jacket | ['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Edox','Mechanicals released to Mfg']       |
      | Jacket | ['CDs','Transparencies','Slides','4/c Flat Art','1/c Flat Art','Other','Edox','Mechanicals released to Mfg']       |


  @ORPHAN @ECIMT958 @P2 @VerifyPubDateOnAllForms
  Scenario Outline: 3- Verify that the Publication dates are all the same

    Given user is logged in OTBPM
     When user is in 'My Projects' view
    And user view the Publication Date of a project
    Then publication date on the "<form>" should be the same as the actual Publication Date
    Examples:
      | form             |
      | Project Details  |
      | Cover Routing    |
      | Interior Routing |
      
  @ORPHAN @ECIMT958 @P2 @VerifyFilesPostedTo
  Scenario Outline: 7- "Verify Files posted to" section picking up content from online form

    Given user is logged in OTBPM
     And the user selects the Transmittal Form of a project
    When the user selects "<type>"
   And selects the required fields "<fields>"
    And user submits the form
    And the user generates a PDF
    Then the content in Verify Files posted to section is displayed as expected.
	Examples:
      | type   | fields                                             |
      | Jacket | ['CDs','Transparencies','Edox','Mechanicals released to Mfg','Files Posted to :']                                       |

  @ORPHAN @ECIMT958 @P3 @HerewithDisplay
  Scenario Outline: 8- Verifying "4/c FlatArt" herewith in online jacket teansmittal form is displaying as 4/c Flat Art on the print pdf

    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    And the user selects "<type>"
    And selects the required fields "<fields>"
     And user submits the form
    And the user generates a PDF
    Then "4/c Flat Art" herewith is displayed as "4/c Flat Art" in the PDF
	Examples:
	  | type   | fields                                            |
      | Jacket | ['4/c Flat Art','Edox','Mechanicals released to Mfg']|

#  @ORPHAN @ECIMT958 @P2 @DateFormatAllLocations
#  Scenario Outline: 9- Validate that all dates within OTBPM are displayed in a consistant format
#
#    Given user is logged in OTBPM
#    When user view the Publication Date of a project
#    And the user navigates to the "<location>" page and views "<date>"
#    Then the dates displayed on the page, should be a consistant format of MM/DD/YYYY
#    Examples:
#       |location				    |date|
#       | My Projects                |['Pub Date','First Use Date']|
##      | Search Results             |['Pub Date','First Use Date']|
##      | Project Details            |['Trade Use','First Use']|
##      | Cover Routing              |Pub Date:|
##      | Interior Routing           |Pub Date:|
##      | Transmittal Form           |Pub Date:|
##      | Generated Transmittal form |Pub Date:|


  @ORPHAN @ECIMT958 @P2 @OtherFields
  Scenario Outline: 10- Verify that actual value entered for the Other options on the Jacket Transmittlal are correctly present on the form

    Given user is logged in OTBPM
    When the user selects the Transmittal Form of a project
    And the user selects "<type>"
    And selects the required fields "<fields>"
     And user submits the form
    And the user generates a PDF
    Then the PDF should contain the same valid value in the type section
    Examples:
      | type       |fields|
      | Jacket	   |['OtherHereWith','OtherProofType','Mechanicals released to Mfg'] |


#  @ORPHAN @ECIMT1062 @P2
#  Scenario Outline: 1- ARC is not displayed in print when unchecked in Transmittal Form
#
#    Given user is logged in OTBPM
#    And new transmittal form is displayed
#    When 'Advanced Reader Copy' check box is unchecked by default
#    And user saves transmittal form for '<type>'
#    Then 'ARC:NO' is not displayed in print pdf
#    Examples:
#      | type     |
#      | Jacket   |
#      | Cover    |
#      | Interior |
#
#
#  @ORPHAN @ECIMT1062 @P2
#  Scenario: 2- ARC:Yes is displayed in print when checked in Transmittal Form
#
#    Given user is logged in OTBPM
#    And new transmittal form is displayed
#    When user checks 'Advanced Reader Copy'
#    And user saves transmittal form for '<type>'
#    Then 'ARC:YES' is displayed in print pdf
#
#
#  @ORPHAN @ECIMT957 @P2
#  Scenario Outline: 1- Cover/Jacket/Interior selector is set to Interior when user is the assigned Interior Designer
#
#    Given user is logged in OTBPM
#    When New 'Transmittal Form' page is displayed for '<project>'
#    Then 'Interior'radiobutton is selected
#    Examples:
#      | user     | project           |
#      | testdev1 | 978-0-545-64013-8 |
#
#
#  @ORPHAN @ECIMT957 @P3
#  Scenario: 2- 'Other' option label should have a capitalized O.
#
#    Given user is logged in OTBPM
#    When New 'Transmittal Form' page is opened
#    Then 'Other' label text has a capitalized "o" under the Mechs, Proof Types and Herewith sections
#
#
#  @ORPHAN @ECIMT957 @P3
#  Scenario Outline: 4- Product information is highlighted with a border.
#
#    Given user is logged in OTBPM
#    When New 'Transmittal Form' page is displayed for '<project>'
#    Then Product 'information' is displayed inside a border section
#    Examples:
#      | information                                                                                                                                                                                             |
#      | ['ISBN 13','Art Director','Pub Date','ISBN','Cover Designer','Trim Size','Record #','Interior Designer','Page Count','Trade Price','Production Editor','Can. Price','Editor','BF Price','Mfg. Manager'] |
#
#
#  @ORPHAN @ECIMT957 @P3
#  Scenario Outline: 5- The title for Mechs/Proofs/Herwith is aligned with the corresponding options
#
#    Given User is logged in OTBPM
#    When New 'Transmittal Form' page is displayed for '<project>'
#    Then name 'title' is bold font and aligned with corresponding options
#    And options listed under 'title' have same spacing
#    Examples:
#      | title                        |
#      | ['Mech','Proofs','Herewith'] |
#
