Feature: OTBPM

  @ORPHAN @ECIMT598 @P3 @NotAutomated
  Scenario Outline: Multiple barcodes can be donwloaded  for project in Project Details page
    Given user is logged in OTBPM
    And user searches for "<project>"
    When User opens Project Details for "<project>"
    And User selects the Barcode icon
    Then list of Available barcodes are displayed in  a new window
    And the number of barcodes listed is "<barcodes_count>"

    Examples: 
      | project           | barcode_count |
      | 978-0-439-35764-7 |             2 |
     # | 978-0-545-44868-0 |             2 |
     # | 978-0-545-86184-7 |             2 |
