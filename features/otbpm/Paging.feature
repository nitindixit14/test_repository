Feature:  Implement selected Paging Approach for MyProjects 
As a Trade Production Ed/Art Director/Designer,
I want to see all Project List data grouped together on the My Project List view
such that I can efficiently view, copy and act on that data.


@PagingOption @Smoke765 @ECIMT765 @Smoke @updated
Scenario: - User can choose how many projects to display per page  									
       Given user is logged in OTBPM									
       When user is in 'My Projects' view									
       Then 'Projects per page' selector is displayed and labeled as 'Show X Per Page'									
       And X is a selectable field with the options ' 50-100-150'									

@REMOVE			
Scenario Outline: - User can view default number of projects per page when landing on My Project Page									
       Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM									
       When user is in 'My Projects' view									
       Then first page is displayed	with "<ASSIGNED_PROJECTS>"								
       And 'Projects per page' value is '5' projects per page								
       And The count of projects displayed on the page is "<PROJECTS_DISPLAYED>"									
Examples:									
| ASSIGNED_PROJECTS | PROJECTS_DISPLAYED |																	
| LESS_THAN_5       | TOTAL_PROJECTS    |				
| MORE_THAN_5       | 5                 |									
| NONE              | 0                 |								
									
@DisplayProjects @Smoke765	@ECIMT765	@Smoke	@updated						
Scenario Outline: - User Selects the number of Projects displayed per page  									
       Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM									
       And user is in 'My Projects' view									
       When 'Projects per page' values is set to "<PROJECTS_PER_PAGE>" 	
       And 'Projects per page' values is  "<PROJECTS_PER_PAGE>"									
       Then The count of projects displayed on the page is "<PROJECTS_DISPLAYED>"									
Examples:									
| ASSIGNED_PROJECTS | PROJECTS_PER_PAGE | PROJECTS_DISPLAYED |									
#| NONE              | 50                 | 0                 |									
##| NONE              | 100                | 0                 |								
##| NONE              | 150                | 0                 |								
| LESS_THAN_50       | 50                 | TOTAL_PROJECTS    |								
| MORE_THAN_50       | 50                 | 50                 |									
| LESS_THAN_100      | 100                | TOTAL_PROJECTS    |								
| MORE_THAN_100      | 100                | 100				|					
| LESS_THAN_150      | 150                | TOTAL_PROJECTS    |									
| MORE_THAN_150      | 150                | 150				|					
									
@PagingForSearchResults	@ECIMT765	@updated							
Scenario Outline: - Number of projects per page persists after Search Results 									
       Given user is logged in OTBPM									
       And user is in 'My Projects' view									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"								
       When User searches for 'At least 150 results'									
       Then first page is displayed								
       And 'Projects per page' values is  "<PROJECTS_PER_PAGE>"									
       And The count of projects displayed on the page is "<PROJECTS_PER_PAGE>"									
Examples:									
| PROJECTS_PER_PAGE |							
| 50             	|						
| 100  				|						
| 150  				|							
									
@ProjectsDisplayedMulitplePages	@Smoke765	@ECIMT765	@Smoke	@updated					
Scenario Outline: - Number of projects per page persists throught multiple pages									
       Given user is logged in OTBPM									
       And User searches for 'At least 301 results'									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
       When User navigates to page "<PAGE_NUMBER>"									
       Then 'Projects per page' values is  "<PROJECTS_PER_PAGE>"									
       And The count of projects displayed on the page is "<PROJECTS_DISPLAYED>"									
Examples:									
| PROJECTS_PER_PAGE |  PAGE_NUMBER | PROJECTS_DISPLAYED |								
| 50                 |  3           | 50                  |	
| 100                |  3           | 100                 |				
| 150                |  2           | 150              |								
		

@EditableNonEditableIconsForNone @Smoke765 @ECIMT765 @Smoke @updated
Scenario Outline: - User cannot navigate through multiple pages									
       Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM									
       And user is in 'My Projects' view									
#       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
#       When 'Page number' is displayed																	
       Then Navigation arrows "<DISPLAYED_ICONS>" are displayed									
       And Navigation arrows "<ENABLED_ICONS>" are enabled									
Examples:									
| ASSIGNED_PROJECTS |  PROJECTS_PER_PAGE| DISPLAYED_ICONS                    | ENABLED_ICONS                      |									
#|  NONE            | 50                      | ['First','Previous','Next','Last'] | ['None']                           |
|  NONE            | 100                      | ['First','Previous','Next','Last'] | ['None']                           |
#|  NONE            | 150                      | ['First','Previous','Next','Last'] | ['None']                           |
									
									
@EditableNonEditableIcons	@Smoke765	@ECIMT765		@Smoke		@updated			
Scenario Outline: - User can navigate through multiple pages									
       Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM									
       And user is in 'My Projects' view									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
       And 'Page number' is displayed									
       When Page displayed is "<PAGE_NUMBER>"								
      Then Navigation arrows "<DISPLAYED_ICONS>" are displayed									
       And Navigation arrows "<ENABLED_ICONS>" are enabled									
Examples:									
| ASSIGNED_PROJECTS | PAGE_NUMBER | PROJECTS_PER_PAGE| DISPLAYED_ICONS                    | ENABLED_ICONS                      |									
|  LESS_THAN_50      |  1          | 50                     | ['First','Previous','Next','Last'] | ['None']                           |									
|  LESS_THAN_100     |  1          |  100                   | ['First','Previous','Next','Last'] | ['None']                           |									
|  LESS_THAN_150     | 1           | 150                    | ['First','Previous','Next','Last'] | ['None']                           |									
|  BETWEEN_301-350    |  1          | 50                          | ['First','Previous','Next','Last'] | ['Next','Last']                    |									
#|  BETWEEN_301-350    |  2          | 50                          | ['First','Previous','Next','Last'] | ['First','Previous','Next','Last'] |									
#|  BETWEEN_301-350    |  3          | 50                          | ['First','Previous','Next','Last'] | ['First','Previous','Next','Last'] |									
#|  BETWEEN_301-350    |  4          | 50                          | ['First','Previous','Next','Last'] | ['First','Previous','Next','Last'] |									
#|  BETWEEN_301-350    |  5          | 50                         | ['First','Previous','Next','Last'] | ['First','Previous','Next','Last'] |									
#|  BETWEEN_301-350    |  LAST       | 50                         | ['First','Previous','Next','Last'] | ['First','Previous']               |									
#|  BETWEEN_301-350    |  1          | 100                        | ['First','Previous','Next','Last'] | ['Next','Last']                    |									
#|  BETWEEN_301-350    |  2          | 100                         | ['First','Previous','Next','Last'] | ['First','Previous','Next','Last'] |									
#|  BETWEEN_301-350   |  3          | 100                          | ['First','Previous','Next','Last'] | ['First','Previous','Next','Last'] |									
#|  BETWEEN_301-350    |  LAST       | 100                         | ['First','Previous','Next','Last'] | ['First','Previous']               |									
#|  BETWEEN_301-350   |  1          | 150                           | ['First','Previous','Next','Last'] | ['Next','Last']                    |									
#|  BETWEEN_301-350    |  2          | 150                       | ['First','Previous','Next','Last'] | ['First','Previous','Next','Last'] |									
#|  BETWEEN_301-350    |  LAST       | 150                         | ['First','Previous','Next','Last'] | ['First','Previous']               |									

@ProjectsPerPageDisplay	@Smoke765	@ECIMT765	@Smoke	@updated					
Scenario Outline: - Total number of projects and position of first & last project on page are displayed in My Projects view									
       Given user is logged in OTBPM									
       When user is in 'My Projects' view									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
       Then Projects count  is displayed as 'Project X -Y of Z' 									
       And X matches position in the list for the first project displayed on the page									
       And Y matches the position on the list for the last project displayed on the page									
      And Z matched the total number of Search Results		
       Examples:									
| PROJECTS_PER_PAGE | 									
| 50             	|									
| 100  				|									
| 150  				|							
									
@PageDisplay @Smoke765		@ECIMT765			@Smoke		@updated		
Scenario Outline: - Total number of pages and page number are displayed in My Projects view									
       Given user is logged in OTBPM									
       When user is in 'My Projects' view									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
       And Page displayed is "<PAGE_NUMBER>"									
       Then Page count is displayed and labeled  as 'Page S of T' 									
       And S matches the number of the page being displayed	"<PAGE_NUMBER>"								
       And T matches the total of pages required to display all the projects	
              Examples:									
| PROJECTS_PER_PAGE | PAGE_NUMBER|									
| 50             	|		1	 |						
| 100  				|		2	 |							
| 150  				|		3	 |						
									
@PagingSmallMyProjectsSet	@Smoke765	@ECIMT765		@Smoke		@updated			
Scenario Outline: - User navigates between pages using the arrows  small in my projects (My Projects/ 16-20 records)									
       Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM									
       When user is in 'My Projects' view								
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"										
       And Page displayed is "<PAGE_NUMBER>"									
       When User selects the "<ARROW_ICON>"									
       Then "<TARGET_PAGE>" page is displayed									
       And number of projects per page and max number of projects displayed remains									
       And numbers for projects is updated for 'X,Y,Z'									
       And pagination values is updated for 'R,S,T'									
Examples:									
|ASSIGNED_PROJECTS| PROJECTS_PER_PAGE | PAGE_NUMBER | ARROW_ICON | TARGET_PAGE |									
|RECORDS_160_200| 50                    | 1                          | NEXT                 | 2                       |									
#|RECORDS_160_200| 50                    | 2                          | NEXT                 | 3                       |									
#|RECORDS_160_200| 50                    | 3                          | NEXT                 | 4                       |									
|RECORDS_160_200| 50                    | 4                          | PREVIOUS        | 3                       |									
#|RECORDS_160_200| 50                    | 3                          | PREVIOUS        | 2                       |									
#|RECORDS_160_200| 50                    | 2                          | PREVIOUS        | 1                       |									
|RECORDS_160_200| 50                    | 1                          | LAST                  | 4                       |									
|RECORDS_160_200| 50                    | 4                          | FIRST                 | 1                       |									
|RECORDS_160_200| 100                  | 1                          | NEXT                 | 2                       |									
#|RECORDS_160_200| 100                  | 2                          | PREVIOUS        | 1                       |									
#|RECORDS_160_200| 100                  | 1                          | LAST                  | 2                       |									
|RECORDS_160_200| 100                  | 2                          | FIRST                | 1                       |									
|RECORDS_160_200| 150                  | 1                          | NEXT                 | 2                       |									
#|RECORDS_160_200| 150                  | 2                          | PREVIOUS        | 1                       |									
#|RECORDS_160_200| 150                  | 1                          | LAST                  | 2                       |									
#|RECORDS_160_200| 150                  | 2                          | FIRST                | 1                       |									
									
@PagingBigResultSet	@ECIMT765  @updated								
Scenario Outline: - User navigates between pages using the arrows in big result set  (Search Results/ 1336-1350 records)									
       Given user is logged in OTBPM									
       And user searches for big result set 									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
       And Page displayed is "<PAGE_NUMBER>"									
       When User selects the "<ARROW_ICON>"								
       Then "<TARGET_PAGE>" page is displayed									
       And number of projects per page and max number of projects displayed remains									
       And numbers for projects is updated for 'X,Y,Z'									
       And pagination values is updated for 'R,S,T'									
Examples:									
| PROJECTS_PER_PAGE | PAGE_NUMBER | ARROW_ICON | TARGET_PAGE |	
| 50         	| 361                        | PREVIOUS                | 360                      |											
| 100		| 290                      | NEXT                 | 291 |		
| 150         | LAST                     | FIRST                | 1                         |					
							
									
@JumpsBetweenPages	@ECIMT765		@updated						
Scenario Outline: - User Jumps between page by typing the number in a big results set (Search Results/1336-1350 records)									
       Given user is logged in OTBPM									
       And user searches for big result set 									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
       When user selects the page "<PAGE_NUMBER>"									
       Then "<PAGE_NUMBER>" page is displayed									
       And number of projects per page and max number of projects displayed remains									
       And numbers for projects is updated for 'X,Y,Z'									
       And pagination values is updated for 'R,S,T'									
Examples:									
| PROJECTS_PER_PAGE | PAGE_NUMBER |									
| 50                 |   120 	  |	
| 100                 | 90     |	
| 150                 | 11         |									
| 150                 | 70     |	
								
									
@ErrorMessage		@Smoke765	@ECIMT765	@Smoke		@REMOVED			
Scenario Outline: - User get error message when typing invalid page numbers to navigate (My Projects/16-20 records)									
       Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM									
       When user is in 'My Projects' view 									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
       When user selects the page "<PAGE_NUMBER>"									
       Then error message is displayed									
 Examples:									
|ASSIGNED_PROJECTS| PROJECTS_PER_PAGE| PAGE_NUMBER |									
|RECORDS_160_200| 50                    | 0                         |									
|RECORDS_160_200| 50                    | #                         |									
|RECORDS_160_200| 50                    | 5873                  |									
|RECORDS_160_200| 100                  |                       |									
|RECORDS_160_200| 100                  | 100                    |									
|RECORDS_160_200| 150                  | 0                         |									
|RECORDS_160_200| 150                  | 0.6                      |									
									
@ErrorMessageWithProjects	@ECIMT765							
Scenario Outline: - User get error message when typing invalid page numbers to navigate (My Projects/16-20 records)									
       Given User with projects "<ASSIGNED_PROJECTS>" is logged in OTBPM									
       When user is in 'My Projects' view 									
       And 'Projects per page' values is set to "<PROJECTS_PER_PAGE>"									
       When user selects the page "<PAGE_NUMBER>"									
       Then error message is displayed									
      And number of projects per page and max number of projects displayed remains										
Examples:									
|ASSIGNED_PROJECTS| PROJECTS_PER_PAGE| PAGE_NUMBER |									
|RECORDS_160_200| 50                    | 0                         |									
|RECORDS_160_200| 50                    | #                         |									
|RECORDS_160_200| 50                    | 5873                  |									
|RECORDS_160_200| 100                  |                       |									
|RECORDS_160_200| 100                  | 100                    |									
|RECORDS_160_200| 150                  | 0                         |									
|RECORDS_160_200| 150                  | 0.6                      |									
									