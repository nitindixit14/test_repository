@OTBPM-ProjectDetails
Feature: OTBPM - Project Details

  
  OTBPM - Project Details
  Jira QA Backlog tickets ECIMT-1534 & ECIMT-2063
  
  
  Stories Covered:  ECIMT-598, ECIMT-1065
  
  ECIMT-598::  APT: Update Project Details with additional data elements & UI Changes
  As a Trade Production Ed/Art Director/Designer, I want to see Project detail data grouped together on the one view such that I can efficiently view, copy and act on that data.
  
  ECIMT-1065::  DEFECT: Resolve the following Project Details Defects found in testing
  AC1 - Managing Editor needs to be Production Editor

 
  @ORPHAN @ECIMT598 @P2 @ProjectDetailsTitle
  Scenario Outline: 1- Title is displayed in 'Product Details' window

    Given user is logged in OTBPM
    When user searches for "<project>"	
    When User opens Project Details for "<project>"
    Then Title is displayed as "Product Details"
    Examples:
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |


  @ORPHAN @ECIMT598 @P1 @ProjectsDetailsInformation
  Scenario Outline: 2- Product information is displayed in Project Details window

    Given user is logged in OTBPM
    When user is in 'My Projects' view
    And user searches for "<project>"	
    And User opens Project Details for "<project>"
    Then label for "<value>" is displayed as "<value>"
    And "<value>" is not editable
    And "<value>" is displayed and populated from database "<dblabel>" using "<project>"
    And "<value>" matches same value displayed in "<projectsPageValues>"
    Examples:
      | project           | value                      | projectsPageValues         |dblabel|
      | 978-0-545-44868-0 | Title                      | Title					     |TITLE_NAME|
      | 978-0-545-44868-0 | Author                     | Author						 |AUTHOR|
      | 978-0-545-44868-0 | Illustrator                | Illustrator 				 |ILLUSTRATOR|
      | 978-0-545-44868-0 | ISBN 13                    | ISBN						 |ISBN_13|
      | 978-0-545-44868-0 | ISBN                       |          				     |ISBN|
      | 978-0-545-25000-9 | UPC                        |           			    	 |UPC|
      | 978-0-545-44868-0 | Record #                   |             				 |RECORD_NO|
      | 978-0-545-44868-0 | Editor                     |             				 |EDITOR|
      | 978-0-545-44868-0 | Production Editor          |             				 |PROD_EDITOR|
      | 978-0-545-44868-0 | Art Director               | Art Director 				 |ART_DIRECTOR_NAME|
      | 978-0-545-83966-2 | Cover Designer             | Cover Designer 			 |COVER_DESIGNER_NAME|
      | 978-0-439-83106-2 | Interior Designer          | Interior Designer 			 |INTERIOR_DESIGNER_NAME|
      | 978-0-545-44868-0 | File Name                  | File Name         	    	 |EPS_FILE_NAME|
      | 978-0-545-44868-0 | Trim Size                  | Trim						 |TRIM_SIZE|
      | 978-0-545-44868-0 | Binding                    |      				         |BINDING|
      | 978-0-545-44868-0 | # of Pages                 | # of Pages				 	 |NO_OF_PAGES|
      | 978-0-545-44868-0 | Trade Price                | Trade Price 				 |TRADE_PRICE|
      | 978-0-545-44868-0 | Canada Price               |            				 |CANADAR_PRICE|
      | 978-0-545-44868-0 | BF Price                   |       			        	 |BF_PRICE|
      | 978-0-545-44868-0 | First use                  |First Use Date				 |FIRST_USE_DATE|
      | 978-0-545-44868-0 | In Store                   |       				     	 |TRADE_IN_STORE_DATE|
      | 978-0-545-44868-0 | Schedule                   |             				 |PRODUCTION_SCHEDULE_CODE|
      | 978-0-545-44868-0 | Trade Use                  |         				     |PUBLICATION_DATE|
      | 978-0-545-44868-0 | First Club Use             |          				     |FIRST_CLUB_USE|
      | 978-0-545-44868-0 | BF Use                     |                 			 |BF_USE|
      | 978-0-545-44868-0 | BF Priority Code           |      				         |BF_PRIORITY_CODE|
      | 978-0-545-44868-0 | Jacket Target Due to Mfg   | Jacket Target Due to Mfg.  |JACKET_TARGET_DUE_MFG|
      | 978-0-545-44868-0 | Interior Target Due to Mfg | Interior Target Due to Mfg.|INTERIOR_TARGET_DUE_MFG|
      | 978-0-545-44868-0 | Cover Target Due to Mfg    | Cover Target Due to Mfg.	 |COVER_TARGET_DUE_MFG|


#  @ORPHAN @ECIMT598 @P2 @UpdatedToP3
#  Scenario: 3-UI Cleanup - An information box is displayed in Project Details
#
#    Given User is logged in OTBPM
#    When User opens Project Details for '<project>'
#    Then Grouped elements are displayed inside a border:('Trim Size', 'Binding', '# of Pages','Trade Price','Canada Price','BF Price' , 'First use','In Store', 'Schedule', 'Trade Use', 'First Club Use', 'BF Use', 'BF Priority Code','Jacket Target Due to Mfg', 'InteriorTarget Due to Mfg', 'Cover Target Due to Mfg')


  @ORPHAN @ECIMT598 @P1 @CoverImageInDetails
  Scenario Outline: 4- Cover image is displayed when available

     Given user is logged in OTBPM
    When user searches for "<project>"	
    When User opens Project Details for "<project>"
    Then Small 'Cover image' is displayed
    Examples:
      | project           |
      | 978-0-545-03517-0 |
      | 978-0-545-44868-0 |


#  @ORPHAN @ECIMT598 @P1 @NotAutomated
#  Scenario Outline: 5- Default image is displayed when cover image is not available
#
#    Given User is logged in OTBPM
#    When User opens Project Details for '<project>'
#    Then Default no cover image is displayed
#    Examples:
#      | project           |
#      | 978-1-338-03705-0 |
#      | 978-0-545-84884-8 |


  @ORPHAN @ECIMT598 @P1 @BarcodeInDetails
  Scenario Outline: 6- Barcode icon is displayed  for all projects

     Given user is logged in OTBPM
    When user searches for "<project>"	
    When User opens Project Details for "<project>"
    Then Barcode icon is displayed
    Examples:
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-87219-5 |


#  @ORPHAN @ECIMT598 @P1 @NotAutomated
#  Scenario Outline: 7- Available barcode can be donwloaded for project in Project Details page
#
#    Given User is logged in OTBPM
#    When User opens Project Details for '<project>'
#    And User selects the Barcode icon
#    Then Available barcode is displayed in  a new window
#   Examples:
#      | project           |
#      | 978-0-545-91627-1 |
#      | 978-0-545-86816-7 |
#      | 978-0-545-16925-7 |


#  @ORPHAN @ECIMT598 @P3  @NotAutomated
Scenario Outline: Multiple barcodes can be donwloaded  for project in Project Details page
    Given user is logged in OTBPM
    And user searches for "<project>"
    When User opens Project Details for "<project>"
    And User selects the Barcode icon
    Then list of Available barcodes are displayed in  a new window
    And the number of barcodes listed is "<barcodes_count>"

    Examples: 
      | project           | barcode_count |
      | 978-0-439-35764-7 |             2 |
      | 978-0-545-44868-0 |             2 |
      | 978-0-545-86184-7 |             2 |


#  @ORPHAN @ECIMT598 @P3  @NotAutomated
  Scenario Outline: No Available barcode message is displayed for project in Project Details page
    Given user is logged in OTBPM
    When User opens Project Details for "<project>"
    And User selects the Barcode icon
    Then No Available barcode message is displayed

    Examples: 
      | project           |
      | 978-0-000-00513-7 |
      # | 978-0-590-30861-8 |
      #| 978-0-794-43317-8 |


  @ORPHAN @ECIMT598 @P2 @ProjectDetailsHistoryButton
  Scenario Outline: 8- Transmittal History button is displayed for project in Project Details

   Given user is logged in OTBPM
    When user searches for "<project>"	
    And User opens Project Details for "<project>"
    Then 'Transmittal History button' is displayed
    Examples:
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |


  @ORPHAN @ECIMT598 @P2 @HistoryWithTransmittals
  Scenario Outline: 9- User opens Transmittal History from Project Details when there are stored Transmittals

     Given user is logged in OTBPM
    When user searches for "<project>"	
    And User opens Project Details for "<project>"
    And User opens 'Transmittal History'
    Then 'Transmittal History' view is displayed in a new window
    Examples:
      | project           |
      | 978-0-545-90883-2 |
#      | 978-0-545-86184-7 |


  @ORPHAN @ECIMT598 @P2 @HistoryWithNoTransmittals
  Scenario Outline: 10- User opens Transmittal History from Project Details when there is no stored Transmittal

    Given user is logged in OTBPM
    When user searches for "<project>"	
    When User opens Project Details for "<project>"
    When User opens 'Transmittal History'
    Then 'Online Transmittal Form' view is displayed in a new window
    Examples:
      | project           |
      |978-0-768-85175-5 |
      | 978-0-545-87219-5 |


  @ORPHAN @ECIMT598 @P2 @TransmittalFormInDetails
  Scenario Outline: 11- Online Transmittal Form can be launched from Project Details for NOT 'Editor/Mgmt/Guest' users

    Given user is logged in OTBPM as "<not_Editor/Mgmt/Guest>"				
     When user searches for "<project>"	
    And User opens Project Details for "<project>"
    Then 'New Transmittal Form button' is displayed
    Examples:
     |not_Editor/Mgmt/Guest	 | project          |
       |APT_Art_Director	 |978-0-545-44868-0 |
#       |APT_Production_Editor|978-0-545-83652-4 |
#       |APT_Designer		 |978-0-545-44868-0 |
#       |APT_Manufaturing	 |978-0-545-83652-4 |
     
      

  @ORPHAN @ECIMT598 @P3 @NoTransmittalForm

Scenario Outline: 12- Online Transmittal Form is not displayed in Project Details for 'Editor/Mgmt/Guest' users
    Given user is logged in OTBPM as "APT_Editor_Mgmt_Guest"
    When user searches for "<project>"
    And User opens Project Details for "<project>"
    Then 'New Transmittal button' is not displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |


#  @ORPHAN @ECIMT598 @P3
 Scenario Outline: 13- User opens New Transmittal Form from Project Details for NOT 'Editor/Mgmt/Guest' users
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    When User opens 'New Transmittal Form'
    Then 'New Transmittal Form' view is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
 #     | 978-0-545-83652-4 |
 


  @ORPHAN @ECIMT598 @P2 @CoverRoutingButtonDetails
  Scenario Outline: 14- Cover Routing button is displayed in Project Details page

    Given user is logged in OTBPM
    When user searches for "<project>"	
    And User opens Project Details for "<project>"
    Then 'Cover Routing button' is displayed
    Examples:
      | project           |
      | 978-0-545-44868-0 |
#      | 978-0-545-83652-4 |


#  @ORPHAN @ECIMT598 @P2
Scenario Outline: 15- Cover Routing Form can be launched from Project detail
    Given user is logged in OTBPM
    When User opens Project Details for "<project>"
    And user opens 'Cover Routing' Form
    Then 'Cover Routing Form' is displayed as pdf

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |


#  @ORPHAN @ECIMT598 @P2
 Scenario Outline: 16- Interior Routing button is displayed in Project Details page
    Given user is logged in OTBPM
    When user searches for "<project>"
    And User opens Project Details for "<project>"
    Then 'Interior Routing button' is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |
#
#
#  @ORPHAN @ECIMT598 @P2
Scenario Outline: 17- Interior Routing can be launched from Project detail
    Given user is logged in OTBPM
    When User opens Project Details for "<project>"
    And user opens 'Interior Routing' Form
    Then 'Interior Routing Form' is displayed as pdf

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |
#
#
#  @ORPHAN @ECIMT598 @P2
Scenario Outline: 18- Assign button is displayed in Project Details page for Art Director or Designer users
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    Then 'Assign button'is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |


  @ORPHAN @ECIMT598 @P2 @AssignProjectDetailsView
  Scenario Outline: 19- Art Director or Designer Assigns the current project from Project detail

     Given user is logged in OTBPM
    When user searches for "<project>"	
    When User opens Project Details for "<project>"
    And user clicks on 'Assign'
    Then 'Assign' view is displayed in a new window
    Examples:
      | project           |
      | 978-0-545-44868-0 |
#      | 978-0-545-83652-4 |


#  @ORPHAN @ECIMT598 @P3
Scenario Outline: 20- Production Tracking information is displayed
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    Then 'Production tracking information' is displayed
    And 'Production tracking information' is collapsed
    And 'Production tracking information' is expandable
    And 'Production tracking information' is collapsable

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
#
#
#  @ORPHAN @ECIMT598 @P3
Scenario Outline: 21- Production Tracking information shows information when expanded
    Given user is logged in OTBPM
    And user searches for "<project>"
    When User opens Project Details for "<project>"
    And user expands the 'Production Tracking information'
    Then 'Production Tracking Information Coming Soon......' text is displayed

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |
#
#
#  @ORPHAN @ECIMT598 @P2
Scenario Outline: 22- EPIC comments section is displayed
    Given user is logged in OTBPM
    When user searches for "<project>"
    When User opens Project Details for "<project>"
    Then 'EPIC comments section' is displayed
    And 'EPIC comments section' is collapsed
    And 'EPIC comments section' is expandable
    And 'EPIC comments section' is collapsable

    Examples: 
      | project           |
      | 978-0-545-44868-0 |
      | 978-0-545-83652-4 |
#
#
#  @ORPHAN @ECIMT598 @P3
#  Scenario Outline: 23- Specific comments can be located in the EPIC Comments section
#
#    Given user is logged in OTBPM
#    When User opens Project Details for '<project>'
#    And user expands 'EPIC comments'
#    Then '<specific_comments>' are displayed when available
#    And label for '<specific_comments>' is displayed as '<label>' when comments are available
#    Examples:
#      | project           | specific_comments | label             |
#      | 978-0-545-03517-0 | Cover             | Cover Comments    |
#      | 978-0-545-03517-0 | Jacket            | Jacket Comments   |
#      | 978-0-545-03517-0 | Interior          | Interior Comments |
#      | 978-0-545-44868-0 | Cover             | Cover Comments    |
#      | 978-0-545-44868-0 | Interior          | Interior Comments |
#      | 978-0-545-44868-0 | Jacket            | Jacket Comments   |
#
#
#  @ORPHAN @ECIMT598 @P3
#  Scenario: 24- UI Cleanup/Removed data for comments, request, soda, cover, RTM and Save button
#
#    Given User is logged in OTBPM
#    When User opens Project Details for '<project>'
#    Then 'Comments Entry' field is not displayed
#    And 'Save' button is not displayed
#    And 'Request for Cover' is not displayed
#    And 'Request for Interior' is not displayed
#    And 'SODA' is not displayed
#    And 'Cover Needed by' is not displayed
#    And 'Request By' is not displayed
#    And 'RTM Date' is not displayed
#    And 'Spine' is not displayed
#    And No secondary window title is displayed
#
#
#  @ORPHAN @ECIMT1065 @P3
Scenario Outline: 1 -- Project Details - Managing Editor is displayed as Production Editor
  
    Given user is logged in OTBPM
    When user searches for "<project>"	
    When User opens Project Details for "<project>"
    And user is in 'Project Details' page for "<project>"
    Then 'Production Editor' label is displayed
   # And 'Managing Editor' label is not displayed
Examples:
      | project           |
      | 978-0-545-44868-0 |
#
